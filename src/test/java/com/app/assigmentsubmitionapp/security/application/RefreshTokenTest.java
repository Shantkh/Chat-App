package com.app.assigmentsubmitionapp.security.application;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import com.app.assigmentsubmitionapp.user.Gender;
import com.app.assigmentsubmitionapp.user.JwtUser;
import com.app.assigmentsubmitionapp.user.UsersDetails;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.UUID;

import org.junit.jupiter.api.Test;

class RefreshTokenTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link RefreshToken#RefreshToken()}
     *   <li>{@link RefreshToken#setCreatedOn(Date)}
     *   <li>{@link RefreshToken#setExpiryDate(Instant)}
     *   <li>{@link RefreshToken#setId(UUID)}
     *   <li>{@link RefreshToken#setRefreshToken(String)}
     *   <li>{@link RefreshToken#setUpdatedOn(Date)}
     *   <li>{@link RefreshToken#setUser(JwtUser)}
     *   <li>{@link RefreshToken#getCreatedOn()}
     *   <li>{@link RefreshToken#getUser()}
     *   <li>{@link RefreshToken#getId()}
     *   <li>{@link RefreshToken#getRefreshToken()}
     *   <li>{@link RefreshToken#getUpdatedOn()}
     * </ul>
     */
    @Test
    void testConstructor() {
        RefreshToken actualRefreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant());
        actualRefreshToken.setCreatedOn(fromResult);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        actualRefreshToken.setExpiryDate(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        UUID randomUUIDResult = UUID.randomUUID();
        actualRefreshToken.setId(randomUUIDResult);
        actualRefreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        actualRefreshToken.setUpdatedOn(fromResult1);
        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(new JwtUser());
        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(new JwtUser());
        usersDetails.setUserVerified(true);
        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(refreshToken);
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(usersDetails);
        RefreshToken refreshToken1 = new RefreshToken();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult2 = Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setCreatedOn(fromResult2);
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setExpiryDate(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setId(UUID.randomUUID());
        refreshToken1.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setUpdatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken1.setUser(jwtUser);
        RefreshToken refreshToken2 = new RefreshToken();
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setCreatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setExpiryDate(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken2.setId(UUID.randomUUID());
        refreshToken2.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setUpdatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken2.setUser(new JwtUser());
        UsersDetails usersDetails1 = new UsersDetails();
        usersDetails1.setAddresses(new HashSet<>());
        usersDetails1.setAge("Age");
        usersDetails1.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setCreatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setGender(Gender.MALE);
        usersDetails1.setId(UUID.randomUUID());
        usersDetails1.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setUpdatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setUser(new JwtUser());
        usersDetails1.setUserVerified(true);
        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(refreshToken2);
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult19 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult19.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(usersDetails1);
        UsersDetails usersDetails2 = new UsersDetails();
        usersDetails2.setAddresses(new HashSet<>());
        usersDetails2.setAge("Age");
        usersDetails2.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult20 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setCreatedOn(Date.from(atStartOfDayResult20.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setGender(Gender.MALE);
        usersDetails2.setId(UUID.randomUUID());
        usersDetails2.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult21 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setUpdatedOn(Date.from(atStartOfDayResult21.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setUser(jwtUser1);
        usersDetails2.setUserVerified(true);
        JwtUser jwtUser2 = new JwtUser();
        LocalDateTime atStartOfDayResult22 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult22.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken1);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult23 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult23.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails2);
        actualRefreshToken.setUser(jwtUser2);
        assertSame(fromResult, actualRefreshToken.getCreatedOn());
        JwtUser user = actualRefreshToken.getUser();
        assertSame(fromResult2, user.getRefreshToken().getCreatedOn());
        assertSame(randomUUIDResult, actualRefreshToken.getId());
        assertEquals("ABC123", actualRefreshToken.getRefreshToken());
        assertSame(fromResult1, actualRefreshToken.getUpdatedOn());
        assertSame(jwtUser2, user);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link RefreshToken#RefreshToken(UUID, Date, Date, JwtUser, String, Instant)}
     *   <li>{@link RefreshToken#RefreshToken()}
     *   <li>{@link RefreshToken#setCreatedOn(Date)}
     *   <li>{@link RefreshToken#setExpiryDate(Instant)}
     *   <li>{@link RefreshToken#setId(UUID)}
     *   <li>{@link RefreshToken#setRefreshToken(String)}
     *   <li>{@link RefreshToken#setUpdatedOn(Date)}
     *   <li>{@link RefreshToken#setUser(JwtUser)}
     *   <li>{@link RefreshToken#getCreatedOn()}
     *   <li>{@link RefreshToken#getUser()}
     *   <li>{@link RefreshToken#getId()}
     *   <li>{@link RefreshToken#getRefreshToken()}
     *   <li>{@link RefreshToken#getUpdatedOn()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        UUID id = UUID.randomUUID();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date createdOn = Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date updatedOn = Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());

        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(new JwtUser());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(new JwtUser());
        usersDetails.setUserVerified(true);

        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(refreshToken);
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(usersDetails);

        RefreshToken refreshToken1 = new RefreshToken();
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setCreatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setExpiryDate(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setId(UUID.randomUUID());
        refreshToken1.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setUpdatedOn(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken1.setUser(jwtUser);

        RefreshToken refreshToken2 = new RefreshToken();
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setCreatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setExpiryDate(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken2.setId(UUID.randomUUID());
        refreshToken2.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setUpdatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken2.setUser(new JwtUser());

        UsersDetails usersDetails1 = new UsersDetails();
        usersDetails1.setAddresses(new HashSet<>());
        usersDetails1.setAge("Age");
        usersDetails1.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setCreatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setGender(Gender.MALE);
        usersDetails1.setId(UUID.randomUUID());
        usersDetails1.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setUpdatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setUser(new JwtUser());
        usersDetails1.setUserVerified(true);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(refreshToken2);
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(usersDetails1);

        UsersDetails usersDetails2 = new UsersDetails();
        usersDetails2.setAddresses(new HashSet<>());
        usersDetails2.setAge("Age");
        usersDetails2.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult19 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setCreatedOn(Date.from(atStartOfDayResult19.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setGender(Gender.MALE);
        usersDetails2.setId(UUID.randomUUID());
        usersDetails2.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult20 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setUpdatedOn(Date.from(atStartOfDayResult20.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setUser(jwtUser1);
        usersDetails2.setUserVerified(true);

        JwtUser jwtUser2 = new JwtUser();
        LocalDateTime atStartOfDayResult21 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult21.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken1);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult22 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult22.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails2);
        LocalDateTime atStartOfDayResult23 = LocalDate.of(1970, 1, 1).atStartOfDay();
        RefreshToken actualRefreshToken = new RefreshToken(id, createdOn, updatedOn, jwtUser2, "ABC123",
                atStartOfDayResult23.atZone(ZoneId.of("UTC")).toInstant());
        LocalDateTime atStartOfDayResult24 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult24.atZone(ZoneId.of("UTC")).toInstant());
        actualRefreshToken.setCreatedOn(fromResult);
        LocalDateTime atStartOfDayResult25 = LocalDate.of(1970, 1, 1).atStartOfDay();
        actualRefreshToken.setExpiryDate(atStartOfDayResult25.atZone(ZoneId.of("UTC")).toInstant());
        UUID randomUUIDResult = UUID.randomUUID();
        actualRefreshToken.setId(randomUUIDResult);
        actualRefreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult26 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult26.atZone(ZoneId.of("UTC")).toInstant());
        actualRefreshToken.setUpdatedOn(fromResult1);
        RefreshToken refreshToken3 = new RefreshToken();
        LocalDateTime atStartOfDayResult27 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken3.setCreatedOn(Date.from(atStartOfDayResult27.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult28 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken3.setExpiryDate(atStartOfDayResult28.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken3.setId(UUID.randomUUID());
        refreshToken3.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult29 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken3.setUpdatedOn(Date.from(atStartOfDayResult29.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken3.setUser(new JwtUser());
        UsersDetails usersDetails3 = new UsersDetails();
        usersDetails3.setAddresses(new HashSet<>());
        usersDetails3.setAge("Age");
        usersDetails3.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult30 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails3.setCreatedOn(Date.from(atStartOfDayResult30.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails3.setGender(Gender.MALE);
        usersDetails3.setId(UUID.randomUUID());
        usersDetails3.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult31 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails3.setUpdatedOn(Date.from(atStartOfDayResult31.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails3.setUser(new JwtUser());
        usersDetails3.setUserVerified(true);
        JwtUser jwtUser3 = new JwtUser();
        LocalDateTime atStartOfDayResult32 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser3.setCreatedOn(Date.from(atStartOfDayResult32.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser3.setEmail("jane.doe@example.org");
        jwtUser3.setEnabled(true);
        jwtUser3.setFirstname("Jane");
        jwtUser3.setId(UUID.randomUUID());
        jwtUser3.setLastname("Doe");
        jwtUser3.setPassword("iloveyou");
        jwtUser3.setRefreshToken(refreshToken3);
        jwtUser3.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult33 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser3.setUpdatedOn(Date.from(atStartOfDayResult33.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser3.setUserEmailVerified(true);
        jwtUser3.setUsername("janedoe");
        jwtUser3.setUsersDetails(usersDetails3);
        RefreshToken refreshToken4 = new RefreshToken();
        LocalDateTime atStartOfDayResult34 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult2 = Date.from(atStartOfDayResult34.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken4.setCreatedOn(fromResult2);
        LocalDateTime atStartOfDayResult35 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken4.setExpiryDate(atStartOfDayResult35.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken4.setId(UUID.randomUUID());
        refreshToken4.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult36 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken4.setUpdatedOn(Date.from(atStartOfDayResult36.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken4.setUser(jwtUser3);
        RefreshToken refreshToken5 = new RefreshToken();
        LocalDateTime atStartOfDayResult37 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken5.setCreatedOn(Date.from(atStartOfDayResult37.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult38 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken5.setExpiryDate(atStartOfDayResult38.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken5.setId(UUID.randomUUID());
        refreshToken5.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult39 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken5.setUpdatedOn(Date.from(atStartOfDayResult39.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken5.setUser(new JwtUser());
        UsersDetails usersDetails4 = new UsersDetails();
        usersDetails4.setAddresses(new HashSet<>());
        usersDetails4.setAge("Age");
        usersDetails4.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult40 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails4.setCreatedOn(Date.from(atStartOfDayResult40.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails4.setGender(Gender.MALE);
        usersDetails4.setId(UUID.randomUUID());
        usersDetails4.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult41 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails4.setUpdatedOn(Date.from(atStartOfDayResult41.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails4.setUser(new JwtUser());
        usersDetails4.setUserVerified(true);
        JwtUser jwtUser4 = new JwtUser();
        LocalDateTime atStartOfDayResult42 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser4.setCreatedOn(Date.from(atStartOfDayResult42.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser4.setEmail("jane.doe@example.org");
        jwtUser4.setEnabled(true);
        jwtUser4.setFirstname("Jane");
        jwtUser4.setId(UUID.randomUUID());
        jwtUser4.setLastname("Doe");
        jwtUser4.setPassword("iloveyou");
        jwtUser4.setRefreshToken(refreshToken5);
        jwtUser4.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult43 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser4.setUpdatedOn(Date.from(atStartOfDayResult43.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser4.setUserEmailVerified(true);
        jwtUser4.setUsername("janedoe");
        jwtUser4.setUsersDetails(usersDetails4);
        UsersDetails usersDetails5 = new UsersDetails();
        usersDetails5.setAddresses(new HashSet<>());
        usersDetails5.setAge("Age");
        usersDetails5.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult44 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails5.setCreatedOn(Date.from(atStartOfDayResult44.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails5.setGender(Gender.MALE);
        usersDetails5.setId(UUID.randomUUID());
        usersDetails5.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult45 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails5.setUpdatedOn(Date.from(atStartOfDayResult45.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails5.setUser(jwtUser4);
        usersDetails5.setUserVerified(true);
        JwtUser jwtUser5 = new JwtUser();
        LocalDateTime atStartOfDayResult46 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser5.setCreatedOn(Date.from(atStartOfDayResult46.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser5.setEmail("jane.doe@example.org");
        jwtUser5.setEnabled(true);
        jwtUser5.setFirstname("Jane");
        jwtUser5.setId(UUID.randomUUID());
        jwtUser5.setLastname("Doe");
        jwtUser5.setPassword("iloveyou");
        jwtUser5.setRefreshToken(refreshToken4);
        jwtUser5.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult47 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser5.setUpdatedOn(Date.from(atStartOfDayResult47.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser5.setUserEmailVerified(true);
        jwtUser5.setUsername("janedoe");
        jwtUser5.setUsersDetails(usersDetails5);
        actualRefreshToken.setUser(jwtUser5);
        assertSame(fromResult, actualRefreshToken.getCreatedOn());
        JwtUser user = actualRefreshToken.getUser();
        assertSame(fromResult2, user.getRefreshToken().getCreatedOn());
        assertSame(randomUUIDResult, actualRefreshToken.getId());
        assertEquals("ABC123", actualRefreshToken.getRefreshToken());
        assertSame(fromResult1, actualRefreshToken.getUpdatedOn());
        assertSame(jwtUser5, user);
    }
}

