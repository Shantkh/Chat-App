package com.app.assigmentsubmitionapp.exceptions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class ApplicationExceptionHandlerTest {
    /**
     * Method under test: {@link ApplicationExceptionHandler#ApplicationExceptions(ApplicationExceptions)}
     */
    @Test
    void testApplicationExceptions() {
        ApplicationExceptionHandler<Object> applicationExceptionHandler = new ApplicationExceptionHandler<>();
        ResponseEntity<ResponseBuilder<?>> actualApplicationExceptionsResult = applicationExceptionHandler
                .ApplicationExceptions(new ApplicationExceptions("An error occurred"));
        assertTrue(actualApplicationExceptionsResult.hasBody());
        assertTrue(actualApplicationExceptionsResult.getHeaders().isEmpty());
        assertEquals(HttpStatus.EXPECTATION_FAILED, actualApplicationExceptionsResult.getStatusCode());
        ResponseBuilder<?> body = actualApplicationExceptionsResult.getBody();
        assertEquals("An error occurred", body.getData());
        assertFalse(body.getIsSuccess());
        assertEquals(HttpStatus.EXPECTATION_FAILED, body.getHttpStatus());
        assertEquals(417, body.getHttpStatusCode().intValue());
    }

    /**
     * Method under test: {@link ApplicationExceptionHandler#ApplicationExceptions(ApplicationExceptions)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testApplicationExceptions2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions.getMessage()" because "paginationInputExceptions" is null
        //       at com.app.assigmentsubmitionapp.exceptions.ApplicationExceptionHandler.ApplicationExceptions(ApplicationExceptionHandler.java:23)
        //   See https://diff.blue/R013 to resolve this issue.

        (new ApplicationExceptionHandler<>()).ApplicationExceptions(null);
    }
}

