package com.app.assigmentsubmitionapp.modules;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class EmailDetailsTest {

    @Test
    public void testEmailDetailsBuilder() {
        UUID id = UUID.randomUUID();
        String recipient = "test@example.com";
        String msgBody = "This is a test message body.";
        String subject = "Test Subject";
        String attachment = "attachment.txt";
        String code = "12345";
        Timestamp createdDate = new Timestamp(System.currentTimeMillis());

        EmailDetails emailDetails = EmailDetails.builder()
                .id(id)
                .recipient(recipient)
                .msgBody(msgBody)
                .subject(subject)
                .attachment(attachment)
                .code(code)
                .createdDate(createdDate)
                .build();

        Assertions.assertEquals(id, emailDetails.getId());
        Assertions.assertEquals(recipient, emailDetails.getRecipient());
        Assertions.assertEquals(msgBody, emailDetails.getMsgBody());
        Assertions.assertEquals(subject, emailDetails.getSubject());
        Assertions.assertEquals(attachment, emailDetails.getAttachment());
        Assertions.assertEquals(code, emailDetails.getCode());
        Assertions.assertEquals(createdDate, emailDetails.getCreatedDate());
    }
}