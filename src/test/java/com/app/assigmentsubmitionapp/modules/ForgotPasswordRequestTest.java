package com.app.assigmentsubmitionapp.modules;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ForgotPasswordRequestTest {

    private static Validator validator;

    @BeforeAll
    static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    void testForgotPasswordRequestBuilder() {
        ForgotPasswordRequest forgotPasswordRequest =
                ForgotPasswordRequest.builder().email("email@example.com").build();

        assertNotNull(forgotPasswordRequest);
        assertEquals("email@example.com", forgotPasswordRequest.getEmail());
    }

    @Test
    void testForgotPasswordRequestSetter() {
        ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest();
        forgotPasswordRequest.setEmail("email@example.com");

        assertEquals("email@example.com", forgotPasswordRequest.getEmail());
    }

    @Test
    void testForgotPasswordRequestEmailValidation() {
        ForgotPasswordRequest forgotPasswordRequest =
                ForgotPasswordRequest.builder().email("invalid-email").build();

        assertNotNull(forgotPasswordRequest);
        assertEquals("invalid-email", forgotPasswordRequest.getEmail());

        Set<ConstraintViolation<ForgotPasswordRequest>> violations = validator.validate(forgotPasswordRequest);
        assertFalse(violations.isEmpty());
        assertEquals("must be a well-formed email address", violations.iterator().next().getMessage());
    }

    @Test
    void testForgotPasswordRequestNotBlankValidation() {
        ForgotPasswordRequest forgotPasswordRequest =
                ForgotPasswordRequest.builder().email("").build();

        assertNotNull(forgotPasswordRequest);
        assertEquals("", forgotPasswordRequest.getEmail());

        Set<ConstraintViolation<ForgotPasswordRequest>> violations = validator.validate(forgotPasswordRequest);
        assertFalse(violations.isEmpty());
        assertEquals("must not be blank", violations.iterator().next().getMessage());
    }

    @Test
    void testForgotPasswordRequestNotNullValidation() {
        ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest();

        assertNotNull(forgotPasswordRequest);
        assertNull(forgotPasswordRequest.getEmail());

        Set<ConstraintViolation<ForgotPasswordRequest>> violations = validator.validate(forgotPasswordRequest);
        assertFalse(violations.isEmpty());
//        assertEquals("must not be blank", violations.iterator().next().getMessage());
    }
}