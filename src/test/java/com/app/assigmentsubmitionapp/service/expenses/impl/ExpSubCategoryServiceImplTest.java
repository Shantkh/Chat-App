package com.app.assigmentsubmitionapp.service.expenses.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.expenses.ExpSubCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.repo.expenses.ExpCategoryRepository;
import com.app.assigmentsubmitionapp.repo.expenses.ExpSubCategoryRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {ExpSubCategoryServiceImpl.class})
@ExtendWith(SpringExtension.class)
class ExpSubCategoryServiceImplTest {
    @MockBean
    private ExpCategoryRepository expCategoryRepository;

    @MockBean
    private ExpSubCategoryMapper expSubCategoryMapper;

    @MockBean
    private ExpSubCategoryRepository expSubCategoryRepository;

    @Autowired
    private ExpSubCategoryServiceImpl expSubCategoryServiceImpl;

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#subCategories(UUID)}
     */
    @Test
    void testSubCategories() {
        when(
                expSubCategoryMapper.fromOptionalSubCategoryObjectToOptionalSubCategoryResponse((List<ExpSubCategory>) any()))
                .thenReturn(Optional.of(new ArrayList<>()));
        when(expSubCategoryRepository.findByExpCategoryId((UUID) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(expCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class, () -> expSubCategoryServiceImpl.subCategories(UUID.randomUUID()));
        verify(expSubCategoryRepository).findByExpCategoryId((UUID) any());
        verify(expCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#subCategories(UUID)}
     */
    @Test
    void testSubCategories2() {
        when(
                expSubCategoryMapper.fromOptionalSubCategoryObjectToOptionalSubCategoryResponse((List<ExpSubCategory>) any()))
                .thenReturn(Optional.of(new ArrayList<>()));
        when(expSubCategoryRepository.findByExpCategoryId((UUID) any())).thenReturn(new ArrayList<>());
        when(expCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class, () -> expSubCategoryServiceImpl.subCategories(UUID.randomUUID()));
        verify(expCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#subCategoriesBySubId(UUID)}
     */
    @Test
    void testSubCategoriesBySubId() {
        when(expSubCategoryMapper
                .fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse((Optional<ExpSubCategory>) any()))
                .thenReturn(Optional.of(new ExpSubCategoryResponse()));
        when(expSubCategoryRepository.findById((UUID) any())).thenThrow(new ApplicationExceptions("An error occurred"));
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class,
                () -> expSubCategoryServiceImpl.subCategoriesBySubId(UUID.randomUUID()));
        verify(expSubCategoryRepository).existsById((UUID) any());
        verify(expSubCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#subCategoriesBySubId(UUID)}
     */
    @Test
    void testSubCategoriesBySubId2() {
        when(expSubCategoryMapper
                .fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse((Optional<ExpSubCategory>) any()))
                .thenReturn(Optional.of(new ExpSubCategoryResponse()));

        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubCategory> ofResult = Optional.of(expSubCategory);
        when(expSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class,
                () -> expSubCategoryServiceImpl.subCategoriesBySubId(UUID.randomUUID()));
        verify(expSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#addSubCategory(ExpSubCategoryRequest)}
     */
    @Test
    void testAddSubCategory() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubCategoryMapper.fromSubCategoryRequestToSubCategoryObject((ExpSubCategoryRequest) any()))
                .thenReturn(expSubCategory);
        ExpSubCategoryResponse expSubCategoryResponse = new ExpSubCategoryResponse();
        when(expSubCategoryMapper.fromSubCategoryObjectToSubCategoryResponse((ExpSubCategory) any()))
                .thenReturn(expSubCategoryResponse);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory1 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory1.setExpCategory(expCategory1);
        expSubCategory1.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory1.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory1.setExpSubSubCategories(new HashSet<>());
        expSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubCategoryRepository.saveAndFlush((ExpSubCategory) any())).thenReturn(expSubCategory1);
        when(expCategoryRepository.existsById((UUID) any())).thenReturn(true);
        Optional<ResponseBuilder<?>> actualAddSubCategoryResult = expSubCategoryServiceImpl
                .addSubCategory(new ExpSubCategoryRequest());
        assertTrue(actualAddSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualAddSubCategoryResult.get();
        assertSame(expSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(201, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.CREATED, getResult.getHttpStatus());
        verify(expSubCategoryMapper).fromSubCategoryRequestToSubCategoryObject((ExpSubCategoryRequest) any());
        verify(expSubCategoryMapper).fromSubCategoryObjectToSubCategoryResponse((ExpSubCategory) any());
        verify(expSubCategoryRepository).saveAndFlush((ExpSubCategory) any());
        verify(expCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#addSubCategory(ExpSubCategoryRequest)}
     */
    @Test
    void testAddSubCategory2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubCategoryMapper.fromSubCategoryRequestToSubCategoryObject((ExpSubCategoryRequest) any()))
                .thenReturn(expSubCategory);
        when(expSubCategoryMapper.fromSubCategoryObjectToSubCategoryResponse((ExpSubCategory) any()))
                .thenReturn(new ExpSubCategoryResponse());
        when(expSubCategoryRepository.saveAndFlush((ExpSubCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(expCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class,
                () -> expSubCategoryServiceImpl.addSubCategory(new ExpSubCategoryRequest()));
        verify(expSubCategoryMapper).fromSubCategoryRequestToSubCategoryObject((ExpSubCategoryRequest) any());
        verify(expSubCategoryRepository).saveAndFlush((ExpSubCategory) any());
        verify(expCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#updateSubCategory(ExpSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubCategory() {
        ExpSubCategoryResponse expSubCategoryResponse = new ExpSubCategoryResponse();
        when(expSubCategoryMapper.fromSubCategoryObjectToSubCategoryResponse((ExpSubCategory) any()))
                .thenReturn(expSubCategoryResponse);

        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubCategory> ofResult = Optional.of(expSubCategory);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory1 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory1.setExpCategory(expCategory1);
        expSubCategory1.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory1.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory1.setExpSubSubCategories(new HashSet<>());
        expSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubCategoryRepository.save((ExpSubCategory) any())).thenReturn(expSubCategory1);
        when(expSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(true);

        ExpCategory expCategory2 = new ExpCategory();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory2.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory2.setExpCategoryDescription("Exp Category Description");
        expCategory2.setExpCategoryName("Exp Category Name");
        expCategory2.setExpSubCategories(new HashSet<>());
        expCategory2.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory2.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpCategory> ofResult1 = Optional.of(expCategory2);
        when(expCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);
        Optional<ResponseBuilder<?>> actualUpdateSubCategoryResult = expSubCategoryServiceImpl
                .updateSubCategory(new ExpSubCategoryUpdateRequest());
        assertTrue(actualUpdateSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateSubCategoryResult.get();
        assertSame(expSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(expSubCategoryMapper).fromSubCategoryObjectToSubCategoryResponse((ExpSubCategory) any());
        verify(expSubCategoryRepository).existsById((UUID) any());
        verify(expSubCategoryRepository).save((ExpSubCategory) any());
        verify(expSubCategoryRepository).findById((UUID) any());
        verify(expCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#updateSubCategory(ExpSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubCategory2() {
        when(expSubCategoryMapper.fromSubCategoryObjectToSubCategoryResponse((ExpSubCategory) any()))
                .thenReturn(new ExpSubCategoryResponse());

        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubCategory> ofResult = Optional.of(expSubCategory);
        when(expSubCategoryRepository.save((ExpSubCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(expSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(true);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpCategory> ofResult1 = Optional.of(expCategory1);
        when(expCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);
        assertThrows(ApplicationExceptions.class,
                () -> expSubCategoryServiceImpl.updateSubCategory(new ExpSubCategoryUpdateRequest()));
        verify(expSubCategoryRepository).existsById((UUID) any());
        verify(expSubCategoryRepository).save((ExpSubCategory) any());
        verify(expSubCategoryRepository).findById((UUID) any());
        verify(expCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#updateSubCategory(ExpSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubCategory3() {
        ExpSubCategoryResponse expSubCategoryResponse = new ExpSubCategoryResponse();
        when(expSubCategoryMapper.fromSubCategoryObjectToSubCategoryResponse((ExpSubCategory) any()))
                .thenReturn(expSubCategoryResponse);

        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        ExpSubCategory expSubCategory = mock(ExpSubCategory.class);
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        when(expSubCategory.getCreatedOn())
                .thenReturn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        doNothing().when(expSubCategory).setCreatedOn((Date) any());
        doNothing().when(expSubCategory).setExpCategory((ExpCategory) any());
        doNothing().when(expSubCategory).setExpSubCategoryDescription((String) any());
        doNothing().when(expSubCategory).setExpSubCategoryName((String) any());
        doNothing().when(expSubCategory).setExpSubSubCategories((Set<ExpSubSubCategory>) any());
        doNothing().when(expSubCategory).setId((UUID) any());
        doNothing().when(expSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubCategory> ofResult = Optional.of(expSubCategory);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory1 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory1.setExpCategory(expCategory1);
        expSubCategory1.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory1.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory1.setExpSubSubCategories(new HashSet<>());
        expSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubCategoryRepository.save((ExpSubCategory) any())).thenReturn(expSubCategory1);
        when(expSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(true);

        ExpCategory expCategory2 = new ExpCategory();
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory2.setCreatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory2.setExpCategoryDescription("Exp Category Description");
        expCategory2.setExpCategoryName("Exp Category Name");
        expCategory2.setExpSubCategories(new HashSet<>());
        expCategory2.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory2.setUpdatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpCategory> ofResult1 = Optional.of(expCategory2);
        when(expCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);
        Optional<ResponseBuilder<?>> actualUpdateSubCategoryResult = expSubCategoryServiceImpl
                .updateSubCategory(new ExpSubCategoryUpdateRequest());
        assertTrue(actualUpdateSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateSubCategoryResult.get();
        assertSame(expSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(expSubCategoryMapper).fromSubCategoryObjectToSubCategoryResponse((ExpSubCategory) any());
        verify(expSubCategoryRepository).existsById((UUID) any());
        verify(expSubCategoryRepository).save((ExpSubCategory) any());
        verify(expSubCategoryRepository).findById((UUID) any());
        verify(expSubCategory).getCreatedOn();
        verify(expSubCategory).setCreatedOn((Date) any());
        verify(expSubCategory).setExpCategory((ExpCategory) any());
        verify(expSubCategory).setExpSubCategoryDescription((String) any());
        verify(expSubCategory).setExpSubCategoryName((String) any());
        verify(expSubCategory).setExpSubSubCategories((Set<ExpSubSubCategory>) any());
        verify(expSubCategory).setId((UUID) any());
        verify(expSubCategory).setUpdatedOn((Date) any());
        verify(expCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#deleteSubCategory(UUID)}
     */
    @Test
    void testDeleteSubCategory() {
        when(expSubCategoryRepository.findById((UUID) any())).thenThrow(new ApplicationExceptions("An error occurred"));
        doThrow(new ApplicationExceptions("An error occurred")).when(expSubCategoryRepository).deleteById((UUID) any());
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class, () -> expSubCategoryServiceImpl.deleteSubCategory(UUID.randomUUID()));
        verify(expSubCategoryRepository).existsById((UUID) any());
        verify(expSubCategoryRepository).deleteById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryServiceImpl#deleteSubCategory(UUID)}
     */
    @Test
    void testDeleteSubCategory2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubCategory> ofResult = Optional.of(expSubCategory);
        when(expSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        doNothing().when(expSubCategoryRepository).deleteById((UUID) any());
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class, () -> expSubCategoryServiceImpl.deleteSubCategory(UUID.randomUUID()));
        verify(expSubCategoryRepository).existsById((UUID) any());
    }
}

