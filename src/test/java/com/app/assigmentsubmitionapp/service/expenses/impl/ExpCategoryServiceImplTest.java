package com.app.assigmentsubmitionapp.service.expenses.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.expenses.ExpCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryResponse;
import com.app.assigmentsubmitionapp.repo.expenses.ExpCategoryRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {ExpCategoryServiceImpl.class})
@ExtendWith(SpringExtension.class)
class ExpCategoryServiceImplTest {
    @MockBean
    private ExpCategoryMapper expCategoryMapper;

    @MockBean
    private ExpCategoryRepository expCategoryRepository;

    @Autowired
    private ExpCategoryServiceImpl expCategoryServiceImpl;

    /**
     * Method under test: {@link ExpCategoryServiceImpl#categories()}
     */
    @Test
    void testCategories() {
        when(expCategoryMapper.fromCategoryObjectToListCategoryResponse(any()))
                .thenReturn(Optional.of(new ArrayList<>()));
        when(expCategoryRepository.findAll()).thenThrow(new ApplicationExceptions("An error occurred"));
        assertThrows(ApplicationExceptions.class, () -> expCategoryServiceImpl.categories());
        verify(expCategoryRepository).findAll();
    }

    /**
     * Method under test: {@link ExpCategoryServiceImpl#updateCategory(ExpCategoryRequest)}
     */
    @Test
    void testUpdateCategory() {
        ExpCategoryResponse expCategoryResponse = new ExpCategoryResponse();
        when(expCategoryMapper.fromCategoryToCategoryResponse((ExpCategory) any())).thenReturn(expCategoryResponse);

        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(expCategoryRepository.saveAndFlush( any())).thenReturn(expCategory1);
        when(expCategoryRepository.getReferenceById( any())).thenReturn(expCategory);
        when(expCategoryRepository.existsById((UUID) any())).thenReturn(true);
        Optional<ResponseBuilder<?>> actualUpdateCategoryResult = expCategoryServiceImpl
                .updateCategory(new ExpCategoryRequest());
        assertTrue(actualUpdateCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateCategoryResult.get();
        assertSame(expCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(expCategoryMapper).fromCategoryToCategoryResponse((ExpCategory) any());
        verify(expCategoryRepository).existsById((UUID) any());
        verify(expCategoryRepository).getReferenceById((UUID) any());
        verify(expCategoryRepository).saveAndFlush((ExpCategory) any());
    }

    /**
     * Method under test: {@link ExpCategoryServiceImpl#updateCategory(ExpCategoryRequest)}
     */
    @Test
    void testUpdateCategory2() {
        when(expCategoryMapper.fromCategoryToCategoryResponse((ExpCategory) any())).thenReturn(new ExpCategoryResponse());

        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(expCategoryRepository.saveAndFlush((ExpCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(expCategoryRepository.getReferenceById((UUID) any())).thenReturn(expCategory);
        when(expCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class, () -> expCategoryServiceImpl.updateCategory(new ExpCategoryRequest()));
        verify(expCategoryRepository).existsById((UUID) any());
        verify(expCategoryRepository).getReferenceById((UUID) any());
        verify(expCategoryRepository).saveAndFlush((ExpCategory) any());
    }

    /**
     * Method under test: {@link ExpCategoryServiceImpl#updateCategory(ExpCategoryRequest)}
     */
    @Test
    void testUpdateCategory3() {
        ExpCategoryResponse expCategoryResponse = new ExpCategoryResponse();
        when(expCategoryMapper.fromCategoryToCategoryResponse((ExpCategory) any())).thenReturn(expCategoryResponse);
        ExpCategory expCategory = mock(ExpCategory.class);
        doNothing().when(expCategory).setCreatedOn((Date) any());
        doNothing().when(expCategory).setExpCategoryDescription((String) any());
        doNothing().when(expCategory).setExpCategoryName((String) any());
        doNothing().when(expCategory).setExpSubCategories((Set<ExpSubCategory>) any());
        doNothing().when(expCategory).setId((UUID) any());
        doNothing().when(expCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(expCategoryRepository.saveAndFlush((ExpCategory) any())).thenReturn(expCategory1);
        when(expCategoryRepository.getReferenceById((UUID) any())).thenReturn(expCategory);
        when(expCategoryRepository.existsById((UUID) any())).thenReturn(true);
        Optional<ResponseBuilder<?>> actualUpdateCategoryResult = expCategoryServiceImpl
                .updateCategory(new ExpCategoryRequest());
        assertTrue(actualUpdateCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateCategoryResult.get();
        assertSame(expCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(expCategoryMapper).fromCategoryToCategoryResponse((ExpCategory) any());
        verify(expCategoryRepository).existsById((UUID) any());
        verify(expCategoryRepository).getReferenceById((UUID) any());
        verify(expCategoryRepository).saveAndFlush((ExpCategory) any());
        verify(expCategory).setCreatedOn((Date) any());
        verify(expCategory, atLeast(1)).setExpCategoryDescription((String) any());
        verify(expCategory, atLeast(1)).setExpCategoryName((String) any());
        verify(expCategory).setExpSubCategories((Set<ExpSubCategory>) any());
        verify(expCategory, atLeast(1)).setId((UUID) any());
        verify(expCategory, atLeast(1)).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link ExpCategoryServiceImpl#updateCategory(ExpCategoryRequest)}
     */
    @Test
    void testUpdateCategory4() {
        when(expCategoryMapper.fromCategoryToCategoryResponse((ExpCategory) any())).thenReturn(new ExpCategoryResponse());
        ExpCategory expCategory = mock(ExpCategory.class);
        doNothing().when(expCategory).setCreatedOn((Date) any());
        doNothing().when(expCategory).setExpCategoryDescription((String) any());
        doNothing().when(expCategory).setExpCategoryName((String) any());
        doNothing().when(expCategory).setExpSubCategories((Set<ExpSubCategory>) any());
        doNothing().when(expCategory).setId((UUID) any());
        doNothing().when(expCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(expCategoryRepository.saveAndFlush((ExpCategory) any())).thenReturn(expCategory1);
        when(expCategoryRepository.getReferenceById((UUID) any())).thenReturn(expCategory);
        when(expCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class, () -> expCategoryServiceImpl.updateCategory(new ExpCategoryRequest()));
        verify(expCategoryRepository).existsById((UUID) any());
        verify(expCategory).setCreatedOn((Date) any());
        verify(expCategory).setExpCategoryDescription((String) any());
        verify(expCategory).setExpCategoryName((String) any());
        verify(expCategory).setExpSubCategories((Set<ExpSubCategory>) any());
        verify(expCategory).setId((UUID) any());
        verify(expCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link ExpCategoryServiceImpl#deleteCategory(UUID)}
     */
    @Test
    void testDeleteCategory() {
        when(expCategoryRepository.findById((UUID) any())).thenThrow(new ApplicationExceptions("An error occurred"));
        doThrow(new ApplicationExceptions("An error occurred")).when(expCategoryRepository).deleteById((UUID) any());
        when(expCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class, () -> expCategoryServiceImpl.deleteCategory(UUID.randomUUID()));
        verify(expCategoryRepository).existsById((UUID) any());
        verify(expCategoryRepository).deleteById((UUID) any());
    }

    /**
     * Method under test: {@link ExpCategoryServiceImpl#deleteCategory(UUID)}
     */
    @Test
    void testDeleteCategory2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpCategory> ofResult = Optional.of(expCategory);
        when(expCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        doNothing().when(expCategoryRepository).deleteById((UUID) any());
        when(expCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class, () -> expCategoryServiceImpl.deleteCategory(UUID.randomUUID()));
        verify(expCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpCategoryServiceImpl#addCategory(ExpCategoryRequest)}
     */
    @Test
    void testAddCategory() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(expCategoryMapper.fromCategoryRequestToCategoryObject((ExpCategoryRequest) any())).thenReturn(expCategory);
        ExpCategoryResponse expCategoryResponse = new ExpCategoryResponse();
        when(expCategoryMapper.fromCategoryToCategoryResponse((ExpCategory) any())).thenReturn(expCategoryResponse);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(expCategoryRepository.saveAndFlush((ExpCategory) any())).thenReturn(expCategory1);
        Optional<ResponseBuilder<?>> actualAddCategoryResult = expCategoryServiceImpl.addCategory(new ExpCategoryRequest());
        assertTrue(actualAddCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualAddCategoryResult.get();
        assertSame(expCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(201, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.CREATED, getResult.getHttpStatus());
        verify(expCategoryMapper).fromCategoryRequestToCategoryObject((ExpCategoryRequest) any());
        verify(expCategoryMapper).fromCategoryToCategoryResponse((ExpCategory) any());
        verify(expCategoryRepository).saveAndFlush((ExpCategory) any());
    }

    /**
     * Method under test: {@link ExpCategoryServiceImpl#addCategory(ExpCategoryRequest)}
     */
    @Test
    void testAddCategory2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(expCategoryMapper.fromCategoryRequestToCategoryObject((ExpCategoryRequest) any())).thenReturn(expCategory);
        when(expCategoryMapper.fromCategoryToCategoryResponse((ExpCategory) any())).thenReturn(new ExpCategoryResponse());
        when(expCategoryRepository.saveAndFlush((ExpCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        assertThrows(ApplicationExceptions.class, () -> expCategoryServiceImpl.addCategory(new ExpCategoryRequest()));
        verify(expCategoryMapper).fromCategoryRequestToCategoryObject((ExpCategoryRequest) any());
        verify(expCategoryRepository).saveAndFlush((ExpCategory) any());
    }
}

