package com.app.assigmentsubmitionapp.service.expenses.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.expenses.ExpSubSubCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.repo.expenses.ExpCategoryRepository;
import com.app.assigmentsubmitionapp.repo.expenses.ExpSubCategoryRepository;
import com.app.assigmentsubmitionapp.repo.expenses.ExpSubSubCategoryRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {ExpSubSubCategoryServiceImpl.class})
@ExtendWith(SpringExtension.class)
class ExpSubSubCategoryServiceImplTest {
    @MockBean
    private ExpCategoryRepository expCategoryRepository;

    @MockBean
    private ExpSubCategoryRepository expSubCategoryRepository;

    @MockBean
    private ExpSubSubCategoryMapper expSubSubCategoryMapper;

    @MockBean
    private ExpSubSubCategoryRepository expSubSubCategoryRepository;

    @Autowired
    private ExpSubSubCategoryServiceImpl expSubSubCategoryServiceImpl;

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#subSubCategories(UUID)}
     */
    @Test
    void testSubSubCategories() {
        when(expSubSubCategoryRepository.findByExpSubCategoryId((UUID) any())).thenReturn(new ArrayList<>());
        when(expSubSubCategoryMapper
                .fromListSubCategoryObjectToOptionalSubSubCategoryResponse((List<ExpSubSubCategory>) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class, () -> expSubSubCategoryServiceImpl.subSubCategories(UUID.randomUUID()));
        verify(expSubSubCategoryRepository).findByExpSubCategoryId((UUID) any());
        verify(expSubSubCategoryMapper)
                .fromListSubCategoryObjectToOptionalSubSubCategoryResponse((List<ExpSubSubCategory>) any());
        verify(expSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#subSubCategories(UUID)}
     */
    @Test
    void testSubSubCategories2() {
        when(expSubSubCategoryRepository.findByExpSubCategoryId((UUID) any())).thenReturn(new ArrayList<>());
        when(expSubSubCategoryMapper
                .fromListSubCategoryObjectToOptionalSubSubCategoryResponse((List<ExpSubSubCategory>) any()))
                .thenReturn(Optional.of(new ArrayList<>()));
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class, () -> expSubSubCategoryServiceImpl.subSubCategories(UUID.randomUUID()));
        verify(expSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#subSubCategoriesBySubSubId(UUID)}
     */
    @Test
    void testSubSubCategoriesBySubSubId() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubSubCategory> ofResult = Optional.of(expSubSubCategory);
        when(expSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(expSubSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        when(expSubSubCategoryMapper
                .fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse((Optional<ExpSubSubCategory>) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        assertThrows(ApplicationExceptions.class,
                () -> expSubSubCategoryServiceImpl.subSubCategoriesBySubSubId(UUID.randomUUID()));
        verify(expSubSubCategoryRepository).existsById((UUID) any());
        verify(expSubSubCategoryRepository).findById((UUID) any());
        verify(expSubSubCategoryMapper)
                .fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse((Optional<ExpSubSubCategory>) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#subSubCategoriesBySubSubId(UUID)}
     */
    @Test
    void testSubSubCategoriesBySubSubId2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubSubCategory> ofResult = Optional.of(expSubSubCategory);
        when(expSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(expSubSubCategoryRepository.existsById((UUID) any())).thenReturn(false);
        when(expSubSubCategoryMapper
                .fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse((Optional<ExpSubSubCategory>) any()))
                .thenReturn(Optional.of(new ExpSubSubCategoryResponse()));
        assertThrows(ApplicationExceptions.class,
                () -> expSubSubCategoryServiceImpl.subSubCategoriesBySubSubId(UUID.randomUUID()));
        verify(expSubSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#updateSubSubCategory(ExpSubSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubSubCategory() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubSubCategory> ofResult = Optional.of(expSubSubCategory);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory1 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory1.setExpCategory(expCategory1);
        expSubCategory1.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory1.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory1.setExpSubSubCategories(new HashSet<>());
        expSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory1 = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory1.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory1.setExpSubCategory(expSubCategory1);
        expSubSubCategory1.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory1.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubSubCategoryRepository.save((ExpSubSubCategory) any())).thenReturn(expSubSubCategory1);
        when(expSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(expSubSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        ExpSubSubCategoryResponse expSubSubCategoryResponse = new ExpSubSubCategoryResponse();
        when(expSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse((ExpSubSubCategory) any()))
                .thenReturn(expSubSubCategoryResponse);

        ExpCategory expCategory2 = new ExpCategory();
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory2.setCreatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory2.setExpCategoryDescription("Exp Category Description");
        expCategory2.setExpCategoryName("Exp Category Name");
        expCategory2.setExpSubCategories(new HashSet<>());
        expCategory2.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory2.setUpdatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory2 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory2.setCreatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory2.setExpCategory(expCategory2);
        expSubCategory2.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory2.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory2.setExpSubSubCategories(new HashSet<>());
        expSubCategory2.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory2.setUpdatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubCategory> ofResult1 = Optional.of(expSubCategory2);
        when(expSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);

        ExpCategory expCategory3 = new ExpCategory();
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory3.setCreatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory3.setExpCategoryDescription("Exp Category Description");
        expCategory3.setExpCategoryName("Exp Category Name");
        expCategory3.setExpSubCategories(new HashSet<>());
        expCategory3.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory3.setUpdatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpCategory> ofResult2 = Optional.of(expCategory3);
        when(expCategoryRepository.findById((UUID) any())).thenReturn(ofResult2);
        Optional<ResponseBuilder<?>> actualUpdateSubSubCategoryResult = expSubSubCategoryServiceImpl
                .updateSubSubCategory(new ExpSubSubCategoryUpdateRequest());
        assertTrue(actualUpdateSubSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateSubSubCategoryResult.get();
        assertSame(expSubSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(expSubSubCategoryRepository).existsById((UUID) any());
        verify(expSubSubCategoryRepository).save((ExpSubSubCategory) any());
        verify(expSubSubCategoryRepository).findById((UUID) any());
        verify(expSubSubCategoryMapper).fromSubSubCategoryObjectToSubSubCategoryResponse((ExpSubSubCategory) any());
        verify(expSubCategoryRepository).findById((UUID) any());
        verify(expCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#updateSubSubCategory(ExpSubSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubSubCategory2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubSubCategory> ofResult = Optional.of(expSubSubCategory);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory1 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory1.setExpCategory(expCategory1);
        expSubCategory1.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory1.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory1.setExpSubSubCategories(new HashSet<>());
        expSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory1 = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory1.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory1.setExpSubCategory(expSubCategory1);
        expSubSubCategory1.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory1.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubSubCategoryRepository.save((ExpSubSubCategory) any())).thenReturn(expSubSubCategory1);
        when(expSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(expSubSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        when(expSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse((ExpSubSubCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));

        ExpCategory expCategory2 = new ExpCategory();
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory2.setCreatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory2.setExpCategoryDescription("Exp Category Description");
        expCategory2.setExpCategoryName("Exp Category Name");
        expCategory2.setExpSubCategories(new HashSet<>());
        expCategory2.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory2.setUpdatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory2 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory2.setCreatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory2.setExpCategory(expCategory2);
        expSubCategory2.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory2.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory2.setExpSubSubCategories(new HashSet<>());
        expSubCategory2.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory2.setUpdatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubCategory> ofResult1 = Optional.of(expSubCategory2);
        when(expSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);

        ExpCategory expCategory3 = new ExpCategory();
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory3.setCreatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory3.setExpCategoryDescription("Exp Category Description");
        expCategory3.setExpCategoryName("Exp Category Name");
        expCategory3.setExpSubCategories(new HashSet<>());
        expCategory3.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory3.setUpdatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpCategory> ofResult2 = Optional.of(expCategory3);
        when(expCategoryRepository.findById((UUID) any())).thenReturn(ofResult2);
        assertThrows(ApplicationExceptions.class,
                () -> expSubSubCategoryServiceImpl.updateSubSubCategory(new ExpSubSubCategoryUpdateRequest()));
        verify(expSubSubCategoryRepository).existsById((UUID) any());
        verify(expSubSubCategoryRepository).save((ExpSubSubCategory) any());
        verify(expSubSubCategoryRepository).findById((UUID) any());
        verify(expSubSubCategoryMapper).fromSubSubCategoryObjectToSubSubCategoryResponse((ExpSubSubCategory) any());
        verify(expSubCategoryRepository).findById((UUID) any());
        verify(expCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#updateSubSubCategory(ExpSubSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubSubCategory3() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        ExpSubSubCategory expSubSubCategory = mock(ExpSubSubCategory.class);
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        when(expSubSubCategory.getCreatedOn())
                .thenReturn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubSubCategory.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(expSubSubCategory).setCreatedOn((Date) any());
        doNothing().when(expSubSubCategory).setExpSubCategory((ExpSubCategory) any());
        doNothing().when(expSubSubCategory).setExpSubSubCategoryDescription((String) any());
        doNothing().when(expSubSubCategory).setExpSubSubCategoryName((String) any());
        doNothing().when(expSubSubCategory).setId((UUID) any());
        doNothing().when(expSubSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubSubCategory> ofResult = Optional.of(expSubSubCategory);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory1 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setCreatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory1.setExpCategory(expCategory1);
        expSubCategory1.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory1.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory1.setExpSubSubCategories(new HashSet<>());
        expSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory1 = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory1.setCreatedOn(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory1.setExpSubCategory(expSubCategory1);
        expSubSubCategory1.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory1.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubSubCategoryRepository.save((ExpSubSubCategory) any())).thenReturn(expSubSubCategory1);
        when(expSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(expSubSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        ExpSubSubCategoryResponse expSubSubCategoryResponse = new ExpSubSubCategoryResponse();
        when(expSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse((ExpSubSubCategory) any()))
                .thenReturn(expSubSubCategoryResponse);

        ExpCategory expCategory2 = new ExpCategory();
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory2.setCreatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory2.setExpCategoryDescription("Exp Category Description");
        expCategory2.setExpCategoryName("Exp Category Name");
        expCategory2.setExpSubCategories(new HashSet<>());
        expCategory2.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory2.setUpdatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory2 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory2.setCreatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory2.setExpCategory(expCategory2);
        expSubCategory2.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory2.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory2.setExpSubSubCategories(new HashSet<>());
        expSubCategory2.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory2.setUpdatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubCategory> ofResult1 = Optional.of(expSubCategory2);
        when(expSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);

        ExpCategory expCategory3 = new ExpCategory();
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory3.setCreatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory3.setExpCategoryDescription("Exp Category Description");
        expCategory3.setExpCategoryName("Exp Category Name");
        expCategory3.setExpSubCategories(new HashSet<>());
        expCategory3.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory3.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpCategory> ofResult2 = Optional.of(expCategory3);
        when(expCategoryRepository.findById((UUID) any())).thenReturn(ofResult2);
        Optional<ResponseBuilder<?>> actualUpdateSubSubCategoryResult = expSubSubCategoryServiceImpl
                .updateSubSubCategory(new ExpSubSubCategoryUpdateRequest());
        assertTrue(actualUpdateSubSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateSubSubCategoryResult.get();
        assertSame(expSubSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(expSubSubCategoryRepository).existsById((UUID) any());
        verify(expSubSubCategoryRepository).save((ExpSubSubCategory) any());
        verify(expSubSubCategoryRepository).findById((UUID) any());
        verify(expSubSubCategory).getCreatedOn();
        verify(expSubSubCategory).getId();
        verify(expSubSubCategory).setCreatedOn((Date) any());
        verify(expSubSubCategory).setExpSubCategory((ExpSubCategory) any());
        verify(expSubSubCategory).setExpSubSubCategoryDescription((String) any());
        verify(expSubSubCategory).setExpSubSubCategoryName((String) any());
        verify(expSubSubCategory).setId((UUID) any());
        verify(expSubSubCategory).setUpdatedOn((Date) any());
        verify(expSubSubCategoryMapper).fromSubSubCategoryObjectToSubSubCategoryResponse((ExpSubSubCategory) any());
        verify(expSubCategoryRepository).findById((UUID) any());
        verify(expCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#deleteSubSubCategory(UUID)}
     */
    @Test
    void testDeleteSubSubCategory() {
        when(expSubSubCategoryRepository.findById((UUID) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        doThrow(new ApplicationExceptions("An error occurred")).when(expSubSubCategoryRepository)
                .deleteById((UUID) any());
        when(expSubSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class,
                () -> expSubSubCategoryServiceImpl.deleteSubSubCategory(UUID.randomUUID()));
        verify(expSubSubCategoryRepository).existsById((UUID) any());
        verify(expSubSubCategoryRepository).deleteById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#deleteSubSubCategory(UUID)}
     */
    @Test
    void testDeleteSubSubCategory2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubSubCategory> ofResult = Optional.of(expSubSubCategory);
        when(expSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        doNothing().when(expSubSubCategoryRepository).deleteById((UUID) any());
        when(expSubSubCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class,
                () -> expSubSubCategoryServiceImpl.deleteSubSubCategory(UUID.randomUUID()));
        verify(expSubSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#addSubSubCategory(ExpSubSubCategoryRequest)}
     */
    @Test
    void testAddSubSubCategory() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubSubCategoryRepository.saveAndFlush((ExpSubSubCategory) any())).thenReturn(expSubSubCategory);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory1 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory1.setExpCategory(expCategory1);
        expSubCategory1.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory1.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory1.setExpSubSubCategories(new HashSet<>());
        expSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory1 = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory1.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory1.setExpSubCategory(expSubCategory1);
        expSubSubCategory1.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory1.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubSubCategoryMapper.fromSubSubCategoryRequestToSubSubCategoryObject((ExpSubSubCategoryRequest) any()))
                .thenReturn(expSubSubCategory1);
        ExpSubSubCategoryResponse expSubSubCategoryResponse = new ExpSubSubCategoryResponse();
        when(expSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse((ExpSubSubCategory) any()))
                .thenReturn(expSubSubCategoryResponse);
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        Optional<ResponseBuilder<?>> actualAddSubSubCategoryResult = expSubSubCategoryServiceImpl
                .addSubSubCategory(new ExpSubSubCategoryRequest());
        assertTrue(actualAddSubSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualAddSubSubCategoryResult.get();
        assertSame(expSubSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(201, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.CREATED, getResult.getHttpStatus());
        verify(expSubSubCategoryRepository).saveAndFlush((ExpSubSubCategory) any());
        verify(expSubSubCategoryMapper).fromSubSubCategoryRequestToSubSubCategoryObject((ExpSubSubCategoryRequest) any());
        verify(expSubSubCategoryMapper).fromSubSubCategoryObjectToSubSubCategoryResponse((ExpSubSubCategory) any());
        verify(expSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryServiceImpl#addSubSubCategory(ExpSubSubCategoryRequest)}
     */
    @Test
    void testAddSubSubCategory2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubSubCategoryRepository.saveAndFlush((ExpSubSubCategory) any())).thenReturn(expSubSubCategory);
        when(expSubSubCategoryMapper.fromSubSubCategoryRequestToSubSubCategoryObject((ExpSubSubCategoryRequest) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(expSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse((ExpSubSubCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(expSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class,
                () -> expSubSubCategoryServiceImpl.addSubSubCategory(new ExpSubSubCategoryRequest()));
        verify(expSubSubCategoryMapper).fromSubSubCategoryRequestToSubSubCategoryObject((ExpSubSubCategoryRequest) any());
        verify(expSubCategoryRepository).existsById((UUID) any());
    }
}

