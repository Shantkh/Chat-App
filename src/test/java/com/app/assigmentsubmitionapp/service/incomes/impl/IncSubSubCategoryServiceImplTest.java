package com.app.assigmentsubmitionapp.service.incomes.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.incomes.IncSubSubCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.repo.incomes.IncCategoryRepository;
import com.app.assigmentsubmitionapp.repo.incomes.IncSubCategoryRepository;
import com.app.assigmentsubmitionapp.repo.incomes.IncSubSubCategoryRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {IncSubSubCategoryServiceImpl.class})
@ExtendWith(SpringExtension.class)
class IncSubSubCategoryServiceImplTest {
    @MockBean
    private IncCategoryRepository incCategoryRepository;

    @MockBean
    private IncSubCategoryRepository incSubCategoryRepository;

    @MockBean
    private IncSubSubCategoryMapper incSubSubCategoryMapper;

    @MockBean
    private IncSubSubCategoryRepository incSubSubCategoryRepository;

    @Autowired
    private IncSubSubCategoryServiceImpl incSubSubCategoryServiceImpl;

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#subSubCategories(UUID)}
     */
    @Test
    void testSubSubCategories() {
        when(incSubSubCategoryRepository.findByIncSubCategoryId((UUID) any())).thenReturn(new ArrayList<>());
        when(incSubSubCategoryMapper
                .fromListSubCategoryObjectToOptionalSubSubCategoryResponse((List<IncSubSubCategory>) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class, () -> incSubSubCategoryServiceImpl.subSubCategories(UUID.randomUUID()));
        verify(incSubSubCategoryRepository).findByIncSubCategoryId((UUID) any());
        verify(incSubSubCategoryMapper)
                .fromListSubCategoryObjectToOptionalSubSubCategoryResponse((List<IncSubSubCategory>) any());
        verify(incSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#subSubCategories(UUID)}
     */
    @Test
    void testSubSubCategories2() {
        when(incSubSubCategoryRepository.findByIncSubCategoryId((UUID) any())).thenReturn(new ArrayList<>());
        when(incSubSubCategoryMapper
                .fromListSubCategoryObjectToOptionalSubSubCategoryResponse((List<IncSubSubCategory>) any()))
                .thenReturn(Optional.of(new ArrayList<>()));
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class, () -> incSubSubCategoryServiceImpl.subSubCategories(UUID.randomUUID()));
        verify(incSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#subSubCategoriesBySubSubId(UUID)}
     */
    @Test
    void testSubSubCategoriesBySubSubId() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory.setId(UUID.randomUUID());
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubSubCategory> ofResult = Optional.of(incSubSubCategory);
        when(incSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(incSubSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        when(incSubSubCategoryMapper
                .fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse((Optional<IncSubSubCategory>) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        assertThrows(ApplicationExceptions.class,
                () -> incSubSubCategoryServiceImpl.subSubCategoriesBySubSubId(UUID.randomUUID()));
        verify(incSubSubCategoryRepository).existsById((UUID) any());
        verify(incSubSubCategoryRepository).findById((UUID) any());
        verify(incSubSubCategoryMapper)
                .fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse((Optional<IncSubSubCategory>) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#subSubCategoriesBySubSubId(UUID)}
     */
    @Test
    void testSubSubCategoriesBySubSubId2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory.setId(UUID.randomUUID());
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubSubCategory> ofResult = Optional.of(incSubSubCategory);
        when(incSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(incSubSubCategoryRepository.existsById((UUID) any())).thenReturn(false);
        when(incSubSubCategoryMapper
                .fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse((Optional<IncSubSubCategory>) any()))
                .thenReturn(Optional.of(new IncSubSubCategoryResponse()));
        assertThrows(ApplicationExceptions.class,
                () -> incSubSubCategoryServiceImpl.subSubCategoriesBySubSubId(UUID.randomUUID()));
        verify(incSubSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#updateSubSubCategory(IncSubSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubSubCategory() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory.setId(UUID.randomUUID());
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubSubCategory> ofResult = Optional.of(incSubSubCategory);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory1 = new IncSubCategory();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory1.setId(UUID.randomUUID());
        incSubCategory1.setIncCategory(incCategory1);
        incSubCategory1.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory1.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory1.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory1 = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory1.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory1.setId(UUID.randomUUID());
        incSubSubCategory1.setIncSubCategory(incSubCategory1);
        incSubSubCategory1.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory1.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubSubCategoryRepository.save((IncSubSubCategory) any())).thenReturn(incSubSubCategory1);
        when(incSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(incSubSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        IncSubSubCategoryResponse incSubSubCategoryResponse = new IncSubSubCategoryResponse();
        when(incSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse((IncSubSubCategory) any()))
                .thenReturn(incSubSubCategoryResponse);

        IncCategory incCategory2 = new IncCategory();
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory2.setCreatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory2.setId(UUID.randomUUID());
        incCategory2.setIncCategoryDescription("Inc Category Description");
        incCategory2.setIncCategoryName("Inc Category Name");
        incCategory2.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory2.setUpdatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory2 = new IncSubCategory();
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory2.setCreatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory2.setId(UUID.randomUUID());
        incSubCategory2.setIncCategory(incCategory2);
        incSubCategory2.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory2.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory2.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory2.setUpdatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubCategory> ofResult1 = Optional.of(incSubCategory2);
        when(incSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);

        IncCategory incCategory3 = new IncCategory();
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory3.setCreatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory3.setId(UUID.randomUUID());
        incCategory3.setIncCategoryDescription("Inc Category Description");
        incCategory3.setIncCategoryName("Inc Category Name");
        incCategory3.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory3.setUpdatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncCategory> ofResult2 = Optional.of(incCategory3);
        when(incCategoryRepository.findById((UUID) any())).thenReturn(ofResult2);
        Optional<ResponseBuilder<?>> actualUpdateSubSubCategoryResult = incSubSubCategoryServiceImpl
                .updateSubSubCategory(new IncSubSubCategoryUpdateRequest());
        assertTrue(actualUpdateSubSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateSubSubCategoryResult.get();
        assertSame(incSubSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(incSubSubCategoryRepository).existsById((UUID) any());
        verify(incSubSubCategoryRepository).save((IncSubSubCategory) any());
        verify(incSubSubCategoryRepository).findById((UUID) any());
        verify(incSubSubCategoryMapper).fromSubSubCategoryObjectToSubSubCategoryResponse((IncSubSubCategory) any());
        verify(incSubCategoryRepository).findById((UUID) any());
        verify(incCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#updateSubSubCategory(IncSubSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubSubCategory2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory.setId(UUID.randomUUID());
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubSubCategory> ofResult = Optional.of(incSubSubCategory);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory1 = new IncSubCategory();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory1.setId(UUID.randomUUID());
        incSubCategory1.setIncCategory(incCategory1);
        incSubCategory1.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory1.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory1.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory1 = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory1.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory1.setId(UUID.randomUUID());
        incSubSubCategory1.setIncSubCategory(incSubCategory1);
        incSubSubCategory1.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory1.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubSubCategoryRepository.save((IncSubSubCategory) any())).thenReturn(incSubSubCategory1);
        when(incSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(incSubSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        when(incSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse((IncSubSubCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));

        IncCategory incCategory2 = new IncCategory();
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory2.setCreatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory2.setId(UUID.randomUUID());
        incCategory2.setIncCategoryDescription("Inc Category Description");
        incCategory2.setIncCategoryName("Inc Category Name");
        incCategory2.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory2.setUpdatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory2 = new IncSubCategory();
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory2.setCreatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory2.setId(UUID.randomUUID());
        incSubCategory2.setIncCategory(incCategory2);
        incSubCategory2.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory2.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory2.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory2.setUpdatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubCategory> ofResult1 = Optional.of(incSubCategory2);
        when(incSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);

        IncCategory incCategory3 = new IncCategory();
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory3.setCreatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory3.setId(UUID.randomUUID());
        incCategory3.setIncCategoryDescription("Inc Category Description");
        incCategory3.setIncCategoryName("Inc Category Name");
        incCategory3.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory3.setUpdatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncCategory> ofResult2 = Optional.of(incCategory3);
        when(incCategoryRepository.findById((UUID) any())).thenReturn(ofResult2);
        assertThrows(ApplicationExceptions.class,
                () -> incSubSubCategoryServiceImpl.updateSubSubCategory(new IncSubSubCategoryUpdateRequest()));
        verify(incSubSubCategoryRepository).existsById((UUID) any());
        verify(incSubSubCategoryRepository).save((IncSubSubCategory) any());
        verify(incSubSubCategoryRepository).findById((UUID) any());
        verify(incSubSubCategoryMapper).fromSubSubCategoryObjectToSubSubCategoryResponse((IncSubSubCategory) any());
        verify(incSubCategoryRepository).findById((UUID) any());
        verify(incCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#updateSubSubCategory(IncSubSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubSubCategory3() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        IncSubSubCategory incSubSubCategory = mock(IncSubSubCategory.class);
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        when(incSubSubCategory.getCreatedOn())
                .thenReturn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubSubCategory.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(incSubSubCategory).setCreatedOn((Date) any());
        doNothing().when(incSubSubCategory).setId((UUID) any());
        doNothing().when(incSubSubCategory).setIncSubCategory((IncSubCategory) any());
        doNothing().when(incSubSubCategory).setIncSubSubCategoryDescription((String) any());
        doNothing().when(incSubSubCategory).setIncSubSubCategoryName((String) any());
        doNothing().when(incSubSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory.setId(UUID.randomUUID());
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubSubCategory> ofResult = Optional.of(incSubSubCategory);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory1 = new IncSubCategory();
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setCreatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory1.setId(UUID.randomUUID());
        incSubCategory1.setIncCategory(incCategory1);
        incSubCategory1.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory1.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory1.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory1 = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory1.setCreatedOn(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory1.setId(UUID.randomUUID());
        incSubSubCategory1.setIncSubCategory(incSubCategory1);
        incSubSubCategory1.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory1.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubSubCategoryRepository.save((IncSubSubCategory) any())).thenReturn(incSubSubCategory1);
        when(incSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(incSubSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        IncSubSubCategoryResponse incSubSubCategoryResponse = new IncSubSubCategoryResponse();
        when(incSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse((IncSubSubCategory) any()))
                .thenReturn(incSubSubCategoryResponse);

        IncCategory incCategory2 = new IncCategory();
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory2.setCreatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory2.setId(UUID.randomUUID());
        incCategory2.setIncCategoryDescription("Inc Category Description");
        incCategory2.setIncCategoryName("Inc Category Name");
        incCategory2.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory2.setUpdatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory2 = new IncSubCategory();
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory2.setCreatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory2.setId(UUID.randomUUID());
        incSubCategory2.setIncCategory(incCategory2);
        incSubCategory2.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory2.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory2.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory2.setUpdatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubCategory> ofResult1 = Optional.of(incSubCategory2);
        when(incSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);

        IncCategory incCategory3 = new IncCategory();
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory3.setCreatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory3.setId(UUID.randomUUID());
        incCategory3.setIncCategoryDescription("Inc Category Description");
        incCategory3.setIncCategoryName("Inc Category Name");
        incCategory3.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory3.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncCategory> ofResult2 = Optional.of(incCategory3);
        when(incCategoryRepository.findById((UUID) any())).thenReturn(ofResult2);
        Optional<ResponseBuilder<?>> actualUpdateSubSubCategoryResult = incSubSubCategoryServiceImpl
                .updateSubSubCategory(new IncSubSubCategoryUpdateRequest());
        assertTrue(actualUpdateSubSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateSubSubCategoryResult.get();
        assertSame(incSubSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(incSubSubCategoryRepository).existsById((UUID) any());
        verify(incSubSubCategoryRepository).save((IncSubSubCategory) any());
        verify(incSubSubCategoryRepository).findById((UUID) any());
        verify(incSubSubCategory).getCreatedOn();
        verify(incSubSubCategory).getId();
        verify(incSubSubCategory).setCreatedOn((Date) any());
        verify(incSubSubCategory).setId((UUID) any());
        verify(incSubSubCategory).setIncSubCategory((IncSubCategory) any());
        verify(incSubSubCategory).setIncSubSubCategoryDescription((String) any());
        verify(incSubSubCategory).setIncSubSubCategoryName((String) any());
        verify(incSubSubCategory).setUpdatedOn((Date) any());
        verify(incSubSubCategoryMapper).fromSubSubCategoryObjectToSubSubCategoryResponse((IncSubSubCategory) any());
        verify(incSubCategoryRepository).findById((UUID) any());
        verify(incCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#deleteSubSubCategory(UUID)}
     */
    @Test
    void testDeleteSubSubCategory() {
        when(incSubSubCategoryRepository.findById((UUID) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        doThrow(new ApplicationExceptions("An error occurred")).when(incSubSubCategoryRepository)
                .deleteById((UUID) any());
        when(incSubSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class,
                () -> incSubSubCategoryServiceImpl.deleteSubSubCategory(UUID.randomUUID()));
        verify(incSubSubCategoryRepository).existsById((UUID) any());
        verify(incSubSubCategoryRepository).deleteById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#deleteSubSubCategory(UUID)}
     */
    @Test
    void testDeleteSubSubCategory2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory.setId(UUID.randomUUID());
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubSubCategory> ofResult = Optional.of(incSubSubCategory);
        when(incSubSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        doNothing().when(incSubSubCategoryRepository).deleteById((UUID) any());
        when(incSubSubCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class,
                () -> incSubSubCategoryServiceImpl.deleteSubSubCategory(UUID.randomUUID()));
        verify(incSubSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#addSubSubCategory(IncSubSubCategoryRequest)}
     */
    @Test
    void testAddSubSubCategory() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory.setId(UUID.randomUUID());
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubSubCategoryRepository.saveAndFlush((IncSubSubCategory) any())).thenReturn(incSubSubCategory);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory1 = new IncSubCategory();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory1.setId(UUID.randomUUID());
        incSubCategory1.setIncCategory(incCategory1);
        incSubCategory1.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory1.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory1.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory1 = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory1.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory1.setId(UUID.randomUUID());
        incSubSubCategory1.setIncSubCategory(incSubCategory1);
        incSubSubCategory1.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory1.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubSubCategoryMapper.fromSubSubCategoryRequestToSubSubCategoryObject((IncSubSubCategoryRequest) any()))
                .thenReturn(incSubSubCategory1);
        IncSubSubCategoryResponse incSubSubCategoryResponse = new IncSubSubCategoryResponse();
        when(incSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse((IncSubSubCategory) any()))
                .thenReturn(incSubSubCategoryResponse);
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        Optional<ResponseBuilder<?>> actualAddSubSubCategoryResult = incSubSubCategoryServiceImpl
                .addSubSubCategory(new IncSubSubCategoryRequest());
        assertTrue(actualAddSubSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualAddSubSubCategoryResult.get();
        assertSame(incSubSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(201, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.CREATED, getResult.getHttpStatus());
        verify(incSubSubCategoryRepository).saveAndFlush((IncSubSubCategory) any());
        verify(incSubSubCategoryMapper).fromSubSubCategoryRequestToSubSubCategoryObject((IncSubSubCategoryRequest) any());
        verify(incSubSubCategoryMapper).fromSubSubCategoryObjectToSubSubCategoryResponse((IncSubSubCategory) any());
        verify(incSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryServiceImpl#addSubSubCategory(IncSubSubCategoryRequest)}
     */
    @Test
    void testAddSubSubCategory2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory.setId(UUID.randomUUID());
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubSubCategoryRepository.saveAndFlush((IncSubSubCategory) any())).thenReturn(incSubSubCategory);
        when(incSubSubCategoryMapper.fromSubSubCategoryRequestToSubSubCategoryObject((IncSubSubCategoryRequest) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(incSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse((IncSubSubCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class,
                () -> incSubSubCategoryServiceImpl.addSubSubCategory(new IncSubSubCategoryRequest()));
        verify(incSubSubCategoryMapper).fromSubSubCategoryRequestToSubSubCategoryObject((IncSubSubCategoryRequest) any());
        verify(incSubCategoryRepository).existsById((UUID) any());
    }
}

