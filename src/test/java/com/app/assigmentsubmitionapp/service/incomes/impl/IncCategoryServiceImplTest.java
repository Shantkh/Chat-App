package com.app.assigmentsubmitionapp.service.incomes.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.incomes.IncCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncCategoryResponse;
import com.app.assigmentsubmitionapp.repo.incomes.IncCategoryRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {IncCategoryServiceImpl.class})
@ExtendWith(SpringExtension.class)
class IncCategoryServiceImplTest {
    @MockBean
    private IncCategoryMapper incCategoryMapper;

    @MockBean
    private IncCategoryRepository incCategoryRepository;

    @Autowired
    private IncCategoryServiceImpl incCategoryServiceImpl;

    /**
     * Method under test: {@link IncCategoryServiceImpl#addCategory(IncCategoryRequest)}
     */
    @Test
    void testAddCategory() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(incCategoryMapper.fromIncCategoryRequestToIncCategoryObject((IncCategoryRequest) any()))
                .thenReturn(incCategory);
        IncCategoryResponse incCategoryResponse = new IncCategoryResponse();
        when(incCategoryMapper.fromIncCategoryObjectToCategoryResponse((IncCategory) any()))
                .thenReturn(incCategoryResponse);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(incCategoryRepository.saveAndFlush((IncCategory) any())).thenReturn(incCategory1);
        Optional<ResponseBuilder<?>> actualAddCategoryResult = incCategoryServiceImpl.addCategory(new IncCategoryRequest());
        assertTrue(actualAddCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualAddCategoryResult.get();
        assertSame(incCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(201, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.CREATED, getResult.getHttpStatus());
        verify(incCategoryMapper).fromIncCategoryRequestToIncCategoryObject((IncCategoryRequest) any());
        verify(incCategoryMapper).fromIncCategoryObjectToCategoryResponse((IncCategory) any());
        verify(incCategoryRepository).saveAndFlush((IncCategory) any());
    }

    /**
     * Method under test: {@link IncCategoryServiceImpl#addCategory(IncCategoryRequest)}
     */
    @Test
    void testAddCategory2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(incCategoryMapper.fromIncCategoryRequestToIncCategoryObject((IncCategoryRequest) any()))
                .thenReturn(incCategory);
        when(incCategoryMapper.fromIncCategoryObjectToCategoryResponse((IncCategory) any()))
                .thenReturn(new IncCategoryResponse());
        when(incCategoryRepository.saveAndFlush((IncCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        assertThrows(ApplicationExceptions.class, () -> incCategoryServiceImpl.addCategory(new IncCategoryRequest()));
        verify(incCategoryMapper).fromIncCategoryRequestToIncCategoryObject((IncCategoryRequest) any());
        verify(incCategoryRepository).saveAndFlush((IncCategory) any());
    }

    /**
     * Method under test: {@link IncCategoryServiceImpl#deleteCategory(UUID)}
     */
    @Test
    void testDeleteCategory() {
        when(incCategoryRepository.findById((UUID) any())).thenThrow(new ApplicationExceptions("An error occurred"));
        doThrow(new ApplicationExceptions("An error occurred")).when(incCategoryRepository).deleteById((UUID) any());
        when(incCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class, () -> incCategoryServiceImpl.deleteCategory(UUID.randomUUID()));
        verify(incCategoryRepository).existsById((UUID) any());
        verify(incCategoryRepository).deleteById((UUID) any());
    }

    /**
     * Method under test: {@link IncCategoryServiceImpl#deleteCategory(UUID)}
     */
    @Test
    void testDeleteCategory2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncCategory> ofResult = Optional.of(incCategory);
        when(incCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        doNothing().when(incCategoryRepository).deleteById((UUID) any());
        when(incCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class, () -> incCategoryServiceImpl.deleteCategory(UUID.randomUUID()));
        verify(incCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncCategoryServiceImpl#categories()}
     */
    @Test
    void testCategories() {
        when(incCategoryMapper.fromCategoryObjectListToListCategoryResponse((List<IncCategory>) any()))
                .thenReturn(Optional.of(new ArrayList<>()));
        when(incCategoryRepository.findAll()).thenThrow(new ApplicationExceptions("An error occurred"));
        assertThrows(ApplicationExceptions.class, () -> incCategoryServiceImpl.categories());
        verify(incCategoryRepository).findAll();
    }

    /**
     * Method under test: {@link IncCategoryServiceImpl#updateCategory(IncCategoryRequest)}
     */
    @Test
    void testUpdateCategory() {
        IncCategoryResponse incCategoryResponse = new IncCategoryResponse();
        when(incCategoryMapper.fromIncCategoryObjectToCategoryResponse((IncCategory) any()))
                .thenReturn(incCategoryResponse);

        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(incCategoryRepository.saveAndFlush((IncCategory) any())).thenReturn(incCategory1);
        when(incCategoryRepository.getReferenceById((UUID) any())).thenReturn(incCategory);
        when(incCategoryRepository.existsById((UUID) any())).thenReturn(true);
        Optional<ResponseBuilder<?>> actualUpdateCategoryResult = incCategoryServiceImpl
                .updateCategory(new IncCategoryRequest());
        assertTrue(actualUpdateCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateCategoryResult.get();
        assertSame(incCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(incCategoryMapper).fromIncCategoryObjectToCategoryResponse((IncCategory) any());
        verify(incCategoryRepository).existsById((UUID) any());
        verify(incCategoryRepository).getReferenceById((UUID) any());
        verify(incCategoryRepository).saveAndFlush((IncCategory) any());
    }

    /**
     * Method under test: {@link IncCategoryServiceImpl#updateCategory(IncCategoryRequest)}
     */
    @Test
    void testUpdateCategory2() {
        when(incCategoryMapper.fromIncCategoryObjectToCategoryResponse((IncCategory) any()))
                .thenReturn(new IncCategoryResponse());

        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        when(incCategoryRepository.saveAndFlush((IncCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(incCategoryRepository.getReferenceById((UUID) any())).thenReturn(incCategory);
        when(incCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class, () -> incCategoryServiceImpl.updateCategory(new IncCategoryRequest()));
        verify(incCategoryRepository).existsById((UUID) any());
        verify(incCategoryRepository).getReferenceById((UUID) any());
        verify(incCategoryRepository).saveAndFlush((IncCategory) any());
    }

    /**
     * Method under test: {@link IncCategoryServiceImpl#updateCategory(IncCategoryRequest)}
     */
    @Test
    void testUpdateCategory3() {
        IncCategoryResponse incCategoryResponse = new IncCategoryResponse();
        when(incCategoryMapper.fromIncCategoryObjectToCategoryResponse((IncCategory) any()))
                .thenReturn(incCategoryResponse);
        IncCategory incCategory = mock(IncCategory.class);
        doNothing().when(incCategory).setCreatedOn((Date) any());
        doNothing().when(incCategory).setId((UUID) any());
        doNothing().when(incCategory).setIncCategoryDescription((String) any());
        doNothing().when(incCategory).setIncCategoryName((String) any());
        doNothing().when(incCategory).setIncSubCategories((Set<IncSubCategory>) any());
        doNothing().when(incCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(incCategoryRepository.saveAndFlush((IncCategory) any())).thenReturn(incCategory1);
        when(incCategoryRepository.getReferenceById((UUID) any())).thenReturn(incCategory);
        when(incCategoryRepository.existsById((UUID) any())).thenReturn(true);
        Optional<ResponseBuilder<?>> actualUpdateCategoryResult = incCategoryServiceImpl
                .updateCategory(new IncCategoryRequest());
        assertTrue(actualUpdateCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateCategoryResult.get();
        assertSame(incCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(incCategoryMapper).fromIncCategoryObjectToCategoryResponse((IncCategory) any());
        verify(incCategoryRepository).existsById((UUID) any());
        verify(incCategoryRepository).getReferenceById((UUID) any());
        verify(incCategoryRepository).saveAndFlush((IncCategory) any());
        verify(incCategory).setCreatedOn((Date) any());
        verify(incCategory, atLeast(1)).setId((UUID) any());
        verify(incCategory, atLeast(1)).setIncCategoryDescription((String) any());
        verify(incCategory, atLeast(1)).setIncCategoryName((String) any());
        verify(incCategory).setIncSubCategories((Set<IncSubCategory>) any());
        verify(incCategory, atLeast(1)).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link IncCategoryServiceImpl#updateCategory(IncCategoryRequest)}
     */
    @Test
    void testUpdateCategory4() {
        when(incCategoryMapper.fromIncCategoryObjectToCategoryResponse((IncCategory) any()))
                .thenReturn(new IncCategoryResponse());
        IncCategory incCategory = mock(IncCategory.class);
        doNothing().when(incCategory).setCreatedOn((Date) any());
        doNothing().when(incCategory).setId((UUID) any());
        doNothing().when(incCategory).setIncCategoryDescription((String) any());
        doNothing().when(incCategory).setIncCategoryName((String) any());
        doNothing().when(incCategory).setIncSubCategories((Set<IncSubCategory>) any());
        doNothing().when(incCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(incCategoryRepository.saveAndFlush((IncCategory) any())).thenReturn(incCategory1);
        when(incCategoryRepository.getReferenceById((UUID) any())).thenReturn(incCategory);
        when(incCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class, () -> incCategoryServiceImpl.updateCategory(new IncCategoryRequest()));
        verify(incCategoryRepository).existsById((UUID) any());
        verify(incCategory).setCreatedOn((Date) any());
        verify(incCategory).setId((UUID) any());
        verify(incCategory).setIncCategoryDescription((String) any());
        verify(incCategory).setIncCategoryName((String) any());
        verify(incCategory).setIncSubCategories((Set<IncSubCategory>) any());
        verify(incCategory).setUpdatedOn((Date) any());
    }

}

