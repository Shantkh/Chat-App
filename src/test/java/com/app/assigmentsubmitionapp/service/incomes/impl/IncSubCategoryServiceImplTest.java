package com.app.assigmentsubmitionapp.service.incomes.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.incomes.IncSubCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.repo.incomes.IncCategoryRepository;
import com.app.assigmentsubmitionapp.repo.incomes.IncSubCategoryRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {IncSubCategoryServiceImpl.class})
@ExtendWith(SpringExtension.class)
class IncSubCategoryServiceImplTest {
    @MockBean
    private IncCategoryRepository incCategoryRepository;

    @MockBean
    private IncSubCategoryMapper incSubCategoryMapper;

    @MockBean
    private IncSubCategoryRepository incSubCategoryRepository;

    @Autowired
    private IncSubCategoryServiceImpl incSubCategoryServiceImpl;

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#subCategories(UUID)}
     */
    @Test
    void testSubCategories() {
        when(
                incSubCategoryMapper.fromOptionalSubCategoryObjectToOptionalSubCategoryResponse((List<IncSubCategory>) any()))
                .thenReturn(Optional.of(new ArrayList<>()));
        when(incSubCategoryRepository.findByIncCategoryId((UUID) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(incCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class, () -> incSubCategoryServiceImpl.subCategories(UUID.randomUUID()));
        verify(incSubCategoryRepository).findByIncCategoryId((UUID) any());
        verify(incCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#subCategories(UUID)}
     */
    @Test
    void testSubCategories2() {
        when(
                incSubCategoryMapper.fromOptionalSubCategoryObjectToOptionalSubCategoryResponse((List<IncSubCategory>) any()))
                .thenReturn(Optional.of(new ArrayList<>()));
        when(incSubCategoryRepository.findByIncCategoryId((UUID) any())).thenReturn(new ArrayList<>());
        when(incCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class, () -> incSubCategoryServiceImpl.subCategories(UUID.randomUUID()));
        verify(incCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#subCategoriesBySubId(UUID)}
     */
    @Test
    void testSubCategoriesBySubId() {
        when(incSubCategoryMapper
                .fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse((Optional<IncSubCategory>) any()))
                .thenReturn(Optional.of(new IncSubCategoryResponse()));
        when(incSubCategoryRepository.findById((UUID) any())).thenThrow(new ApplicationExceptions("An error occurred"));
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class,
                () -> incSubCategoryServiceImpl.subCategoriesBySubId(UUID.randomUUID()));
        verify(incSubCategoryRepository).existsById((UUID) any());
        verify(incSubCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#subCategoriesBySubId(UUID)}
     */
    @Test
    void testSubCategoriesBySubId2() {
        when(incSubCategoryMapper
                .fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse((Optional<IncSubCategory>) any()))
                .thenReturn(Optional.of(new IncSubCategoryResponse()));

        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubCategory> ofResult = Optional.of(incSubCategory);
        when(incSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class,
                () -> incSubCategoryServiceImpl.subCategoriesBySubId(UUID.randomUUID()));
        verify(incSubCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#addSubCategory(IncSubCategoryRequest)}
     */
    @Test
    void testAddSubCategory() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubCategoryMapper.fromSubCategoryRequestToSubCategoryObject((IncSubCategoryRequest) any()))
                .thenReturn(incSubCategory);
        IncSubCategoryResponse incSubCategoryResponse = new IncSubCategoryResponse();
        when(incSubCategoryMapper.fromSubCategoryObjectToSubCategoryResponse((IncSubCategory) any()))
                .thenReturn(incSubCategoryResponse);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory1 = new IncSubCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory1.setId(UUID.randomUUID());
        incSubCategory1.setIncCategory(incCategory1);
        incSubCategory1.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory1.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory1.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubCategoryRepository.saveAndFlush((IncSubCategory) any())).thenReturn(incSubCategory1);
        when(incCategoryRepository.existsById((UUID) any())).thenReturn(true);
        Optional<ResponseBuilder<?>> actualAddSubCategoryResult = incSubCategoryServiceImpl
                .addSubCategory(new IncSubCategoryRequest());
        assertTrue(actualAddSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualAddSubCategoryResult.get();
        assertSame(incSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(201, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.CREATED, getResult.getHttpStatus());
        verify(incSubCategoryMapper).fromSubCategoryRequestToSubCategoryObject((IncSubCategoryRequest) any());
        verify(incSubCategoryMapper).fromSubCategoryObjectToSubCategoryResponse((IncSubCategory) any());
        verify(incSubCategoryRepository).saveAndFlush((IncSubCategory) any());
        verify(incCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#addSubCategory(IncSubCategoryRequest)}
     */
    @Test
    void testAddSubCategory2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubCategoryMapper.fromSubCategoryRequestToSubCategoryObject((IncSubCategoryRequest) any()))
                .thenReturn(incSubCategory);
        when(incSubCategoryMapper.fromSubCategoryObjectToSubCategoryResponse((IncSubCategory) any()))
                .thenReturn(new IncSubCategoryResponse());
        when(incSubCategoryRepository.saveAndFlush((IncSubCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(incCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class,
                () -> incSubCategoryServiceImpl.addSubCategory(new IncSubCategoryRequest()));
        verify(incSubCategoryMapper).fromSubCategoryRequestToSubCategoryObject((IncSubCategoryRequest) any());
        verify(incSubCategoryRepository).saveAndFlush((IncSubCategory) any());
        verify(incCategoryRepository).existsById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#updateSubCategory(IncSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubCategory() {
        IncSubCategoryResponse incSubCategoryResponse = new IncSubCategoryResponse();
        when(incSubCategoryMapper.fromSubCategoryObjectToSubCategoryResponse((IncSubCategory) any()))
                .thenReturn(incSubCategoryResponse);

        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubCategory> ofResult = Optional.of(incSubCategory);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory1 = new IncSubCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory1.setId(UUID.randomUUID());
        incSubCategory1.setIncCategory(incCategory1);
        incSubCategory1.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory1.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory1.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubCategoryRepository.save((IncSubCategory) any())).thenReturn(incSubCategory1);
        when(incSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(true);

        IncCategory incCategory2 = new IncCategory();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory2.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory2.setId(UUID.randomUUID());
        incCategory2.setIncCategoryDescription("Inc Category Description");
        incCategory2.setIncCategoryName("Inc Category Name");
        incCategory2.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory2.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncCategory> ofResult1 = Optional.of(incCategory2);
        when(incCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);
        Optional<ResponseBuilder<?>> actualUpdateSubCategoryResult = incSubCategoryServiceImpl
                .updateSubCategory(new IncSubCategoryUpdateRequest());
        assertTrue(actualUpdateSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateSubCategoryResult.get();
        assertSame(incSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(incSubCategoryMapper).fromSubCategoryObjectToSubCategoryResponse((IncSubCategory) any());
        verify(incSubCategoryRepository).existsById((UUID) any());
        verify(incSubCategoryRepository).save((IncSubCategory) any());
        verify(incSubCategoryRepository).findById((UUID) any());
        verify(incCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#updateSubCategory(IncSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubCategory2() {
        when(incSubCategoryMapper.fromSubCategoryObjectToSubCategoryResponse((IncSubCategory) any()))
                .thenReturn(new IncSubCategoryResponse());

        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubCategory> ofResult = Optional.of(incSubCategory);
        when(incSubCategoryRepository.save((IncSubCategory) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(incSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(true);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncCategory> ofResult1 = Optional.of(incCategory1);
        when(incCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);
        assertThrows(ApplicationExceptions.class,
                () -> incSubCategoryServiceImpl.updateSubCategory(new IncSubCategoryUpdateRequest()));
        verify(incSubCategoryRepository).existsById((UUID) any());
        verify(incSubCategoryRepository).save((IncSubCategory) any());
        verify(incSubCategoryRepository).findById((UUID) any());
        verify(incCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#updateSubCategory(IncSubCategoryUpdateRequest)}
     */
    @Test
    void testUpdateSubCategory3() {
        IncSubCategoryResponse incSubCategoryResponse = new IncSubCategoryResponse();
        when(incSubCategoryMapper.fromSubCategoryObjectToSubCategoryResponse((IncSubCategory) any()))
                .thenReturn(incSubCategoryResponse);

        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        IncSubCategory incSubCategory = mock(IncSubCategory.class);
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        when(incSubCategory.getCreatedOn())
                .thenReturn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        doNothing().when(incSubCategory).setCreatedOn((Date) any());
        doNothing().when(incSubCategory).setId((UUID) any());
        doNothing().when(incSubCategory).setIncCategory((IncCategory) any());
        doNothing().when(incSubCategory).setIncSubCategoryDescription((String) any());
        doNothing().when(incSubCategory).setIncSubCategoryName((String) any());
        doNothing().when(incSubCategory).setIncSubSubCategories((Set<IncSubSubCategory>) any());
        doNothing().when(incSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubCategory> ofResult = Optional.of(incSubCategory);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory1 = new IncSubCategory();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory1.setId(UUID.randomUUID());
        incSubCategory1.setIncCategory(incCategory1);
        incSubCategory1.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory1.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory1.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubCategoryRepository.save((IncSubCategory) any())).thenReturn(incSubCategory1);
        when(incSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(true);

        IncCategory incCategory2 = new IncCategory();
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory2.setCreatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory2.setId(UUID.randomUUID());
        incCategory2.setIncCategoryDescription("Inc Category Description");
        incCategory2.setIncCategoryName("Inc Category Name");
        incCategory2.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory2.setUpdatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncCategory> ofResult1 = Optional.of(incCategory2);
        when(incCategoryRepository.findById((UUID) any())).thenReturn(ofResult1);
        Optional<ResponseBuilder<?>> actualUpdateSubCategoryResult = incSubCategoryServiceImpl
                .updateSubCategory(new IncSubCategoryUpdateRequest());
        assertTrue(actualUpdateSubCategoryResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateSubCategoryResult.get();
        assertSame(incSubCategoryResponse, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(incSubCategoryMapper).fromSubCategoryObjectToSubCategoryResponse((IncSubCategory) any());
        verify(incSubCategoryRepository).existsById((UUID) any());
        verify(incSubCategoryRepository).save((IncSubCategory) any());
        verify(incSubCategoryRepository).findById((UUID) any());
        verify(incSubCategory).getCreatedOn();
        verify(incSubCategory).setCreatedOn((Date) any());
        verify(incSubCategory).setId((UUID) any());
        verify(incSubCategory).setIncCategory((IncCategory) any());
        verify(incSubCategory).setIncSubCategoryDescription((String) any());
        verify(incSubCategory).setIncSubCategoryName((String) any());
        verify(incSubCategory).setIncSubSubCategories((Set<IncSubSubCategory>) any());
        verify(incSubCategory).setUpdatedOn((Date) any());
        verify(incCategoryRepository).findById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#deleteSubCategory(UUID)}
     */
    @Test
    void testDeleteSubCategory() {
        when(incSubCategoryRepository.findById((UUID) any())).thenThrow(new ApplicationExceptions("An error occurred"));
        doThrow(new ApplicationExceptions("An error occurred")).when(incSubCategoryRepository).deleteById((UUID) any());
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class, () -> incSubCategoryServiceImpl.deleteSubCategory(UUID.randomUUID()));
        verify(incSubCategoryRepository).existsById((UUID) any());
        verify(incSubCategoryRepository).deleteById((UUID) any());
    }

    /**
     * Method under test: {@link IncSubCategoryServiceImpl#deleteSubCategory(UUID)}
     */
    @Test
    void testDeleteSubCategory2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubCategory> ofResult = Optional.of(incSubCategory);
        when(incSubCategoryRepository.findById((UUID) any())).thenReturn(ofResult);
        doNothing().when(incSubCategoryRepository).deleteById((UUID) any());
        when(incSubCategoryRepository.existsById((UUID) any())).thenReturn(false);
        assertThrows(ApplicationExceptions.class, () -> incSubCategoryServiceImpl.deleteSubCategory(UUID.randomUUID()));
        verify(incSubCategoryRepository).existsById((UUID) any());
    }
}

