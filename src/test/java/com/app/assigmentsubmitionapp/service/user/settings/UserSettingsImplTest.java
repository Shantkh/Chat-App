package com.app.assigmentsubmitionapp.service.user.settings;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.enums.currency.Currencies;
import com.app.assigmentsubmitionapp.enums.localsettings.Lengths;
import com.app.assigmentsubmitionapp.enums.localsettings.Mass;
import com.app.assigmentsubmitionapp.enums.localsettings.Temperature;
import com.app.assigmentsubmitionapp.enums.localsettings.Volume;
import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.localsettings.LocalSettings;
import com.app.assigmentsubmitionapp.repo.localsettings.LocalSettingsRepository;
import com.app.assigmentsubmitionapp.user.dto.UserLocalSettings;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {UserSettingsImpl.class})
@ExtendWith(SpringExtension.class)
class UserSettingsImplTest {
    @MockBean
    private LocalSettingsRepository localSettingsRepository;

    @Autowired
    private UserSettingsImpl userSettingsImpl;

    /**
     * Method under test: {@link UserSettingsImpl#updateLocalSettings(LocalSettings)}
     */
    @Test
    void testUpdateLocalSettings() {
        LocalSettings localSettings = new LocalSettings();
        localSettings.setCountry("GB");
        localSettings.setCurrency(Currencies.USD);
        localSettings.setId(UUID.randomUUID());
        localSettings.setLengths(Lengths.METER);
        localSettings.setMass(Mass.KILOGRAM);
        localSettings.setSymbol("Symbol");
        localSettings.setTemperature(Temperature.CELSIUS);
        localSettings.setUserEmail("jane.doe@example.org");
        localSettings.setVolume(Volume.LITER);
        Optional<LocalSettings> ofResult = Optional.of(localSettings);

        LocalSettings localSettings1 = new LocalSettings();
        localSettings1.setCountry("GB");
        localSettings1.setCurrency(Currencies.USD);
        localSettings1.setId(UUID.randomUUID());
        localSettings1.setLengths(Lengths.METER);
        localSettings1.setMass(Mass.KILOGRAM);
        localSettings1.setSymbol("Symbol");
        localSettings1.setTemperature(Temperature.CELSIUS);
        localSettings1.setUserEmail("jane.doe@example.org");
        localSettings1.setVolume(Volume.LITER);
        when(localSettingsRepository.saveAndFlush((LocalSettings) any())).thenReturn(localSettings1);
        when(localSettingsRepository.findAllByUserEmail((String) any())).thenReturn(ofResult);

        LocalSettings localSettings2 = new LocalSettings();
        localSettings2.setCountry("GB");
        localSettings2.setCurrency(Currencies.USD);
        localSettings2.setId(UUID.randomUUID());
        localSettings2.setLengths(Lengths.METER);
        localSettings2.setMass(Mass.KILOGRAM);
        localSettings2.setSymbol("Symbol");
        localSettings2.setTemperature(Temperature.CELSIUS);
        localSettings2.setUserEmail("jane.doe@example.org");
        localSettings2.setVolume(Volume.LITER);
        Optional<ResponseBuilder<?>> actualUpdateLocalSettingsResult = userSettingsImpl.updateLocalSettings(localSettings2);
        assertTrue(actualUpdateLocalSettingsResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateLocalSettingsResult.get();
        assertSame(localSettings1, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(localSettingsRepository).saveAndFlush((LocalSettings) any());
        verify(localSettingsRepository).findAllByUserEmail((String) any());
    }

    /**
     * Method under test: {@link UserSettingsImpl#updateLocalSettings(LocalSettings)}
     */
    @Test
    void testUpdateLocalSettings2() {
        LocalSettings localSettings = new LocalSettings();
        localSettings.setCountry("GB");
        localSettings.setCurrency(Currencies.USD);
        localSettings.setId(UUID.randomUUID());
        localSettings.setLengths(Lengths.METER);
        localSettings.setMass(Mass.KILOGRAM);
        localSettings.setSymbol("Symbol");
        localSettings.setTemperature(Temperature.CELSIUS);
        localSettings.setUserEmail("jane.doe@example.org");
        localSettings.setVolume(Volume.LITER);
        Optional<LocalSettings> ofResult = Optional.of(localSettings);
        when(localSettingsRepository.saveAndFlush((LocalSettings) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(localSettingsRepository.findAllByUserEmail((String) any())).thenReturn(ofResult);

        LocalSettings localSettings1 = new LocalSettings();
        localSettings1.setCountry("GB");
        localSettings1.setCurrency(Currencies.USD);
        localSettings1.setId(UUID.randomUUID());
        localSettings1.setLengths(Lengths.METER);
        localSettings1.setMass(Mass.KILOGRAM);
        localSettings1.setSymbol("Symbol");
        localSettings1.setTemperature(Temperature.CELSIUS);
        localSettings1.setUserEmail("jane.doe@example.org");
        localSettings1.setVolume(Volume.LITER);
        assertThrows(ApplicationExceptions.class, () -> userSettingsImpl.updateLocalSettings(localSettings1));
        verify(localSettingsRepository).saveAndFlush((LocalSettings) any());
        verify(localSettingsRepository).findAllByUserEmail((String) any());
    }

    /**
     * Method under test: {@link UserSettingsImpl#updateLocalSettings(LocalSettings)}
     */
    @Test
    void testUpdateLocalSettings3() {
        LocalSettings localSettings = mock(LocalSettings.class);
        when(localSettings.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(localSettings).setCountry((String) any());
        doNothing().when(localSettings).setCurrency((Currencies) any());
        doNothing().when(localSettings).setId((UUID) any());
        doNothing().when(localSettings).setLengths((Lengths) any());
        doNothing().when(localSettings).setMass((Mass) any());
        doNothing().when(localSettings).setSymbol((String) any());
        doNothing().when(localSettings).setTemperature((Temperature) any());
        doNothing().when(localSettings).setUserEmail((String) any());
        doNothing().when(localSettings).setVolume((Volume) any());
        localSettings.setCountry("GB");
        localSettings.setCurrency(Currencies.USD);
        localSettings.setId(UUID.randomUUID());
        localSettings.setLengths(Lengths.METER);
        localSettings.setMass(Mass.KILOGRAM);
        localSettings.setSymbol("Symbol");
        localSettings.setTemperature(Temperature.CELSIUS);
        localSettings.setUserEmail("jane.doe@example.org");
        localSettings.setVolume(Volume.LITER);
        Optional<LocalSettings> ofResult = Optional.of(localSettings);

        LocalSettings localSettings1 = new LocalSettings();
        localSettings1.setCountry("GB");
        localSettings1.setCurrency(Currencies.USD);
        localSettings1.setId(UUID.randomUUID());
        localSettings1.setLengths(Lengths.METER);
        localSettings1.setMass(Mass.KILOGRAM);
        localSettings1.setSymbol("Symbol");
        localSettings1.setTemperature(Temperature.CELSIUS);
        localSettings1.setUserEmail("jane.doe@example.org");
        localSettings1.setVolume(Volume.LITER);
        when(localSettingsRepository.saveAndFlush((LocalSettings) any())).thenReturn(localSettings1);
        when(localSettingsRepository.findAllByUserEmail((String) any())).thenReturn(ofResult);

        LocalSettings localSettings2 = new LocalSettings();
        localSettings2.setCountry("GB");
        localSettings2.setCurrency(Currencies.USD);
        localSettings2.setId(UUID.randomUUID());
        localSettings2.setLengths(Lengths.METER);
        localSettings2.setMass(Mass.KILOGRAM);
        localSettings2.setSymbol("Symbol");
        localSettings2.setTemperature(Temperature.CELSIUS);
        localSettings2.setUserEmail("jane.doe@example.org");
        localSettings2.setVolume(Volume.LITER);
        Optional<ResponseBuilder<?>> actualUpdateLocalSettingsResult = userSettingsImpl
                .updateLocalSettings(localSettings2);
        assertTrue(actualUpdateLocalSettingsResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateLocalSettingsResult.get();
        assertSame(localSettings1, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(localSettingsRepository).saveAndFlush((LocalSettings) any());
        verify(localSettingsRepository).findAllByUserEmail((String) any());
        verify(localSettings).getId();
        verify(localSettings).setCountry((String) any());
        verify(localSettings).setCurrency((Currencies) any());
        verify(localSettings).setId((UUID) any());
        verify(localSettings).setLengths((Lengths) any());
        verify(localSettings).setMass((Mass) any());
        verify(localSettings).setSymbol((String) any());
        verify(localSettings).setTemperature((Temperature) any());
        verify(localSettings).setUserEmail((String) any());
        verify(localSettings).setVolume((Volume) any());
    }


    /**
     * Method under test: {@link UserSettingsImpl#updateLocalSettings(LocalSettings)}
     */
    @Test
    void testUpdateLocalSettings5() {
        LocalSettings localSettings = mock(LocalSettings.class);
        when(localSettings.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(localSettings).setCountry((String) any());
        doNothing().when(localSettings).setCurrency((Currencies) any());
        doNothing().when(localSettings).setId((UUID) any());
        doNothing().when(localSettings).setLengths((Lengths) any());
        doNothing().when(localSettings).setMass((Mass) any());
        doNothing().when(localSettings).setSymbol((String) any());
        doNothing().when(localSettings).setTemperature((Temperature) any());
        doNothing().when(localSettings).setUserEmail((String) any());
        doNothing().when(localSettings).setVolume((Volume) any());
        localSettings.setCountry("GB");
        localSettings.setCurrency(Currencies.USD);
        localSettings.setId(UUID.randomUUID());
        localSettings.setLengths(Lengths.METER);
        localSettings.setMass(Mass.KILOGRAM);
        localSettings.setSymbol("Symbol");
        localSettings.setTemperature(Temperature.CELSIUS);
        localSettings.setUserEmail("jane.doe@example.org");
        localSettings.setVolume(Volume.LITER);
        Optional<LocalSettings> ofResult = Optional.of(localSettings);

        LocalSettings localSettings1 = new LocalSettings();
        localSettings1.setCountry("GB");
        localSettings1.setCurrency(Currencies.USD);
        localSettings1.setId(UUID.randomUUID());
        localSettings1.setLengths(Lengths.METER);
        localSettings1.setMass(Mass.KILOGRAM);
        localSettings1.setSymbol("Symbol");
        localSettings1.setTemperature(Temperature.CELSIUS);
        localSettings1.setUserEmail("jane.doe@example.org");
        localSettings1.setVolume(Volume.LITER);
        when(localSettingsRepository.saveAndFlush((LocalSettings) any())).thenReturn(localSettings1);
        when(localSettingsRepository.findAllByUserEmail((String) any())).thenReturn(ofResult);
        LocalSettings localSettings2 = mock(LocalSettings.class);
        when(localSettings2.getCurrency()).thenReturn(Currencies.USD);
        when(localSettings2.getLengths()).thenReturn(Lengths.METER);
        when(localSettings2.getMass()).thenReturn(Mass.KILOGRAM);
        when(localSettings2.getTemperature()).thenReturn(Temperature.CELSIUS);
        when(localSettings2.getVolume()).thenReturn(Volume.LITER);
        when(localSettings2.getUserEmail()).thenReturn("jane.doe@example.org");
        doNothing().when(localSettings2).setCountry((String) any());
        doNothing().when(localSettings2).setCurrency((Currencies) any());
        doNothing().when(localSettings2).setId((UUID) any());
        doNothing().when(localSettings2).setLengths((Lengths) any());
        doNothing().when(localSettings2).setMass((Mass) any());
        doNothing().when(localSettings2).setSymbol((String) any());
        doNothing().when(localSettings2).setTemperature((Temperature) any());
        doNothing().when(localSettings2).setUserEmail((String) any());
        doNothing().when(localSettings2).setVolume((Volume) any());
        localSettings2.setCountry("GB");
        localSettings2.setCurrency(Currencies.USD);
        localSettings2.setId(UUID.randomUUID());
        localSettings2.setLengths(Lengths.METER);
        localSettings2.setMass(Mass.KILOGRAM);
        localSettings2.setSymbol("Symbol");
        localSettings2.setTemperature(Temperature.CELSIUS);
        localSettings2.setUserEmail("jane.doe@example.org");
        localSettings2.setVolume(Volume.LITER);
        Optional<ResponseBuilder<?>> actualUpdateLocalSettingsResult = userSettingsImpl
                .updateLocalSettings(localSettings2);
        assertTrue(actualUpdateLocalSettingsResult.isPresent());
        ResponseBuilder<?> getResult = actualUpdateLocalSettingsResult.get();
        assertSame(localSettings1, getResult.getData());
        assertTrue(getResult.getIsSuccess());
        assertEquals(200, getResult.getHttpStatusCode().intValue());
        assertEquals(HttpStatus.OK, getResult.getHttpStatus());
        verify(localSettingsRepository).saveAndFlush((LocalSettings) any());
        verify(localSettingsRepository).findAllByUserEmail((String) any());
        verify(localSettings).getId();
        verify(localSettings).setCountry((String) any());
        verify(localSettings).setCurrency((Currencies) any());
        verify(localSettings).setId((UUID) any());
        verify(localSettings).setLengths((Lengths) any());
        verify(localSettings).setMass((Mass) any());
        verify(localSettings).setSymbol((String) any());
        verify(localSettings).setTemperature((Temperature) any());
        verify(localSettings).setUserEmail((String) any());
        verify(localSettings).setVolume((Volume) any());
        verify(localSettings2, atLeast(1)).getCurrency();
        verify(localSettings2).getLengths();
        verify(localSettings2).getMass();
        verify(localSettings2).getTemperature();
        verify(localSettings2).getVolume();
        verify(localSettings2, atLeast(1)).getUserEmail();
        verify(localSettings2).setCountry((String) any());
        verify(localSettings2).setCurrency((Currencies) any());
        verify(localSettings2).setId((UUID) any());
        verify(localSettings2).setLengths((Lengths) any());
        verify(localSettings2).setMass((Mass) any());
        verify(localSettings2).setSymbol((String) any());
        verify(localSettings2).setTemperature((Temperature) any());
        verify(localSettings2).setUserEmail((String) any());
        verify(localSettings2).setVolume((Volume) any());
    }

    /**
     * Method under test: {@link UserSettingsImpl#userLocalSettings(UserLocalSettings)}
     */
    @Test
    void testUserLocalSettings3() {
        when(localSettingsRepository.findAllByUserEmail((String) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        assertThrows(ApplicationExceptions.class,
                () -> userSettingsImpl.userLocalSettings(new UserLocalSettings("jane.doe@example.org")));
        verify(localSettingsRepository).findAllByUserEmail((String) any());
    }
}

