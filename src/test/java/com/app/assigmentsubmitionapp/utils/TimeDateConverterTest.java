package com.app.assigmentsubmitionapp.utils;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;

import java.sql.Timestamp;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.postgresql.util.PGTimestamp;

class TimeDateConverterTest {
    /**
     * Method under test: {@link TimeDateConverter#convertToDatabaseColumn(Timestamp)}
     */
    @Test
    void testConvertToDatabaseColumn() {
        assertThrows(ApplicationExceptions.class,
                () -> (new TimeDateConverter()).convertToDatabaseColumn(mock(Timestamp.class)));
    }


    /**
     * Method under test: {@link TimeDateConverter#convertToEntityAttribute(String)}
     */
    @Test
    void testConvertToEntityAttribute() {
        assertThrows(ApplicationExceptions.class, () -> (new TimeDateConverter()).convertToEntityAttribute("Db Data"));
        assertThrows(ApplicationExceptions.class, () -> (new TimeDateConverter()).convertToEntityAttribute("."));
        assertThrows(ApplicationExceptions.class, () -> (new TimeDateConverter()).convertToEntityAttribute(","));
    }
}

