package com.app.assigmentsubmitionapp.utils;

import static com.app.assigmentsubmitionapp.enums.ExceptionEnums.EMAIL_ALREADY_EXSIST;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.app.assigmentsubmitionapp.enums.MessagesConst;
import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.modules.EmailDetails;
import com.app.assigmentsubmitionapp.modules.ForgotPasswordRequest;
import com.app.assigmentsubmitionapp.repo.JwtUserRepository;
import com.app.assigmentsubmitionapp.security.application.RefreshToken;
import com.app.assigmentsubmitionapp.security.domain.LoginCredentials;
import com.app.assigmentsubmitionapp.user.Gender;
import com.app.assigmentsubmitionapp.user.JwtUser;
import com.app.assigmentsubmitionapp.user.Role;
import com.app.assigmentsubmitionapp.user.UsersDetails;
import com.app.assigmentsubmitionapp.user.dto.UserRegistrationRequest;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {UserPreconditions.class})
@ExtendWith(SpringExtension.class)
class UserPreconditionsTest {
    @MockBean
    private JwtUserRepository jwtUserRepository;

    @Autowired
    private UserPreconditions userPreconditions;


    /**
     * Method under test: {@link UserPreconditions#isEmailVerified(Optional)}
     */
    @Test
    void testIsEmailVerified2() {
        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(new RefreshToken());
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(new UsersDetails());

        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(jwtUser);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(new RefreshToken());
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(new UsersDetails());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(jwtUser1);
        usersDetails.setUserVerified(true);
        JwtUser jwtUser2 = mock(JwtUser.class);
        when(jwtUser2.isUserEmailVerified()).thenReturn(true);
        doNothing().when(jwtUser2).setCreatedOn((Date) any());
        doNothing().when(jwtUser2).setEmail((String) any());
        doNothing().when(jwtUser2).setEnabled(anyBoolean());
        doNothing().when(jwtUser2).setFirstname((String) any());
        doNothing().when(jwtUser2).setId((UUID) any());
        doNothing().when(jwtUser2).setLastname((String) any());
        doNothing().when(jwtUser2).setPassword((String) any());
        doNothing().when(jwtUser2).setRefreshToken((RefreshToken) any());
        doNothing().when(jwtUser2).setRole((Set<Role>) any());
        doNothing().when(jwtUser2).setUpdatedOn((Date) any());
        doNothing().when(jwtUser2).setUserEmailVerified(anyBoolean());
        doNothing().when(jwtUser2).setUsername((String) any());
        doNothing().when(jwtUser2).setUsersDetails((UsersDetails) any());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails);
        userPreconditions.isEmailVerified(Optional.of(jwtUser2));
        verify(jwtUser2).isUserEmailVerified();
        verify(jwtUser2).setCreatedOn((Date) any());
        verify(jwtUser2).setEmail((String) any());
        verify(jwtUser2).setEnabled(anyBoolean());
        verify(jwtUser2).setFirstname((String) any());
        verify(jwtUser2).setId((UUID) any());
        verify(jwtUser2).setLastname((String) any());
        verify(jwtUser2).setPassword((String) any());
        verify(jwtUser2).setRefreshToken((RefreshToken) any());
        verify(jwtUser2).setRole(any());
        verify(jwtUser2).setUpdatedOn((Date) any());
        verify(jwtUser2).setUserEmailVerified(anyBoolean());
        verify(jwtUser2).setUsername((String) any());
        verify(jwtUser2).setUsersDetails((UsersDetails) any());
    }

    /**
     * Method under test: {@link UserPreconditions#isEmailVerified(Optional)}
     */
    @Test
    void testIsEmailVerified3() {
        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(new RefreshToken());
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(new UsersDetails());

        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(jwtUser);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(new RefreshToken());
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(new UsersDetails());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(jwtUser1);
        usersDetails.setUserVerified(true);
        JwtUser jwtUser2 = mock(JwtUser.class);
        when(jwtUser2.isUserEmailVerified()).thenReturn(false);
        doNothing().when(jwtUser2).setCreatedOn((Date) any());
        doNothing().when(jwtUser2).setEmail((String) any());
        doNothing().when(jwtUser2).setEnabled(anyBoolean());
        doNothing().when(jwtUser2).setFirstname((String) any());
        doNothing().when(jwtUser2).setId((UUID) any());
        doNothing().when(jwtUser2).setLastname((String) any());
        doNothing().when(jwtUser2).setPassword((String) any());
        doNothing().when(jwtUser2).setRefreshToken((RefreshToken) any());
        doNothing().when(jwtUser2).setRole((Set<Role>) any());
        doNothing().when(jwtUser2).setUpdatedOn((Date) any());
        doNothing().when(jwtUser2).setUserEmailVerified(anyBoolean());
        doNothing().when(jwtUser2).setUsername((String) any());
        doNothing().when(jwtUser2).setUsersDetails((UsersDetails) any());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails);
        assertThrows(ApplicationExceptions.class, () -> userPreconditions.isEmailVerified(Optional.of(jwtUser2)));
        verify(jwtUser2).isUserEmailVerified();
        verify(jwtUser2).setCreatedOn((Date) any());
        verify(jwtUser2).setEmail((String) any());
        verify(jwtUser2).setEnabled(anyBoolean());
        verify(jwtUser2).setFirstname((String) any());
        verify(jwtUser2).setId((UUID) any());
        verify(jwtUser2).setLastname((String) any());
        verify(jwtUser2).setPassword((String) any());
        verify(jwtUser2).setRefreshToken((RefreshToken) any());
        verify(jwtUser2).setRole((Set<Role>) any());
        verify(jwtUser2).setUpdatedOn((Date) any());
        verify(jwtUser2).setUserEmailVerified(anyBoolean());
        verify(jwtUser2).setUsername((String) any());
        verify(jwtUser2).setUsersDetails((UsersDetails) any());
    }

    /**
     * Method under test: {@link UserPreconditions#isPoneNumberVerified(Optional)}
     */
    @Test
    void testIsPoneNumberVerified2() {
        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(new RefreshToken());
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(new UsersDetails());

        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(jwtUser);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(new RefreshToken());
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(new UsersDetails());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(jwtUser1);
        usersDetails.setUserVerified(true);

        RefreshToken refreshToken1 = new RefreshToken();
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setCreatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setExpiryDate(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setId(UUID.randomUUID());
        refreshToken1.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setUpdatedOn(Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken1.setUser(new JwtUser());

        UsersDetails usersDetails1 = new UsersDetails();
        usersDetails1.setAddresses(new HashSet<>());
        usersDetails1.setAge("Age");
        usersDetails1.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setCreatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setGender(Gender.MALE);
        usersDetails1.setId(UUID.randomUUID());
        usersDetails1.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setUpdatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setUser(new JwtUser());
        usersDetails1.setUserVerified(true);

        JwtUser jwtUser2 = new JwtUser();
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken1);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails1);

        RefreshToken refreshToken2 = new RefreshToken();
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setCreatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setExpiryDate(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken2.setId(UUID.randomUUID());
        refreshToken2.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken2.setUser(jwtUser2);

        RefreshToken refreshToken3 = new RefreshToken();
        LocalDateTime atStartOfDayResult19 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken3.setCreatedOn(Date.from(atStartOfDayResult19.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult20 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken3.setExpiryDate(atStartOfDayResult20.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken3.setId(UUID.randomUUID());
        refreshToken3.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult21 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken3.setUpdatedOn(Date.from(atStartOfDayResult21.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken3.setUser(new JwtUser());

        UsersDetails usersDetails2 = new UsersDetails();
        usersDetails2.setAddresses(new HashSet<>());
        usersDetails2.setAge("Age");
        usersDetails2.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult22 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setCreatedOn(Date.from(atStartOfDayResult22.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setGender(Gender.MALE);
        usersDetails2.setId(UUID.randomUUID());
        usersDetails2.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult23 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setUpdatedOn(Date.from(atStartOfDayResult23.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setUser(new JwtUser());
        usersDetails2.setUserVerified(true);

        JwtUser jwtUser3 = new JwtUser();
        LocalDateTime atStartOfDayResult24 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser3.setCreatedOn(Date.from(atStartOfDayResult24.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser3.setEmail("jane.doe@example.org");
        jwtUser3.setEnabled(true);
        jwtUser3.setFirstname("Jane");
        jwtUser3.setId(UUID.randomUUID());
        jwtUser3.setLastname("Doe");
        jwtUser3.setPassword("iloveyou");
        jwtUser3.setRefreshToken(refreshToken3);
        jwtUser3.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult25 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser3.setUpdatedOn(Date.from(atStartOfDayResult25.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser3.setUserEmailVerified(true);
        jwtUser3.setUsername("janedoe");
        jwtUser3.setUsersDetails(usersDetails2);

        UsersDetails usersDetails3 = new UsersDetails();
        usersDetails3.setAddresses(new HashSet<>());
        usersDetails3.setAge("Age");
        usersDetails3.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult26 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails3.setCreatedOn(Date.from(atStartOfDayResult26.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails3.setGender(Gender.MALE);
        usersDetails3.setId(UUID.randomUUID());
        usersDetails3.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult27 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails3.setUpdatedOn(Date.from(atStartOfDayResult27.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails3.setUser(jwtUser3);
        usersDetails3.setUserVerified(true);

        JwtUser jwtUser4 = new JwtUser();
        LocalDateTime atStartOfDayResult28 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser4.setCreatedOn(Date.from(atStartOfDayResult28.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser4.setEmail("jane.doe@example.org");
        jwtUser4.setEnabled(true);
        jwtUser4.setFirstname("Jane");
        jwtUser4.setId(UUID.randomUUID());
        jwtUser4.setLastname("Doe");
        jwtUser4.setPassword("iloveyou");
        jwtUser4.setRefreshToken(refreshToken2);
        jwtUser4.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult29 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser4.setUpdatedOn(Date.from(atStartOfDayResult29.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser4.setUserEmailVerified(true);
        jwtUser4.setUsername("janedoe");
        jwtUser4.setUsersDetails(usersDetails3);

        UsersDetails usersDetails4 = new UsersDetails();
        usersDetails4.setAddresses(new HashSet<>());
        usersDetails4.setAge("Age");
        usersDetails4.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult30 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails4.setCreatedOn(Date.from(atStartOfDayResult30.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails4.setGender(Gender.MALE);
        usersDetails4.setId(UUID.randomUUID());
        usersDetails4.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult31 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails4.setUpdatedOn(Date.from(atStartOfDayResult31.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails4.setUser(jwtUser4);
        usersDetails4.setUserVerified(true);
        JwtUser jwtUser5 = mock(JwtUser.class);
        when(jwtUser5.getUsersDetails()).thenReturn(usersDetails4);
        doNothing().when(jwtUser5).setCreatedOn((Date) any());
        doNothing().when(jwtUser5).setEmail((String) any());
        doNothing().when(jwtUser5).setEnabled(anyBoolean());
        doNothing().when(jwtUser5).setFirstname((String) any());
        doNothing().when(jwtUser5).setId((UUID) any());
        doNothing().when(jwtUser5).setLastname((String) any());
        doNothing().when(jwtUser5).setPassword((String) any());
        doNothing().when(jwtUser5).setRefreshToken((RefreshToken) any());
        doNothing().when(jwtUser5).setRole((Set<Role>) any());
        doNothing().when(jwtUser5).setUpdatedOn((Date) any());
        doNothing().when(jwtUser5).setUserEmailVerified(anyBoolean());
        doNothing().when(jwtUser5).setUsername((String) any());
        doNothing().when(jwtUser5).setUsersDetails((UsersDetails) any());
        LocalDateTime atStartOfDayResult32 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser5.setCreatedOn(Date.from(atStartOfDayResult32.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser5.setEmail("jane.doe@example.org");
        jwtUser5.setEnabled(true);
        jwtUser5.setFirstname("Jane");
        jwtUser5.setId(UUID.randomUUID());
        jwtUser5.setLastname("Doe");
        jwtUser5.setPassword("iloveyou");
        jwtUser5.setRefreshToken(refreshToken);
        jwtUser5.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult33 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser5.setUpdatedOn(Date.from(atStartOfDayResult33.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser5.setUserEmailVerified(true);
        jwtUser5.setUsername("janedoe");
        jwtUser5.setUsersDetails(usersDetails);
        userPreconditions.isPoneNumberVerified(Optional.of(jwtUser5));
        verify(jwtUser5).getUsersDetails();
        verify(jwtUser5).setCreatedOn((Date) any());
        verify(jwtUser5).setEmail((String) any());
        verify(jwtUser5).setEnabled(anyBoolean());
        verify(jwtUser5).setFirstname((String) any());
        verify(jwtUser5).setId((UUID) any());
        verify(jwtUser5).setLastname((String) any());
        verify(jwtUser5).setPassword((String) any());
        verify(jwtUser5).setRefreshToken((RefreshToken) any());
        verify(jwtUser5).setRole((Set<Role>) any());
        verify(jwtUser5).setUpdatedOn((Date) any());
        verify(jwtUser5).setUserEmailVerified(anyBoolean());
        verify(jwtUser5).setUsername((String) any());
        verify(jwtUser5).setUsersDetails((UsersDetails) any());
    }

    /**
     * Method under test: {@link UserPreconditions#checkUserLoginDetails(LoginCredentials)}
     */
    @Test
    void testCheckUserLoginDetails() {
        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(new JwtUser());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(new JwtUser());
        usersDetails.setUserVerified(true);

        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(refreshToken);
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(usersDetails);

        RefreshToken refreshToken1 = new RefreshToken();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setExpiryDate(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setId(UUID.randomUUID());
        refreshToken1.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken1.setUser(jwtUser);

        RefreshToken refreshToken2 = new RefreshToken();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setExpiryDate(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken2.setId(UUID.randomUUID());
        refreshToken2.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setUpdatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken2.setUser(new JwtUser());

        UsersDetails usersDetails1 = new UsersDetails();
        usersDetails1.setAddresses(new HashSet<>());
        usersDetails1.setAge("Age");
        usersDetails1.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setCreatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setGender(Gender.MALE);
        usersDetails1.setId(UUID.randomUUID());
        usersDetails1.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setUpdatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setUser(new JwtUser());
        usersDetails1.setUserVerified(true);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(refreshToken2);
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(usersDetails1);

        UsersDetails usersDetails2 = new UsersDetails();
        usersDetails2.setAddresses(new HashSet<>());
        usersDetails2.setAge("Age");
        usersDetails2.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setCreatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setGender(Gender.MALE);
        usersDetails2.setId(UUID.randomUUID());
        usersDetails2.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setUser(jwtUser1);
        usersDetails2.setUserVerified(true);

        JwtUser jwtUser2 = new JwtUser();
        LocalDateTime atStartOfDayResult19 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult19.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken1);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult20 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult20.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails2);
        Optional<JwtUser> ofResult = Optional.of(jwtUser2);

        RefreshToken refreshToken3 = new RefreshToken();
        LocalDateTime atStartOfDayResult21 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken3.setCreatedOn(Date.from(atStartOfDayResult21.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult22 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken3.setExpiryDate(atStartOfDayResult22.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken3.setId(UUID.randomUUID());
        refreshToken3.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult23 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken3.setUpdatedOn(Date.from(atStartOfDayResult23.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken3.setUser(new JwtUser());

        UsersDetails usersDetails3 = new UsersDetails();
        usersDetails3.setAddresses(new HashSet<>());
        usersDetails3.setAge("Age");
        usersDetails3.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult24 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails3.setCreatedOn(Date.from(atStartOfDayResult24.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails3.setGender(Gender.MALE);
        usersDetails3.setId(UUID.randomUUID());
        usersDetails3.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult25 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails3.setUpdatedOn(Date.from(atStartOfDayResult25.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails3.setUser(new JwtUser());
        usersDetails3.setUserVerified(true);

        JwtUser jwtUser3 = new JwtUser();
        LocalDateTime atStartOfDayResult26 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser3.setCreatedOn(Date.from(atStartOfDayResult26.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser3.setEmail("jane.doe@example.org");
        jwtUser3.setEnabled(true);
        jwtUser3.setFirstname("Jane");
        jwtUser3.setId(UUID.randomUUID());
        jwtUser3.setLastname("Doe");
        jwtUser3.setPassword("iloveyou");
        jwtUser3.setRefreshToken(refreshToken3);
        jwtUser3.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult27 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser3.setUpdatedOn(Date.from(atStartOfDayResult27.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser3.setUserEmailVerified(true);
        jwtUser3.setUsername("janedoe");
        jwtUser3.setUsersDetails(usersDetails3);

        RefreshToken refreshToken4 = new RefreshToken();
        LocalDateTime atStartOfDayResult28 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken4.setCreatedOn(Date.from(atStartOfDayResult28.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult29 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken4.setExpiryDate(atStartOfDayResult29.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken4.setId(UUID.randomUUID());
        refreshToken4.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult30 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken4.setUpdatedOn(Date.from(atStartOfDayResult30.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken4.setUser(jwtUser3);

        RefreshToken refreshToken5 = new RefreshToken();
        LocalDateTime atStartOfDayResult31 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken5.setCreatedOn(Date.from(atStartOfDayResult31.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult32 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken5.setExpiryDate(atStartOfDayResult32.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken5.setId(UUID.randomUUID());
        refreshToken5.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult33 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken5.setUpdatedOn(Date.from(atStartOfDayResult33.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken5.setUser(new JwtUser());

        UsersDetails usersDetails4 = new UsersDetails();
        usersDetails4.setAddresses(new HashSet<>());
        usersDetails4.setAge("Age");
        usersDetails4.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult34 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails4.setCreatedOn(Date.from(atStartOfDayResult34.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails4.setGender(Gender.MALE);
        usersDetails4.setId(UUID.randomUUID());
        usersDetails4.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult35 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails4.setUpdatedOn(Date.from(atStartOfDayResult35.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails4.setUser(new JwtUser());
        usersDetails4.setUserVerified(true);

        JwtUser jwtUser4 = new JwtUser();
        LocalDateTime atStartOfDayResult36 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser4.setCreatedOn(Date.from(atStartOfDayResult36.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser4.setEmail("jane.doe@example.org");
        jwtUser4.setEnabled(true);
        jwtUser4.setFirstname("Jane");
        jwtUser4.setId(UUID.randomUUID());
        jwtUser4.setLastname("Doe");
        jwtUser4.setPassword("iloveyou");
        jwtUser4.setRefreshToken(refreshToken5);
        jwtUser4.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult37 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser4.setUpdatedOn(Date.from(atStartOfDayResult37.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser4.setUserEmailVerified(true);
        jwtUser4.setUsername("janedoe");
        jwtUser4.setUsersDetails(usersDetails4);

        UsersDetails usersDetails5 = new UsersDetails();
        usersDetails5.setAddresses(new HashSet<>());
        usersDetails5.setAge("Age");
        usersDetails5.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult38 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails5.setCreatedOn(Date.from(atStartOfDayResult38.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails5.setGender(Gender.MALE);
        usersDetails5.setId(UUID.randomUUID());
        usersDetails5.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult39 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails5.setUpdatedOn(Date.from(atStartOfDayResult39.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails5.setUser(jwtUser4);
        usersDetails5.setUserVerified(true);

        JwtUser jwtUser5 = new JwtUser();
        LocalDateTime atStartOfDayResult40 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser5.setCreatedOn(Date.from(atStartOfDayResult40.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser5.setEmail("jane.doe@example.org");
        jwtUser5.setEnabled(true);
        jwtUser5.setFirstname("Jane");
        jwtUser5.setId(UUID.randomUUID());
        jwtUser5.setLastname("Doe");
        jwtUser5.setPassword("iloveyou");
        jwtUser5.setRefreshToken(refreshToken4);
        jwtUser5.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult41 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser5.setUpdatedOn(Date.from(atStartOfDayResult41.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser5.setUserEmailVerified(true);
        jwtUser5.setUsername("janedoe");
        jwtUser5.setUsersDetails(usersDetails5);
        Optional<JwtUser> ofResult1 = Optional.of(jwtUser5);
        when(jwtUserRepository.findJwtUserByUsername((String) any())).thenReturn(ofResult1);
        when(jwtUserRepository.findJwtUserByEmail((String) any())).thenReturn(ofResult);
        userPreconditions.checkUserLoginDetails(new LoginCredentials("jane.doe@example.org", "iloveyou"));
        verify(jwtUserRepository).findJwtUserByEmail((String) any());
        verify(jwtUserRepository).findJwtUserByUsername((String) any());
    }

    /**
     * Method under test: {@link UserPreconditions#checkUserLoginDetails(LoginCredentials)}
     */
    @Test
    void testCheckUserLoginDetails2() {
        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(new JwtUser());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(new JwtUser());
        usersDetails.setUserVerified(true);

        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(refreshToken);
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(usersDetails);

        RefreshToken refreshToken1 = new RefreshToken();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setExpiryDate(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setId(UUID.randomUUID());
        refreshToken1.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken1.setUser(jwtUser);

        RefreshToken refreshToken2 = new RefreshToken();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setExpiryDate(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken2.setId(UUID.randomUUID());
        refreshToken2.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setUpdatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken2.setUser(new JwtUser());

        UsersDetails usersDetails1 = new UsersDetails();
        usersDetails1.setAddresses(new HashSet<>());
        usersDetails1.setAge("Age");
        usersDetails1.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setCreatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setGender(Gender.MALE);
        usersDetails1.setId(UUID.randomUUID());
        usersDetails1.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setUpdatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setUser(new JwtUser());
        usersDetails1.setUserVerified(true);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(refreshToken2);
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(usersDetails1);

        UsersDetails usersDetails2 = new UsersDetails();
        usersDetails2.setAddresses(new HashSet<>());
        usersDetails2.setAge("Age");
        usersDetails2.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setCreatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setGender(Gender.MALE);
        usersDetails2.setId(UUID.randomUUID());
        usersDetails2.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setUser(jwtUser1);
        usersDetails2.setUserVerified(true);

        JwtUser jwtUser2 = new JwtUser();
        LocalDateTime atStartOfDayResult19 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult19.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken1);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult20 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult20.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails2);
        Optional<JwtUser> ofResult = Optional.of(jwtUser2);
        when(jwtUserRepository.findJwtUserByUsername((String) any()))
                .thenThrow(new ApplicationExceptions("An error occurred"));
        when(jwtUserRepository.findJwtUserByEmail((String) any())).thenReturn(ofResult);
        assertThrows(ApplicationExceptions.class,
                () -> userPreconditions.checkUserLoginDetails(new LoginCredentials("jane.doe@example.org", "iloveyou")));
        verify(jwtUserRepository).findJwtUserByEmail((String) any());
        verify(jwtUserRepository).findJwtUserByUsername((String) any());
    }

    /**
     * Method under test: {@link UserPreconditions#checkUserRegisterDetails(UserRegistrationRequest)}
     */
    @Test
    void testCheckUserRegisterDetails() {
        when(jwtUserRepository.existsByEmail((String) any())).thenReturn(true);
        assertThrows(ApplicationExceptions.class,
                () -> userPreconditions.checkUserRegisterDetails(new UserRegistrationRequest()));
        verify(jwtUserRepository).existsByEmail(any());
    }

    /**
     * Method under test: {@link UserPreconditions#checkUserRegisterDetails(UserRegistrationRequest)}
     */
    @Test
    void testCheckUserRegisterDetails2() {
        when(jwtUserRepository.existsByEmail((String) any())).thenReturn(false);
        userPreconditions.checkUserRegisterDetails(new UserRegistrationRequest());
        verify(jwtUserRepository, atLeast(1)).existsByEmail((String) any());
    }


    /**
     * Method under test: {@link UserPreconditions#checkUserRegisterDetails(UserRegistrationRequest)}
     */
    @Test
    void testCheckUserRegisterDetails4() {
        when(jwtUserRepository.existsByEmail((String) any())).thenThrow(new ApplicationExceptions("An error occurred"));
        assertThrows(ApplicationExceptions.class,
                () -> userPreconditions.checkUserRegisterDetails(new UserRegistrationRequest()));
        verify(jwtUserRepository).existsByEmail((String) any());
    }

    /**
     * Method under test: {@link UserPreconditions#checkIfUserExists(LoginCredentials)}
     */
    @Test
    void testCheckIfUserExists() {
        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(new JwtUser());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(new JwtUser());
        usersDetails.setUserVerified(true);

        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(refreshToken);
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(usersDetails);

        RefreshToken refreshToken1 = new RefreshToken();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setExpiryDate(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setId(UUID.randomUUID());
        refreshToken1.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken1.setUser(jwtUser);

        RefreshToken refreshToken2 = new RefreshToken();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setExpiryDate(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken2.setId(UUID.randomUUID());
        refreshToken2.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setUpdatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken2.setUser(new JwtUser());

        UsersDetails usersDetails1 = new UsersDetails();
        usersDetails1.setAddresses(new HashSet<>());
        usersDetails1.setAge("Age");
        usersDetails1.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setCreatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setGender(Gender.MALE);
        usersDetails1.setId(UUID.randomUUID());
        usersDetails1.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setUpdatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setUser(new JwtUser());
        usersDetails1.setUserVerified(true);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(refreshToken2);
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(usersDetails1);

        UsersDetails usersDetails2 = new UsersDetails();
        usersDetails2.setAddresses(new HashSet<>());
        usersDetails2.setAge("Age");
        usersDetails2.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setCreatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setGender(Gender.MALE);
        usersDetails2.setId(UUID.randomUUID());
        usersDetails2.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setUser(jwtUser1);
        usersDetails2.setUserVerified(true);

        JwtUser jwtUser2 = new JwtUser();
        LocalDateTime atStartOfDayResult19 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult19.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken1);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult20 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult20.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails2);
        Optional<JwtUser> ofResult = Optional.of(jwtUser2);
        when(jwtUserRepository.findJwtUserByEmail((String) any())).thenReturn(ofResult);
        LoginCredentials loginCredentials = new LoginCredentials("jane.doe@example.org", "iloveyou");

        userPreconditions.checkIfUserExists(loginCredentials);
        verify(jwtUserRepository).findJwtUserByEmail((String) any());
        assertEquals("jane.doe@example.org", loginCredentials.getEmail());
        assertEquals("iloveyou", loginCredentials.getPassword());
    }

    /**
     * Method under test: {@link UserPreconditions#present(Optional)}
     */
    @Test
    void testPresent2() {
        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(new RefreshToken());
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(new UsersDetails());

        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(jwtUser);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(new RefreshToken());
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(new UsersDetails());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(jwtUser1);
        usersDetails.setUserVerified(true);
        JwtUser jwtUser2 = mock(JwtUser.class);
        doNothing().when(jwtUser2).setCreatedOn((Date) any());
        doNothing().when(jwtUser2).setEmail((String) any());
        doNothing().when(jwtUser2).setEnabled(anyBoolean());
        doNothing().when(jwtUser2).setFirstname((String) any());
        doNothing().when(jwtUser2).setId((UUID) any());
        doNothing().when(jwtUser2).setLastname((String) any());
        doNothing().when(jwtUser2).setPassword((String) any());
        doNothing().when(jwtUser2).setRefreshToken((RefreshToken) any());
        doNothing().when(jwtUser2).setRole((Set<Role>) any());
        doNothing().when(jwtUser2).setUpdatedOn((Date) any());
        doNothing().when(jwtUser2).setUserEmailVerified(anyBoolean());
        doNothing().when(jwtUser2).setUsername((String) any());
        doNothing().when(jwtUser2).setUsersDetails((UsersDetails) any());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails);
        userPreconditions.present(Optional.of(jwtUser2));
        verify(jwtUser2).setCreatedOn((Date) any());
        verify(jwtUser2).setEmail((String) any());
        verify(jwtUser2).setEnabled(anyBoolean());
        verify(jwtUser2).setFirstname((String) any());
        verify(jwtUser2).setId((UUID) any());
        verify(jwtUser2).setLastname((String) any());
        verify(jwtUser2).setPassword((String) any());
        verify(jwtUser2).setRefreshToken((RefreshToken) any());
        verify(jwtUser2).setRole((Set<Role>) any());
        verify(jwtUser2).setUpdatedOn((Date) any());
        verify(jwtUser2).setUserEmailVerified(anyBoolean());
        verify(jwtUser2).setUsername((String) any());
        verify(jwtUser2).setUsersDetails((UsersDetails) any());
    }

    /**
     * Method under test: {@link UserPreconditions#checkUserEmailExsist(ForgotPasswordRequest)}
     */
    @Test
    void testCheckUserEmailExsist() {
        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(new JwtUser());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(new JwtUser());
        usersDetails.setUserVerified(true);

        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(refreshToken);
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(usersDetails);

        RefreshToken refreshToken1 = new RefreshToken();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setExpiryDate(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setId(UUID.randomUUID());
        refreshToken1.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken1.setUser(jwtUser);

        RefreshToken refreshToken2 = new RefreshToken();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setExpiryDate(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken2.setId(UUID.randomUUID());
        refreshToken2.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setUpdatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken2.setUser(new JwtUser());

        UsersDetails usersDetails1 = new UsersDetails();
        usersDetails1.setAddresses(new HashSet<>());
        usersDetails1.setAge("Age");
        usersDetails1.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setCreatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setGender(Gender.MALE);
        usersDetails1.setId(UUID.randomUUID());
        usersDetails1.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setUpdatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setUser(new JwtUser());
        usersDetails1.setUserVerified(true);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(refreshToken2);
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(usersDetails1);

        UsersDetails usersDetails2 = new UsersDetails();
        usersDetails2.setAddresses(new HashSet<>());
        usersDetails2.setAge("Age");
        usersDetails2.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setCreatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setGender(Gender.MALE);
        usersDetails2.setId(UUID.randomUUID());
        usersDetails2.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setUser(jwtUser1);
        usersDetails2.setUserVerified(true);

        JwtUser jwtUser2 = new JwtUser();
        LocalDateTime atStartOfDayResult19 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult19.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken1);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult20 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult20.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails2);
        Optional<JwtUser> ofResult = Optional.of(jwtUser2);
        when(jwtUserRepository.findJwtUserByUsername((String) any())).thenReturn(ofResult);
        ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest("jane.doe@example.org");
        userPreconditions.checkUserEmailExsist(forgotPasswordRequest);
        verify(jwtUserRepository).findJwtUserByUsername((String) any());
        assertEquals("jane.doe@example.org", forgotPasswordRequest.getEmail());
    }

    /**
     * Method under test: {@link UserPreconditions#checkCodeAndEmail(JwtUser, EmailDetails)}
     */
    @Test
    void testCheckCodeAndEmail() {
        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(new JwtUser());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(new JwtUser());
        usersDetails.setUserVerified(true);

        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(refreshToken);
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(usersDetails);

        RefreshToken refreshToken1 = new RefreshToken();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setExpiryDate(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setId(UUID.randomUUID());
        refreshToken1.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken1.setUser(jwtUser);

        RefreshToken refreshToken2 = new RefreshToken();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setExpiryDate(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken2.setId(UUID.randomUUID());
        refreshToken2.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setUpdatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken2.setUser(new JwtUser());

        UsersDetails usersDetails1 = new UsersDetails();
        usersDetails1.setAddresses(new HashSet<>());
        usersDetails1.setAge("Age");
        usersDetails1.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setCreatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setGender(Gender.MALE);
        usersDetails1.setId(UUID.randomUUID());
        usersDetails1.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setUpdatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setUser(new JwtUser());
        usersDetails1.setUserVerified(true);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(refreshToken2);
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(usersDetails1);

        UsersDetails usersDetails2 = new UsersDetails();
        usersDetails2.setAddresses(new HashSet<>());
        usersDetails2.setAge("Age");
        usersDetails2.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setCreatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setGender(Gender.MALE);
        usersDetails2.setId(UUID.randomUUID());
        usersDetails2.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setUser(jwtUser1);
        usersDetails2.setUserVerified(true);

        JwtUser jwtUser2 = new JwtUser();
        LocalDateTime atStartOfDayResult19 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult19.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken1);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult20 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult20.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails2);

        EmailDetails emailDetails = new EmailDetails();
        emailDetails.setAttachment("Attachment");
        emailDetails.setCode("Code");
        emailDetails.setCreatedDate(mock(Timestamp.class));
        emailDetails.setId(UUID.randomUUID());
        emailDetails.setMsgBody("Not all who wander are lost");
        emailDetails.setRecipient("Recipient");
        emailDetails.setSubject("Hello from the Dreaming Spires");
        assertThrows(ApplicationExceptions.class, () -> userPreconditions.checkCodeAndEmail(jwtUser2, emailDetails));
    }

    /**
     * Method under test: {@link UserPreconditions#checkCodeAndEmail(JwtUser, EmailDetails)}
     */
    @Test
    void testCheckCodeAndEmail2() {
        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(new JwtUser());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(new JwtUser());
        usersDetails.setUserVerified(true);

        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(refreshToken);
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(usersDetails);

        RefreshToken refreshToken1 = new RefreshToken();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setExpiryDate(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setId(UUID.randomUUID());
        refreshToken1.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken1.setUser(jwtUser);

        RefreshToken refreshToken2 = new RefreshToken();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setExpiryDate(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken2.setId(UUID.randomUUID());
        refreshToken2.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setUpdatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken2.setUser(new JwtUser());

        UsersDetails usersDetails1 = new UsersDetails();
        usersDetails1.setAddresses(new HashSet<>());
        usersDetails1.setAge("Age");
        usersDetails1.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setCreatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setGender(Gender.MALE);
        usersDetails1.setId(UUID.randomUUID());
        usersDetails1.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setUpdatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setUser(new JwtUser());
        usersDetails1.setUserVerified(true);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(refreshToken2);
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(usersDetails1);

        UsersDetails usersDetails2 = new UsersDetails();
        usersDetails2.setAddresses(new HashSet<>());
        usersDetails2.setAge("Age");
        usersDetails2.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setCreatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setGender(Gender.MALE);
        usersDetails2.setId(UUID.randomUUID());
        usersDetails2.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setUser(jwtUser1);
        usersDetails2.setUserVerified(true);
        JwtUser jwtUser2 = mock(JwtUser.class);
        when(jwtUser2.getEmail()).thenReturn("jane.doe@example.org");
        doNothing().when(jwtUser2).setCreatedOn((Date) any());
        doNothing().when(jwtUser2).setEmail((String) any());
        doNothing().when(jwtUser2).setEnabled(anyBoolean());
        doNothing().when(jwtUser2).setFirstname((String) any());
        doNothing().when(jwtUser2).setId((UUID) any());
        doNothing().when(jwtUser2).setLastname((String) any());
        doNothing().when(jwtUser2).setPassword((String) any());
        doNothing().when(jwtUser2).setRefreshToken((RefreshToken) any());
        doNothing().when(jwtUser2).setRole((Set<Role>) any());
        doNothing().when(jwtUser2).setUpdatedOn((Date) any());
        doNothing().when(jwtUser2).setUserEmailVerified(anyBoolean());
        doNothing().when(jwtUser2).setUsername((String) any());
        doNothing().when(jwtUser2).setUsersDetails((UsersDetails) any());
        LocalDateTime atStartOfDayResult19 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult19.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken1);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult20 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult20.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails2);

        EmailDetails emailDetails = new EmailDetails();
        emailDetails.setAttachment("Attachment");
        emailDetails.setCode("Code");
        emailDetails.setCreatedDate(mock(Timestamp.class));
        emailDetails.setId(UUID.randomUUID());
        emailDetails.setMsgBody("Not all who wander are lost");
        emailDetails.setRecipient("Recipient");
        emailDetails.setSubject("Hello from the Dreaming Spires");
        assertThrows(ApplicationExceptions.class, () -> userPreconditions.checkCodeAndEmail(jwtUser2, emailDetails));
        verify(jwtUser2).getEmail();
        verify(jwtUser2).setCreatedOn((Date) any());
        verify(jwtUser2).setEmail((String) any());
        verify(jwtUser2).setEnabled(anyBoolean());
        verify(jwtUser2).setFirstname((String) any());
        verify(jwtUser2).setId((UUID) any());
        verify(jwtUser2).setLastname((String) any());
        verify(jwtUser2).setPassword((String) any());
        verify(jwtUser2).setRefreshToken((RefreshToken) any());
        verify(jwtUser2).setRole((Set<Role>) any());
        verify(jwtUser2).setUpdatedOn((Date) any());
        verify(jwtUser2).setUserEmailVerified(anyBoolean());
        verify(jwtUser2).setUsername((String) any());
        verify(jwtUser2).setUsersDetails((UsersDetails) any());
    }

    /**
     * Method under test: {@link UserPreconditions#checkCodeAndEmail(JwtUser, EmailDetails)}
     */
    @Test
    void testCheckCodeAndEmail3() {
        RefreshToken refreshToken = new RefreshToken();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setExpiryDate(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken.setId(UUID.randomUUID());
        refreshToken.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken.setUpdatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken.setUser(new JwtUser());

        UsersDetails usersDetails = new UsersDetails();
        usersDetails.setAddresses(new HashSet<>());
        usersDetails.setAge("Age");
        usersDetails.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setCreatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setGender(Gender.MALE);
        usersDetails.setId(UUID.randomUUID());
        usersDetails.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails.setUpdatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails.setUser(new JwtUser());
        usersDetails.setUserVerified(true);

        JwtUser jwtUser = new JwtUser();
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setCreatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setEmail("jane.doe@example.org");
        jwtUser.setEnabled(true);
        jwtUser.setFirstname("Jane");
        jwtUser.setId(UUID.randomUUID());
        jwtUser.setLastname("Doe");
        jwtUser.setPassword("iloveyou");
        jwtUser.setRefreshToken(refreshToken);
        jwtUser.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser.setUpdatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser.setUserEmailVerified(true);
        jwtUser.setUsername("janedoe");
        jwtUser.setUsersDetails(usersDetails);

        RefreshToken refreshToken1 = new RefreshToken();
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setCreatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setExpiryDate(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken1.setId(UUID.randomUUID());
        refreshToken1.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken1.setUser(jwtUser);

        RefreshToken refreshToken2 = new RefreshToken();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setCreatedOn(Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setExpiryDate(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant());
        refreshToken2.setId(UUID.randomUUID());
        refreshToken2.setRefreshToken("ABC123");
        LocalDateTime atStartOfDayResult12 = LocalDate.of(1970, 1, 1).atStartOfDay();
        refreshToken2.setUpdatedOn(Date.from(atStartOfDayResult12.atZone(ZoneId.of("UTC")).toInstant()));
        refreshToken2.setUser(new JwtUser());

        UsersDetails usersDetails1 = new UsersDetails();
        usersDetails1.setAddresses(new HashSet<>());
        usersDetails1.setAge("Age");
        usersDetails1.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult13 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setCreatedOn(Date.from(atStartOfDayResult13.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setGender(Gender.MALE);
        usersDetails1.setId(UUID.randomUUID());
        usersDetails1.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult14 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails1.setUpdatedOn(Date.from(atStartOfDayResult14.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails1.setUser(new JwtUser());
        usersDetails1.setUserVerified(true);

        JwtUser jwtUser1 = new JwtUser();
        LocalDateTime atStartOfDayResult15 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setCreatedOn(Date.from(atStartOfDayResult15.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setEmail("jane.doe@example.org");
        jwtUser1.setEnabled(true);
        jwtUser1.setFirstname("Jane");
        jwtUser1.setId(UUID.randomUUID());
        jwtUser1.setLastname("Doe");
        jwtUser1.setPassword("iloveyou");
        jwtUser1.setRefreshToken(refreshToken2);
        jwtUser1.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult16 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser1.setUpdatedOn(Date.from(atStartOfDayResult16.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser1.setUserEmailVerified(true);
        jwtUser1.setUsername("janedoe");
        jwtUser1.setUsersDetails(usersDetails1);

        UsersDetails usersDetails2 = new UsersDetails();
        usersDetails2.setAddresses(new HashSet<>());
        usersDetails2.setAge("Age");
        usersDetails2.setAvatar("Avatar");
        LocalDateTime atStartOfDayResult17 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setCreatedOn(Date.from(atStartOfDayResult17.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setGender(Gender.MALE);
        usersDetails2.setId(UUID.randomUUID());
        usersDetails2.setPhoneNumbers(new HashSet<>());
        LocalDateTime atStartOfDayResult18 = LocalDate.of(1970, 1, 1).atStartOfDay();
        usersDetails2.setUpdatedOn(Date.from(atStartOfDayResult18.atZone(ZoneId.of("UTC")).toInstant()));
        usersDetails2.setUser(jwtUser1);
        usersDetails2.setUserVerified(true);
        JwtUser jwtUser2 = mock(JwtUser.class);
        when(jwtUser2.getEmail()).thenReturn("Recipient");
        doNothing().when(jwtUser2).setCreatedOn((Date) any());
        doNothing().when(jwtUser2).setEmail((String) any());
        doNothing().when(jwtUser2).setEnabled(anyBoolean());
        doNothing().when(jwtUser2).setFirstname((String) any());
        doNothing().when(jwtUser2).setId((UUID) any());
        doNothing().when(jwtUser2).setLastname((String) any());
        doNothing().when(jwtUser2).setPassword((String) any());
        doNothing().when(jwtUser2).setRefreshToken((RefreshToken) any());
        doNothing().when(jwtUser2).setRole((Set<Role>) any());
        doNothing().when(jwtUser2).setUpdatedOn((Date) any());
        doNothing().when(jwtUser2).setUserEmailVerified(anyBoolean());
        doNothing().when(jwtUser2).setUsername((String) any());
        doNothing().when(jwtUser2).setUsersDetails((UsersDetails) any());
        LocalDateTime atStartOfDayResult19 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setCreatedOn(Date.from(atStartOfDayResult19.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setEmail("jane.doe@example.org");
        jwtUser2.setEnabled(true);
        jwtUser2.setFirstname("Jane");
        jwtUser2.setId(UUID.randomUUID());
        jwtUser2.setLastname("Doe");
        jwtUser2.setPassword("iloveyou");
        jwtUser2.setRefreshToken(refreshToken1);
        jwtUser2.setRole(new HashSet<>());
        LocalDateTime atStartOfDayResult20 = LocalDate.of(1970, 1, 1).atStartOfDay();
        jwtUser2.setUpdatedOn(Date.from(atStartOfDayResult20.atZone(ZoneId.of("UTC")).toInstant()));
        jwtUser2.setUserEmailVerified(true);
        jwtUser2.setUsername("janedoe");
        jwtUser2.setUsersDetails(usersDetails2);

        EmailDetails emailDetails = new EmailDetails();
        emailDetails.setAttachment("Attachment");
        emailDetails.setCode("Code");
        emailDetails.setCreatedDate(mock(Timestamp.class));
        emailDetails.setId(UUID.randomUUID());
        emailDetails.setMsgBody("Not all who wander are lost");
        emailDetails.setRecipient("Recipient");
        emailDetails.setSubject("Hello from the Dreaming Spires");
        userPreconditions.checkCodeAndEmail(jwtUser2, emailDetails);
        verify(jwtUser2).getEmail();
        verify(jwtUser2).setCreatedOn((Date) any());
        verify(jwtUser2).setEmail((String) any());
        verify(jwtUser2).setEnabled(anyBoolean());
        verify(jwtUser2).setFirstname((String) any());
        verify(jwtUser2).setId((UUID) any());
        verify(jwtUser2).setLastname((String) any());
        verify(jwtUser2).setPassword((String) any());
        verify(jwtUser2).setRefreshToken((RefreshToken) any());
        verify(jwtUser2).setRole(any());
        verify(jwtUser2).setUpdatedOn((Date) any());
        verify(jwtUser2).setUserEmailVerified(anyBoolean());
        verify(jwtUser2).setUsername((String) any());
        verify(jwtUser2).setUsersDetails((UsersDetails) any());
    }
}

