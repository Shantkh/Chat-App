package com.app.assigmentsubmitionapp.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {VerificationUtils.class})
@ExtendWith(SpringExtension.class)
class VerificationUtilsTest {
    @Autowired
    private VerificationUtils verificationUtils;

    /**
     * Method under test: {@link VerificationUtils#getRandomNumberString()}
     */
    @Test
    void testGetRandomNumberString() {
        Assertions.assertNotNull(verificationUtils.getRandomNumberString());
    }
}

