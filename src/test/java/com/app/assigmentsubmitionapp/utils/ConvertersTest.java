package com.app.assigmentsubmitionapp.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class ConvertersTest {
    /**
     * Method under test: {@link Converters#convertObjectToInt(Object)}
     */
    @Test
    void testConvertObjectToInt() {
        assertEquals(42, Converters.convertObjectToInt("42").intValue());
    }

    /**
     * Method under test: {@link Converters#convertObjectToInt(Object)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testConvertObjectToInt2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NumberFormatException: For input string: "foo"
        //       at java.lang.NumberFormatException.forInputString(NumberFormatException.java:67)
        //       at java.lang.Integer.parseInt(Integer.java:668)
        //       at java.lang.Integer.valueOf(Integer.java:999)
        //       at com.app.assigmentsubmitionapp.utils.Converters.convertObjectToInt(Converters.java:9)
        //   See https://diff.blue/R013 to resolve this issue.

        Converters.convertObjectToInt("foo");
    }
}

