package com.app.assigmentsubmitionapp.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;

class AppPreconditionsTest {
    /**
     * Method under test: {@link AppPreconditions#checkNotNullOrEmptyList(List)}
     */
    @Test
    void testCheckNotNullOrEmptyList() {
        assertThrows(ApplicationExceptions.class, () -> AppPreconditions.checkNotNullOrEmptyList(new ArrayList<>()));
    }

    /**
     * Method under test: {@link AppPreconditions#checkNotNullOrEmptyList(List)}
     */
    @Test
    void testCheckNotNullOrEmptyList2() {
        ArrayList<Object> objectList = new ArrayList<>();
        objectList.add("42");
        assertEquals(1, AppPreconditions.checkNotNullOrEmptyList(objectList).size());
    }

    /**
     * Method under test: {@link AppPreconditions#checkNotNullOrEmpty(Object)}
     */
    @Test
    void testCheckNotNullOrEmpty() {
        assertEquals("42", AppPreconditions.checkNotNullOrEmpty("42"));
        assertThrows(ApplicationExceptions.class, () -> AppPreconditions.checkNotNullOrEmpty(null));
    }

    /**
     * Method under test: {@link AppPreconditions#checkLimitationOfInts(Object, int)}
     */
    @Test
    void testCheckLimitationOfInts() {
        assertEquals("42", AppPreconditions.checkLimitationOfInts("42", 1));
        assertThrows(ApplicationExceptions.class, () -> AppPreconditions.checkLimitationOfInts(0, 1));
        assertThrows(ApplicationExceptions.class, () -> AppPreconditions.checkLimitationOfInts("42", 1, 1));
        assertThrows(ApplicationExceptions.class, () -> AppPreconditions.checkLimitationOfInts(0, 1, 1));
    }

    /**
     * Method under test: {@link AppPreconditions#checkLimitationOfInts(Object, int, int)}
     */
    @Test
    void testCheckLimitationOfInts3() {
        // TODO: Complete this test.
        //   Diffblue AI was unable to find a test

        AppPreconditions.checkLimitationOfInts(1, 1, 1);
    }

    /**
     * Method under test: {@link AppPreconditions#checkLimitationOfString(Object, String[])}
     */
    @Test
    void testCheckLimitationOfString() {
        assertThrows(ApplicationExceptions.class, () -> AppPreconditions.checkLimitationOfString("42", "foo"));
        assertEquals("foo", AppPreconditions.checkLimitationOfString("foo", "foo"));
    }

    /**
     * Method under test: {@link AppPreconditions#checkNotNullOrEmptySet(Set)}
     */
    @Test
    void testCheckNotNullOrEmptySet() {
        assertThrows(ApplicationExceptions.class, () -> AppPreconditions.checkNotNullOrEmptySet(new HashSet<>()));
    }

    /**
     * Method under test: {@link AppPreconditions#checkNotNullOrEmptySet(Set)}
     */
    @Test
    void testCheckNotNullOrEmptySet2() {
        HashSet<Object> objectSet = new HashSet<>();
        objectSet.add("42");
        assertEquals(1, ((Collection<String>) AppPreconditions.checkNotNullOrEmptySet(objectSet)).size());
    }

    /**
     * Method under test: {@link AppPreconditions#checkNotNullOrEmptyOptional(Optional)}
     */
    @Test
    void testCheckNotNullOrEmptyOptional() {
        assertThrows(ApplicationExceptions.class, () -> AppPreconditions.checkNotNullOrEmptyOptional(Optional.empty()));
    }
}

