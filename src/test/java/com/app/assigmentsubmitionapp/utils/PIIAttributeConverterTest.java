package com.app.assigmentsubmitionapp.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {PIIAttributeConverter.class})
@ExtendWith(SpringExtension.class)
class PIIAttributeConverterTest {
    @Autowired
    private PIIAttributeConverter pIIAttributeConverter;

    /**
     * Method under test: {@link PIIAttributeConverter#convertToDatabaseColumn(String)}
     */
    @Test
    void testConvertToDatabaseColumn() {
        assertEquals("Jnh/iKxFkhMa/Dy8etiULQ==", pIIAttributeConverter.convertToDatabaseColumn("Attribute"));
        assertEquals(" ", pIIAttributeConverter.convertToDatabaseColumn(null));
    }

    /**
     * Method under test: {@link PIIAttributeConverter#convertToEntityAttribute(String)}
     */
    @Test
    void testConvertToEntityAttribute() {
        assertThrows(ApplicationExceptions.class, () -> pIIAttributeConverter.convertToEntityAttribute("Db Data"));
        assertThrows(ApplicationExceptions.class, () -> pIIAttributeConverter.convertToEntityAttribute("AES"));
        assertEquals("", pIIAttributeConverter.convertToEntityAttribute(null));
        assertEquals("", pIIAttributeConverter.convertToEntityAttribute(""));
    }
}

