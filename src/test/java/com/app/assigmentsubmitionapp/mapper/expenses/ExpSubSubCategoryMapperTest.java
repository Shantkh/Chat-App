package com.app.assigmentsubmitionapp.mapper.expenses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryResponse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {ExpSubSubCategoryMapper.class})
@ExtendWith(SpringExtension.class)
class ExpSubSubCategoryMapperTest {
    @Autowired
    private ExpSubSubCategoryMapper expSubSubCategoryMapper;

    /**
     * Method under test: {@link ExpSubSubCategoryMapper#fromListSubCategoryObjectToOptionalSubSubCategoryResponse(List)}
     */
    @Test
    void testFromListSubCategoryObjectToOptionalSubSubCategoryResponse() {
        assertTrue(expSubSubCategoryMapper.fromListSubCategoryObjectToOptionalSubSubCategoryResponse(new ArrayList<>())
                .isPresent());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryMapper#fromListSubCategoryObjectToOptionalSubSubCategoryResponse(List)}
     */
    @Test
    void testFromListSubCategoryObjectToOptionalSubSubCategoryResponse2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        expSubSubCategory.setCreatedOn(fromResult);
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        UUID randomUUIDResult = UUID.randomUUID();
        expSubSubCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        expSubSubCategory.setUpdatedOn(fromResult1);

        ArrayList<ExpSubSubCategory> expSubSubCategoryList = new ArrayList<>();
        expSubSubCategoryList.add(expSubSubCategory);
        Optional<List<ExpSubSubCategoryResponse>> actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult = expSubSubCategoryMapper
                .fromListSubCategoryObjectToOptionalSubSubCategoryResponse(expSubSubCategoryList);
        assertTrue(actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult.isPresent());
        ExpSubSubCategoryResponse getResult = actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult.get()
                .get(0);
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Exp Sub Sub Category Name", getResult.getSubSubCategoryName());
        assertEquals("Exp Sub Sub Category Description", getResult.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryMapper#fromListSubCategoryObjectToOptionalSubSubCategoryResponse(List)}
     */
    @Test
    void testFromListSubCategoryObjectToOptionalSubSubCategoryResponse3() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        expSubSubCategory.setCreatedOn(fromResult);
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        UUID randomUUIDResult = UUID.randomUUID();
        expSubSubCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        expSubSubCategory.setUpdatedOn(fromResult1);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory1 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory1.setExpCategory(expCategory1);
        expSubCategory1.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory1.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory1.setExpSubSubCategories(new HashSet<>());
        expSubCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory1 = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult2 = Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant());
        expSubSubCategory1.setCreatedOn(fromResult2);
        expSubSubCategory1.setExpSubCategory(expSubCategory1);
        expSubSubCategory1.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory1.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        UUID randomUUIDResult1 = UUID.randomUUID();
        expSubSubCategory1.setId(randomUUIDResult1);
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult3 = Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant());
        expSubSubCategory1.setUpdatedOn(fromResult3);

        ArrayList<ExpSubSubCategory> expSubSubCategoryList = new ArrayList<>();
        expSubSubCategoryList.add(expSubSubCategory1);
        expSubSubCategoryList.add(expSubSubCategory);
        Optional<List<ExpSubSubCategoryResponse>> actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult = expSubSubCategoryMapper
                .fromListSubCategoryObjectToOptionalSubSubCategoryResponse(expSubSubCategoryList);
        assertTrue(actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult.isPresent());
        List<ExpSubSubCategoryResponse> getResult = actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult
                .get();
        ExpSubSubCategoryResponse getResult1 = getResult.get(0);
        assertSame(fromResult3, getResult1.getUpdatedOn());
        ExpSubSubCategoryResponse getResult2 = getResult.get(1);
        assertSame(fromResult1, getResult2.getUpdatedOn());
        assertEquals("Exp Sub Sub Category Name", getResult2.getSubSubCategoryName());
        assertEquals("Exp Sub Sub Category Description", getResult2.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, getResult2.getId());
        assertSame(fromResult, getResult2.getCreatedOn());
        assertSame(fromResult2, getResult1.getCreatedOn());
        assertEquals("Exp Sub Sub Category Description", getResult1.getSubSubCategoryDescription());
        assertEquals("Exp Sub Sub Category Name", getResult1.getSubSubCategoryName());
        assertSame(randomUUIDResult1, getResult1.getId());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryMapper#fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(Optional)}
     */
    @Test
    void testFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        expSubSubCategory.setCreatedOn(fromResult);
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        UUID randomUUIDResult = UUID.randomUUID();
        expSubSubCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        expSubSubCategory.setUpdatedOn(fromResult1);
        Optional<ExpSubSubCategoryResponse> actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult = expSubSubCategoryMapper
                .fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(Optional.of(expSubSubCategory));
        assertTrue(actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult.isPresent());
        ExpSubSubCategoryResponse getResult = actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult
                .get();
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Exp Sub Sub Category Name", getResult.getSubSubCategoryName());
        assertEquals("Exp Sub Sub Category Description", getResult.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryMapper#fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(Optional)}
     */
    @Test
    void testFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        ExpSubSubCategory expSubSubCategory = mock(ExpSubSubCategory.class);
        when(expSubSubCategory.getExpSubSubCategoryDescription()).thenReturn("Exp Sub Sub Category Description");
        when(expSubSubCategory.getExpSubSubCategoryName()).thenReturn("Exp Sub Sub Category Name");
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        when(expSubSubCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        when(expSubSubCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(expSubSubCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(expSubSubCategory).setCreatedOn((Date) any());
        doNothing().when(expSubSubCategory).setExpSubCategory((ExpSubCategory) any());
        doNothing().when(expSubSubCategory).setExpSubSubCategoryDescription((String) any());
        doNothing().when(expSubSubCategory).setExpSubSubCategoryName((String) any());
        doNothing().when(expSubSubCategory).setId((UUID) any());
        doNothing().when(expSubSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubSubCategoryResponse> actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult = expSubSubCategoryMapper
                .fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(Optional.of(expSubSubCategory));
        assertTrue(actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult.isPresent());
        ExpSubSubCategoryResponse getResult = actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult
                .get();
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Exp Sub Sub Category Name", getResult.getSubSubCategoryName());
        assertEquals("Exp Sub Sub Category Description", getResult.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
        verify(expSubSubCategory).getExpSubSubCategoryDescription();
        verify(expSubSubCategory).getExpSubSubCategoryName();
        verify(expSubSubCategory).getCreatedOn();
        verify(expSubSubCategory).getUpdatedOn();
        verify(expSubSubCategory).getId();
        verify(expSubSubCategory).setCreatedOn((Date) any());
        verify(expSubSubCategory).setExpSubCategory((ExpSubCategory) any());
        verify(expSubSubCategory).setExpSubSubCategoryDescription((String) any());
        verify(expSubSubCategory).setExpSubSubCategoryName((String) any());
        verify(expSubSubCategory).setId((UUID) any());
        verify(expSubSubCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryMapper#fromSubSubCategoryObjectToSubSubCategoryResponse(ExpSubSubCategory)}
     */
    @Test
    void testFromSubSubCategoryObjectToSubSubCategoryResponse() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubSubCategory expSubSubCategory = new ExpSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        expSubSubCategory.setCreatedOn(fromResult);
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        UUID randomUUIDResult = UUID.randomUUID();
        expSubSubCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        expSubSubCategory.setUpdatedOn(fromResult1);
        ExpSubSubCategoryResponse actualFromSubSubCategoryObjectToSubSubCategoryResponseResult = expSubSubCategoryMapper
                .fromSubSubCategoryObjectToSubSubCategoryResponse(expSubSubCategory);
        assertSame(fromResult, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getCreatedOn());
        assertSame(fromResult1, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getUpdatedOn());
        assertEquals("Exp Sub Sub Category Name",
                actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getSubSubCategoryName());
        assertEquals("Exp Sub Sub Category Description",
                actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getId());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryMapper#fromSubSubCategoryObjectToSubSubCategoryResponse(ExpSubSubCategory)}
     */
    @Test
    void testFromSubSubCategoryObjectToSubSubCategoryResponse2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        ExpSubSubCategory expSubSubCategory = mock(ExpSubSubCategory.class);
        when(expSubSubCategory.getExpSubSubCategoryDescription()).thenReturn("Exp Sub Sub Category Description");
        when(expSubSubCategory.getExpSubSubCategoryName()).thenReturn("Exp Sub Sub Category Name");
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        when(expSubSubCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        when(expSubSubCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(expSubSubCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(expSubSubCategory).setCreatedOn((Date) any());
        doNothing().when(expSubSubCategory).setExpSubCategory((ExpSubCategory) any());
        doNothing().when(expSubSubCategory).setExpSubSubCategoryDescription((String) any());
        doNothing().when(expSubSubCategory).setExpSubSubCategoryName((String) any());
        doNothing().when(expSubSubCategory).setId((UUID) any());
        doNothing().when(expSubSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        expSubSubCategory.setExpSubCategory(expSubCategory);
        expSubSubCategory.setExpSubSubCategoryDescription("Exp Sub Sub Category Description");
        expSubSubCategory.setExpSubSubCategoryName("Exp Sub Sub Category Name");
        expSubSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        ExpSubSubCategoryResponse actualFromSubSubCategoryObjectToSubSubCategoryResponseResult = expSubSubCategoryMapper
                .fromSubSubCategoryObjectToSubSubCategoryResponse(expSubSubCategory);
        assertSame(fromResult, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getCreatedOn());
        assertSame(fromResult1, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getUpdatedOn());
        assertEquals("Exp Sub Sub Category Name",
                actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getSubSubCategoryName());
        assertEquals("Exp Sub Sub Category Description",
                actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getId());
        verify(expSubSubCategory).getExpSubSubCategoryDescription();
        verify(expSubSubCategory).getExpSubSubCategoryName();
        verify(expSubSubCategory).getCreatedOn();
        verify(expSubSubCategory).getUpdatedOn();
        verify(expSubSubCategory).getId();
        verify(expSubSubCategory).setCreatedOn((Date) any());
        verify(expSubSubCategory).setExpSubCategory((ExpSubCategory) any());
        verify(expSubSubCategory).setExpSubSubCategoryDescription((String) any());
        verify(expSubSubCategory).setExpSubSubCategoryName((String) any());
        verify(expSubSubCategory).setId((UUID) any());
        verify(expSubSubCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryMapper#fromSubSubCategoryRequestToSubSubCategoryObject(ExpSubSubCategoryRequest)}
     */
    @Test
    void testFromSubSubCategoryRequestToSubSubCategoryObject() {
        ExpSubSubCategory actualFromSubSubCategoryRequestToSubSubCategoryObjectResult = expSubSubCategoryMapper
                .fromSubSubCategoryRequestToSubSubCategoryObject(new ExpSubSubCategoryRequest());
        assertNull(actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getId());
        assertNull(actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getExpSubSubCategoryName());
        assertNull(actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getExpSubSubCategoryDescription());
        ExpSubCategory expSubCategory = actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getExpSubCategory();
        assertNull(expSubCategory.getCreatedOn());
        assertNull(expSubCategory.getUpdatedOn());
        assertNull(expSubCategory.getId());
        assertNull(expSubCategory.getExpSubSubCategories());
        assertNull(expSubCategory.getExpSubCategoryName());
        assertNull(expSubCategory.getExpSubCategoryDescription());
        assertNull(expSubCategory.getExpCategory());
    }

    /**
     * Method under test: {@link ExpSubSubCategoryMapper#fromSubSubCategoryRequestToSubSubCategoryObject(ExpSubSubCategoryRequest)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testFromSubSubCategoryRequestToSubSubCategoryObject2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest.getSubCategoryId()" because "s" is null
        //       at com.app.assigmentsubmitionapp.mapper.expenses.ExpSubSubCategoryMapper.fromSubSubCategoryRequestToSubSubCategoryObject(ExpSubSubCategoryMapper.java:53)
        //   See https://diff.blue/R013 to resolve this issue.

        expSubSubCategoryMapper.fromSubSubCategoryRequestToSubSubCategoryObject(null);
    }

    /**
     * Method under test: {@link ExpSubSubCategoryMapper#fromSubSubCategoryRequestToSubSubCategoryObject(ExpSubSubCategoryRequest)}
     */
    @Test
    void testFromSubSubCategoryRequestToSubSubCategoryObject3() {
        ExpSubSubCategoryRequest expSubSubCategoryRequest = mock(ExpSubSubCategoryRequest.class);
        when(expSubSubCategoryRequest.getSubSubCategoryDescription()).thenReturn("Sub Sub Category Description");
        when(expSubSubCategoryRequest.getSubSubCategoryName()).thenReturn("Sub Sub Category Name");
        UUID randomUUIDResult = UUID.randomUUID();
        when(expSubSubCategoryRequest.getSubCategoryId()).thenReturn(randomUUIDResult);
        ExpSubSubCategory actualFromSubSubCategoryRequestToSubSubCategoryObjectResult = expSubSubCategoryMapper
                .fromSubSubCategoryRequestToSubSubCategoryObject(expSubSubCategoryRequest);
        assertNull(actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getId());
        assertEquals("Sub Sub Category Name",
                actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getExpSubSubCategoryName());
        assertEquals("Sub Sub Category Description",
                actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getExpSubSubCategoryDescription());
        ExpSubCategory expSubCategory = actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getExpSubCategory();
        assertNull(expSubCategory.getCreatedOn());
        assertNull(expSubCategory.getUpdatedOn());
        assertSame(randomUUIDResult, expSubCategory.getId());
        assertNull(expSubCategory.getExpSubSubCategories());
        assertNull(expSubCategory.getExpCategory());
        assertNull(expSubCategory.getExpSubCategoryName());
        assertNull(expSubCategory.getExpSubCategoryDescription());
        verify(expSubSubCategoryRequest).getSubSubCategoryDescription();
        verify(expSubSubCategoryRequest).getSubSubCategoryName();
        verify(expSubSubCategoryRequest).getSubCategoryId();
    }
}

