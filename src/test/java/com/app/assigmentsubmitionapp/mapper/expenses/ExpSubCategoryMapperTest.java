package com.app.assigmentsubmitionapp.mapper.expenses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryResponse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {ExpSubCategoryMapper.class})
@ExtendWith(SpringExtension.class)
class ExpSubCategoryMapperTest {
    @Autowired
    private ExpSubCategoryMapper expSubCategoryMapper;

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(List)}
     */
    @Test
    void testFromOptionalSubCategoryObjectToOptionalSubCategoryResponse() {
        assertTrue(expSubCategoryMapper.fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(new ArrayList<>())
                .isPresent());
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(List)}
     */
    @Test
    void testFromOptionalSubCategoryObjectToOptionalSubCategoryResponse2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        expSubCategory.setCreatedOn(fromResult);
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        UUID randomUUIDResult = UUID.randomUUID();
        expSubCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        expSubCategory.setUpdatedOn(fromResult1);

        ArrayList<ExpSubCategory> expSubCategoryList = new ArrayList<>();
        expSubCategoryList.add(expSubCategory);
        Optional<List<ExpSubCategoryResponse>> actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult = expSubCategoryMapper
                .fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(expSubCategoryList);
        assertTrue(actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        ExpSubCategoryResponse getResult = actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult.get()
                .get(0);
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Exp Sub Category Name", getResult.getSubCategoryName());
        assertEquals("Exp Sub Category Description", getResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(List)}
     */
    @Test
    void testFromOptionalSubCategoryObjectToOptionalSubCategoryResponse3() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        expSubCategory.setCreatedOn(fromResult);
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        UUID randomUUIDResult = UUID.randomUUID();
        expSubCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        expSubCategory.setUpdatedOn(fromResult1);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        expCategory1.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory1.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory1 = new ExpSubCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult2 = Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant());
        expSubCategory1.setCreatedOn(fromResult2);
        expSubCategory1.setExpCategory(expCategory1);
        expSubCategory1.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory1.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory1.setExpSubSubCategories(new HashSet<>());
        UUID randomUUIDResult1 = UUID.randomUUID();
        expSubCategory1.setId(randomUUIDResult1);
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult3 = Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant());
        expSubCategory1.setUpdatedOn(fromResult3);

        ArrayList<ExpSubCategory> expSubCategoryList = new ArrayList<>();
        expSubCategoryList.add(expSubCategory1);
        expSubCategoryList.add(expSubCategory);
        Optional<List<ExpSubCategoryResponse>> actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult = expSubCategoryMapper
                .fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(expSubCategoryList);
        assertTrue(actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        List<ExpSubCategoryResponse> getResult = actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult
                .get();
        ExpSubCategoryResponse getResult1 = getResult.get(0);
        assertSame(fromResult3, getResult1.getUpdatedOn());
        ExpSubCategoryResponse getResult2 = getResult.get(1);
        assertSame(fromResult1, getResult2.getUpdatedOn());
        assertEquals("Exp Sub Category Name", getResult2.getSubCategoryName());
        assertEquals("Exp Sub Category Description", getResult2.getSubCategoryDescription());
        assertSame(randomUUIDResult, getResult2.getId());
        assertSame(fromResult, getResult2.getCreatedOn());
        assertSame(fromResult2, getResult1.getCreatedOn());
        assertEquals("Exp Sub Category Description", getResult1.getSubCategoryDescription());
        assertEquals("Exp Sub Category Name", getResult1.getSubCategoryName());
        assertSame(randomUUIDResult1, getResult1.getId());
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(List)}
     */
    @Test
    void testFromOptionalSubCategoryObjectToOptionalSubCategoryResponse4() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        ExpSubCategory expSubCategory = mock(ExpSubCategory.class);
        when(expSubCategory.getExpSubCategoryDescription()).thenReturn("Exp Sub Category Description");
        when(expSubCategory.getExpSubCategoryName()).thenReturn("Exp Sub Category Name");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        when(expSubCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        when(expSubCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(expSubCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(expSubCategory).setCreatedOn((Date) any());
        doNothing().when(expSubCategory).setExpCategory((ExpCategory) any());
        doNothing().when(expSubCategory).setExpSubCategoryDescription((String) any());
        doNothing().when(expSubCategory).setExpSubCategoryName((String) any());
        doNothing().when(expSubCategory).setExpSubSubCategories((Set<ExpSubSubCategory>) any());
        doNothing().when(expSubCategory).setId((UUID) any());
        doNothing().when(expSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        ArrayList<ExpSubCategory> expSubCategoryList = new ArrayList<>();
        expSubCategoryList.add(expSubCategory);
        Optional<List<ExpSubCategoryResponse>> actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult = expSubCategoryMapper
                .fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(expSubCategoryList);
        assertTrue(actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        ExpSubCategoryResponse getResult = actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult.get()
                .get(0);
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Exp Sub Category Name", getResult.getSubCategoryName());
        assertEquals("Exp Sub Category Description", getResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
        verify(expSubCategory).getExpSubCategoryDescription();
        verify(expSubCategory).getExpSubCategoryName();
        verify(expSubCategory).getCreatedOn();
        verify(expSubCategory).getUpdatedOn();
        verify(expSubCategory).getId();
        verify(expSubCategory).setCreatedOn((Date) any());
        verify(expSubCategory).setExpCategory((ExpCategory) any());
        verify(expSubCategory).setExpSubCategoryDescription((String) any());
        verify(expSubCategory).setExpSubCategoryName((String) any());
        verify(expSubCategory).setExpSubSubCategories((Set<ExpSubSubCategory>) any());
        verify(expSubCategory).setId((UUID) any());
        verify(expSubCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional)}
     */
    @Test
    void testFromOptionalObSubCategoryObjectToOptionalSubCategoryResponse() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        expSubCategory.setCreatedOn(fromResult);
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        UUID randomUUIDResult = UUID.randomUUID();
        expSubCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        expSubCategory.setUpdatedOn(fromResult1);
        Optional<ExpSubCategoryResponse> actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult = expSubCategoryMapper
                .fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional.of(expSubCategory));
        assertTrue(actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        ExpSubCategoryResponse getResult = actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult.get();
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Exp Sub Category Name", getResult.getSubCategoryName());
        assertEquals("Exp Sub Category Description", getResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional)}
     */
    @Test
    void testFromOptionalObSubCategoryObjectToOptionalSubCategoryResponse2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        ExpSubCategory expSubCategory = mock(ExpSubCategory.class);
        when(expSubCategory.getExpSubCategoryDescription()).thenReturn("Exp Sub Category Description");
        when(expSubCategory.getExpSubCategoryName()).thenReturn("Exp Sub Category Name");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        when(expSubCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        when(expSubCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(expSubCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(expSubCategory).setCreatedOn((Date) any());
        doNothing().when(expSubCategory).setExpCategory((ExpCategory) any());
        doNothing().when(expSubCategory).setExpSubCategoryDescription((String) any());
        doNothing().when(expSubCategory).setExpSubCategoryName((String) any());
        doNothing().when(expSubCategory).setExpSubSubCategories((Set<ExpSubSubCategory>) any());
        doNothing().when(expSubCategory).setId((UUID) any());
        doNothing().when(expSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubCategoryResponse> actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult = expSubCategoryMapper
                .fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional.of(expSubCategory));
        assertTrue(actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        ExpSubCategoryResponse getResult = actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult.get();
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Exp Sub Category Name", getResult.getSubCategoryName());
        assertEquals("Exp Sub Category Description", getResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
        verify(expSubCategory).getExpSubCategoryDescription();
        verify(expSubCategory).getExpSubCategoryName();
        verify(expSubCategory).getCreatedOn();
        verify(expSubCategory).getUpdatedOn();
        verify(expSubCategory).getId();
        verify(expSubCategory).setCreatedOn((Date) any());
        verify(expSubCategory).setExpCategory((ExpCategory) any());
        verify(expSubCategory).setExpSubCategoryDescription((String) any());
        verify(expSubCategory).setExpSubCategoryName((String) any());
        verify(expSubCategory).setExpSubSubCategories((Set<ExpSubSubCategory>) any());
        verify(expSubCategory).setId((UUID) any());
        verify(expSubCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional)}
     */
    @Test
    void testFromOptionalObSubCategoryObjectToOptionalSubCategoryResponse3() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        ExpSubCategory expSubCategory = mock(ExpSubCategory.class);
        when(expSubCategory.getExpSubCategoryDescription()).thenReturn("Exp Sub Category Description");
        when(expSubCategory.getExpSubCategoryName()).thenReturn("Exp Sub Category Name");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        when(expSubCategory.getCreatedOn())
                .thenReturn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        when(expSubCategory.getUpdatedOn())
                .thenReturn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(expSubCategory.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(expSubCategory).setCreatedOn((Date) any());
        doNothing().when(expSubCategory).setExpCategory((ExpCategory) any());
        doNothing().when(expSubCategory).setExpSubCategoryDescription((String) any());
        doNothing().when(expSubCategory).setExpSubCategoryName((String) any());
        doNothing().when(expSubCategory).setExpSubSubCategories((Set<ExpSubSubCategory>) any());
        doNothing().when(expSubCategory).setId((UUID) any());
        doNothing().when(expSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<ExpSubCategory> emptyResult = Optional.empty();
        Optional<ExpSubCategoryResponse> actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult = expSubCategoryMapper
                .fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(emptyResult);
        assertSame(emptyResult, actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult);
        assertFalse(actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        verify(expSubCategory).setCreatedOn((Date) any());
        verify(expSubCategory).setExpCategory((ExpCategory) any());
        verify(expSubCategory).setExpSubCategoryDescription((String) any());
        verify(expSubCategory).setExpSubCategoryName((String) any());
        verify(expSubCategory).setExpSubSubCategories((Set<ExpSubSubCategory>) any());
        verify(expSubCategory).setId((UUID) any());
        verify(expSubCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromSubCategoryRequestToSubCategoryObject(ExpSubCategoryRequest)}
     */
    @Test
    void testFromSubCategoryRequestToSubCategoryObject() {
        ExpSubCategory actualFromSubCategoryRequestToSubCategoryObjectResult = expSubCategoryMapper
                .fromSubCategoryRequestToSubCategoryObject(new ExpSubCategoryRequest());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getId());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getExpSubSubCategories());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getExpSubCategoryName());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getExpSubCategoryDescription());
        ExpCategory expCategory = actualFromSubCategoryRequestToSubCategoryObjectResult.getExpCategory();
        assertNull(expCategory.getCreatedOn());
        assertNull(expCategory.getUpdatedOn());
        assertNull(expCategory.getId());
        assertNull(expCategory.getExpSubCategories());
        assertNull(expCategory.getExpCategoryName());
        assertNull(expCategory.getExpCategoryDescription());
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromSubCategoryRequestToSubCategoryObject(ExpSubCategoryRequest)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testFromSubCategoryRequestToSubCategoryObject2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest.getCategoryId()" because "categoryRequest" is null
        //       at com.app.assigmentsubmitionapp.mapper.expenses.ExpSubCategoryMapper.fromSubCategoryRequestToSubCategoryObject(ExpSubCategoryMapper.java:42)
        //   See https://diff.blue/R013 to resolve this issue.

        expSubCategoryMapper.fromSubCategoryRequestToSubCategoryObject(null);
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromSubCategoryRequestToSubCategoryObject(ExpSubCategoryRequest)}
     */
    @Test
    void testFromSubCategoryRequestToSubCategoryObject3() {
        ExpSubCategoryRequest expSubCategoryRequest = mock(ExpSubCategoryRequest.class);
        when(expSubCategoryRequest.getSubCategoryDescription()).thenReturn("Sub Category Description");
        when(expSubCategoryRequest.getSubCategoryName()).thenReturn("Sub Category Name");
        UUID randomUUIDResult = UUID.randomUUID();
        when(expSubCategoryRequest.getCategoryId()).thenReturn(randomUUIDResult);
        ExpSubCategory actualFromSubCategoryRequestToSubCategoryObjectResult = expSubCategoryMapper
                .fromSubCategoryRequestToSubCategoryObject(expSubCategoryRequest);
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getId());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getExpSubSubCategories());
        assertEquals("Sub Category Name", actualFromSubCategoryRequestToSubCategoryObjectResult.getExpSubCategoryName());
        assertEquals("Sub Category Description",
                actualFromSubCategoryRequestToSubCategoryObjectResult.getExpSubCategoryDescription());
        ExpCategory expCategory = actualFromSubCategoryRequestToSubCategoryObjectResult.getExpCategory();
        assertNull(expCategory.getCreatedOn());
        assertNull(expCategory.getUpdatedOn());
        assertSame(randomUUIDResult, expCategory.getId());
        assertNull(expCategory.getExpSubCategories());
        assertNull(expCategory.getExpCategoryDescription());
        assertNull(expCategory.getExpCategoryName());
        verify(expSubCategoryRequest).getSubCategoryDescription();
        verify(expSubCategoryRequest).getSubCategoryName();
        verify(expSubCategoryRequest).getCategoryId();
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromSubCategoryRequestToSubCategoryObjectById(ExpSubCategoryRequest)}
     */
    @Test
    void testFromSubCategoryRequestToSubCategoryObjectById() {
        ExpSubCategory actualFromSubCategoryRequestToSubCategoryObjectByIdResult = expSubCategoryMapper
                .fromSubCategoryRequestToSubCategoryObjectById(new ExpSubCategoryRequest());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getCreatedOn());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getUpdatedOn());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getId());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getExpSubSubCategories());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getExpSubCategoryName());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getExpSubCategoryDescription());
        ExpCategory expCategory = actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getExpCategory();
        assertNull(expCategory.getCreatedOn());
        assertNull(expCategory.getUpdatedOn());
        assertNull(expCategory.getId());
        assertNull(expCategory.getExpSubCategories());
        assertNull(expCategory.getExpCategoryName());
        assertNull(expCategory.getExpCategoryDescription());
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromSubCategoryRequestToSubCategoryObjectById(ExpSubCategoryRequest)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testFromSubCategoryRequestToSubCategoryObjectById2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest.getCategoryId()" because "categoryRequest" is null
        //       at com.app.assigmentsubmitionapp.mapper.expenses.ExpSubCategoryMapper.fromSubCategoryRequestToSubCategoryObjectById(ExpSubCategoryMapper.java:53)
        //   See https://diff.blue/R013 to resolve this issue.

        expSubCategoryMapper.fromSubCategoryRequestToSubCategoryObjectById(null);
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromSubCategoryRequestToSubCategoryObjectById(ExpSubCategoryRequest)}
     */
    @Test
    void testFromSubCategoryRequestToSubCategoryObjectById3() {
        ExpSubCategoryRequest expSubCategoryRequest = mock(ExpSubCategoryRequest.class);
        when(expSubCategoryRequest.getSubCategoryDescription()).thenReturn("Sub Category Description");
        when(expSubCategoryRequest.getSubCategoryName()).thenReturn("Sub Category Name");
        UUID randomUUIDResult = UUID.randomUUID();
        when(expSubCategoryRequest.getCategoryId()).thenReturn(randomUUIDResult);
        ExpSubCategory actualFromSubCategoryRequestToSubCategoryObjectByIdResult = expSubCategoryMapper
                .fromSubCategoryRequestToSubCategoryObjectById(expSubCategoryRequest);
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getCreatedOn());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getUpdatedOn());
        UUID id = actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getId();
        assertSame(randomUUIDResult, id);
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getExpSubSubCategories());
        assertEquals("Sub Category Name",
                actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getExpSubCategoryName());
        assertEquals("Sub Category Description",
                actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getExpSubCategoryDescription());
        ExpCategory expCategory = actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getExpCategory();
        assertSame(id, expCategory.getId());
        assertNull(expCategory.getExpSubCategories());
        assertNull(expCategory.getExpCategoryName());
        assertNull(expCategory.getExpCategoryDescription());
        assertNull(expCategory.getCreatedOn());
        assertNull(expCategory.getUpdatedOn());
        verify(expSubCategoryRequest).getSubCategoryDescription();
        verify(expSubCategoryRequest).getSubCategoryName();
        verify(expSubCategoryRequest, atLeast(1)).getCategoryId();
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromSubCategoryObjectToSubCategoryResponse(ExpSubCategory)}
     */
    @Test
    void testFromSubCategoryObjectToSubCategoryResponse() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        ExpSubCategory expSubCategory = new ExpSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        expSubCategory.setCreatedOn(fromResult);
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        UUID randomUUIDResult = UUID.randomUUID();
        expSubCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        expSubCategory.setUpdatedOn(fromResult1);
        ExpSubCategoryResponse actualFromSubCategoryObjectToSubCategoryResponseResult = expSubCategoryMapper
                .fromSubCategoryObjectToSubCategoryResponse(expSubCategory);
        assertSame(fromResult, actualFromSubCategoryObjectToSubCategoryResponseResult.getCreatedOn());
        assertSame(fromResult1, actualFromSubCategoryObjectToSubCategoryResponseResult.getUpdatedOn());
        assertEquals("Exp Sub Category Name",
                actualFromSubCategoryObjectToSubCategoryResponseResult.getSubCategoryName());
        assertEquals("Exp Sub Category Description",
                actualFromSubCategoryObjectToSubCategoryResponseResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, actualFromSubCategoryObjectToSubCategoryResponseResult.getId());
    }

    /**
     * Method under test: {@link ExpSubCategoryMapper#fromSubCategoryObjectToSubCategoryResponse(ExpSubCategory)}
     */
    @Test
    void testFromSubCategoryObjectToSubCategoryResponse2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        ExpSubCategory expSubCategory = mock(ExpSubCategory.class);
        when(expSubCategory.getExpSubCategoryDescription()).thenReturn("Exp Sub Category Description");
        when(expSubCategory.getExpSubCategoryName()).thenReturn("Exp Sub Category Name");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        when(expSubCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        when(expSubCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(expSubCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(expSubCategory).setCreatedOn((Date) any());
        doNothing().when(expSubCategory).setExpCategory((ExpCategory) any());
        doNothing().when(expSubCategory).setExpSubCategoryDescription((String) any());
        doNothing().when(expSubCategory).setExpSubCategoryName((String) any());
        doNothing().when(expSubCategory).setExpSubSubCategories((Set<ExpSubSubCategory>) any());
        doNothing().when(expSubCategory).setId((UUID) any());
        doNothing().when(expSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        expSubCategory.setExpCategory(expCategory);
        expSubCategory.setExpSubCategoryDescription("Exp Sub Category Description");
        expSubCategory.setExpSubCategoryName("Exp Sub Category Name");
        expSubCategory.setExpSubSubCategories(new HashSet<>());
        expSubCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        ExpSubCategoryResponse actualFromSubCategoryObjectToSubCategoryResponseResult = expSubCategoryMapper
                .fromSubCategoryObjectToSubCategoryResponse(expSubCategory);
        assertSame(fromResult, actualFromSubCategoryObjectToSubCategoryResponseResult.getCreatedOn());
        assertSame(fromResult1, actualFromSubCategoryObjectToSubCategoryResponseResult.getUpdatedOn());
        assertEquals("Exp Sub Category Name",
                actualFromSubCategoryObjectToSubCategoryResponseResult.getSubCategoryName());
        assertEquals("Exp Sub Category Description",
                actualFromSubCategoryObjectToSubCategoryResponseResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, actualFromSubCategoryObjectToSubCategoryResponseResult.getId());
        verify(expSubCategory).getExpSubCategoryDescription();
        verify(expSubCategory).getExpSubCategoryName();
        verify(expSubCategory).getCreatedOn();
        verify(expSubCategory).getUpdatedOn();
        verify(expSubCategory).getId();
        verify(expSubCategory).setCreatedOn((Date) any());
        verify(expSubCategory).setExpCategory((ExpCategory) any());
        verify(expSubCategory).setExpSubCategoryDescription((String) any());
        verify(expSubCategory).setExpSubCategoryName((String) any());
        verify(expSubCategory).setExpSubSubCategories((Set<ExpSubSubCategory>) any());
        verify(expSubCategory).setId((UUID) any());
        verify(expSubCategory).setUpdatedOn((Date) any());
    }
}

