package com.app.assigmentsubmitionapp.mapper.expenses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryResponse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {ExpCategoryMapper.class})
@ExtendWith(SpringExtension.class)
class ExpCategoryMapperTest {
    @Autowired
    private ExpCategoryMapper expCategoryMapper;

    /**
     * Method under test: {@link ExpCategoryMapper#fromCategoryObjectToListCategoryResponse(List)}
     */
    @Test
    void testFromCategoryObjectToListCategoryResponse() {
        assertTrue(expCategoryMapper.fromCategoryObjectToListCategoryResponse(new ArrayList<>()).isPresent());
    }

    /**
     * Method under test: {@link ExpCategoryMapper#fromCategoryObjectToListCategoryResponse(List)}
     */
    @Test
    void testFromCategoryObjectToListCategoryResponse2() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant());
        expCategory.setCreatedOn(fromResult);
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        UUID randomUUIDResult = UUID.randomUUID();
        expCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        expCategory.setUpdatedOn(fromResult1);

        ArrayList<ExpCategory> expCategoryList = new ArrayList<>();
        expCategoryList.add(expCategory);
        Optional<List<ExpCategoryResponse>> actualFromCategoryObjectToListCategoryResponseResult = expCategoryMapper
                .fromCategoryObjectToListCategoryResponse(expCategoryList);
        assertTrue(actualFromCategoryObjectToListCategoryResponseResult.isPresent());
        ExpCategoryResponse getResult = actualFromCategoryObjectToListCategoryResponseResult.get().get(0);
        assertEquals("Exp Category Description", getResult.getCategoryDescription());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertSame(randomUUIDResult, getResult.getId());
        assertSame(fromResult, getResult.getCreatedOn());
        assertEquals("Exp Category Name", getResult.getCategoryName());
    }

    /**
     * Method under test: {@link ExpCategoryMapper#fromCategoryObjectToListCategoryResponse(List)}
     */
    @Test
    void testFromCategoryObjectToListCategoryResponse3() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant());
        expCategory.setCreatedOn(fromResult);
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        UUID randomUUIDResult = UUID.randomUUID();
        expCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        expCategory.setUpdatedOn(fromResult1);

        ExpCategory expCategory1 = new ExpCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult2 = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        expCategory1.setCreatedOn(fromResult2);
        expCategory1.setExpCategoryDescription("Exp Category Description");
        expCategory1.setExpCategoryName("Exp Category Name");
        expCategory1.setExpSubCategories(new HashSet<>());
        UUID randomUUIDResult1 = UUID.randomUUID();
        expCategory1.setId(randomUUIDResult1);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult3 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        expCategory1.setUpdatedOn(fromResult3);

        ArrayList<ExpCategory> expCategoryList = new ArrayList<>();
        expCategoryList.add(expCategory1);
        expCategoryList.add(expCategory);
        Optional<List<ExpCategoryResponse>> actualFromCategoryObjectToListCategoryResponseResult = expCategoryMapper
                .fromCategoryObjectToListCategoryResponse(expCategoryList);
        assertTrue(actualFromCategoryObjectToListCategoryResponseResult.isPresent());
        List<ExpCategoryResponse> getResult = actualFromCategoryObjectToListCategoryResponseResult.get();
        ExpCategoryResponse getResult1 = getResult.get(0);
        assertSame(fromResult3, getResult1.getUpdatedOn());
        ExpCategoryResponse getResult2 = getResult.get(1);
        assertSame(fromResult1, getResult2.getUpdatedOn());
        assertSame(randomUUIDResult, getResult2.getId());
        assertSame(fromResult, getResult2.getCreatedOn());
        assertEquals("Exp Category Description", getResult1.getCategoryDescription());
        assertSame(fromResult2, getResult1.getCreatedOn());
        assertSame(randomUUIDResult1, getResult1.getId());
        assertEquals("Exp Category Name", getResult1.getCategoryName());
        assertEquals("Exp Category Description", getResult2.getCategoryDescription());
        assertEquals("Exp Category Name", getResult2.getCategoryName());
    }

    /**
     * Method under test: {@link ExpCategoryMapper#fromCategoryObjectToListCategoryResponse(List)}
     */
    @Test
    void testFromCategoryObjectToListCategoryResponse4() {
        ExpCategory expCategory = mock(ExpCategory.class);
        when(expCategory.getExpCategoryDescription()).thenReturn("Exp Category Description");
        when(expCategory.getExpCategoryName()).thenReturn("Exp Category Name");
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant());
        when(expCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        when(expCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(expCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(expCategory).setCreatedOn((Date) any());
        doNothing().when(expCategory).setExpCategoryDescription((String) any());
        doNothing().when(expCategory).setExpCategoryName((String) any());
        doNothing().when(expCategory).setExpSubCategories((Set<ExpSubCategory>) any());
        doNothing().when(expCategory).setId((UUID) any());
        doNothing().when(expCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        ArrayList<ExpCategory> expCategoryList = new ArrayList<>();
        expCategoryList.add(expCategory);
        Optional<List<ExpCategoryResponse>> actualFromCategoryObjectToListCategoryResponseResult = expCategoryMapper
                .fromCategoryObjectToListCategoryResponse(expCategoryList);
        assertTrue(actualFromCategoryObjectToListCategoryResponseResult.isPresent());
        ExpCategoryResponse getResult = actualFromCategoryObjectToListCategoryResponseResult.get().get(0);
        assertEquals("Exp Category Description", getResult.getCategoryDescription());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertSame(randomUUIDResult, getResult.getId());
        assertSame(fromResult, getResult.getCreatedOn());
        assertEquals("Exp Category Name", getResult.getCategoryName());
        verify(expCategory).getExpCategoryDescription();
        verify(expCategory).getExpCategoryName();
        verify(expCategory).getCreatedOn();
        verify(expCategory).getUpdatedOn();
        verify(expCategory).getId();
        verify(expCategory).setCreatedOn((Date) any());
        verify(expCategory).setExpCategoryDescription((String) any());
        verify(expCategory).setExpCategoryName((String) any());
        verify(expCategory).setExpSubCategories((Set<ExpSubCategory>) any());
        verify(expCategory).setId((UUID) any());
        verify(expCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link ExpCategoryMapper#fromCategoryRequestToCategoryObject(ExpCategoryRequest)}
     */
    @Test
    void testFromCategoryRequestToCategoryObject() {
        ExpCategory actualFromCategoryRequestToCategoryObjectResult = expCategoryMapper
                .fromCategoryRequestToCategoryObject(new ExpCategoryRequest());
        assertNull(actualFromCategoryRequestToCategoryObjectResult.getId());
        assertNull(actualFromCategoryRequestToCategoryObjectResult.getExpSubCategories());
        assertNull(actualFromCategoryRequestToCategoryObjectResult.getExpCategoryName());
        assertNull(actualFromCategoryRequestToCategoryObjectResult.getExpCategoryDescription());
    }

    /**
     * Method under test: {@link ExpCategoryMapper#fromCategoryRequestToCategoryObject(ExpCategoryRequest)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testFromCategoryRequestToCategoryObject2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryRequest.getExpCategoryName()" because "categoryRequest" is null
        //       at com.app.assigmentsubmitionapp.mapper.expenses.ExpCategoryMapper.fromCategoryRequestToCategoryObject(ExpCategoryMapper.java:32)
        //   See https://diff.blue/R013 to resolve this issue.

        expCategoryMapper.fromCategoryRequestToCategoryObject(null);
    }

    /**
     * Method under test: {@link ExpCategoryMapper#fromCategoryRequestToCategoryObject(ExpCategoryRequest)}
     */
    @Test
    void testFromCategoryRequestToCategoryObject3() {
        ExpCategoryRequest expCategoryRequest = mock(ExpCategoryRequest.class);
        when(expCategoryRequest.getExpCategoryDescription()).thenReturn("Exp Category Description");
        when(expCategoryRequest.getExpCategoryName()).thenReturn("Exp Category Name");
        ExpCategory actualFromCategoryRequestToCategoryObjectResult = expCategoryMapper
                .fromCategoryRequestToCategoryObject(expCategoryRequest);
        assertNull(actualFromCategoryRequestToCategoryObjectResult.getId());
        assertNull(actualFromCategoryRequestToCategoryObjectResult.getExpSubCategories());
        assertEquals("Exp Category Name", actualFromCategoryRequestToCategoryObjectResult.getExpCategoryName());
        assertEquals("Exp Category Description",
                actualFromCategoryRequestToCategoryObjectResult.getExpCategoryDescription());
        verify(expCategoryRequest).getExpCategoryDescription();
        verify(expCategoryRequest).getExpCategoryName();
    }

    /**
     * Method under test: {@link ExpCategoryMapper#fromCategoryToCategoryResponse(ExpCategory)}
     */
    @Test
    void testFromCategoryToCategoryResponse() {
        ExpCategory expCategory = new ExpCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant());
        expCategory.setCreatedOn(fromResult);
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        UUID randomUUIDResult = UUID.randomUUID();
        expCategory.setId(randomUUIDResult);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        expCategory.setUpdatedOn(fromResult1);
        ExpCategoryResponse actualFromCategoryToCategoryResponseResult = expCategoryMapper
                .fromCategoryToCategoryResponse(expCategory);
        assertEquals("Exp Category Description", actualFromCategoryToCategoryResponseResult.getCategoryDescription());
        assertSame(fromResult1, actualFromCategoryToCategoryResponseResult.getUpdatedOn());
        assertSame(randomUUIDResult, actualFromCategoryToCategoryResponseResult.getId());
        assertSame(fromResult, actualFromCategoryToCategoryResponseResult.getCreatedOn());
        assertEquals("Exp Category Name", actualFromCategoryToCategoryResponseResult.getCategoryName());
    }

    /**
     * Method under test: {@link ExpCategoryMapper#fromCategoryToCategoryResponse(ExpCategory)}
     */
    @Test
    void testFromCategoryToCategoryResponse2() {
        ExpCategory expCategory = mock(ExpCategory.class);
        when(expCategory.getExpCategoryDescription()).thenReturn("Exp Category Description");
        when(expCategory.getExpCategoryName()).thenReturn("Exp Category Name");
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant());
        when(expCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant());
        when(expCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(expCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(expCategory).setCreatedOn((Date) any());
        doNothing().when(expCategory).setExpCategoryDescription((String) any());
        doNothing().when(expCategory).setExpCategoryName((String) any());
        doNothing().when(expCategory).setExpSubCategories((Set<ExpSubCategory>) any());
        doNothing().when(expCategory).setId((UUID) any());
        doNothing().when(expCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        expCategory.setExpCategoryDescription("Exp Category Description");
        expCategory.setExpCategoryName("Exp Category Name");
        expCategory.setExpSubCategories(new HashSet<>());
        expCategory.setId(UUID.randomUUID());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        expCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        ExpCategoryResponse actualFromCategoryToCategoryResponseResult = expCategoryMapper
                .fromCategoryToCategoryResponse(expCategory);
        assertEquals("Exp Category Description", actualFromCategoryToCategoryResponseResult.getCategoryDescription());
        assertSame(fromResult1, actualFromCategoryToCategoryResponseResult.getUpdatedOn());
        assertSame(randomUUIDResult, actualFromCategoryToCategoryResponseResult.getId());
        assertSame(fromResult, actualFromCategoryToCategoryResponseResult.getCreatedOn());
        assertEquals("Exp Category Name", actualFromCategoryToCategoryResponseResult.getCategoryName());
        verify(expCategory).getExpCategoryDescription();
        verify(expCategory).getExpCategoryName();
        verify(expCategory).getCreatedOn();
        verify(expCategory).getUpdatedOn();
        verify(expCategory).getId();
        verify(expCategory).setCreatedOn((Date) any());
        verify(expCategory).setExpCategoryDescription((String) any());
        verify(expCategory).setExpCategoryName((String) any());
        verify(expCategory).setExpSubCategories((Set<ExpSubCategory>) any());
        verify(expCategory).setId((UUID) any());
        verify(expCategory).setUpdatedOn((Date) any());
    }
}

