package com.app.assigmentsubmitionapp.mapper.incomes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryResponse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {IncSubCategoryMapper.class})
@ExtendWith(SpringExtension.class)
class IncSubCategoryMapperTest {
    @Autowired
    private IncSubCategoryMapper incSubCategoryMapper;

    /**
     * Method under test: {@link IncSubCategoryMapper#fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(List)}
     */
    @Test
    void testFromOptionalSubCategoryObjectToOptionalSubCategoryResponse() {
        assertTrue(incSubCategoryMapper.fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(new ArrayList<>())
                .isPresent());
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(List)}
     */
    @Test
    void testFromOptionalSubCategoryObjectToOptionalSubCategoryResponse2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        incSubCategory.setCreatedOn(fromResult);
        UUID randomUUIDResult = UUID.randomUUID();
        incSubCategory.setId(randomUUIDResult);
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        incSubCategory.setUpdatedOn(fromResult1);

        ArrayList<IncSubCategory> incSubCategoryList = new ArrayList<>();
        incSubCategoryList.add(incSubCategory);
        Optional<List<IncSubCategoryResponse>> actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult = incSubCategoryMapper
                .fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(incSubCategoryList);
        assertTrue(actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        IncSubCategoryResponse getResult = actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult.get()
                .get(0);
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Inc Sub Category Name", getResult.getSubCategoryName());
        assertEquals("Inc Sub Category Description", getResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(List)}
     */
    @Test
    void testFromOptionalSubCategoryObjectToOptionalSubCategoryResponse3() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        incSubCategory.setCreatedOn(fromResult);
        UUID randomUUIDResult = UUID.randomUUID();
        incSubCategory.setId(randomUUIDResult);
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        incSubCategory.setUpdatedOn(fromResult1);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory1 = new IncSubCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult2 = Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant());
        incSubCategory1.setCreatedOn(fromResult2);
        UUID randomUUIDResult1 = UUID.randomUUID();
        incSubCategory1.setId(randomUUIDResult1);
        incSubCategory1.setIncCategory(incCategory1);
        incSubCategory1.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory1.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory1.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult3 = Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant());
        incSubCategory1.setUpdatedOn(fromResult3);

        ArrayList<IncSubCategory> incSubCategoryList = new ArrayList<>();
        incSubCategoryList.add(incSubCategory1);
        incSubCategoryList.add(incSubCategory);
        Optional<List<IncSubCategoryResponse>> actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult = incSubCategoryMapper
                .fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(incSubCategoryList);
        assertTrue(actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        List<IncSubCategoryResponse> getResult = actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult
                .get();
        IncSubCategoryResponse getResult1 = getResult.get(0);
        assertSame(fromResult3, getResult1.getUpdatedOn());
        IncSubCategoryResponse getResult2 = getResult.get(1);
        assertSame(fromResult1, getResult2.getUpdatedOn());
        assertEquals("Inc Sub Category Name", getResult2.getSubCategoryName());
        assertEquals("Inc Sub Category Description", getResult2.getSubCategoryDescription());
        assertSame(randomUUIDResult, getResult2.getId());
        assertSame(fromResult, getResult2.getCreatedOn());
        assertSame(fromResult2, getResult1.getCreatedOn());
        assertEquals("Inc Sub Category Description", getResult1.getSubCategoryDescription());
        assertEquals("Inc Sub Category Name", getResult1.getSubCategoryName());
        assertSame(randomUUIDResult1, getResult1.getId());
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(List)}
     */
    @Test
    void testFromOptionalSubCategoryObjectToOptionalSubCategoryResponse4() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        IncSubCategory incSubCategory = mock(IncSubCategory.class);
        when(incSubCategory.getIncSubCategoryDescription()).thenReturn("Inc Sub Category Description");
        when(incSubCategory.getIncSubCategoryName()).thenReturn("Inc Sub Category Name");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        when(incSubCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        when(incSubCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(incSubCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(incSubCategory).setCreatedOn((Date) any());
        doNothing().when(incSubCategory).setId((UUID) any());
        doNothing().when(incSubCategory).setIncCategory((IncCategory) any());
        doNothing().when(incSubCategory).setIncSubCategoryDescription((String) any());
        doNothing().when(incSubCategory).setIncSubCategoryName((String) any());
        doNothing().when(incSubCategory).setIncSubSubCategories((Set<IncSubSubCategory>) any());
        doNothing().when(incSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));

        ArrayList<IncSubCategory> incSubCategoryList = new ArrayList<>();
        incSubCategoryList.add(incSubCategory);
        Optional<List<IncSubCategoryResponse>> actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult = incSubCategoryMapper
                .fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(incSubCategoryList);
        assertTrue(actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        IncSubCategoryResponse getResult = actualFromOptionalSubCategoryObjectToOptionalSubCategoryResponseResult.get()
                .get(0);
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Inc Sub Category Name", getResult.getSubCategoryName());
        assertEquals("Inc Sub Category Description", getResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
        verify(incSubCategory).getIncSubCategoryDescription();
        verify(incSubCategory).getIncSubCategoryName();
        verify(incSubCategory).getCreatedOn();
        verify(incSubCategory).getUpdatedOn();
        verify(incSubCategory).getId();
        verify(incSubCategory).setCreatedOn((Date) any());
        verify(incSubCategory).setId((UUID) any());
        verify(incSubCategory).setIncCategory((IncCategory) any());
        verify(incSubCategory).setIncSubCategoryDescription((String) any());
        verify(incSubCategory).setIncSubCategoryName((String) any());
        verify(incSubCategory).setIncSubSubCategories((Set<IncSubSubCategory>) any());
        verify(incSubCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional)}
     */
    @Test
    void testFromOptionalObSubCategoryObjectToOptionalSubCategoryResponse() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        incSubCategory.setCreatedOn(fromResult);
        UUID randomUUIDResult = UUID.randomUUID();
        incSubCategory.setId(randomUUIDResult);
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        incSubCategory.setUpdatedOn(fromResult1);
        Optional<IncSubCategoryResponse> actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult = incSubCategoryMapper
                .fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional.of(incSubCategory));
        assertTrue(actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        IncSubCategoryResponse getResult = actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult.get();
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Inc Sub Category Name", getResult.getSubCategoryName());
        assertEquals("Inc Sub Category Name", getResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional)}
     */
    @Test
    void testFromOptionalObSubCategoryObjectToOptionalSubCategoryResponse2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        IncSubCategory incSubCategory = mock(IncSubCategory.class);
        when(incSubCategory.getIncSubCategoryName()).thenReturn("Inc Sub Category Name");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        when(incSubCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        when(incSubCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(incSubCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(incSubCategory).setCreatedOn((Date) any());
        doNothing().when(incSubCategory).setId((UUID) any());
        doNothing().when(incSubCategory).setIncCategory((IncCategory) any());
        doNothing().when(incSubCategory).setIncSubCategoryDescription((String) any());
        doNothing().when(incSubCategory).setIncSubCategoryName((String) any());
        doNothing().when(incSubCategory).setIncSubSubCategories((Set<IncSubSubCategory>) any());
        doNothing().when(incSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubCategoryResponse> actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult = incSubCategoryMapper
                .fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional.of(incSubCategory));
        assertTrue(actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        IncSubCategoryResponse getResult = actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult.get();
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Inc Sub Category Name", getResult.getSubCategoryName());
        assertEquals("Inc Sub Category Name", getResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
        verify(incSubCategory, atLeast(1)).getIncSubCategoryName();
        verify(incSubCategory).getCreatedOn();
        verify(incSubCategory).getUpdatedOn();
        verify(incSubCategory).getId();
        verify(incSubCategory).setCreatedOn((Date) any());
        verify(incSubCategory).setId((UUID) any());
        verify(incSubCategory).setIncCategory((IncCategory) any());
        verify(incSubCategory).setIncSubCategoryDescription((String) any());
        verify(incSubCategory).setIncSubCategoryName((String) any());
        verify(incSubCategory).setIncSubSubCategories((Set<IncSubSubCategory>) any());
        verify(incSubCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional)}
     */
    @Test
    void testFromOptionalObSubCategoryObjectToOptionalSubCategoryResponse3() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        IncSubCategory incSubCategory = mock(IncSubCategory.class);
        when(incSubCategory.getIncSubCategoryName()).thenReturn("Inc Sub Category Name");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        when(incSubCategory.getCreatedOn())
                .thenReturn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        when(incSubCategory.getUpdatedOn())
                .thenReturn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        when(incSubCategory.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(incSubCategory).setCreatedOn((Date) any());
        doNothing().when(incSubCategory).setId((UUID) any());
        doNothing().when(incSubCategory).setIncCategory((IncCategory) any());
        doNothing().when(incSubCategory).setIncSubCategoryDescription((String) any());
        doNothing().when(incSubCategory).setIncSubCategoryName((String) any());
        doNothing().when(incSubCategory).setIncSubSubCategories((Set<IncSubSubCategory>) any());
        doNothing().when(incSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubCategory> emptyResult = Optional.empty();
        Optional<IncSubCategoryResponse> actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult = incSubCategoryMapper
                .fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(emptyResult);
        assertSame(emptyResult, actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult);
        assertFalse(actualFromOptionalObSubCategoryObjectToOptionalSubCategoryResponseResult.isPresent());
        verify(incSubCategory).setCreatedOn((Date) any());
        verify(incSubCategory).setId((UUID) any());
        verify(incSubCategory).setIncCategory((IncCategory) any());
        verify(incSubCategory).setIncSubCategoryDescription((String) any());
        verify(incSubCategory).setIncSubCategoryName((String) any());
        verify(incSubCategory).setIncSubSubCategories((Set<IncSubSubCategory>) any());
        verify(incSubCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromSubCategoryRequestToSubCategoryObject(IncSubCategoryRequest)}
     */
    @Test
    void testFromSubCategoryRequestToSubCategoryObject() {
        IncSubCategory actualFromSubCategoryRequestToSubCategoryObjectResult = incSubCategoryMapper
                .fromSubCategoryRequestToSubCategoryObject(new IncSubCategoryRequest());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getIncSubSubCategories());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getIncSubCategoryName());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getIncSubCategoryDescription());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getId());
        IncCategory incCategory = actualFromSubCategoryRequestToSubCategoryObjectResult.getIncCategory();
        assertNull(incCategory.getUpdatedOn());
        assertNull(incCategory.getIncSubCategories());
        assertNull(incCategory.getIncCategoryName());
        assertNull(incCategory.getIncCategoryDescription());
        assertNull(incCategory.getId());
        assertNull(incCategory.getCreatedOn());
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromSubCategoryRequestToSubCategoryObject(IncSubCategoryRequest)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testFromSubCategoryRequestToSubCategoryObject2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryRequest.getCategoryId()" because "categoryRequest" is null
        //       at com.app.assigmentsubmitionapp.mapper.incomes.IncSubCategoryMapper.fromSubCategoryRequestToSubCategoryObject(IncSubCategoryMapper.java:46)
        //   See https://diff.blue/R013 to resolve this issue.

        incSubCategoryMapper.fromSubCategoryRequestToSubCategoryObject(null);
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromSubCategoryRequestToSubCategoryObject(IncSubCategoryRequest)}
     */
    @Test
    void testFromSubCategoryRequestToSubCategoryObject3() {
        IncSubCategoryRequest incSubCategoryRequest = mock(IncSubCategoryRequest.class);
        when(incSubCategoryRequest.getSubCategoryDescription()).thenReturn("Sub Category Description");
        when(incSubCategoryRequest.getSubCategoryName()).thenReturn("Sub Category Name");
        UUID randomUUIDResult = UUID.randomUUID();
        when(incSubCategoryRequest.getCategoryId()).thenReturn(randomUUIDResult);
        IncSubCategory actualFromSubCategoryRequestToSubCategoryObjectResult = incSubCategoryMapper
                .fromSubCategoryRequestToSubCategoryObject(incSubCategoryRequest);
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getIncSubSubCategories());
        assertEquals("Sub Category Name", actualFromSubCategoryRequestToSubCategoryObjectResult.getIncSubCategoryName());
        assertEquals("Sub Category Description",
                actualFromSubCategoryRequestToSubCategoryObjectResult.getIncSubCategoryDescription());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectResult.getId());
        IncCategory incCategory = actualFromSubCategoryRequestToSubCategoryObjectResult.getIncCategory();
        assertNull(incCategory.getUpdatedOn());
        assertNull(incCategory.getIncSubCategories());
        assertNull(incCategory.getIncCategoryName());
        assertNull(incCategory.getIncCategoryDescription());
        assertSame(randomUUIDResult, incCategory.getId());
        assertNull(incCategory.getCreatedOn());
        verify(incSubCategoryRequest).getSubCategoryDescription();
        verify(incSubCategoryRequest).getSubCategoryName();
        verify(incSubCategoryRequest).getCategoryId();
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromSubCategoryRequestToSubCategoryObjectById(IncSubCategoryRequest)}
     */
    @Test
    void testFromSubCategoryRequestToSubCategoryObjectById() {
        IncSubCategory actualFromSubCategoryRequestToSubCategoryObjectByIdResult = incSubCategoryMapper
                .fromSubCategoryRequestToSubCategoryObjectById(new IncSubCategoryRequest());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getCreatedOn());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getUpdatedOn());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getIncSubSubCategories());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getIncSubCategoryName());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getIncSubCategoryDescription());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getId());
        IncCategory incCategory = actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getIncCategory();
        assertNull(incCategory.getUpdatedOn());
        assertNull(incCategory.getIncSubCategories());
        assertNull(incCategory.getIncCategoryName());
        assertNull(incCategory.getIncCategoryDescription());
        assertNull(incCategory.getId());
        assertNull(incCategory.getCreatedOn());
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromSubCategoryRequestToSubCategoryObjectById(IncSubCategoryRequest)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testFromSubCategoryRequestToSubCategoryObjectById2() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryRequest.getCategoryId()" because "categoryRequest" is null
        //       at com.app.assigmentsubmitionapp.mapper.incomes.IncSubCategoryMapper.fromSubCategoryRequestToSubCategoryObjectById(IncSubCategoryMapper.java:57)
        //   See https://diff.blue/R013 to resolve this issue.

        incSubCategoryMapper.fromSubCategoryRequestToSubCategoryObjectById(null);
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromSubCategoryRequestToSubCategoryObjectById(IncSubCategoryRequest)}
     */
    @Test
    void testFromSubCategoryRequestToSubCategoryObjectById3() {
        IncSubCategoryRequest incSubCategoryRequest = mock(IncSubCategoryRequest.class);
        when(incSubCategoryRequest.getSubCategoryDescription()).thenReturn("Sub Category Description");
        when(incSubCategoryRequest.getSubCategoryName()).thenReturn("Sub Category Name");
        UUID randomUUIDResult = UUID.randomUUID();
        when(incSubCategoryRequest.getCategoryId()).thenReturn(randomUUIDResult);
        IncSubCategory actualFromSubCategoryRequestToSubCategoryObjectByIdResult = incSubCategoryMapper
                .fromSubCategoryRequestToSubCategoryObjectById(incSubCategoryRequest);
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getCreatedOn());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getUpdatedOn());
        assertNull(actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getIncSubSubCategories());
        assertEquals("Sub Category Name",
                actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getIncSubCategoryName());
        assertEquals("Sub Category Description",
                actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getIncSubCategoryDescription());
        UUID id = actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getId();
        assertSame(randomUUIDResult, id);
        IncCategory incCategory = actualFromSubCategoryRequestToSubCategoryObjectByIdResult.getIncCategory();
        assertNull(incCategory.getUpdatedOn());
        assertNull(incCategory.getIncSubCategories());
        assertNull(incCategory.getIncCategoryName());
        assertNull(incCategory.getIncCategoryDescription());
        assertSame(id, incCategory.getId());
        assertNull(incCategory.getCreatedOn());
        verify(incSubCategoryRequest).getSubCategoryDescription();
        verify(incSubCategoryRequest).getSubCategoryName();
        verify(incSubCategoryRequest, atLeast(1)).getCategoryId();
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromSubCategoryObjectToSubCategoryResponse(IncSubCategory)}
     */
    @Test
    void testFromSubCategoryObjectToSubCategoryResponse() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        incSubCategory.setCreatedOn(fromResult);
        UUID randomUUIDResult = UUID.randomUUID();
        incSubCategory.setId(randomUUIDResult);
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        incSubCategory.setUpdatedOn(fromResult1);
        IncSubCategoryResponse actualFromSubCategoryObjectToSubCategoryResponseResult = incSubCategoryMapper
                .fromSubCategoryObjectToSubCategoryResponse(incSubCategory);
        assertSame(fromResult, actualFromSubCategoryObjectToSubCategoryResponseResult.getCreatedOn());
        assertSame(fromResult1, actualFromSubCategoryObjectToSubCategoryResponseResult.getUpdatedOn());
        assertEquals("Inc Sub Category Name",
                actualFromSubCategoryObjectToSubCategoryResponseResult.getSubCategoryName());
        assertEquals("Inc Sub Category Description",
                actualFromSubCategoryObjectToSubCategoryResponseResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, actualFromSubCategoryObjectToSubCategoryResponseResult.getId());
    }

    /**
     * Method under test: {@link IncSubCategoryMapper#fromSubCategoryObjectToSubCategoryResponse(IncSubCategory)}
     */
    @Test
    void testFromSubCategoryObjectToSubCategoryResponse2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));
        IncSubCategory incSubCategory = mock(IncSubCategory.class);
        when(incSubCategory.getIncSubCategoryDescription()).thenReturn("Inc Sub Category Description");
        when(incSubCategory.getIncSubCategoryName()).thenReturn("Inc Sub Category Name");
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant());
        when(incSubCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant());
        when(incSubCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(incSubCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(incSubCategory).setCreatedOn((Date) any());
        doNothing().when(incSubCategory).setId((UUID) any());
        doNothing().when(incSubCategory).setIncCategory((IncCategory) any());
        doNothing().when(incSubCategory).setIncSubCategoryDescription((String) any());
        doNothing().when(incSubCategory).setIncSubCategoryName((String) any());
        doNothing().when(incSubCategory).setIncSubSubCategories((Set<IncSubSubCategory>) any());
        doNothing().when(incSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant()));
        IncSubCategoryResponse actualFromSubCategoryObjectToSubCategoryResponseResult = incSubCategoryMapper
                .fromSubCategoryObjectToSubCategoryResponse(incSubCategory);
        assertSame(fromResult, actualFromSubCategoryObjectToSubCategoryResponseResult.getCreatedOn());
        assertSame(fromResult1, actualFromSubCategoryObjectToSubCategoryResponseResult.getUpdatedOn());
        assertEquals("Inc Sub Category Name",
                actualFromSubCategoryObjectToSubCategoryResponseResult.getSubCategoryName());
        assertEquals("Inc Sub Category Description",
                actualFromSubCategoryObjectToSubCategoryResponseResult.getSubCategoryDescription());
        assertSame(randomUUIDResult, actualFromSubCategoryObjectToSubCategoryResponseResult.getId());
        verify(incSubCategory).getIncSubCategoryDescription();
        verify(incSubCategory).getIncSubCategoryName();
        verify(incSubCategory).getCreatedOn();
        verify(incSubCategory).getUpdatedOn();
        verify(incSubCategory).getId();
        verify(incSubCategory).setCreatedOn((Date) any());
        verify(incSubCategory).setId((UUID) any());
        verify(incSubCategory).setIncCategory((IncCategory) any());
        verify(incSubCategory).setIncSubCategoryDescription((String) any());
        verify(incSubCategory).setIncSubCategoryName((String) any());
        verify(incSubCategory).setIncSubSubCategories((Set<IncSubSubCategory>) any());
        verify(incSubCategory).setUpdatedOn((Date) any());
    }
}

