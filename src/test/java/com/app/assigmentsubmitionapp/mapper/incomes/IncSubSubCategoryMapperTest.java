package com.app.assigmentsubmitionapp.mapper.incomes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryResponse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {IncSubSubCategoryMapper.class})
@ExtendWith(SpringExtension.class)
class IncSubSubCategoryMapperTest {
    @Autowired
    private IncSubSubCategoryMapper incSubSubCategoryMapper;

    /**
     * Method under test: {@link IncSubSubCategoryMapper#fromListSubCategoryObjectToOptionalSubSubCategoryResponse(List)}
     */
    @Test
    void testFromListSubCategoryObjectToOptionalSubSubCategoryResponse() {
        assertTrue(incSubSubCategoryMapper.fromListSubCategoryObjectToOptionalSubSubCategoryResponse(new ArrayList<>())
                .isPresent());
    }

    /**
     * Method under test: {@link IncSubSubCategoryMapper#fromListSubCategoryObjectToOptionalSubSubCategoryResponse(List)}
     */
    @Test
    void testFromListSubCategoryObjectToOptionalSubSubCategoryResponse2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        incSubSubCategory.setCreatedOn(fromResult);
        UUID randomUUIDResult = UUID.randomUUID();
        incSubSubCategory.setId(randomUUIDResult);
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        incSubSubCategory.setUpdatedOn(fromResult1);

        ArrayList<IncSubSubCategory> incSubSubCategoryList = new ArrayList<>();
        incSubSubCategoryList.add(incSubSubCategory);
        Optional<List<IncSubSubCategoryResponse>> actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult = incSubSubCategoryMapper
                .fromListSubCategoryObjectToOptionalSubSubCategoryResponse(incSubSubCategoryList);
        assertTrue(actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult.isPresent());
        IncSubSubCategoryResponse getResult = actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult.get()
                .get(0);
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Inc Sub Sub Category Name", getResult.getSubSubCategoryName());
        assertEquals("Inc Sub Sub Category Description", getResult.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link IncSubSubCategoryMapper#fromListSubCategoryObjectToOptionalSubSubCategoryResponse(List)}
     */
    @Test
    void testFromListSubCategoryObjectToOptionalSubSubCategoryResponse3() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        incSubSubCategory.setCreatedOn(fromResult);
        UUID randomUUIDResult = UUID.randomUUID();
        incSubSubCategory.setId(randomUUIDResult);
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        incSubSubCategory.setUpdatedOn(fromResult1);

        IncCategory incCategory1 = new IncCategory();
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory1.setId(UUID.randomUUID());
        incCategory1.setIncCategoryDescription("Inc Category Description");
        incCategory1.setIncCategoryName("Inc Category Name");
        incCategory1.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory1.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory1 = new IncSubCategory();
        LocalDateTime atStartOfDayResult8 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setCreatedOn(Date.from(atStartOfDayResult8.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory1.setId(UUID.randomUUID());
        incSubCategory1.setIncCategory(incCategory1);
        incSubCategory1.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory1.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory1.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult9 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory1.setUpdatedOn(Date.from(atStartOfDayResult9.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory1 = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult10 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult2 = Date.from(atStartOfDayResult10.atZone(ZoneId.of("UTC")).toInstant());
        incSubSubCategory1.setCreatedOn(fromResult2);
        UUID randomUUIDResult1 = UUID.randomUUID();
        incSubSubCategory1.setId(randomUUIDResult1);
        incSubSubCategory1.setIncSubCategory(incSubCategory1);
        incSubSubCategory1.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory1.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult11 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult3 = Date.from(atStartOfDayResult11.atZone(ZoneId.of("UTC")).toInstant());
        incSubSubCategory1.setUpdatedOn(fromResult3);

        ArrayList<IncSubSubCategory> incSubSubCategoryList = new ArrayList<>();
        incSubSubCategoryList.add(incSubSubCategory1);
        incSubSubCategoryList.add(incSubSubCategory);
        Optional<List<IncSubSubCategoryResponse>> actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult = incSubSubCategoryMapper
                .fromListSubCategoryObjectToOptionalSubSubCategoryResponse(incSubSubCategoryList);
        assertTrue(actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult.isPresent());
        List<IncSubSubCategoryResponse> getResult = actualFromListSubCategoryObjectToOptionalSubSubCategoryResponseResult
                .get();
        IncSubSubCategoryResponse getResult1 = getResult.get(0);
        assertSame(fromResult3, getResult1.getUpdatedOn());
        IncSubSubCategoryResponse getResult2 = getResult.get(1);
        assertSame(fromResult1, getResult2.getUpdatedOn());
        assertEquals("Inc Sub Sub Category Name", getResult2.getSubSubCategoryName());
        assertEquals("Inc Sub Sub Category Description", getResult2.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, getResult2.getId());
        assertSame(fromResult, getResult2.getCreatedOn());
        assertSame(fromResult2, getResult1.getCreatedOn());
        assertEquals("Inc Sub Sub Category Description", getResult1.getSubSubCategoryDescription());
        assertEquals("Inc Sub Sub Category Name", getResult1.getSubSubCategoryName());
        assertSame(randomUUIDResult1, getResult1.getId());
    }

    /**
     * Method under test: {@link IncSubSubCategoryMapper#fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(Optional)}
     */
    @Test
    void testFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        incSubSubCategory.setCreatedOn(fromResult);
        UUID randomUUIDResult = UUID.randomUUID();
        incSubSubCategory.setId(randomUUIDResult);
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        incSubSubCategory.setUpdatedOn(fromResult1);
        Optional<IncSubSubCategoryResponse> actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult = incSubSubCategoryMapper
                .fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(Optional.of(incSubSubCategory));
        assertTrue(actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult.isPresent());
        IncSubSubCategoryResponse getResult = actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult
                .get();
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Inc Sub Sub Category Name", getResult.getSubSubCategoryName());
        assertEquals("Inc Sub Sub Category Description", getResult.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
    }

    /**
     * Method under test: {@link IncSubSubCategoryMapper#fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(Optional)}
     */
    @Test
    void testFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        IncSubSubCategory incSubSubCategory = mock(IncSubSubCategory.class);
        when(incSubSubCategory.getIncSubSubCategoryDescription()).thenReturn("Inc Sub Sub Category Description");
        when(incSubSubCategory.getIncSubSubCategoryName()).thenReturn("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        when(incSubSubCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        when(incSubSubCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(incSubSubCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(incSubSubCategory).setCreatedOn((Date) any());
        doNothing().when(incSubSubCategory).setId((UUID) any());
        doNothing().when(incSubSubCategory).setIncSubCategory((IncSubCategory) any());
        doNothing().when(incSubSubCategory).setIncSubSubCategoryDescription((String) any());
        doNothing().when(incSubSubCategory).setIncSubSubCategoryName((String) any());
        doNothing().when(incSubSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory.setId(UUID.randomUUID());
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        Optional<IncSubSubCategoryResponse> actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult = incSubSubCategoryMapper
                .fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(Optional.of(incSubSubCategory));
        assertTrue(actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult.isPresent());
        IncSubSubCategoryResponse getResult = actualFromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponseResult
                .get();
        assertSame(fromResult, getResult.getCreatedOn());
        assertSame(fromResult1, getResult.getUpdatedOn());
        assertEquals("Inc Sub Sub Category Name", getResult.getSubSubCategoryName());
        assertEquals("Inc Sub Sub Category Description", getResult.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, getResult.getId());
        verify(incSubSubCategory).getIncSubSubCategoryDescription();
        verify(incSubSubCategory).getIncSubSubCategoryName();
        verify(incSubSubCategory).getCreatedOn();
        verify(incSubSubCategory).getUpdatedOn();
        verify(incSubSubCategory).getId();
        verify(incSubSubCategory).setCreatedOn((Date) any());
        verify(incSubSubCategory).setId((UUID) any());
        verify(incSubSubCategory).setIncSubCategory((IncSubCategory) any());
        verify(incSubSubCategory).setIncSubSubCategoryDescription((String) any());
        verify(incSubSubCategory).setIncSubSubCategoryName((String) any());
        verify(incSubSubCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryMapper#fromSubSubCategoryObjectToSubSubCategoryResponse(IncSubSubCategory)}
     */
    @Test
    void testFromSubSubCategoryObjectToSubSubCategoryResponse() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubSubCategory incSubSubCategory = new IncSubSubCategory();
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        incSubSubCategory.setCreatedOn(fromResult);
        UUID randomUUIDResult = UUID.randomUUID();
        incSubSubCategory.setId(randomUUIDResult);
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        incSubSubCategory.setUpdatedOn(fromResult1);
        IncSubSubCategoryResponse actualFromSubSubCategoryObjectToSubSubCategoryResponseResult = incSubSubCategoryMapper
                .fromSubSubCategoryObjectToSubSubCategoryResponse(incSubSubCategory);
        assertSame(fromResult, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getCreatedOn());
        assertSame(fromResult1, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getUpdatedOn());
        assertEquals("Inc Sub Sub Category Name",
                actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getSubSubCategoryName());
        assertEquals("Inc Sub Sub Category Description",
                actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getId());
    }

    /**
     * Method under test: {@link IncSubSubCategoryMapper#fromSubSubCategoryObjectToSubSubCategoryResponse(IncSubSubCategory)}
     */
    @Test
    void testFromSubSubCategoryObjectToSubSubCategoryResponse2() {
        IncCategory incCategory = new IncCategory();
        LocalDateTime atStartOfDayResult = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setCreatedOn(Date.from(atStartOfDayResult.atZone(ZoneId.of("UTC")).toInstant()));
        incCategory.setId(UUID.randomUUID());
        incCategory.setIncCategoryDescription("Inc Category Description");
        incCategory.setIncCategoryName("Inc Category Name");
        incCategory.setIncSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult1 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incCategory.setUpdatedOn(Date.from(atStartOfDayResult1.atZone(ZoneId.of("UTC")).toInstant()));

        IncSubCategory incSubCategory = new IncSubCategory();
        LocalDateTime atStartOfDayResult2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setCreatedOn(Date.from(atStartOfDayResult2.atZone(ZoneId.of("UTC")).toInstant()));
        incSubCategory.setId(UUID.randomUUID());
        incSubCategory.setIncCategory(incCategory);
        incSubCategory.setIncSubCategoryDescription("Inc Sub Category Description");
        incSubCategory.setIncSubCategoryName("Inc Sub Category Name");
        incSubCategory.setIncSubSubCategories(new HashSet<>());
        LocalDateTime atStartOfDayResult3 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubCategory.setUpdatedOn(Date.from(atStartOfDayResult3.atZone(ZoneId.of("UTC")).toInstant()));
        IncSubSubCategory incSubSubCategory = mock(IncSubSubCategory.class);
        when(incSubSubCategory.getIncSubSubCategoryDescription()).thenReturn("Inc Sub Sub Category Description");
        when(incSubSubCategory.getIncSubSubCategoryName()).thenReturn("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult4 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult = Date.from(atStartOfDayResult4.atZone(ZoneId.of("UTC")).toInstant());
        when(incSubSubCategory.getCreatedOn()).thenReturn(fromResult);
        LocalDateTime atStartOfDayResult5 = LocalDate.of(1970, 1, 1).atStartOfDay();
        Date fromResult1 = Date.from(atStartOfDayResult5.atZone(ZoneId.of("UTC")).toInstant());
        when(incSubSubCategory.getUpdatedOn()).thenReturn(fromResult1);
        UUID randomUUIDResult = UUID.randomUUID();
        when(incSubSubCategory.getId()).thenReturn(randomUUIDResult);
        doNothing().when(incSubSubCategory).setCreatedOn((Date) any());
        doNothing().when(incSubSubCategory).setId((UUID) any());
        doNothing().when(incSubSubCategory).setIncSubCategory((IncSubCategory) any());
        doNothing().when(incSubSubCategory).setIncSubSubCategoryDescription((String) any());
        doNothing().when(incSubSubCategory).setIncSubSubCategoryName((String) any());
        doNothing().when(incSubSubCategory).setUpdatedOn((Date) any());
        LocalDateTime atStartOfDayResult6 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setCreatedOn(Date.from(atStartOfDayResult6.atZone(ZoneId.of("UTC")).toInstant()));
        incSubSubCategory.setId(UUID.randomUUID());
        incSubSubCategory.setIncSubCategory(incSubCategory);
        incSubSubCategory.setIncSubSubCategoryDescription("Inc Sub Sub Category Description");
        incSubSubCategory.setIncSubSubCategoryName("Inc Sub Sub Category Name");
        LocalDateTime atStartOfDayResult7 = LocalDate.of(1970, 1, 1).atStartOfDay();
        incSubSubCategory.setUpdatedOn(Date.from(atStartOfDayResult7.atZone(ZoneId.of("UTC")).toInstant()));
        IncSubSubCategoryResponse actualFromSubSubCategoryObjectToSubSubCategoryResponseResult = incSubSubCategoryMapper
                .fromSubSubCategoryObjectToSubSubCategoryResponse(incSubSubCategory);
        assertSame(fromResult, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getCreatedOn());
        assertSame(fromResult1, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getUpdatedOn());
        assertEquals("Inc Sub Sub Category Name",
                actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getSubSubCategoryName());
        assertEquals("Inc Sub Sub Category Description",
                actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getSubSubCategoryDescription());
        assertSame(randomUUIDResult, actualFromSubSubCategoryObjectToSubSubCategoryResponseResult.getId());
        verify(incSubSubCategory).getIncSubSubCategoryDescription();
        verify(incSubSubCategory).getIncSubSubCategoryName();
        verify(incSubSubCategory).getCreatedOn();
        verify(incSubSubCategory).getUpdatedOn();
        verify(incSubSubCategory).getId();
        verify(incSubSubCategory).setCreatedOn((Date) any());
        verify(incSubSubCategory).setId((UUID) any());
        verify(incSubSubCategory).setIncSubCategory((IncSubCategory) any());
        verify(incSubSubCategory).setIncSubSubCategoryDescription((String) any());
        verify(incSubSubCategory).setIncSubSubCategoryName((String) any());
        verify(incSubSubCategory).setUpdatedOn((Date) any());
    }

    /**
     * Method under test: {@link IncSubSubCategoryMapper#fromSubSubCategoryRequestToSubSubCategoryObject(IncSubSubCategoryRequest)}
     */
    @Test
    void testFromSubSubCategoryRequestToSubSubCategoryObject() {
        IncSubSubCategory actualFromSubSubCategoryRequestToSubSubCategoryObjectResult = incSubSubCategoryMapper
                .fromSubSubCategoryRequestToSubSubCategoryObject(new IncSubSubCategoryRequest());
        assertNull(actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getIncSubSubCategoryName());
        assertNull(actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getIncSubSubCategoryDescription());
        assertNull(actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getId());
        IncSubCategory incSubCategory = actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getIncSubCategory();
        assertNull(incSubCategory.getUpdatedOn());
        assertNull(incSubCategory.getIncSubSubCategories());
        assertNull(incSubCategory.getIncSubCategoryName());
        assertNull(incSubCategory.getIncSubCategoryDescription());
        assertNull(incSubCategory.getIncCategory());
        assertNull(incSubCategory.getId());
        assertNull(incSubCategory.getCreatedOn());
    }

    /**
     * Method under test: {@link IncSubSubCategoryMapper#fromSubSubCategoryRequestToSubSubCategoryObject(IncSubSubCategoryRequest)}
     */
    @Test
    void testFromSubSubCategoryRequestToSubSubCategoryObject3() {
        IncSubSubCategoryRequest incSubSubCategoryRequest = mock(IncSubSubCategoryRequest.class);
        when(incSubSubCategoryRequest.getSubSubCategoryDescription()).thenReturn("Sub Sub Category Description");
        when(incSubSubCategoryRequest.getSubSubCategoryName()).thenReturn("Sub Sub Category Name");
        UUID randomUUIDResult = UUID.randomUUID();
        when(incSubSubCategoryRequest.getSubCategoryId()).thenReturn(randomUUIDResult);
        IncSubSubCategory actualFromSubSubCategoryRequestToSubSubCategoryObjectResult = incSubSubCategoryMapper
                .fromSubSubCategoryRequestToSubSubCategoryObject(incSubSubCategoryRequest);
        assertEquals("Sub Sub Category Name",
                actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getIncSubSubCategoryName());
        assertEquals("Sub Sub Category Description",
                actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getIncSubSubCategoryDescription());
        assertNull(actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getId());
        IncSubCategory incSubCategory = actualFromSubSubCategoryRequestToSubSubCategoryObjectResult.getIncSubCategory();
        assertNull(incSubCategory.getUpdatedOn());
        assertNull(incSubCategory.getIncSubSubCategories());
        assertNull(incSubCategory.getIncSubCategoryName());
        assertNull(incSubCategory.getIncSubCategoryDescription());
        assertNull(incSubCategory.getIncCategory());
        assertSame(randomUUIDResult, incSubCategory.getId());
        assertNull(incSubCategory.getCreatedOn());
        verify(incSubSubCategoryRequest).getSubSubCategoryDescription();
        verify(incSubSubCategoryRequest).getSubSubCategoryName();
        verify(incSubSubCategoryRequest).getSubCategoryId();
    }
}

