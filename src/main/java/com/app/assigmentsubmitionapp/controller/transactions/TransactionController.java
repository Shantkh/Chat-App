package com.app.assigmentsubmitionapp.controller.transactions;

import com.app.assigmentsubmitionapp.api.transactional.TransactionsApi;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.neo4j.UsersNode;
import com.app.assigmentsubmitionapp.service.transaction.impl.TransactionServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class TransactionController implements TransactionsApi {

    private final TransactionServiceImpl transactionService;

    @Override
    public ResponseEntity<ResponseBuilder<?>> createNodeUser(UsersNode usersNode) {
        var result = transactionService.createUserNode(usersNode);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> createExpense(final List<List<String>> expenseActions,
                                                            final UUID expenseId) {
        Optional<ResponseBuilder<?>> result = transactionService.createExpense(expenseActions, expenseId);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> getCountriesWithUsersCount() {
        //todo: add Pagination and sorting
        Optional<ResponseBuilder<?>> result = transactionService.getAllCountriesWithUsersCount();
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> getCountries() {
        //todo: add Pagination and sorting
        Optional<ResponseBuilder<?>> result = transactionService.getAllCountries();
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> getCountriesWithUsersPercentage() {
        Optional<ResponseBuilder<?>> result = transactionService.getAllCountriesWithUsersPercentage();
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }
}
