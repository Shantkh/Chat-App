package com.app.assigmentsubmitionapp.controller;

import com.app.assigmentsubmitionapp.api.AdminApi;
import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.service.jwtusers.impl.JwtUserServiceImpl;
import com.app.assigmentsubmitionapp.user.PaginationWithSorting;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class AdminController implements AdminApi {

    private final JwtUserServiceImpl jwtUserService;

    @Override
    public ResponseEntity<ResponseBuilder<?>> getUsersList(PaginationWithSorting paginationWithSorting) throws ApplicationExceptions {
        Optional<ResponseBuilder<?>> result = jwtUserService.getJwtUsers(paginationWithSorting);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }
}
