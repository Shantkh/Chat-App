package com.app.assigmentsubmitionapp.controller.incomes;

import com.app.assigmentsubmitionapp.api.expenses.ExpSubSubCategoryApi;
import com.app.assigmentsubmitionapp.api.incomes.IncSubSubCategoryApi;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.service.expenses.impl.ExpSubSubCategoryServiceImpl;
import com.app.assigmentsubmitionapp.service.incomes.impl.IncSubSubCategoryServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class SubSubIncCategoryController implements IncSubSubCategoryApi {

    private final IncSubSubCategoryServiceImpl expSubSubCategoryService;

    @Override
    public ResponseEntity<ResponseBuilder<?>> allIncSubCategories(UUID subSubIncCategoryId) {
        Optional<ResponseBuilder<?>> result = expSubSubCategoryService.subSubCategories(subSubIncCategoryId);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> subIncCategoriesByExpCategoryId(UUID subSubExpCategoryId) {
        Optional<ResponseBuilder<?>> result = expSubSubCategoryService.subSubCategoriesBySubSubId(subSubExpCategoryId);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }


    @Override
    public ResponseEntity<ResponseBuilder<?>> updateIncSubSubCategory(IncSubSubCategoryUpdateRequest subCategoryUpdateRequest) {
        Optional<ResponseBuilder<?>> result = expSubSubCategoryService.updateSubSubCategory(subCategoryUpdateRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> deleteIncSubSubCategory(UUID id) {
        Optional<ResponseBuilder<?>> result = expSubSubCategoryService.deleteSubSubCategory(id);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> addIncSubCategory(IncSubSubCategoryRequest categoryRequest) {
        Optional<ResponseBuilder<?>> result = expSubSubCategoryService.addSubSubCategory(categoryRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }
}
