package com.app.assigmentsubmitionapp.controller.incomes;

import com.app.assigmentsubmitionapp.api.incomes.IncCategoryApi;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncCategoryRequest;
import com.app.assigmentsubmitionapp.service.incomes.impl.IncCategoryServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class IncCategoryController implements IncCategoryApi {

    private final IncCategoryServiceImpl incCategoryServiceImpl;

    @Override
    public ResponseEntity<ResponseBuilder<?>> addIncCategory(IncCategoryRequest incCategoryRequest) {
        Optional<ResponseBuilder<?>> result = incCategoryServiceImpl.addCategory(incCategoryRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> deleteIncCategory(UUID id) {
        Optional<ResponseBuilder<?>> result = incCategoryServiceImpl.deleteCategory(id);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> allIncCategories() {
        Optional<ResponseBuilder<?>> result = incCategoryServiceImpl.categories();
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> updateIncCategory(IncCategoryRequest categoryRequest) {
        Optional<ResponseBuilder<?>> result = incCategoryServiceImpl.updateCategory(categoryRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }
}
