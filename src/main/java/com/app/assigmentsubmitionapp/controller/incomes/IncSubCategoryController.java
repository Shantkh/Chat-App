package com.app.assigmentsubmitionapp.controller.incomes;

import com.app.assigmentsubmitionapp.api.incomes.IncSubCategoryApi;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.service.incomes.impl.IncSubCategoryServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class IncSubCategoryController implements IncSubCategoryApi {

    private final IncSubCategoryServiceImpl subCategoryService;
    @Override
    public ResponseEntity<ResponseBuilder<?>> allIncSubCategories(UUID categoryId) {
        Optional<ResponseBuilder<?>> result = subCategoryService.subCategories(categoryId);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> subIncCategoriesByIncCategoryId(UUID subCategoryId) {
        Optional<ResponseBuilder<?>> result = subCategoryService.subCategoriesBySubId(subCategoryId);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> updateIncSubCategory(IncSubCategoryUpdateRequest subCategoryUpdateRequest) {
        Optional<ResponseBuilder<?>> result = subCategoryService.updateSubCategory(subCategoryUpdateRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> addIncSubCategory(IncSubCategoryRequest categoryRequest) {
        Optional<ResponseBuilder<?>> result = subCategoryService.addSubCategory(categoryRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> deleteIncSubCategory(UUID id) {
        Optional<ResponseBuilder<?>> result = subCategoryService.deleteSubCategory(id);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }
}
