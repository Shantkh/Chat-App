package com.app.assigmentsubmitionapp.controller.expense;

import com.app.assigmentsubmitionapp.api.expenses.ExpSubCategoryApi;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.service.expenses.impl.ExpSubCategoryServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ExSubCategoryController implements ExpSubCategoryApi {

    private final ExpSubCategoryServiceImpl subCategoryService;
    @Override
    public ResponseEntity<ResponseBuilder<?>> allExpSubCategories(UUID categoryId) {
        Optional<ResponseBuilder<?>> result = subCategoryService.subCategories(categoryId);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> subExpCategoriesByExpCategoryId(UUID subCategoryId) {
        Optional<ResponseBuilder<?>> result = subCategoryService.subCategoriesBySubId(subCategoryId);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> updateExpSubCategory(ExpSubCategoryUpdateRequest subCategoryUpdateRequest) {
        Optional<ResponseBuilder<?>> result = subCategoryService.updateSubCategory(subCategoryUpdateRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> addExpSubCategory(ExpSubCategoryRequest categoryRequest) {
        Optional<ResponseBuilder<?>> result = subCategoryService.addSubCategory(categoryRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> deleteExpSubCategory(UUID id) {
        Optional<ResponseBuilder<?>> result = subCategoryService.deleteSubCategory(id);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }
}
