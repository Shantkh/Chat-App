package com.app.assigmentsubmitionapp.controller.expense;

import com.app.assigmentsubmitionapp.api.expenses.ExpSubSubCategoryApi;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.service.expenses.impl.ExpSubCategoryServiceImpl;
import com.app.assigmentsubmitionapp.service.expenses.impl.ExpSubSubCategoryServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;
@RestController
@RequiredArgsConstructor
public class SubSubExpCategoryController implements ExpSubSubCategoryApi {

    private final ExpSubSubCategoryServiceImpl expSubSubCategoryService;

    @Override
    public ResponseEntity<ResponseBuilder<?>> allExpSubCategories(UUID subSubExpCategoryId) {
        Optional<ResponseBuilder<?>> result = expSubSubCategoryService.subSubCategories(subSubExpCategoryId);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> subExpCategoriesByExpCategoryId(UUID subSubExpCategoryId) {
        Optional<ResponseBuilder<?>> result = expSubSubCategoryService.subSubCategoriesBySubSubId(subSubExpCategoryId);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }


    @Override
    public ResponseEntity<ResponseBuilder<?>> updateExpSubSubCategory(ExpSubSubCategoryUpdateRequest subCategoryUpdateRequest) {
        Optional<ResponseBuilder<?>> result = expSubSubCategoryService.updateSubSubCategory(subCategoryUpdateRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> deleteExpSubSubCategory(UUID id) {
        Optional<ResponseBuilder<?>> result = expSubSubCategoryService.deleteSubSubCategory(id);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> addExpSubCategory(ExpSubSubCategoryRequest categoryRequest) {
        Optional<ResponseBuilder<?>> result = expSubSubCategoryService.addSubSubCategory(categoryRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }
}
