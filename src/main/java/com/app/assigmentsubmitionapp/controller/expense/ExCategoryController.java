package com.app.assigmentsubmitionapp.controller.expense;

import com.app.assigmentsubmitionapp.api.expenses.ExpCategoryApi;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryRequest;
import com.app.assigmentsubmitionapp.service.expenses.impl.ExpCategoryServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ExCategoryController implements ExpCategoryApi {
    private final ExpCategoryServiceImpl categoryService;

    @Override
    public ResponseEntity<ResponseBuilder<?>> allExpCategories() {
        Optional<ResponseBuilder<?>> result = categoryService.categories();
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> updateExpCategory(ExpCategoryRequest categoryRequest){
        Optional<ResponseBuilder<?>> result = categoryService.updateCategory(categoryRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> deleteExpCategory(UUID id) {
        Optional<ResponseBuilder<?>> result = categoryService.deleteCategory(id);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> addExpCategory(ExpCategoryRequest categoryRequest) {
        Optional<ResponseBuilder<?>> result = categoryService.addCategory(categoryRequest);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }
}
