package com.app.assigmentsubmitionapp.controller;

import com.app.assigmentsubmitionapp.api.WebApi;
import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.modules.ForgotPasswordRequest;
import com.app.assigmentsubmitionapp.modules.NewPasswordRequest;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.service.jwtusers.impl.JwtUserServiceImpl;
import com.app.assigmentsubmitionapp.user.dto.UserRegistrationRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class WebController implements WebApi {
    private final JwtUserServiceImpl jwtUserService;

    @Override
    public ResponseEntity<ResponseBuilder<?>> save(UserRegistrationRequest userRegistrationRequest,
                                                   HttpServletRequest request) throws ApplicationExceptions, IOException {
        Optional<ResponseBuilder<?>> result = jwtUserService.register(userRegistrationRequest,request);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> accountVerification(String email, String code) {
        Optional<ResponseBuilder<?>> response = jwtUserService.activateAccount(email, code);
        return new ResponseEntity<>(response.get(), response.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> forgotPassword(ForgotPasswordRequest forgotPasswordRequest,
                                                             HttpServletRequest request) throws ApplicationExceptions {
        Optional<ResponseBuilder<?>> response = jwtUserService.forgotPassword(forgotPasswordRequest,request);
        return new ResponseEntity<>(response.get(), response.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> forgotPasswordVerificationEmailCode(String email, String code) {
        Optional<ResponseBuilder<?>> response = jwtUserService.forgotPasswordVerification(email, code);
        return new ResponseEntity<>(response.get(), response.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> updatePassword(NewPasswordRequest newPasswordRequest) {
        Optional<ResponseBuilder<?>> response = jwtUserService.updatePasswordRequest(newPasswordRequest);
        return new ResponseEntity<>(response.get(), response.get().getHttpStatus());
    }
}
