package com.app.assigmentsubmitionapp.controller;

import com.app.assigmentsubmitionapp.api.UserApi;
import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.localsettings.LocalSettings;
import com.app.assigmentsubmitionapp.service.user.settings.UserSettingsImpl;
import com.app.assigmentsubmitionapp.user.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserSettingsImpl userSettingsImpl;

    @Override
    public ResponseEntity<ResponseBuilder<?>> changePassword(UserChangePassword userChangePassword) throws ApplicationExceptions {
        return null;
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> changeRole(UserChangeRole userChangeRole) throws ApplicationExceptions {
        return null;
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> closeAccount(UserCloseAccount userCloseAccount) throws ApplicationExceptions {
        return null;
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> localSettingUpdate(LocalSettings localSettings) throws IllegalAccessException {
        Optional<ResponseBuilder<?>> result = userSettingsImpl.updateLocalSettings(localSettings);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }

    @Override
    public ResponseEntity<ResponseBuilder<?>> userLocalSetting(UserLocalSettings userLocalSettings) throws IllegalAccessException {
        Optional<ResponseBuilder<?>> result = userSettingsImpl.userLocalSettings(userLocalSettings);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }
}
