package com.app.assigmentsubmitionapp.controller;

import com.app.assigmentsubmitionapp.user.dto.UserRegistrationRequest;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;


public class ExampleController {

    @RequestMapping(value = "/register", method = RequestMethod.GET, produces = "application/json")
    @Operation(summary = "User registration with Role buyer/seller.")
    public ExampleMessage sellerEndpoint(@RequestBody UserRegistrationRequest userRegistrationRequest) {
        return new ExampleMessage("Hello seller!");
    }

    @GetMapping("/buyer")
    public ExampleMessage buyerEndpoint() {
        return new ExampleMessage("Hello buyer!");
    }

    @GetMapping("/admin")
    public ExampleMessage adminEndpoint() {
        return new ExampleMessage("Hello admin!");
    }

}