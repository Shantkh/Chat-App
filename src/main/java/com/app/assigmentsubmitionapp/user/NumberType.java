package com.app.assigmentsubmitionapp.user;

public enum NumberType {
    HOME,
    BUSINESS,
    PERSONAL,
    ASSISTANCE,
    ACCOUNTING,
    MANUFACTURE,
    NOT_SET
}
