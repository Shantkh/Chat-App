package com.app.assigmentsubmitionapp.user;

import com.app.assigmentsubmitionapp.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@DynamicInsert
public class PhoneNumbers {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "phone_number_id")
    private UUID id;

    @CreationTimestamp
    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    private Date createdOn;

    @UpdateTimestamp
    @Column(name = "updated_on")
    @Convert(converter = TimeDateConverter.class)
    private Date updatedOn;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column
    @Enumerated(EnumType.STRING)
    private NumberType numberTypes;

    @Column(columnDefinition = "boolean default false")
    private boolean isNumberVerified;

    @JsonIgnore
    @OneToOne
    @JoinColumn(nullable = false, name = "user_id")
    private JwtUser user;

}
