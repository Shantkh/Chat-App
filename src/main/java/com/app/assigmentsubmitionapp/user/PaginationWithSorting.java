package com.app.assigmentsubmitionapp.user;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaginationWithSorting implements Serializable {
    private int pageNumber;
    private int contentSize;
    private String sortDirection;
    private String sortElement;
}
