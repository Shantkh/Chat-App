package com.app.assigmentsubmitionapp.user;

public enum Gender {
    MALE,FEMALE, NOT_SET, OTHER
}
