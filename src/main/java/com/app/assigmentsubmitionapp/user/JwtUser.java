package com.app.assigmentsubmitionapp.user;

import com.app.assigmentsubmitionapp.security.application.RefreshToken;
import com.app.assigmentsubmitionapp.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@EqualsAndHashCode()
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class JwtUser implements UserDetails {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "user_id")
    private UUID id;

    @CreationTimestamp
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    @Convert(converter = TimeDateConverter.class)
    private Date createdOn;

    @UpdateTimestamp
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    @Convert(converter = TimeDateConverter.class)
    private Date updatedOn;

    @Column
    private String firstname;

    @Column
    private String lastname;

    @Column
    private String username;

    @Column(unique = true)
    private String email;


    @Column(name = "password")
    @JsonIgnore
    private String password;

    @OneToOne
    @PrimaryKeyJoinColumn
    private UsersDetails usersDetails;

    @Column
    @Enumerated(EnumType.STRING)
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Role> role = new HashSet<>();


    @Column
    @Builder.Default
    private boolean userEmailVerified = false;
    @Column
    @Builder.Default
    private boolean enabled = false;

    @OneToOne
    @PrimaryKeyJoinColumn
    private RefreshToken refreshToken;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new HashSet<>();
        for (var r : this.role) {
            var sga = new SimpleGrantedAuthority(r.name());
            authorities.add(sga);
        }
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}