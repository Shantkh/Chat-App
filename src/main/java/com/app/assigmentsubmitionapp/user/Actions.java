package com.app.assigmentsubmitionapp.user;

public enum Actions {
    ADMIN_GET_ALL_USERS,
    NEW_USER_REGISTER,
    SELLER_LOGIN,
    ADMIN_LOGIN,
    BUYER_LOGIN,
    SELLER_LOGOUT,
    ADMIN_LOGOUT,
    BUYER_LOGOUT,
}
