package com.app.assigmentsubmitionapp.user.dto;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserCloseAccount {
    private UUID userId;
    private String email;
    private String reason;
}
