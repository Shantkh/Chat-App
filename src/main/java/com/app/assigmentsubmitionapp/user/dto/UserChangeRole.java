package com.app.assigmentsubmitionapp.user.dto;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserChangeRole {
    private UUID userId;
    private String email;
    private String oldRole;
    private String newRole;
    private String reason;
}
