package com.app.assigmentsubmitionapp.user.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
public class SettingUser {
    private String email;
}
