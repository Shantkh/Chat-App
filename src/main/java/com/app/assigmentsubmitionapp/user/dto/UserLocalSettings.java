package com.app.assigmentsubmitionapp.user.dto;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserLocalSettings {
    @Email
    @NotNull
    @NotBlank
    @NotEmpty
    private String userEmail;
}
