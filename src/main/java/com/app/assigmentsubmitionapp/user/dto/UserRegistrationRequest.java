package com.app.assigmentsubmitionapp.user.dto;

import com.app.assigmentsubmitionapp.user.Role;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserRegistrationRequest implements Serializable {

    @NotNull
    private String firstname;
    @NotNull
    private String lastname;
    @NotNull
    private String age;
    @NotNull
    private String Gender;
    @NotNull
    private Set<Role> roles;
    @NotNull
    private String email;
    @NotNull
    private String password;
    @NotNull
    private String country;
}
