package com.app.assigmentsubmitionapp.user.dto;

import com.app.assigmentsubmitionapp.user.Role;
import lombok.*;

import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserRegisterResponse {
    private UUID id;
    private String email;
    private Set<Role> roles;
    private boolean isEnabled;
    private boolean isEmailVerified;


}
