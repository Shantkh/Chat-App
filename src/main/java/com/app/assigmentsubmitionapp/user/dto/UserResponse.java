package com.app.assigmentsubmitionapp.user.dto;

import com.app.assigmentsubmitionapp.user.*;
import lombok.*;

import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserResponse {
    private UUID id;
    private String email;
    private String firstName;
    private String lastName;
    private UsersDetails usersDetails;
    private Set<Role> roles;
    private boolean isEnabled;
    private boolean isEmailVerified;


}
