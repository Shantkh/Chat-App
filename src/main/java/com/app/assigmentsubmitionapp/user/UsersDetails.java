package com.app.assigmentsubmitionapp.user;

import com.app.assigmentsubmitionapp.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "profile")
@EqualsAndHashCode
public class UsersDetails {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Column(name = "profile_id")
    private UUID id;

    @CreatedDate
    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    private Date createdOn;

    @CreatedDate
    @Column(name = "updated_on")
    @Convert(converter = TimeDateConverter.class)
    private Date updatedOn;

    @Column(name = "image_url")
    private String avatar;

    @Column(name = "address")
    @ElementCollection
    private Set<Address> addresses;

    @Column(name = "phone_numbers")
    @ElementCollection
    private Set<PhoneNumbers> phoneNumbers;

    @Column(name = "age")
    private String age;

    @Column
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column
    @Builder.Default
    private boolean userVerified = false;

    @JsonIgnore
    @OneToOne(mappedBy = "usersDetails")
    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    private JwtUser user;
}