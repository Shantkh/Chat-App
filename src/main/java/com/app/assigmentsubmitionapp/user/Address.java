package com.app.assigmentsubmitionapp.user;

import com.app.assigmentsubmitionapp.utils.PIIAttributeConverter;
import com.app.assigmentsubmitionapp.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Address {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    private UUID id;

    @CreationTimestamp
    @Convert(converter = TimeDateConverter.class)
    private Date createdOn;

    @UpdateTimestamp
    @Convert(converter = TimeDateConverter.class)
    private Date updatedOn;

    private String homeId;

    private String address;

    private String state;

    private String zipCode;

    private String city;

    private String country;

    @Column(columnDefinition = "boolean default false")
    private boolean isLocationValidated;

    @Column
    @Enumerated(EnumType.STRING)
    @Convert(converter = PIIAttributeConverter.class)
    private AddressType addressTypes;

    @JsonIgnore
    @OneToOne
    @JoinColumn(nullable = false, name = "user_id")
    private JwtUser user;


}
