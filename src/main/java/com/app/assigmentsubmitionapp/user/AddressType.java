package com.app.assigmentsubmitionapp.user;

public enum AddressType {
    HOME,
    BUSINESS,
    OFFICE,
    PRODUCTION,
    MANUFACTORY,
    OTHER,
    NOT_SET
}
