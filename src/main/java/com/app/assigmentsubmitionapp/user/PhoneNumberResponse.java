package com.app.assigmentsubmitionapp.user;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PhoneNumberResponse {
    private long Id;
    private UUID phoneNumberId;
    private String homePhoneNumber;
    private String workHomeNumber;
    private String mobileNumber;
    private boolean isHomeNumberVerified;
    private boolean isWorkNumberVerified;
    private boolean isMobileNumberVerified;


}
