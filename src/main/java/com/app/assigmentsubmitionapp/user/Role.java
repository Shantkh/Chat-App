package com.app.assigmentsubmitionapp.user;

public enum Role {
    ROLE_BUYER,
    ROLE_USER,
    ROLE_SELLER,
    ROLE_ADMIN
}
