package com.app.assigmentsubmitionapp.modules;

import com.app.assigmentsubmitionapp.utils.TimeDateConverter;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.http.HttpStatus;

import javax.persistence.Convert;
import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseBuilder<T>{
    private Object data;
    private HttpStatus httpStatus;
    private Integer httpStatusCode;
    private Boolean isSuccess;
    @CreationTimestamp
//    @Convert(converter = TimeDateConverter.class)
    private Timestamp date;
}