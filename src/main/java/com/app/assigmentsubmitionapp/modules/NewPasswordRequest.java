package com.app.assigmentsubmitionapp.modules;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NewPasswordRequest implements Serializable {
    @NotNull
    @NotBlank
    private String id;

    @NotNull
    @NotBlank
    private String password;
}