package com.app.assigmentsubmitionapp.modules;


import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Entity
@Data(staticConstructor = "of")
@Table(name = "email_details")
public class EmailDetails {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "recipient")
    private String recipient;

    @Column(name = "msg_bdy")
    private String msgBody;

    @Column(name = "subject")
    private String subject;

    @Column(name = "attachment")
    private String attachment;

    @Column(name = "code")
    private String code;

    @Column(name = "created_date")
    private Timestamp createdDate;
}