package com.app.assigmentsubmitionapp.modules.incomes.dto;

import lombok.*;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IncSubCategoryResponse {
    private UUID id;
    private Date createdOn;
    private Date updatedOn;
    private String subCategoryName;
    private String subCategoryDescription;
}
