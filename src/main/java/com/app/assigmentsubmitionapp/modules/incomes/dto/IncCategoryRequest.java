package com.app.assigmentsubmitionapp.modules.incomes.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IncCategoryRequest {
    @NotEmpty
    @NotNull
    @NotBlank
    private UUID categoryId;
    @NotEmpty
    @NotNull
    @NotBlank
    private String incCategoryName;
    private String incCategoryDescription;

}
