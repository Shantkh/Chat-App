package com.app.assigmentsubmitionapp.modules.incomes;

import com.app.assigmentsubmitionapp.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "inc_sub_sub_category")
public class IncSubSubCategory {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Column(name = "inc_sub_sub_category_id")
    private UUID id;

    @CreatedDate
    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    private Date createdOn;

    @CreatedDate
    @Column(name = "updated_on")
    @Convert(converter = TimeDateConverter.class)
    private Date updatedOn;

    private String incSubSubCategoryName;
    private String incSubSubCategoryDescription;


    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE, optional = false ,fetch = FetchType.LAZY)
    @JoinColumn(name="inc_sub_category_id")
    private IncSubCategory incSubCategory;
}
