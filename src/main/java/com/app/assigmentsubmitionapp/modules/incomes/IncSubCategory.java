package com.app.assigmentsubmitionapp.modules.incomes;

import com.app.assigmentsubmitionapp.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "inc_sub_category")
public class IncSubCategory {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Column(name = "inc_sub_category_id")
    private UUID id;

    @CreatedDate
    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    private Date createdOn;

    @CreatedDate
    @Column(name = "updated_on")
    @Convert(converter = TimeDateConverter.class)
    private Date updatedOn;

    private String incSubCategoryName;
    private String incSubCategoryDescription;

    @Column(name = "inc_sub_sub_category")
    @ElementCollection
    @OneToMany(mappedBy = "incSubCategory")
    private Set<IncSubSubCategory> incSubSubCategories;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE, optional = false,fetch = FetchType.LAZY)
    @JoinColumn(name = "inc_category_id")
    private IncCategory incCategory;
}
