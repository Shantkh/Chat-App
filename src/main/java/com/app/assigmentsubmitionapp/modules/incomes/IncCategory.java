package com.app.assigmentsubmitionapp.modules.incomes;

import com.app.assigmentsubmitionapp.utils.TimeDateConverter;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "inc_category")
public class IncCategory {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Column(name = "inc_category_id")
    private UUID id;

    @CreatedDate
    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    private Date createdOn;

    @CreatedDate
    @Column(name = "updated_on")
    @Convert(converter = TimeDateConverter.class)
    private Date updatedOn;

    private String incCategoryName;
    private String incCategoryDescription;

    @Column(name = "inc_sub_category")
    @ElementCollection
    @OneToMany(mappedBy = "incCategory")
    private Set<IncSubCategory> incSubCategories;

}
