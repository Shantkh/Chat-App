package com.app.assigmentsubmitionapp.modules;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogDocument {
    private UUID id;
    private String log;
    private String key;
    private String app;
}
