package com.app.assigmentsubmitionapp.modules.localsettings;

import com.app.assigmentsubmitionapp.enums.currency.Currencies;
import com.app.assigmentsubmitionapp.enums.localsettings.Lengths;
import com.app.assigmentsubmitionapp.enums.localsettings.Mass;
import com.app.assigmentsubmitionapp.enums.localsettings.Temperature;
import com.app.assigmentsubmitionapp.enums.localsettings.Volume;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "local_settings")
public class LocalSettings {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Email
    private String userEmail;

    @Enumerated(EnumType.STRING)
    private Lengths lengths;

    @Enumerated(EnumType.STRING)
    private Mass mass;

    @Enumerated(EnumType.STRING)
    private Temperature temperature;

    @Enumerated(EnumType.STRING)
    private Volume volume;

    @Enumerated(EnumType.STRING)
    private Currencies currency;

    private String country;
    private String symbol;

}
