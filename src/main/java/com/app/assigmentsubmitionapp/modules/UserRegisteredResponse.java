package com.app.assigmentsubmitionapp.modules;

import lombok.*;

import java.io.Serializable;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserRegisteredResponse implements Serializable {
    private String id;
}
