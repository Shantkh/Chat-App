package com.app.assigmentsubmitionapp.modules.neo4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import javax.persistence.GeneratedValue;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Node("expenses")
@Getter
@Setter
@AllArgsConstructor
@Builder
public class ExpensesNode {
    @Id
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    private final UUID id;

    @Property("user_id")
    private final UUID userId;

    @Property("created_at")
    @CreatedDate
    private final LocalDateTime createdAt;

    @Property("updated_at")
    @LastModifiedDate
    private final LocalDateTime updatedAt;

    @Relationship(type = "CREATE_EXPENSE")
    private UsersNode usersNode;

    @Relationship(type = "HOUSE_EXPENSE", direction = Relationship.Direction.INCOMING)
    private List<Housing> incomesNodes = new ArrayList<>();
}