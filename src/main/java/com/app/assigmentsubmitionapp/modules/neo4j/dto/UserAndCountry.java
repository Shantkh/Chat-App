package com.app.assigmentsubmitionapp.modules.neo4j.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserAndCountry {
    private String userEmail;
    private String countryName;
    private String expenseId;
    private String incomeId;
}
