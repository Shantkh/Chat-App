package com.app.assigmentsubmitionapp.modules.neo4j;


import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.data.neo4j.core.schema.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Node("country")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Country {

    @Id
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    private UUID id;

    @Property(name = "name")
    private  String name;

    @Relationship(type = "COUNTRY_FROM", direction = Relationship.Direction.OUTGOING)
    private List<UsersNode> users = new ArrayList<>();

    public void addUser(UsersNode usersNode){
        if(users == null ) users = new ArrayList<>();
        users.add(usersNode);
    }

}
