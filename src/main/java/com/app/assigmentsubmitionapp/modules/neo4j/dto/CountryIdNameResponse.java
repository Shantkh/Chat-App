package com.app.assigmentsubmitionapp.modules.neo4j.dto;

import lombok.*;

import java.util.UUID;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CountryIdNameResponse {
    private UUID id;
    private String name;
}
