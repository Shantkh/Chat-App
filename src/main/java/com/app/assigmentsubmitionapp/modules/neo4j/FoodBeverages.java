package com.app.assigmentsubmitionapp.modules.neo4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
@Node("foodBeverages")
@Getter
@Setter
@Builder
@AllArgsConstructor
@GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator", parameters = {
        @Parameter(name = "uuid_gen_strategy_class", value = "org.hibernate.id.uuid.CustomVersionOneStrategy"),
        @Parameter(name = "uuid_gen_strategy_arguments", value = "org.hibernate.id.uuid.CustomVersionOneStrategy:1.0")
})
public class FoodBeverages {
    @Id
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    private UUID id;

    @Property("user_id")
    private  UUID userId;

    @Property("created_at")
    @CreatedDate
    private LocalDateTime createdAt;

    @Property("updated_at")
    @LastModifiedDate
    private LocalDateTime updatedAt;

    @Property("name")
    private  String name;

    @Property("price")
    private BigDecimal price;

    @Property("currency")
    private  String currency;

    @Relationship(type = "FOOD_EXPENSE")
    private ExpensesNode expensesNode;
}