package com.app.assigmentsubmitionapp.modules.neo4j;

import lombok.*;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UnknownResponse {
    private UUID id;
    private String message;
    private String expense;
    private BigDecimal price;
    private String currency;
    private String category;

}
