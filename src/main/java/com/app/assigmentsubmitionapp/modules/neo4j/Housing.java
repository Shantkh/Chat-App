package com.app.assigmentsubmitionapp.modules.neo4j;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Property;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Node("house")
@Getter
@Setter
@AllArgsConstructor
@Builder
public class Housing {
    @Id
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    private final UUID id;

    @Property("user_id")
    private final UUID userId;

    @Property("created_at")
    @CreatedDate
    private final LocalDateTime createdAt;

    @Property("updated_at")
    @LastModifiedDate
    private final LocalDateTime updatedAt;

    @Property("name")
    private final String name;

    @Property("price")
    private final BigDecimal price;

    @Property("currency")
    private final String currency;

    @Relationship(type = "HOUSE_EXPENSE")
    private ExpensesNode expensesNode;
}
