package com.app.assigmentsubmitionapp.modules.neo4j.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CountryWithUsersPercentageResponse {
    private String name;
    private int numofusers;
    private Double percentage;
}
