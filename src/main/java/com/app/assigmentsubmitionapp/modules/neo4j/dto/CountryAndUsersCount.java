package com.app.assigmentsubmitionapp.modules.neo4j.dto;

import lombok.*;

@Builder
public record CountryAndUsersCount(String countryName, Integer usersCount) {
}
