package com.app.assigmentsubmitionapp.modules.neo4j;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCount {
    private String countryName;
    private int count;
}