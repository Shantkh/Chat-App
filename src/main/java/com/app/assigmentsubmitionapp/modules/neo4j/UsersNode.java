package com.app.assigmentsubmitionapp.modules.neo4j;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.neo4j.core.schema.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Node("user")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsersNode {

    @Id
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    private UUID id;

    @Property("user_id")
    private  UUID userId;

    @Property("created_at")
    @CreatedDate
    private  LocalDateTime createdAt;

    @Property("updated_at")
    @LastModifiedDate
    private  LocalDateTime updatedAt;

    @Property("user_name")
    private String name;

    @Property("user_email")
    private  String email;

    @Relationship(type = "COUNTRY_FROM")
    private Country country;

    @Relationship(type = "CREATE_EXPENSE", direction = Relationship.Direction.INCOMING)
    private List<ExpensesNode> expensesNodes = new ArrayList<>();

    @Relationship(type = "CREATE_INCOME", direction = Relationship.Direction.INCOMING)
    private List<IncomesNode> incomesNodes = new ArrayList<>();

    public void setCountry(Country country) {
        this.country = country;
        country.addUser(this);
    }

}
