package com.app.assigmentsubmitionapp.modules.expenses.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExpSubSubCategoryRequest {
    @NotEmpty
    @NotNull
    @NotBlank
    private UUID subCategoryId;
    @NotEmpty
    @NotNull
    @NotBlank
    private UUID subSubCategoryId;
    @NotEmpty
    @NotNull
    @NotBlank
    private String subSubCategoryName;

    private String subSubCategoryDescription;
}
