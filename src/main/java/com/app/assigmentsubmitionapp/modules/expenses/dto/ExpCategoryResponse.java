package com.app.assigmentsubmitionapp.modules.expenses.dto;

import lombok.*;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExpCategoryResponse {
    private UUID id;
    private Date createdOn;
    private Date updatedOn;
    private String categoryName;
    private String categoryDescription;
}
