package com.app.assigmentsubmitionapp.modules.expenses;

import com.app.assigmentsubmitionapp.utils.TimeDateConverter;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "exp_category")
public class ExpCategory {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Column(name = "exp_category_id")
    private UUID id;

    @CreatedDate
    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    private Date createdOn;

    @CreatedDate
    @Column(name = "updated_on")
    @Convert(converter = TimeDateConverter.class)
    private Date updatedOn;

    private String expCategoryName;
    private String expCategoryDescription;

    @Column(name = "exp_sub_category")
    @ElementCollection
    @OneToMany(mappedBy = "expCategory")
    private Set<ExpSubCategory> expSubCategories;


}
