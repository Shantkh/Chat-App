package com.app.assigmentsubmitionapp.modules.expenses;

import com.app.assigmentsubmitionapp.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "exp_sub_sub_category")
public class ExpSubSubCategory {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )

    @Column(name = "exp_sub_sub_category_id")
    private UUID id;

    @CreatedDate
    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    private Date createdOn;

    @CreatedDate
    @Column(name = "updated_on")
    @Convert(converter = TimeDateConverter.class)
    private Date updatedOn;

    private String expSubSubCategoryName;
    private String expSubSubCategoryDescription;


    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE, optional = false ,fetch = FetchType.LAZY)
    @JoinColumn(name="exp_sub_category_id")
    private ExpSubCategory expSubCategory;
}
