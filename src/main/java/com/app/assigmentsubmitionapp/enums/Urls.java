package com.app.assigmentsubmitionapp.enums;

public class Urls {

    /* web */
    public final static String REGISTER = "/register";
    public final static String ACTIVATE_ACCOUNT = "/activate-account/{email}/{code}";
    public final static String FORGOT_PASSWORD = "/forgot-password";
    public final static String FORGOT_PASSWORD_CODE_VERIFY = "/change-password-code/{email}/{code}";
    public final static String UPDATE_PASSWORD = "/update-password";


    /* Admin */
    public final static String ADMIN_ROOT = "/api/v1/admin";
    public final static String GET_ALL_USERS_ADMIN = ADMIN_ROOT + "/all";

    /* Admin Expenses Category */
    public final static String GET_ALL_EXPENSES_CATEGORIES = ADMIN_ROOT + "/expenses/categories";
    public final static String UPDATE_EXPENSES_CATEGORY = ADMIN_ROOT + "/expenses/categories/update";
    public final static String ADD_EXPENSES_CATEGORY = ADMIN_ROOT + "/expenses/categories/add";
    public final static String DELETE_EXPENSES_CATEGORY = ADMIN_ROOT + "/expenses/categories/delete/{id}";


    /* Admin Income Category */
    public final static String GET_ALL_INCOME_CATEGORIES = ADMIN_ROOT + "/incomes/categories";
    public final static String UPDATE_INCOME_CATEGORY = ADMIN_ROOT + "/incomes/categories/update";
    public final static String ADD_INCOME_CATEGORY = ADMIN_ROOT + "/incomes/categories/add";
    public final static String DELETE_INCOME_CATEGORY = ADMIN_ROOT + "/incomes/categories/delete/{id}";


    /* Admin expense SubCategory */
    public final static String GET_ALL_EXPENSES_SUB_CATEGORY_BY_ID = ADMIN_ROOT + "/expenses/categories/sub-category/{catId}";
    public final static String GET_SUB_EXPENSES_CATEGORY_BY_ID = ADMIN_ROOT + "/expenses/categories/sub-category/id/{subId}";
    public final static String UPDATE_SUB_EXPENSES_CATEGORY = ADMIN_ROOT + "/expenses/categories/sub-category/update";
    public final static String ADD_SUB_EXPENSES_CATEGORY = ADMIN_ROOT + "/expenses/categories/sub-category/add";
    public final static String DELETE_SUB_EXPENSES_CATEGORY = ADMIN_ROOT + "/expenses/categories/sub-category/delete/{id}";


    /* Admin income SubCategory */
    public final static String GET_ALL_INCOME_SUB_CATEGORY_BY_ID = ADMIN_ROOT + "/incomes/categories/sub-category/{catId}";
    public final static String GET_SUB_INCOME_CATEGORY_BY_ID = ADMIN_ROOT + "/incomes/categories/sub-category/id/{subId}";
    public final static String UPDATE_SUB_INCOME_CATEGORY = ADMIN_ROOT + "/incomes/categories/sub-category/update";
    public final static String ADD_SUB_INCOME_CATEGORY = ADMIN_ROOT + "/incomes/categories/sub-category/add";
    public final static String DELETE_SUB_INCOME_CATEGORY = ADMIN_ROOT + "/incomes/categories/sub-category/delete/{id}";


    /* Admin expenses SubSUBCategory */
    public final static String GET_ALL_SUB_SUB_EXPENSES_SUB_CATEGORY_ID = ADMIN_ROOT + "/expenses/categories/sub-category/sub-category/{sub_cat_id}";
    public final static String GET_SUB_SUB_EXPENSES_CATEGORY_BY_ID = ADMIN_ROOT + "/expenses/categories/sub-category/sub-category/id/{sub_sub_cat_id}";
    public final static String UPDATE_SUB_SUB_EXPENSES_CATEGORY = ADMIN_ROOT + "/expenses/categories/sub-category/sub-category/update";
    public final static String ADD_SUB_SUB_EXPENSES_CATEGORY = ADMIN_ROOT + "/expenses/categories/sub-category/sub-category/add";
    public final static String DELETE_SUB_SUB_EXPENSES_CATEGORY = ADMIN_ROOT + "/expenses/categories/sub-category/sub-category/delete/{id}";

    /* Admin income SubSUBCategory */
    public final static String GET_ALL_SUB_SUB_INCOME_SUB_CATEGORY_ID = ADMIN_ROOT + "/incomes/categories/sub-category/sub-category/{sub_cat_id}";
    public final static String GET_SUB_SUB_INCOME_CATEGORY_BY_ID = ADMIN_ROOT + "/incomes/categories/sub-category/sub-category/id/{sub_sub_cat_id}";
    public final static String UPDATE_SUB_SUB_INCOME_CATEGORY = ADMIN_ROOT + "/incomes/categories/sub-category/sub-category/update";
    public final static String ADD_SUB_SUB_INCOME_CATEGORY = ADMIN_ROOT + "/incomes/categories/sub-category/sub-category/add";
    public final static String DELETE_SUB_SUB_INCOME_CATEGORY = ADMIN_ROOT + "/incomes/categories/sub-category/sub-category/delete/{id}";


    /* user */
    public final static String USERS_ROOT = "/api/v1/users";
    public final static String CHANGE_PASSWORD_USERS = USERS_ROOT + "/password";
    public final static String CHANGE_ROLE_USERS = USERS_ROOT + "/role";
    public final static String CLOSE_ACCOUNT_USERS = USERS_ROOT + "/close";

    public final static String UPDATE_LOCAL_SETTINGS = USERS_ROOT + "/update-locals";
    public final static String USER_LOCAL_SETTINGS = USERS_ROOT + "/locals-settings";


    /* ner */

    public final static String NER_ROOT = "/api/v1/ner";
    public final static String NER_WITH_DETECTION = "/";
    public final static String NER_DETECTOR = NER_ROOT + "/all";


    /* Transactional */
    public final static String TRANSACTION_ROOT = "/api/v1/transactions/";
    public final static String TRANSACTION_ADMIN_ROOT = "/api/v1/admin/transactions/";
    public final static String TRANSACTION_CREATE_USER = TRANSACTION_ROOT + "node/create/user-node";
    public final static String TRANSACTION_GET_COUNTRIES_WITH_USER_COUNT = TRANSACTION_ADMIN_ROOT + "node/countries/users";
    public final static String TRANSACTION_GET_COUNTRIES = TRANSACTION_ADMIN_ROOT + "node/countries";
    public final static String TRANSACTION_GET_COUNTRIES_USERS_PERCENTAGE = TRANSACTION_ADMIN_ROOT + "node/countries/users/perdentage";
    public final static String TRANSACTION_EXPENSES_CATEGORY = TRANSACTION_ROOT + "node/add/expense/{expense_id}";
}