package com.app.assigmentsubmitionapp.enums;

public class KafkaConstants {
    public static final String CREDENTIAL_TOPIC = "users-credentials";
    public static final String GROUP_CREDENTIALS_ID = "users-credentials";

    public static final String CONFIRM_TOPIC = "users-confirm";
    public static final String GROUP_CONFIRMS_ID = "users-confirms";


    public static final String GROUP_NEW_USER_TOPIC = "users-registration";
    public static final String GROUP_NEW_USER_ID = "users-registration";


    public static final String WELCOME_TOPIC = "users-welcome";
    public static final String GROUP_WELCOME_ID = "user-welcome";
    public static final String SETTING_TOPIC = "users-database";
    public static final String SETTING_ID = "users-database";



}