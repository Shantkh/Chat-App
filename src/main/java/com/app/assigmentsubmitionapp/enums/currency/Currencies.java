package com.app.assigmentsubmitionapp.enums.currency;

public enum Currencies {
    USD("US Dollar", "$"),
    EUR("Euro", "€"),
    JPY("Japanese Yen", "¥"),
    GBP("British Pound", "£"),
    CHF("Swiss Franc", "CHF"),
    AUD("Australian Dollar", "$"),
    CAD("Canadian Dollar", "$"),
    CNY("Chinese Yuan", "¥"),
    HKD("Hong Kong Dollar", "$"),
    SGD("Singapore Dollar", "$"),
    NZD("New Zealand Dollar", "$"),
    KRW("South Korean Won", "₩"),
    MXN("Mexican Peso", "$"),
    BRL("Brazilian Real", "R$"),
    ZAR("South African Rand", "R"),
    ILS("Israeli Shekel", "₪"),
    INR("Indian Rupee", "₹"),
    MYR("Malaysian Ringgit", "RM"),
    PHP("Philippine Peso", "₱"),
    PLN("Polish Zloty", "zł"),
    RUB("Russian Ruble", "₽"),
    SEK("Swedish Krona", "kr"),
    THB("Thai Baht", "฿"),
    TRY("Turkish Lira", "₺"),
    TWD("Taiwan Dollar", "$"),
    AED("United Arab Emirates Dirham", "AED"),
    VND("Vietnamese Dong", "₫"),
    ARS("Argentine Peso", "$");

    private final String country;
    private final String symbol;

    Currencies(String country, String symbol) {
        this.country = country;
        this.symbol = symbol;
    }

    public String getCountry() {
        return country;
    }

    public String getSymbol() {
        return symbol;
    }
}