package com.app.assigmentsubmitionapp.enums;

public class MessagesConst {

    public static final String LIST_EMPTY = "The list is empty.";
    public static final String EMPTY_DATA = "Data not found.";
    public static final String ITS_NULL = "The object is null.";
    public static final String ORDER_ERROR = "Order not found or passed the 5 minutes.";
    public static final String DONT_HAVE_PERMISSION = "Sorry but you dont have permission to search orders.";
    public static final String PAGE_NUMBER_ERROR_MESSAGE = "Please check pagination or size page number.";
    public static final String SIZE_NEGATIVE_ERROR_MESSAGE = "Please check pagination size must not be negative value.";
    public static final String DIRECTION_OR_ELEMENT_ERROR = "Please check the sorting direction, or the sorting element.";
    public static final String UNKNOWN_ERROR = "something went wrong in Users get all list";
    public static final String REGISTERED_USER = "User already registered.";
    public static final String EMAIL_NOT_FOUND_USER = "User not found by email!";
    public static final String USERNAME_NOT_FOUND_USER = "User not found by username!";
    public static final String DATA_NOT_FOUND = "Not found in our database.!";

}