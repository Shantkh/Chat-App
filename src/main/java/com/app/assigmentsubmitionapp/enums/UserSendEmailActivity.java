package com.app.assigmentsubmitionapp.enums;

import com.app.assigmentsubmitionapp.utils.VerificationUtils;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@RequiredArgsConstructor
public class UserSendEmailActivity {
    @Setter
    private static VerificationUtils verificationUtils;
    public final static String FORGOT_PASSWORD = "Forgot password";
    public final static String WELCOME_TO_OUR_APP = "Welcome message";
    public final static String NEW_REGISTERED_USER = "Please activate your account.";
    public final static String CONFIRM_PASSWORD_CHANGED = "Password Changed.";
    public final static String FORGOT_PASSWORD_CODE = getRandomString();
    public final static String FORGOT_PASSWORD_BODY = "<p>" + FORGOT_PASSWORD_CODE + "</p>";

    private static String getRandomString() {
        return Objects.requireNonNull(verificationUtils).getRandomNumberString();
    }
}