package com.app.assigmentsubmitionapp.enums;

public enum ExpenseType {
        PRICE,
        CATEGORY,
        CURRENCY,
        EXPENSE
    }