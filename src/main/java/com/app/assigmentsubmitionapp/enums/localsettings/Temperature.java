package com.app.assigmentsubmitionapp.enums.localsettings;

public enum Temperature {
    CELSIUS,
    FAHRENHEIT
}
