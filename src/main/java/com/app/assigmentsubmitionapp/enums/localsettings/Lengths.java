package com.app.assigmentsubmitionapp.enums.localsettings;

public enum Lengths {
    METER,
    INCH
}
