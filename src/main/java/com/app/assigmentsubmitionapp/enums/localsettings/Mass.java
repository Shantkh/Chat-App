package com.app.assigmentsubmitionapp.enums.localsettings;

public enum Mass {
    KILOGRAM,
    POUND
}
