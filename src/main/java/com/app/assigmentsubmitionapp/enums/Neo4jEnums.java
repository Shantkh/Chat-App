package com.app.assigmentsubmitionapp.enums;

public class Neo4jEnums {
    public static final String USER_ALREADY_EXISTS = "User already exists.";
    public static final String PRICE = "price";
    public static final String CATEGORY = "category";
    public static final String EXPENSE = "expense";
    public static final String CURRENCY = "currency";
    public static final String HOUSING = "housing";
    public static final String FRUIT = "fruit";
    public static final String UNKNOWN = "unknown";
}
