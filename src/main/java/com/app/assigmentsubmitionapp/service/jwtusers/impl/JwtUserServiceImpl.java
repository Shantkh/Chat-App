package com.app.assigmentsubmitionapp.service.jwtusers.impl;

import com.app.assigmentsubmitionapp.enums.MessagesConst;
import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.kafka.utils.*;
import com.app.assigmentsubmitionapp.mapper.JwtUserMapper;
import com.app.assigmentsubmitionapp.modules.*;
import com.app.assigmentsubmitionapp.repo.EmailRepository;
import com.app.assigmentsubmitionapp.repo.JwtUserRepository;
import com.app.assigmentsubmitionapp.service.jwtusers.JwtUserService;
import com.app.assigmentsubmitionapp.user.JwtUser;
import com.app.assigmentsubmitionapp.user.PaginationWithSorting;
import com.app.assigmentsubmitionapp.user.dto.SettingUser;
import com.app.assigmentsubmitionapp.user.dto.UserRegisterResponse;
import com.app.assigmentsubmitionapp.user.dto.UserRegistrationRequest;
import com.app.assigmentsubmitionapp.utils.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

import static com.app.assigmentsubmitionapp.enums.MessagesConst.*;
import static com.app.assigmentsubmitionapp.enums.UserSendEmailActivity.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class JwtUserServiceImpl implements JwtUserService {

    private final JwtUserRepository jwtUserRepository;
    private final JwtUserMapper jwtUserMapper;
    private final PagePagination pagePagination;
    private final KafkaNewUserRegisteredEmailUtils kafkaNewUserRegisteredEmailUtils;
    private final EmailRepository emailRepository;
    private final KafkaUserEmailConfirmEmailUtils kafkaUserEmailConfirmEmailUtils;
    private final KafkaForgotPasswordEmailUtils kafkaForgotPasswordEmailUtils;
    private final KafkaPasswordChangedConfirmEmailUtils kafkaPasswordConfirmEmailUtils;
    private final KafkaNewUserSettingsDatabaseCreationUtils kafkaNewUserSettingsDatabaseCreationUtils;

    @Override
    public JwtUser save(JwtUser user) {
        return jwtUserRepository.save(user);
    }

    @Override
    public Optional<ResponseBuilder<?>> register(UserRegistrationRequest userRegistrationRequest,
                                                 HttpServletRequest request) {

        AppPreconditions.checkNotNullOrEmpty(userRegistrationRequest);
        UserPreconditions userPreconditions = new UserPreconditions(jwtUserRepository);
        userPreconditions.checkUserRegisterDetails(userRegistrationRequest);

        JwtUser mappedUser = jwtUserMapper.convertFromUserRequestToUserObject(userRegistrationRequest);

        JwtUser savedUser = save(mappedUser);

        EmailDetails emailDetails = EmailUtils.createNewRegisterEmailValidationWithCode(request,
                userRegistrationRequest.getEmail(),
                NEW_REGISTERED_USER);

        kafkaNewUserRegisteredEmailUtils.userRegisteredAction(emailDetails);
        SettingUser settingUser = SettingUser.builder()
                .email(savedUser.getEmail()).build();
        kafkaNewUserSettingsDatabaseCreationUtils.userDatabaseCreationAction(settingUser);

        Optional<UserRegisterResponse> response = jwtUserMapper.convertFromUserObjectToUserResponse(savedUser);
        return Optional.of(response
                .map(e -> new ResponseBuilder<>(
                        e,
                        HttpStatus.CREATED,
                        HttpStatus.CREATED.value(),
                        true,
                        DateUtils.newTimeStamp()))
                .orElseThrow(() -> new ApplicationExceptions(MessagesConst.UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> activateAccount(String email, String code) {
        byte[] decodedEmail = Base64.getDecoder().decode(email);
        byte[] decodedCode = Base64.getDecoder().decode(code);
        String theEmail = new String(decodedEmail);
        String theCode = new String(decodedCode);

        UserPreconditions userPreconditions = new UserPreconditions(jwtUserRepository);
        Optional<JwtUser> user = jwtUserRepository.findJwtUserByEmail(theEmail);
        EmailDetails emailDetailsByCode = emailRepository.findAllByCode(theCode);

        AppPreconditions.checkNotNullOrEmpty(emailDetailsByCode);
        userPreconditions.checkCodeAndEmail(user.get(), emailDetailsByCode);

        user.get().setUpdatedOn(DateUtils.newTimeStamp());
        user.get().setUserEmailVerified(true);
        user.get().setEnabled(true);

        JwtUser savedUser = jwtUserRepository.save(user.get());

        EmailDetails emailDetails = EmailUtils.createdEmailConfirmDetails(user.get(), emailDetailsByCode,
                WELCOME_TO_OUR_APP);

        kafkaUserEmailConfirmEmailUtils.welcomeToOurApp(emailDetails);

        Optional<UserRegisteredResponse> result = jwtUserMapper.convertFromUserObjectToUserApiResponse(savedUser);
        return Optional.ofNullable(Optional.of(result)
                .map(e -> new ResponseBuilder<>(
                        e,
                        HttpStatus.OK,
                        HttpStatus.OK.value(),
                        true,
                        DateUtils.newTimeStamp()))
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> forgotPassword(ForgotPasswordRequest
                                                               forgotPasswordRequest, HttpServletRequest request) {
        UserPreconditions userPreconditions = new UserPreconditions(jwtUserRepository);
        userPreconditions.checkUserEmailExsist(forgotPasswordRequest);

        EmailDetails emailDetails = EmailUtils.createdForgotPasswordCode(request,
                forgotPasswordRequest.getEmail(),
                FORGOT_PASSWORD);

        kafkaForgotPasswordEmailUtils.userForgotPasswordActivity(emailDetails);

        return Optional.ofNullable(Optional.of("Please check your email for verification code.")
                .map(e -> new ResponseBuilder<>(
                        e,
                        HttpStatus.OK,
                        HttpStatus.OK.value(),
                        true,
                        DateUtils.newTimeStamp()))
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> forgotPasswordVerification(String email, String code) {
        byte[] decodedEmail = Base64.getDecoder().decode(email);
        byte[] decodedCode = Base64.getDecoder().decode(code);
        String theEmail = new String(decodedEmail);
        String theCode = new String(decodedCode);

        UserPreconditions userPreconditions = new UserPreconditions(jwtUserRepository);
        Optional<JwtUser> user = jwtUserRepository.findJwtUserByEmail(theEmail);
        EmailDetails emailDetailsByCode = emailRepository.findAllByCode(theCode);

        AppPreconditions.checkNotNullOrEmpty(emailDetailsByCode);
        userPreconditions.checkCodeAndEmail(user.get(), emailDetailsByCode);

        emailRepository.delete(emailDetailsByCode);

        Optional<UserRegisteredResponse> result = jwtUserMapper.convertFromUserObjectToUserApiResponse(user.get());

        return Optional.ofNullable(Optional.of(result)
                .map(e -> new ResponseBuilder<>(
                        e,
                        HttpStatus.OK,
                        HttpStatus.OK.value(),
                        true,
                        DateUtils.newTimeStamp()))
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> updatePasswordRequest(NewPasswordRequest newPasswordRequest) {
        AppPreconditions.checkNotNullOrEmpty(newPasswordRequest);

        JwtUser user = jwtUserRepository.findById(UUID.fromString(newPasswordRequest.getId()))
                .orElseThrow();

        AppPreconditions.checkNotNullOrEmpty(user);
        String email = user.getEmail();

        //TODO: check if the password changed


        EmailDetails emailRepoDetails = emailRepository.findAllByRecipient(user.getEmail());
        AppPreconditions.checkNotNullOrEmpty(emailRepoDetails);

        EmailDetails emailDetails = EmailUtils.createdPasswordChanged(
                email,
                CONFIRM_PASSWORD_CHANGED);


        kafkaPasswordConfirmEmailUtils.userPasswordConfirmActivity(emailDetails);
        Optional<UserRegisteredResponse> result = jwtUserMapper.convertFromUserObjectToUserApiResponse(user);
        return Optional.ofNullable(Optional.of(result)
                .map(e -> new ResponseBuilder<>(
                        e,
                        HttpStatus.OK,
                        HttpStatus.OK.value(),
                        true,
                        DateUtils.newTimeStamp()))
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<JwtUser> findJwtUserByEmail(String email) {
        return jwtUserRepository.findJwtUserByEmail(email);
    }

    @Override
    public JwtUser getJwtUserByEmail(String email) {
        return jwtUserRepository.findJwtUserByEmail(email)
                .orElseThrow(() -> new ApplicationExceptions(EMAIL_NOT_FOUND_USER));
    }

    @Override
    public JwtUser getJwtUserByUsername(String username) {
        return jwtUserRepository.findJwtUserByUsername(username)
                .orElseThrow(() -> new ApplicationExceptions(USERNAME_NOT_FOUND_USER));
    }

    @Override
    public Optional<ResponseBuilder<?>> getJwtUsers(PaginationWithSorting paginationWithSorting) {
        AppPreconditions.checkNotNullOrEmpty(paginationWithSorting);
        PageRequest pageReq = pagePagination.pagePagination(paginationWithSorting);
        List<JwtUser> result = jwtUserRepository.findAll(pageReq).getContent();
        var response = jwtUserMapper.convertFromJwtUserObjectToJwtUsersListResponse(result);
        return Optional.of(response
                .map(e -> new ResponseBuilder<>(
                        e,
                        HttpStatus.OK,
                        HttpStatus.OK.value(),
                        true,
                        DateUtils.newTimeStamp()))
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }
}