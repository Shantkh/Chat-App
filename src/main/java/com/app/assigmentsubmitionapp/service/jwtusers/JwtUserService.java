package com.app.assigmentsubmitionapp.service.jwtusers;

import com.app.assigmentsubmitionapp.modules.ForgotPasswordRequest;
import com.app.assigmentsubmitionapp.modules.NewPasswordRequest;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.user.JwtUser;
import com.app.assigmentsubmitionapp.user.PaginationWithSorting;
import com.app.assigmentsubmitionapp.user.dto.UserRegistrationRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

public interface JwtUserService {
    JwtUser save(JwtUser user);

    Optional<JwtUser> findJwtUserByEmail(String email);
    JwtUser getJwtUserByEmail(String email);

    JwtUser getJwtUserByUsername(String username);

    Optional<ResponseBuilder<?>> getJwtUsers(PaginationWithSorting paginationWithSorting);

    Optional<ResponseBuilder<?>> register(UserRegistrationRequest userRegistrationRequest,
                                          HttpServletRequest request) throws IOException;

    Optional<ResponseBuilder<?>> activateAccount(String email, String code);

    Optional<ResponseBuilder<?>> forgotPassword(ForgotPasswordRequest forgotPasswordRequest,
                                                HttpServletRequest request);

    Optional<ResponseBuilder<?>> forgotPasswordVerification(String email, String code);

    Optional<ResponseBuilder<?>> updatePasswordRequest(NewPasswordRequest newPasswordRequest);

}
