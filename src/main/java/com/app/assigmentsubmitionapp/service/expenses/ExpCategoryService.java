package com.app.assigmentsubmitionapp.service.expenses;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryRequest;

import java.util.Optional;
import java.util.UUID;

public interface ExpCategoryService {

    Optional<ResponseBuilder<?>> categories();

    Optional<ResponseBuilder<?>> updateCategory(ExpCategoryRequest categoryRequest);

    Optional<ResponseBuilder<?>> deleteCategory(UUID id);

    Optional<ResponseBuilder<?>> addCategory(ExpCategoryRequest categoryRequest);
}
