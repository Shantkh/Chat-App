package com.app.assigmentsubmitionapp.service.expenses.impl;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.expenses.ExpSubSubCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.*;
import com.app.assigmentsubmitionapp.repo.expenses.ExpCategoryRepository;
import com.app.assigmentsubmitionapp.repo.expenses.ExpSubCategoryRepository;
import com.app.assigmentsubmitionapp.repo.expenses.ExpSubSubCategoryRepository;
import com.app.assigmentsubmitionapp.service.expenses.ExpSubSubCategoryService;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.app.assigmentsubmitionapp.enums.MessagesConst.DATA_NOT_FOUND;
import static com.app.assigmentsubmitionapp.enums.MessagesConst.UNKNOWN_ERROR;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExpSubSubCategoryServiceImpl implements ExpSubSubCategoryService {
    private final ExpSubSubCategoryRepository expSubSubCategoryRepository;
    private final ExpSubSubCategoryMapper expSubSubCategoryMapper;
    private final ExpSubCategoryRepository expSubCategoryRepository;

    private final ExpCategoryRepository expCategoryRepository;

    @Override
    public Optional<ResponseBuilder<?>> subSubCategories(UUID subSubExpCategoryId) {
        isSubCategoryExist(subSubExpCategoryId);
        List<ExpSubSubCategory> subCategories = expSubSubCategoryRepository.findByExpSubCategoryId(subSubExpCategoryId);
        Optional<List<ExpSubSubCategoryResponse>> result = expSubSubCategoryMapper.fromListSubCategoryObjectToOptionalSubSubCategoryResponse(subCategories);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }


    @Override
    public Optional<ResponseBuilder<?>> subSubCategoriesBySubSubId(UUID subSubExpCategoryId) {
        isSubSubCategoryExist(subSubExpCategoryId);
        Optional<ExpSubSubCategory> subCategories = expSubSubCategoryRepository.findById(subSubExpCategoryId);
        Optional<ExpSubSubCategoryResponse> result = expSubSubCategoryMapper.fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(subCategories);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> updateSubSubCategory(ExpSubSubCategoryUpdateRequest subCategoryUpdateRequest) {
        isSubSubCategoryExist(subCategoryUpdateRequest.getSubSubCategoryId());

        Optional<ExpSubCategory> dataSubCategory = expSubCategoryRepository.findById(subCategoryUpdateRequest.getSubCategoryId());
        Optional<ExpSubSubCategory> dataSubSubCategory = expSubSubCategoryRepository.findById(subCategoryUpdateRequest.getSubSubCategoryId());
        Optional<ExpCategory> dataCategory = expCategoryRepository.findById(dataSubCategory.get().getExpCategory().getId());

        ExpCategory expCategory = ExpCategory.builder()
                .id(dataCategory.get().getId())
                .createdOn(dataCategory.get().getCreatedOn())
                .updatedOn(dataCategory.get().getUpdatedOn())
                .expCategoryName(dataCategory.get().getExpCategoryName())
                .expCategoryDescription(dataCategory.get().getExpCategoryDescription())
                .build();

        ExpSubCategory exCategory = ExpSubCategory.builder()
                .id(dataSubCategory.get().getId())
                .createdOn(dataSubCategory.get().getCreatedOn())
                .updatedOn(dataSubCategory.get().getUpdatedOn())
                .expSubCategoryName(dataSubCategory.get().getExpSubCategoryName())
                .expSubCategoryDescription(dataSubCategory.get().getExpSubCategoryDescription())
                .expCategory(expCategory)
                .build();

        ExpSubSubCategory expSubCategory = ExpSubSubCategory.builder()
                .id(dataSubSubCategory.get().getId())
                .expSubSubCategoryName(subCategoryUpdateRequest.getSubSubCategoryName())
                .expSubSubCategoryDescription(subCategoryUpdateRequest.getSubSubCategoryDescription())
                .updatedOn(DateUtils.newTimeStamp())
                .createdOn(dataSubSubCategory.get().getCreatedOn())
                .expSubCategory(exCategory)
                .build();

        var savedData = expSubSubCategoryRepository.save(expSubCategory);
        ExpSubSubCategoryResponse result = expSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse(savedData);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }


    @Override
    public Optional<ResponseBuilder<?>> deleteSubSubCategory(UUID id) {
        isSubSubCategoryExist(id);
        expSubSubCategoryRepository.deleteById(id);
        var result = expSubSubCategoryRepository.findById(id);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> addSubSubCategory(ExpSubSubCategoryRequest categoryRequest) {
        isSubCategoryExist(categoryRequest.getSubCategoryId());
        ExpSubSubCategory expSubCategory = expSubSubCategoryMapper.fromSubSubCategoryRequestToSubSubCategoryObject(categoryRequest);
        ExpSubSubCategory savedExpSubCategory = expSubSubCategoryRepository.saveAndFlush(expSubCategory);
        ExpSubSubCategoryResponse result = expSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse(savedExpSubCategory);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.CREATED)
                        .httpStatusCode(HttpStatus.CREATED.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    private void isSubSubCategoryExist(UUID subSubExpCategoryId) {
        var result = expSubSubCategoryRepository.existsById(subSubExpCategoryId);
        if (!result) throw new ApplicationExceptions(DATA_NOT_FOUND);
    }

    private void isSubCategoryExist(UUID subSubExpCategoryId) {
        var result = expSubCategoryRepository.existsById(subSubExpCategoryId);
        if (!result) throw new ApplicationExceptions(DATA_NOT_FOUND);
    }


}
