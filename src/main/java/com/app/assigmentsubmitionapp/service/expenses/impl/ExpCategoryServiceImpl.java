package com.app.assigmentsubmitionapp.service.expenses.impl;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.expenses.ExpCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryRequest;
import com.app.assigmentsubmitionapp.repo.expenses.ExpCategoryRepository;
import com.app.assigmentsubmitionapp.service.expenses.ExpCategoryService;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

import static com.app.assigmentsubmitionapp.enums.MessagesConst.DATA_NOT_FOUND;
import static com.app.assigmentsubmitionapp.enums.MessagesConst.UNKNOWN_ERROR;

@Service
@RequiredArgsConstructor
public class ExpCategoryServiceImpl implements ExpCategoryService {

    private final ExpCategoryMapper categoryMapper;
    private final ExpCategoryRepository expCategoryRepository;

    @Override
    public Optional<ResponseBuilder<?>> categories() {
        var categoryList = expCategoryRepository.findAll();
        var result = categoryMapper.fromCategoryObjectToListCategoryResponse(categoryList);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> updateCategory(ExpCategoryRequest categoryRequest) {
        checkExists(categoryRequest.getCategoryId());
        var category = expCategoryRepository.getReferenceById(categoryRequest.getCategoryId());
        category.setUpdatedOn(DateUtils.newTimeStamp());
        category.setExpCategoryName(categoryRequest.getExpCategoryName());
        category.setExpCategoryDescription(categoryRequest.getExpCategoryDescription());
        category.setId(categoryRequest.getCategoryId());
        var response = expCategoryRepository.saveAndFlush(category);
        var result = categoryMapper.fromCategoryToCategoryResponse(response);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> deleteCategory(UUID id) {
        checkExists(id);
        expCategoryRepository.deleteById(id);
        var result = expCategoryRepository.findById(id);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> addCategory(ExpCategoryRequest categoryRequest) {
        var category = categoryMapper.fromCategoryRequestToCategoryObject(categoryRequest);
        var savedResponse = expCategoryRepository.saveAndFlush(category);
        var result = categoryMapper.fromCategoryToCategoryResponse(savedResponse);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.CREATED)
                        .httpStatusCode(HttpStatus.CREATED.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    private void checkExists(UUID id) {
        var isExists = expCategoryRepository.existsById(id);
        if (!isExists) throw new ApplicationExceptions(DATA_NOT_FOUND);
    }
}
