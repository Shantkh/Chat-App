package com.app.assigmentsubmitionapp.service.expenses;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryUpdateRequest;

import java.util.Optional;
import java.util.UUID;

public interface ExpSubSubCategoryService {
    Optional<ResponseBuilder<?>> subSubCategories(UUID subSubExpCategoryId);

    Optional<ResponseBuilder<?>> subSubCategoriesBySubSubId(UUID subSubExpCategoryId);

    Optional<ResponseBuilder<?>> updateSubSubCategory(ExpSubSubCategoryUpdateRequest subCategoryUpdateRequest);

    Optional<ResponseBuilder<?>> deleteSubSubCategory(UUID id);

    Optional<ResponseBuilder<?>> addSubSubCategory(ExpSubSubCategoryRequest categoryRequest);

}
