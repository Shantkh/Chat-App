package com.app.assigmentsubmitionapp.service.expenses;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryUpdateRequest;

import java.util.Optional;
import java.util.UUID;

public interface ExpSubCategoryService {
    Optional<ResponseBuilder<?>> subCategories(UUID categoryId);

    Optional<ResponseBuilder<?>> subCategoriesBySubId(UUID subCategoryId);

    Optional<ResponseBuilder<?>> addSubCategory(ExpSubCategoryRequest categoryRequest);

    Optional<ResponseBuilder<?>> updateSubCategory(ExpSubCategoryUpdateRequest subCategoryUpdateRequest);

    Optional<ResponseBuilder<?>> deleteSubCategory(UUID id);
}
