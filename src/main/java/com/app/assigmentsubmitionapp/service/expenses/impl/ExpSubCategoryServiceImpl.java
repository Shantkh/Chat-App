package com.app.assigmentsubmitionapp.service.expenses.impl;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.expenses.ExpSubCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.repo.expenses.ExpCategoryRepository;
import com.app.assigmentsubmitionapp.repo.expenses.ExpSubCategoryRepository;
import com.app.assigmentsubmitionapp.service.expenses.ExpSubCategoryService;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.app.assigmentsubmitionapp.enums.MessagesConst.DATA_NOT_FOUND;
import static com.app.assigmentsubmitionapp.enums.MessagesConst.UNKNOWN_ERROR;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExpSubCategoryServiceImpl implements ExpSubCategoryService {

    private final ExpSubCategoryMapper subCategoryMapper;
    private final ExpSubCategoryRepository expSubCategoryRepository;
    private final ExpCategoryRepository expCategoryRepository;

    @Override
    public Optional<ResponseBuilder<?>> subCategories(UUID categoryId) {
        isCategoryExist(categoryId);
        List<ExpSubCategory> subCategories = expSubCategoryRepository.findByExpCategoryId(categoryId);
        Optional<List<ExpSubCategoryResponse>> result = subCategoryMapper.fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(subCategories);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> subCategoriesBySubId(UUID subCategoryId) {
        isSubCategoryExist(subCategoryId);
        Optional<ExpSubCategory> subCategories = expSubCategoryRepository.findById(subCategoryId);
        Optional<ExpSubCategoryResponse> result = subCategoryMapper.fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(subCategories);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> addSubCategory(ExpSubCategoryRequest categoryRequest) {
        isCategoryExist(categoryRequest.getCategoryId());
        ExpSubCategory expSubCategory = subCategoryMapper.fromSubCategoryRequestToSubCategoryObject(categoryRequest);
        ExpSubCategory savedExpSubCategory = expSubCategoryRepository.saveAndFlush(expSubCategory);
        ExpSubCategoryResponse result = subCategoryMapper.fromSubCategoryObjectToSubCategoryResponse(savedExpSubCategory);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.CREATED)
                        .httpStatusCode(HttpStatus.CREATED.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));

    }

    @Override
    public Optional<ResponseBuilder<?>> updateSubCategory(ExpSubCategoryUpdateRequest subCategoryUpdateRequest) {
        isSubCategoryExist(subCategoryUpdateRequest.getSubCategoryId());

        Optional<ExpCategory> dataCategory = expCategoryRepository.findById(subCategoryUpdateRequest.getCategoryId());
        Optional<ExpSubCategory> dataSubCategory = expSubCategoryRepository.findById(subCategoryUpdateRequest.getSubCategoryId());


        ExpCategory category = ExpCategory.builder()
                .id(dataCategory.get().getId())
                .createdOn(dataCategory.get().getCreatedOn())
                .updatedOn(dataCategory.get().getUpdatedOn())
                .expCategoryName(dataCategory.get().getExpCategoryName())
                .expCategoryDescription(dataCategory.get().getExpCategoryDescription())
                .build();

        ExpSubCategory expSubCategory = ExpSubCategory.builder()
                .id(subCategoryUpdateRequest.getSubCategoryId())
                .expSubCategoryName(subCategoryUpdateRequest.getSubCategoryName())
                .expSubCategoryDescription(subCategoryUpdateRequest.getSubCategoryDescription())
                .updatedOn(DateUtils.newTimeStamp())
                .createdOn(dataSubCategory.get().getCreatedOn())
                .expCategory(category)
                .build();

        log.info(String.valueOf(category));
        var savedData = expSubCategoryRepository.save(expSubCategory);
        ExpSubCategoryResponse result = subCategoryMapper.fromSubCategoryObjectToSubCategoryResponse(savedData);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }


    @Override
    public Optional<ResponseBuilder<?>> deleteSubCategory(UUID id) {
        isSubCategoryExist(id);
        expSubCategoryRepository.deleteById(id);
        var result = expSubCategoryRepository.findById(id);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    private void isSubCategoryExist(UUID subCategoryId) {
        var result = expSubCategoryRepository.existsById(subCategoryId);
        if (!result) throw new ApplicationExceptions(DATA_NOT_FOUND);
    }

    private void isCategoryExist(UUID categoryId) {
        var result = expCategoryRepository.existsById(categoryId);
        if (!result) throw new ApplicationExceptions(DATA_NOT_FOUND);
    }
}
