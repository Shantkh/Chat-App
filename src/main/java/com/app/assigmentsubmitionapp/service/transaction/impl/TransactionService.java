package com.app.assigmentsubmitionapp.service.transaction.impl;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.neo4j.UsersNode;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TransactionService {
    Optional<ResponseBuilder<?>> createUserNode(UsersNode usersNode);

    Optional<ResponseBuilder<?>> createExpense(final List<List<String>> expenseActions, UUID expenseId);
    Optional<ResponseBuilder<?>> getAllCountriesWithUsersCount();
    Optional<ResponseBuilder<?>> getAllCountries();

    Optional<ResponseBuilder<?>> getAllCountriesWithUsersPercentage();

}
