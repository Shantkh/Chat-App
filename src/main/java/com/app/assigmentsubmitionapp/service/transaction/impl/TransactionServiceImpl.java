package com.app.assigmentsubmitionapp.service.transaction.impl;

import com.app.assigmentsubmitionapp.enums.ExpenseType;
import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.transactional.TransactionalMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.neo4j.*;
import com.app.assigmentsubmitionapp.repo.neo4j.*;
import com.app.assigmentsubmitionapp.utils.TransactionalUtils.ExpensesUtils;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import com.app.assigmentsubmitionapp.utils.TransactionalUtils.*;
import com.app.assigmentsubmitionapp.utils.others.ResponseUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.app.assigmentsubmitionapp.enums.Neo4jEnums.*;

@Service
@RequiredArgsConstructor
@Log4j2
public class TransactionServiceImpl implements TransactionService {
    private final CountryRepository countryRepository;

    private final TransactionalUtils transactionalUtils;
    private final ExpensesUtils expensesTransactionalService;
    private final FoodUtils foodUtils;
    private final UnknownUtils unknownUtils;
    private final HousingUtils housingUtils;
    private final ExpenseUtils expenseUtils;
    private final IncomeUtils incomeUtils;
    private final CountryUtils countryUtils;
    private final UsersUtils usersUtils;

    private final ResponseUtils responseUtils;
    private final TransactionalMapper transactionalMapper;


    @Override
    public Optional<ResponseBuilder<?>> createUserNode(UsersNode usersNode) {
        var usersList = usersUtils.findAllByEmail(usersNode);
        var country = countryUtils.findByCountryName(usersNode);
        UsersNode user;
        ExpensesNode expensesNode;
        IncomesNode incomesNode;
        if (country == null && usersList.size() == 0) {
            country = countryUtils.createCountry(usersNode.getCountry().getName());
            user = usersUtils.createUser(usersNode, country);
            usersUtils.saveUser(user);
            countryUtils.saveCountry(country);

        } else {
            if (usersList.size() == 0) {
                user = usersUtils.createUser(usersNode, country);
                usersUtils.saveUser(user);
            } else {
                throw new ApplicationExceptions("User already registered.");
            }
        }
        expensesNode = expenseUtils.createExpenseNode(usersNode.getUserId(), user);
        incomesNode = incomeUtils.createIncomeNode(usersNode.getUserId(), user);
        expenseUtils.saveExpense(expensesNode);
        incomeUtils.saveIncoming(incomesNode);

        var result = countryUtils.findByUserAndName(usersNode, country);

        return Optional.ofNullable(ResponseBuilder.builder()
                .data(result)
                .date(DateUtils.newTimeStamp())
                .httpStatus(HttpStatus.CREATED)
                .httpStatusCode(HttpStatus.CREATED.value())
                .isSuccess(false)
                .build());
    }

    @Override
    public Optional<ResponseBuilder<?>> getAllCountriesWithUsersCount() {
        var result = countryRepository.findAllByNameAndCount();
        return Optional.ofNullable(ResponseBuilder.builder()
                .data(result)
                .date(DateUtils.newTimeStamp())
                .httpStatus(HttpStatus.OK)
                .httpStatusCode(HttpStatus.OK.value())
                .isSuccess(false)
                .build());
    }

    @Override
    public Optional<ResponseBuilder<?>> getAllCountries() {
        var result = countryRepository.findAllCountries();
        var mapped = transactionalMapper.convertFromListCountryObjectToListCountryIdNameResponse(result);
        return responseUtils.responseWithOk(mapped);
    }

    @Override
    public Optional<ResponseBuilder<?>> getAllCountriesWithUsersPercentage() {
        var result = countryRepository.findAllCountryWithUsersPercentage();
        return responseUtils.responseWithOk(result);
    }

    @Override
    public Optional<ResponseBuilder<?>> createExpense(final List<List<String>> expenseActions, UUID userId) {
        ExpensesNode expensesNode = expensesTransactionalService.findUser(userId);
        List<UnknownResponse> expenseActionsList = new ArrayList<>();
        expenseActionsList = expenseActions.stream().flatMap(e -> checkData(e, expensesNode).stream()).toList();

        return Optional.ofNullable(Optional.ofNullable(ResponseBuilder.builder()
                .data(expenseActionsList)
                .date(DateUtils.newTimeStamp())
                .httpStatus(HttpStatus.CREATED)
                .httpStatusCode(HttpStatus.CREATED.value())
                .isSuccess(false)
                .build()).orElseThrow(ApplicationExceptions::new));
    }

    private List<UnknownResponse> checkData(List<String> expenseAction, ExpensesNode expensesNode) {

        Map<String, String> map = new HashMap<>();
        for (String s : expenseAction) {
            switch (getExpenseType(s)) {
                case PRICE -> map.put(PRICE, s);
                case CATEGORY -> map.put(CATEGORY, s);
                case CURRENCY -> map.put(CURRENCY, s);
                default -> map.put(EXPENSE, s);
            }
        }
        List<UnknownResponse> str = new ArrayList<>();
        switch (map.get(CATEGORY).toLowerCase()) {
            case HOUSING -> createHousingExpense(map, expensesNode);
            case FRUIT -> createFoodAngGrocery(map, expensesNode);
            case UNKNOWN -> str = createUnknownNotification(map, expensesNode);
        }
        return str;
    }

    private ExpenseType getExpenseType(String s) {
        if (transactionalUtils.checkNumber(s)) {
            return ExpenseType.PRICE;
        } else if (transactionalUtils.checkUpperCase(s)) {
            return ExpenseType.CATEGORY;
        } else if (transactionalUtils.checkSymbols(s)) {
            return ExpenseType.CURRENCY;
        } else {
            return ExpenseType.EXPENSE;
        }
    }



    private void createHousingExpense(Map<String, String> map, ExpensesNode expensesNode) {
        var result = housingUtils.buildAndSaveExpense(map, expensesNode);
        log.info("result : {}", result);
        housingUtils.saveHousing(result);
    }

    private void createFoodAngGrocery(Map<String, String> map, ExpensesNode expensesNode) {
        var result = foodUtils.buildAndSaveExpense(map, expensesNode);
        log.info("result : {}", result);
        foodUtils.saveFoodAngGrocery(result);
    }

    private List<UnknownResponse> createUnknownNotification(Map<String, String> map, ExpensesNode expenseAction) {
        return unknownUtils.buildAndReturnAndSave(map, expenseAction);
    }

}
