package com.app.assigmentsubmitionapp.service.incomes;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryUpdateRequest;

import java.util.Optional;
import java.util.UUID;

public interface IncSubCategoryService {
    Optional<ResponseBuilder<?>> subCategories(UUID categoryId);

    Optional<ResponseBuilder<?>> subCategoriesBySubId(UUID subCategoryId);

    Optional<ResponseBuilder<?>> addSubCategory(IncSubCategoryRequest categoryRequest);

    Optional<ResponseBuilder<?>> updateSubCategory(IncSubCategoryUpdateRequest subCategoryUpdateRequest);

    Optional<ResponseBuilder<?>> deleteSubCategory(UUID id);
}
