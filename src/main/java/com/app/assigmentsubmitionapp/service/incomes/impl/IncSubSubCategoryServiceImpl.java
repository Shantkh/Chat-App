package com.app.assigmentsubmitionapp.service.incomes.impl;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.expenses.ExpSubSubCategoryMapper;
import com.app.assigmentsubmitionapp.mapper.incomes.IncSubSubCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.repo.expenses.ExpCategoryRepository;
import com.app.assigmentsubmitionapp.repo.expenses.ExpSubCategoryRepository;
import com.app.assigmentsubmitionapp.repo.expenses.ExpSubSubCategoryRepository;
import com.app.assigmentsubmitionapp.repo.incomes.IncCategoryRepository;
import com.app.assigmentsubmitionapp.repo.incomes.IncSubCategoryRepository;
import com.app.assigmentsubmitionapp.repo.incomes.IncSubSubCategoryRepository;
import com.app.assigmentsubmitionapp.service.expenses.ExpSubSubCategoryService;
import com.app.assigmentsubmitionapp.service.incomes.IncSubSubCategoryService;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.app.assigmentsubmitionapp.enums.MessagesConst.DATA_NOT_FOUND;
import static com.app.assigmentsubmitionapp.enums.MessagesConst.UNKNOWN_ERROR;

@Service
@RequiredArgsConstructor
@Slf4j
public class IncSubSubCategoryServiceImpl implements IncSubSubCategoryService {
    private final IncSubSubCategoryRepository expSubSubCategoryRepository;
    private final IncSubSubCategoryMapper expSubSubCategoryMapper;
    private final IncSubCategoryRepository expSubCategoryRepository;

    private final IncCategoryRepository expCategoryRepository;

    @Override
    public Optional<ResponseBuilder<?>> subSubCategories(UUID subSubIncCategoryId) {
        isSubCategoryExist(subSubIncCategoryId);
        List<IncSubSubCategory> subCategories = expSubSubCategoryRepository.findByIncSubCategoryId(subSubIncCategoryId);
        Optional<List<IncSubSubCategoryResponse>> result = expSubSubCategoryMapper.fromListSubCategoryObjectToOptionalSubSubCategoryResponse(subCategories);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }


    @Override
    public Optional<ResponseBuilder<?>> subSubCategoriesBySubSubId(UUID subSubIncCategoryId) {
        isSubSubCategoryExist(subSubIncCategoryId);
        Optional<IncSubSubCategory> subCategories = expSubSubCategoryRepository.findById(subSubIncCategoryId);
        Optional<IncSubSubCategoryResponse> result = expSubSubCategoryMapper.fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(subCategories);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> updateSubSubCategory(IncSubSubCategoryUpdateRequest subCategoryUpdateRequest) {
        isSubSubCategoryExist(subCategoryUpdateRequest.getSubSubCategoryId());

        Optional<IncSubCategory> dataSubCategory = expSubCategoryRepository.findById(subCategoryUpdateRequest.getSubCategoryId());
        Optional<IncSubSubCategory> dataSubSubCategory = expSubSubCategoryRepository.findById(subCategoryUpdateRequest.getSubSubCategoryId());
        Optional<IncCategory> dataCategory = expCategoryRepository.findById(dataSubCategory.get().getIncCategory().getId());

        IncCategory expCategory = IncCategory.builder()
                .id(dataCategory.get().getId())
                .createdOn(dataCategory.get().getCreatedOn())
                .updatedOn(dataCategory.get().getUpdatedOn())
                .incCategoryName(dataCategory.get().getIncCategoryName())
                .incCategoryDescription(dataCategory.get().getIncCategoryDescription())
                .build();

        IncSubCategory exCategory = IncSubCategory.builder()
                .id(dataSubCategory.get().getId())
                .createdOn(dataSubCategory.get().getCreatedOn())
                .updatedOn(dataSubCategory.get().getUpdatedOn())
                .incSubCategoryName(dataSubCategory.get().getIncSubCategoryName())
                .incSubCategoryDescription(dataSubCategory.get().getIncSubCategoryDescription())
                .incCategory(expCategory)
                .build();

        IncSubSubCategory expSubCategory = IncSubSubCategory.builder()
                .id(dataSubSubCategory.get().getId())
                .incSubSubCategoryName(subCategoryUpdateRequest.getSubSubCategoryName())
                .incSubSubCategoryDescription(subCategoryUpdateRequest.getSubSubCategoryDescription())
                .updatedOn(DateUtils.newTimeStamp())
                .createdOn(dataSubSubCategory.get().getCreatedOn())
                .incSubCategory(exCategory)
                .build();

        var savedData = expSubSubCategoryRepository.save(expSubCategory);
        IncSubSubCategoryResponse result = expSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse(savedData);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }


    @Override
    public Optional<ResponseBuilder<?>> deleteSubSubCategory(UUID id) {
        isSubSubCategoryExist(id);
        expSubSubCategoryRepository.deleteById(id);
        var result = expSubSubCategoryRepository.findById(id);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> addSubSubCategory(IncSubSubCategoryRequest categoryRequest) {
        isSubCategoryExist(categoryRequest.getSubCategoryId());
        IncSubSubCategory expSubCategory = expSubSubCategoryMapper.fromSubSubCategoryRequestToSubSubCategoryObject(categoryRequest);
        IncSubSubCategory savedExpSubCategory = expSubSubCategoryRepository.saveAndFlush(expSubCategory);
        IncSubSubCategoryResponse result = expSubSubCategoryMapper.fromSubSubCategoryObjectToSubSubCategoryResponse(savedExpSubCategory);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.CREATED)
                        .httpStatusCode(HttpStatus.CREATED.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    private void isSubSubCategoryExist(UUID subSubExpCategoryId) {
        var result = expSubSubCategoryRepository.existsById(subSubExpCategoryId);
        if (!result) throw new ApplicationExceptions(DATA_NOT_FOUND);
    }

    private void isSubCategoryExist(UUID subSubExpCategoryId) {
        var result = expSubCategoryRepository.existsById(subSubExpCategoryId);
        if (!result) throw new ApplicationExceptions(DATA_NOT_FOUND);
    }


}
