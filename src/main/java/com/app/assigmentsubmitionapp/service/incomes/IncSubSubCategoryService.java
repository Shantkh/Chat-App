package com.app.assigmentsubmitionapp.service.incomes;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryUpdateRequest;

import java.util.Optional;
import java.util.UUID;

public interface IncSubSubCategoryService {
    Optional<ResponseBuilder<?>> subSubCategories(UUID subSubincCategoryId);

    Optional<ResponseBuilder<?>> subSubCategoriesBySubSubId(UUID subSubIncCategoryId);

    Optional<ResponseBuilder<?>> updateSubSubCategory(IncSubSubCategoryUpdateRequest subCategoryUpdateRequest);

    Optional<ResponseBuilder<?>> deleteSubSubCategory(UUID id);

    Optional<ResponseBuilder<?>> addSubSubCategory(IncSubSubCategoryRequest categoryRequest);

}
