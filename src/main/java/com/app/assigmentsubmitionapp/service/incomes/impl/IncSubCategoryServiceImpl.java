package com.app.assigmentsubmitionapp.service.incomes.impl;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.incomes.IncSubCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.repo.incomes.IncCategoryRepository;
import com.app.assigmentsubmitionapp.repo.incomes.IncSubCategoryRepository;
import com.app.assigmentsubmitionapp.service.incomes.IncSubCategoryService;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.app.assigmentsubmitionapp.enums.MessagesConst.DATA_NOT_FOUND;
import static com.app.assigmentsubmitionapp.enums.MessagesConst.UNKNOWN_ERROR;

@Service
@RequiredArgsConstructor
@Slf4j
public class IncSubCategoryServiceImpl implements IncSubCategoryService {

    private final IncSubCategoryMapper subCategoryMapper;
    private final IncSubCategoryRepository incSubCategoryRepository;
    private final IncCategoryRepository incCategoryRepository;

    @Override
    public Optional<ResponseBuilder<?>> subCategories(UUID categoryId) {
        isCategoryExist(categoryId);
        var subCategories = incSubCategoryRepository.findByIncCategoryId(categoryId);
        var result = subCategoryMapper.fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(subCategories);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> subCategoriesBySubId(UUID subCategoryId) {
        isSubCategoryExist(subCategoryId);
        var subCategories = incSubCategoryRepository.findById(subCategoryId);
        var result = subCategoryMapper.fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(subCategories);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> addSubCategory(IncSubCategoryRequest categoryRequest) {
        isCategoryExist(categoryRequest.getCategoryId());
        var expSubCategory = subCategoryMapper.fromSubCategoryRequestToSubCategoryObject(categoryRequest);
        var savedExpSubCategory = incSubCategoryRepository.saveAndFlush(expSubCategory);
        var result = subCategoryMapper.fromSubCategoryObjectToSubCategoryResponse(savedExpSubCategory);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.CREATED)
                        .httpStatusCode(HttpStatus.CREATED.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));

    }

    @Override
    public Optional<ResponseBuilder<?>> updateSubCategory(IncSubCategoryUpdateRequest subCategoryUpdateRequest) {
        isSubCategoryExist(subCategoryUpdateRequest.getSubCategoryId());

        var dataCategory = incCategoryRepository.findById(subCategoryUpdateRequest.getCategoryId());
        var dataSubCategory = incSubCategoryRepository.findById(subCategoryUpdateRequest.getSubCategoryId());


        var category = IncCategory.builder()
                .id(dataCategory.get().getId())
                .createdOn(dataCategory.get().getCreatedOn())
                .updatedOn(dataCategory.get().getUpdatedOn())
                .incCategoryName(dataCategory.get().getIncCategoryName())
                .incCategoryDescription(dataCategory.get().getIncCategoryDescription())
                .build();

        var expSubCategory = IncSubCategory.builder()
                .id(subCategoryUpdateRequest.getSubCategoryId())
                .incSubCategoryName(subCategoryUpdateRequest.getSubCategoryName())
                .incSubCategoryDescription(subCategoryUpdateRequest.getSubCategoryDescription())
                .updatedOn(DateUtils.newTimeStamp())
                .createdOn(dataSubCategory.get().getCreatedOn())
                .incCategory(category)
                .build();

        var savedData = incSubCategoryRepository.save(expSubCategory);
        var result = subCategoryMapper.fromSubCategoryObjectToSubCategoryResponse(savedData);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }


    @Override
    public Optional<ResponseBuilder<?>> deleteSubCategory(UUID id) {
        isSubCategoryExist(id);
        incSubCategoryRepository.deleteById(id);
        var result = incSubCategoryRepository.findById(id);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    private void isSubCategoryExist(UUID subCategoryId) {
        var result = incSubCategoryRepository.existsById(subCategoryId);
        if (!result) throw new ApplicationExceptions(DATA_NOT_FOUND);
    }

    private void isCategoryExist(UUID categoryId) {
        var result = incCategoryRepository.existsById(categoryId);
        if (!result) throw new ApplicationExceptions(DATA_NOT_FOUND);
    }
}
