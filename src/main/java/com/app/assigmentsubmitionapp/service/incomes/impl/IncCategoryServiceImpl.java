package com.app.assigmentsubmitionapp.service.incomes.impl;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.mapper.incomes.IncCategoryMapper;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncCategoryRequest;
import com.app.assigmentsubmitionapp.repo.incomes.IncCategoryRepository;
import com.app.assigmentsubmitionapp.service.incomes.IncCategoryService;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

import static com.app.assigmentsubmitionapp.enums.MessagesConst.DATA_NOT_FOUND;
import static com.app.assigmentsubmitionapp.enums.MessagesConst.UNKNOWN_ERROR;

@Service
@RequiredArgsConstructor
public class IncCategoryServiceImpl implements IncCategoryService {

    private final IncCategoryMapper incCategoryMapper;
    private final IncCategoryRepository incCategoryRepository;

    @Override
    public Optional<ResponseBuilder<?>> addCategory(IncCategoryRequest incCategoryRequest) {
        var category = incCategoryMapper.fromIncCategoryRequestToIncCategoryObject(incCategoryRequest);
        var savedResponse = incCategoryRepository.saveAndFlush(category);
        var result = incCategoryMapper.fromIncCategoryObjectToCategoryResponse(savedResponse);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.CREATED)
                        .httpStatusCode(HttpStatus.CREATED.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> deleteCategory(UUID id) {
        checkExists(id);
        incCategoryRepository.deleteById(id);
        var result = incCategoryRepository.findById(id);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> categories() {
        var categoryList = incCategoryRepository.findAll();
        var result = incCategoryMapper.fromCategoryObjectListToListCategoryResponse(categoryList);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> updateCategory(IncCategoryRequest categoryRequest) {
        checkExists(categoryRequest.getCategoryId());
        var category = incCategoryRepository.getReferenceById(categoryRequest.getCategoryId());
        category.setUpdatedOn(DateUtils.newTimeStamp());
        category.setIncCategoryName(categoryRequest.getIncCategoryName());
        category.setIncCategoryDescription(categoryRequest.getIncCategoryDescription());
        category.setId(categoryRequest.getCategoryId());
        var response = incCategoryRepository.saveAndFlush(category);
        var result = incCategoryMapper.fromIncCategoryObjectToCategoryResponse(response);
        return Optional.ofNullable(Optional.of(ResponseBuilder.builder()
                        .data(result)
                        .httpStatus(HttpStatus.OK)
                        .httpStatusCode(HttpStatus.OK.value())
                        .isSuccess(true)
                        .date(DateUtils.newTimeStamp())
                        .build())
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    private void checkExists(UUID id) {
        var isExists = incCategoryRepository.existsById(id);
        if (!isExists) throw new ApplicationExceptions(DATA_NOT_FOUND);
    }
}
