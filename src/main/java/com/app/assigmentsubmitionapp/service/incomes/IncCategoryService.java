package com.app.assigmentsubmitionapp.service.incomes;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncCategoryRequest;

import java.util.Optional;
import java.util.UUID;

public interface IncCategoryService {
    Optional<ResponseBuilder<?>> addCategory(IncCategoryRequest incCategoryRequest);

    Optional<ResponseBuilder<?>> deleteCategory(UUID id);

    Optional<ResponseBuilder<?>> categories();

    Optional<ResponseBuilder<?>> updateCategory(IncCategoryRequest categoryRequest);
}
