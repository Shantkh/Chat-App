package com.app.assigmentsubmitionapp.service.user.settings;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.localsettings.LocalSettings;
import com.app.assigmentsubmitionapp.repo.localsettings.LocalSettingsRepository;
import com.app.assigmentsubmitionapp.service.user.UserSettings;
import com.app.assigmentsubmitionapp.user.dto.UserLocalSettings;
import com.app.assigmentsubmitionapp.utils.AppPreconditions;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.app.assigmentsubmitionapp.enums.MessagesConst.UNKNOWN_ERROR;

@Service
@RequiredArgsConstructor
public class UserSettingsImpl implements UserSettings {
    private final LocalSettingsRepository localSettingsRepository;

    @Override
    public Optional<ResponseBuilder<?>> updateLocalSettings(LocalSettings l) {
        AppPreconditions.checkNotNullOrEmpty(l);
        Optional<LocalSettings> response = localSettingsRepository.findAllByUserEmail(l.getUserEmail());
        AppPreconditions.checkNotNullOrEmptyOptional(response);
        LocalSettings localSettings = LocalSettings.builder()
                .id(response.get().getId())
                .mass(l.getMass())
                .userEmail(l.getUserEmail())
                .temperature(l.getTemperature())
                .volume(l.getVolume())
                .lengths(l.getLengths())
                .currency(l.getCurrency())
                .country(l.getCurrency().getCountry())
                .symbol(l.getCurrency().getSymbol())
                .build();

        LocalSettings result = localSettingsRepository.saveAndFlush(localSettings);
        return Optional.ofNullable(Optional.of(result)
                .map(e -> new ResponseBuilder<>(
                        e,
                        HttpStatus.OK,
                        HttpStatus.OK.value(),
                        true,
                        DateUtils.newTimeStamp()))
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }

    @Override
    public Optional<ResponseBuilder<?>> userLocalSettings(UserLocalSettings u) {
        AppPreconditions.checkNotNullOrEmpty(u);
        Optional<LocalSettings> result = localSettingsRepository.findAllByUserEmail(u.getUserEmail());
        AppPreconditions.checkNotNullOrEmptyOptional(result);
        return Optional.ofNullable(Optional.of(result)
                .map(e -> new ResponseBuilder<>(
                        e,
                        HttpStatus.OK,
                        HttpStatus.OK.value(),
                        true,
                        DateUtils.newTimeStamp()))
                .orElseThrow(() -> new ApplicationExceptions(UNKNOWN_ERROR)));
    }
}
