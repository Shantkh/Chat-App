package com.app.assigmentsubmitionapp.service.user;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.localsettings.LocalSettings;
import com.app.assigmentsubmitionapp.user.dto.UserLocalSettings;

import java.util.Optional;

public interface UserSettings {
    Optional<ResponseBuilder<?>> updateLocalSettings(LocalSettings localSettings);

    Optional<ResponseBuilder<?>> userLocalSettings(UserLocalSettings userLocalSettings);
}
