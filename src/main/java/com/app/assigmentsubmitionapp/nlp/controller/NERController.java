package com.app.assigmentsubmitionapp.nlp.controller;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;


@RestController
@Slf4j
public class NERController {

    @Autowired
    private StanfordCoreNLP stanfordCoreNLP;

//    @PostMapping(value = "/ner")
//    public Set<String> ner(@RequestBody final String input, @RequestParam final Type type) {
//        CoreDocument coreDocument = new CoreDocument(input);
//        stanfordCoreNLP.annotate(coreDocument);
//        List<CoreLabel> coreLabels = coreDocument.tokens();
//        return new HashSet<>(collectList(coreLabels, type));
//    }

    @PostMapping(value = "/ner/all")
    public Set<String> ner(@RequestBody final String input) {
        return null;
    }

//    private List<String> collectList(List<CoreLabel> coreLabels, final Type type) {
//
//        return coreLabels
//                .stream()
//                .filter(coreLabel -> type.getName().equalsIgnoreCase(coreLabel.get(CoreAnnotations.NamedEntityTagAnnotation.class)))
//                .map(CoreLabel::originalText)
//                .collect(Collectors.toList());
//    }
}