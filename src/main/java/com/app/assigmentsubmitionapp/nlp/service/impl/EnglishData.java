package com.app.assigmentsubmitionapp.nlp.service.impl;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Service
public class EnglishData {

    public Map<String, String> getEnglishNLP(String sentence) throws IOException {
        Map<String, String> result = new HashMap<>();
        String[] executionArray = fillDataInEnglish();
        if (Arrays.asList(executionArray).contains(sentence.toLowerCase())) {
            int index = Arrays.asList(executionArray).indexOf(sentence.toLowerCase());
            result.put(executionArray[index], executionArray[index + 1]);
        } else {
            result.put(sentence, "unknown");
        }
        return result;
    }

    private String[] fillDataInEnglish() throws IOException {
        var content = new String(Files.readAllBytes(Paths.get("./eng-ner-dict.txt")));
        return content.split(",");
    }
}
