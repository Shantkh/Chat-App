package com.app.assigmentsubmitionapp;

import com.app.assigmentsubmitionapp.repo.JwtUserRepository;
import com.app.assigmentsubmitionapp.service.jwtusers.JwtUserService;
import com.app.assigmentsubmitionapp.user.*;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.UUID;

@Component
@RequiredArgsConstructor
@Slf4j
public class InitUsers implements CommandLineRunner {

    private final JwtUserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUserService jwtUserService;

    @Override
    public void run(String... args) throws Exception {
        try {
            if (jwtUserService.findJwtUserByEmail("admin@test.com").isEmpty()) {
                JwtUser u = JwtUser.builder()
                        .id(UUID.randomUUID())
                        .username("admin@test.com")
                        .email("admin@test.com")
                        .password(passwordEncoder.encode("test123"))
                        .role(Set.of(Role.ROLE_ADMIN, Role.ROLE_BUYER, Role.ROLE_SELLER))
                        .firstname("Shant")
                        .lastname("Khayalian")
                        .enabled(true)
                        .userEmailVerified(true)
                        .createdOn(DateUtils.newTimeStamp())
                        .updatedOn(DateUtils.newTimeStamp())
                        .build();
                repository.save(u);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        if (jwtUserService.findJwtUserByEmail("buyer@test.com").isEmpty()) {
            JwtUser s = JwtUser.builder()
                    .username("buyer@test.com")
                    .email("buyer@test.com")
                    .password(passwordEncoder.encode("test123"))
                    .role(Set.of(Role.ROLE_USER))
                    .firstname("Shant1")
                    .lastname("Khayalian1")
                    .userEmailVerified(true)
                    .enabled(true)
                    .build();
            repository.save(s);
        }
        if (jwtUserService.findJwtUserByEmail("seller@test.com").isEmpty()) {
            JwtUser u = JwtUser.builder()
                    .username("seller@test.com")
                    .email("seller@test.com")
                    .password(passwordEncoder.encode("test123"))
                    .role(Set.of(Role.ROLE_USER, Role.ROLE_ADMIN))
                    .firstname("Shant2")
                    .lastname("Khayalian2")
                    .userEmailVerified(true)
                    .enabled(true)
                    .build();
            repository.save(u);
        }
    }

}