package com.app.assigmentsubmitionapp.nernlp.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data(staticConstructor = "of")
@Builder
public class LanguageDetect implements Serializable {
    private final String languageCode;
    private final String language;
}
