package com.app.assigmentsubmitionapp.nernlp.utils;


import com.app.assigmentsubmitionapp.nernlp.model.LanguageDetect;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import opennlp.tools.langdetect.LanguageDetectorME;
import opennlp.tools.langdetect.LanguageDetectorModel;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


@RequiredArgsConstructor
@Slf4j
@Component
public class LanguageDetectController {
    private final LanguageCountryUtils languageCountryUtils;

    public LanguageDetect detectLang(final String text) throws IOException {
        InputStream modelIn = new FileInputStream("langdetect-183.bin");
        LanguageDetectorModel model = new LanguageDetectorModel(modelIn);
        LanguageDetectorME detector = new LanguageDetectorME(model);

        String language = String.valueOf(detector.predictLanguage(text));
        var result = languageCountryUtils.detectingLanguage(language);
        log.info("Detected language: {} ", result);
        return LanguageDetect.builder()
                .language(result)
                .languageCode(language.substring(0,3))
                .build();
    }

}