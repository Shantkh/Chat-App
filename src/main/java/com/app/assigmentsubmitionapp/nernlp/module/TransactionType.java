package com.app.assigmentsubmitionapp.nernlp.module;

public enum TransactionType {
    INCOME, EXPENSE
}
