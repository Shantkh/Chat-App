package com.app.assigmentsubmitionapp.nernlp.module;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data(staticConstructor = "of")
public class UserTransactionsStringRequest implements Serializable {
    private  String screenShotString;
}
