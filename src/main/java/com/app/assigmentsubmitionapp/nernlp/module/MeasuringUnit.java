package com.app.assigmentsubmitionapp.nernlp.module;

public enum MeasuringUnit {

    WEIGHT,
    VOLUME,
    QUANTITY,
    EACH,
    PACK,
    JUG,
    CARTON,
    LOAF,
    BOX,
    ROLL,
    BOTTLE,
    CAN,
    BUNCH,

}
