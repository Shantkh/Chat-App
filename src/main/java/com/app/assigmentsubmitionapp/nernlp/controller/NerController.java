package com.app.assigmentsubmitionapp.nernlp.controller;

import com.app.assigmentsubmitionapp.api.NERApi;
import com.app.assigmentsubmitionapp.nernlp.module.UserTransactionsStringRequest;
import com.app.assigmentsubmitionapp.nernlp.service.NerService;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Optional;
@RestController
@RequiredArgsConstructor
public class NerController implements NERApi {

    private final NerService nerServiceImpl;


    @Override
    public ResponseEntity<ResponseBuilder<?>> NERWithLanguages(final UserTransactionsStringRequest nerUserModule) throws IOException {
        Optional<ResponseBuilder<?>> result = nerServiceImpl.readStringWithType(nerUserModule);
        return new ResponseEntity<>(result.get(), result.get().getHttpStatus());
    }
}
