package com.app.assigmentsubmitionapp.nernlp.service.impl;

import com.app.assigmentsubmitionapp.nernlp.config.NLPConfig;
import com.app.assigmentsubmitionapp.nernlp.model.LanguageDetect;
import com.app.assigmentsubmitionapp.nernlp.module.UserTransactionsStringRequest;
import com.app.assigmentsubmitionapp.nernlp.service.NerService;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.nernlp.utils.LanguageDetectController;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class NerServiceImpl implements NerService {

    private static final String STRING_CHECK = "O";
    private static final String NEURAL_NETWORK = "NN";
    private static final String CONCEPTUAL_DEPENDENCY = "CD";
    private static final String PROPER_NOUN = "NNP";
    private static final String NOT_RELATED = "NOTRELATED";
    private static final String AI_NEED_TO_LEARN = "Undefined by Application AI";


    private final LanguageDetectController languageDetectController;

    @Override
    public Optional<ResponseBuilder<?>> readStringWithType(UserTransactionsStringRequest userTransactions) throws IOException {
        Map<String, String> result = new HashMap<>();

        var language = languageDetectController.detectLang(userTransactions.getScreenShotString());
        result = startFilterNLP(language, userTransactions.getScreenShotString());


        return Optional.ofNullable(ResponseBuilder.builder()
                .date(DateUtils.newTimeStamp())
                .httpStatus(HttpStatus.CREATED)
                .httpStatusCode(HttpStatus.CREATED.value())
                .isSuccess(true)
                .data(result)
                .build());
    }

    private Map<String, String> startFilterNLP(LanguageDetect language, String userTransactions) throws IOException {
        Map<String, String> aiFilter = aiFiltering(userTransactions);
        Map<String, String> appFilter = detectLanguageAndResults(language, aiFilter);

        return appFilter;

    }

    private Map<String, String> detectLanguageAndResults(LanguageDetect language, Map<String, String> aiFilter) throws IOException {
        switch (language.getLanguageCode()) {
            case "eng":
                return getEnglishNLP(aiFilter);
        }
        return new HashMap<>();
    }

    private Map<String, String> getEnglishNLP(Map<String, String> aiFilter) throws IOException {
        String[] executionArray = fillDataInEnglish();
        Iterator<Map.Entry<String, String>> iterator = aiFilter.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String key = entry.getKey().toLowerCase();
            String value = entry.getValue();
            if (isValue(value)) {
                if (isContains(executionArray, key)) {
                    var index = Arrays.asList(executionArray).indexOf(key);
                    if(!executionArray[index + 1].equals(NOT_RELATED)) {
                        entry.setValue(executionArray[index + 1]);
                    }else{
                        removeData(iterator);
                    }
                } else if (!isContains(executionArray, key)) {
                    var result = filterByPartOfSpeech(key);
                    if (!isResult(result)) {
                        removeData(iterator);
                    }else {
                        entry.setValue(AI_NEED_TO_LEARN);
                    }
                }
            }
        }
        return aiFilter;
    }

    private void removeData(Iterator<Map.Entry<String, String>> iterator) {
        iterator.remove();
    }

    private boolean isResult(String result) {
        return result.equals(NEURAL_NETWORK) || result.equals(CONCEPTUAL_DEPENDENCY) || result.equals(PROPER_NOUN);
    }

    private boolean isValue(String value) {
        return value.equals(STRING_CHECK);
    }

    private boolean isContains(String[] executionArray, String key) {
        return Arrays.asList(executionArray).contains(key);
    }


    private String filterByPartOfSpeech(String key) {
        String result = null;
        StanfordCoreNLP stanfordCoreNLP = NLPConfig.getInstance();
        CoreDocument coreDocument = new CoreDocument(key);
        stanfordCoreNLP.annotate(coreDocument);
        List<CoreLabel> coreLabelList = coreDocument.tokens();
        for (CoreLabel coreLabel : coreLabelList) {

            result = coreLabel.get(CoreAnnotations.PartOfSpeechAnnotation.class);
            log.info("results {} ", result);
        }
        return result;
    }


    private Map<String, String> aiFiltering(String screenShotString) {
        log.info("screenShotString {} ", screenShotString);
        Map<String, String> result = new LinkedHashMap<>();

        List<String> stringResult = new ArrayList<>();

        StanfordCoreNLP stanfordCoreNLP = NLPConfig.getInstance();
        CoreDocument coreDocument = new CoreDocument(screenShotString.toLowerCase());
        stanfordCoreNLP.annotate(coreDocument);
        List<CoreLabel> coreLabels = coreDocument.tokens();

        for (CoreLabel coreLabel : coreLabels) {
            log.info("coreLabel {} ", coreLabel);
            String ner = coreLabel.get(CoreAnnotations.NamedEntityTagAnnotation.class);
            result.put(coreLabel.originalText(), ner);
            stringResult.add(coreLabel.originalText());
            stringResult.add(ner);
            log.info("ner {} ", ner);
        }
        System.out.println(stringResult);
        return result;
    }

    private String[] fillDataInEnglish() throws IOException {
        var content = new String(Files.readAllBytes(Paths.get("./eng-ner-dict.txt")));
        return content.split(",");
    }
}
