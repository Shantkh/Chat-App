package com.app.assigmentsubmitionapp.nernlp.service;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.nernlp.module.UserTransactionsStringRequest;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface NerService {
    Optional<ResponseBuilder<?>> readStringWithType(UserTransactionsStringRequest input) throws IOException;
}
