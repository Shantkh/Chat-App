package com.app.assigmentsubmitionapp.security.application;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.repo.JwtUserRepository;
import com.app.assigmentsubmitionapp.security.JwtUtils;
import com.app.assigmentsubmitionapp.security.application.repo.RefreshTokenRepository;
import com.app.assigmentsubmitionapp.security.domain.JwtRefreshRequestDto;
import com.app.assigmentsubmitionapp.security.domain.JwtResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service

public class RefreshTokenService {
    @Value("${jwt.refreshToken.expiration}")
    private Long refreshTokenDurationMs;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private JwtUserRepository userRepository;

    @Autowired
    private JwtUtils jwtUtils;

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByRefreshToken(token);
    }


    public RefreshToken createRefreshToken(UUID userId) {
        RefreshToken refreshToken = new RefreshToken();

        refreshToken.setUser(userRepository.findById(userId).get());
        refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDurationMs));
        refreshToken.setRefreshToken(UUID.randomUUID().toString());
        refreshToken.setId(UUID.randomUUID());

        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public JwtResponseDto refreshToken(JwtRefreshRequestDto refreshRequestDto) {
        var tokenOpt = refreshTokenRepository.findByRefreshToken(refreshRequestDto.getRefreshToken());
        if (tokenOpt.isEmpty()) {
            throw new ApplicationExceptions("Refresh Token %s not found!" + (refreshRequestDto.getRefreshToken()));
        }
        var token = tokenOpt.get();
        if (isExpired(token)) {
            verifyExpiration(token);
            refreshTokenRepository.delete(token);
            throw new ApplicationExceptions("Refresh Token %s was expired!" + (refreshRequestDto.getRefreshToken()));
        }
        String jwt = jwtUtils.createJwt(token.getUser().getEmail());
        updateToken(token);
        return JwtResponseDto.of(jwt, token.getRefreshToken());
    }

    private void updateToken(RefreshToken token) {
        token.setExpiryDate(Instant.now());
        refreshTokenRepository.save(token);
    }

    public boolean isExpired(RefreshToken token) {
       return  token.getExpiryDate().compareTo(Instant.now()) < 0;
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new ApplicationExceptions(token.getRefreshToken());
        }

        return token;
    }

    @Transactional
    public int deleteByUserId(UUID userId) {
        return refreshTokenRepository.deleteByUser(userRepository.findById(userId).get());
    }
}