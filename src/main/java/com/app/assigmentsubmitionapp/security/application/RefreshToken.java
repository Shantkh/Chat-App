package com.app.assigmentsubmitionapp.security.application;

import com.app.assigmentsubmitionapp.user.JwtUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import javax.persistence.*;

@Entity(name = "refreshtoken")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RefreshToken {
  @Id
  @Column(name = "refresh_token_id")
  private UUID id;

  @Column(name = "created_on")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdOn;

  @Column(name = "updated_on")
  @Temporal(TemporalType.TIMESTAMP)
  private Date updatedOn;

  @JsonIgnore
  @OneToOne(mappedBy = "refreshToken", fetch = FetchType.LAZY)
  @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
  private JwtUser user;

  @Column(nullable = false, unique = true)
  private String refreshToken;

  @Column(nullable = false)
  private Instant expiryDate;

}