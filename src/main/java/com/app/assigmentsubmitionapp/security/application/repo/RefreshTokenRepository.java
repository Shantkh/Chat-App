package com.app.assigmentsubmitionapp.security.application.repo;

import com.app.assigmentsubmitionapp.security.application.RefreshToken;
import com.app.assigmentsubmitionapp.user.JwtUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, UUID> {

    Optional<RefreshToken> findByRefreshToken(String token);

    @Modifying
    int deleteByUser(JwtUser user);

}