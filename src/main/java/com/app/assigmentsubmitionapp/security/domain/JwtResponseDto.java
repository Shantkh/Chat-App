package com.app.assigmentsubmitionapp.security.domain;

import com.app.assigmentsubmitionapp.user.JwtUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class JwtResponseDto {

    private String token;
    private String refreshToken;
    private JwtUser jwtUser;

    public JwtResponseDto(String token, String refreshToken) {
        this.token = token;
        this.refreshToken = refreshToken;
    }

    public static JwtResponseDto of(String token, String refreshToken, JwtUser jwtUser) {
        return new JwtResponseDto(token, refreshToken, jwtUser);
    }

    public static JwtResponseDto of(String token, String refreshToken) {
        return new JwtResponseDto(token, refreshToken);
    }
}