package com.app.assigmentsubmitionapp.config;

import org.springframework.data.neo4j.core.Neo4jClient;
import org.springframework.data.neo4j.core.schema.IdGenerator;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class MyIdGenerator implements IdGenerator<String> {

    private final Neo4jClient neo4jClient;

    public MyIdGenerator(Neo4jClient neo4jClient) {
        this.neo4jClient = neo4jClient;
    }

    @Override
    public String generateId(String primaryLabel, Object entity) {
        return neo4jClient.query(UUID.randomUUID().toString())
                .fetchAs(String.class)
                .one()
                .get();
    }
}
