package com.app.assigmentsubmitionapp.repo.expenses;

import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ExpSubCategoryRepository extends JpaRepository<ExpSubCategory, UUID> {
    List<ExpSubCategory> findByExpCategoryId(UUID categoryId);
}
