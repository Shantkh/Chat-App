package com.app.assigmentsubmitionapp.repo.expenses;

import com.app.assigmentsubmitionapp.modules.expenses.ExpSubSubCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
@Repository
public interface ExpSubSubCategoryRepository extends JpaRepository<ExpSubSubCategory, UUID> {

    List<ExpSubSubCategory> findByExpSubCategoryId(UUID expSubCategoryId);
}
