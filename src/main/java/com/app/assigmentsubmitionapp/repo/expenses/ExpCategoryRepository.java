package com.app.assigmentsubmitionapp.repo.expenses;

import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ExpCategoryRepository extends JpaRepository<ExpCategory, UUID> {
}
