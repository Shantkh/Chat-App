package com.app.assigmentsubmitionapp.repo.localsettings;

import com.app.assigmentsubmitionapp.modules.localsettings.LocalSettings;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface LocalSettingsRepository extends JpaRepository<LocalSettings, UUID> {
    Optional<LocalSettings> findAllByUserEmail(String email);
}
