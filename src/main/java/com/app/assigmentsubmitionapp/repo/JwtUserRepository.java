package com.app.assigmentsubmitionapp.repo;

import com.app.assigmentsubmitionapp.user.JwtUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface JwtUserRepository extends JpaRepository<JwtUser, UUID> {

    Optional<JwtUser> findJwtUserByUsername(String username);
    Optional<JwtUser> findJwtUserByEmail(String email);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);


}