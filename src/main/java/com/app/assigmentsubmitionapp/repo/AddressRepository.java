package com.app.assigmentsubmitionapp.repo;

import com.app.assigmentsubmitionapp.user.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AddressRepository extends JpaRepository<Address, UUID> {

}
