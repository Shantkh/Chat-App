package com.app.assigmentsubmitionapp.repo.incomes;

import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface IncSubCategoryRepository extends JpaRepository<IncSubCategory, UUID> {
    List<IncSubCategory> findByIncCategoryId(UUID categoryId);
}
