package com.app.assigmentsubmitionapp.repo.incomes;

import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface IncCategoryRepository extends JpaRepository<IncCategory, UUID> {
}
