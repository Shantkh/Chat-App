package com.app.assigmentsubmitionapp.repo.incomes;

import com.app.assigmentsubmitionapp.modules.expenses.ExpSubSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubSubCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface IncSubSubCategoryRepository extends JpaRepository<IncSubSubCategory, UUID> {

    List<IncSubSubCategory> findByIncSubCategoryId(UUID incSubCategoryId);
}
