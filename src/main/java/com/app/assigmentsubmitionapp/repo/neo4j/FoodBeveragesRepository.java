package com.app.assigmentsubmitionapp.repo.neo4j;

import com.app.assigmentsubmitionapp.modules.neo4j.FoodBeverages;
import org.springframework.data.neo4j.repository.Neo4jRepository;

import java.util.UUID;

public interface FoodBeveragesRepository extends Neo4jRepository<FoodBeverages, UUID> {
}
