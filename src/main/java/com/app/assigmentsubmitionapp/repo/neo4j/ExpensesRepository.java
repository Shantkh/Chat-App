package com.app.assigmentsubmitionapp.repo.neo4j;

import com.app.assigmentsubmitionapp.modules.neo4j.ExpensesNode;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.ReactiveNeo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface ExpensesRepository extends Neo4jRepository<ExpensesNode, UUID> {
    @Query("MATCH(e:expenses) WHERE e.user_id = $userId RETURN e")
    List<ExpensesNode> findByUserId(@Param("userId") UUID userId);
}
