package com.app.assigmentsubmitionapp.repo.neo4j;

import com.app.assigmentsubmitionapp.modules.neo4j.IncomesNode;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface IncomesRepository extends Neo4jRepository<IncomesNode, UUID> {
    @Query("MATCH(e:expenses) WHERE e.user_id = $userId RETURN e")
    List<IncomesNode> findByUserId(@Param("userId") UUID userId);
}
