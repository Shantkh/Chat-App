package com.app.assigmentsubmitionapp.repo.neo4j;

import com.app.assigmentsubmitionapp.modules.neo4j.Housing;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;

import java.util.UUID;

public interface HousingRepository extends Neo4jRepository<Housing, UUID> {

}
