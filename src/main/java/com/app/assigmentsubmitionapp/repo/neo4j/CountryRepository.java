package com.app.assigmentsubmitionapp.repo.neo4j;

import com.app.assigmentsubmitionapp.modules.neo4j.Country;
import com.app.assigmentsubmitionapp.modules.neo4j.dto.CountryAndUsersCount;
import com.app.assigmentsubmitionapp.modules.neo4j.dto.CountryIdNameResponse;
import com.app.assigmentsubmitionapp.modules.neo4j.dto.CountryWithUsersPercentageResponse;
import com.app.assigmentsubmitionapp.modules.neo4j.dto.UserAndCountry;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;


public interface CountryRepository extends Neo4jRepository<Country, UUID> {

    @Query("MATCH (c:country {name: $name}) RETURN count(c) > 0")
    Boolean existsByCountryName(@Param("name") String name);

    @Query("MATCH (u:country) WHERE u.name = $name RETURN *")
    Country findByName(@Param("name") String name);

    @Query("MATCH (e:expenses)-[:CREATE_EXPENSE]->(u:user {user_email: $userEmail})" +
            "MATCH (i:incomes)-[:CREATE_INCOME]->(u:user)-[:COUNTRY_FROM]->(c:country {name: $countryName})" +
            "RETURN u.user_email AS userEmail, c.name AS countryName, i.id AS incomeId, e.id AS expenseId")
    UserAndCountry findByUsersEqualsAndName(@Param("userEmail") String userEmail,
                                            @Param("countryName") String countryName);

    @Query("MATCH (u:user)-[:COUNTRY_FROM]->(c:country) RETURN c.name AS countryName, count(u) as usersCount ORDER BY c.name ASC")
    List<CountryAndUsersCount> findAllByNameAndCount();

    @Query("MATCH(c:country) return c.id AS id, c.name AS name ORDER BY c.name ASC")
    List<Country> findAllCountries();

    @Query("MATCH (c:country {id: $id})<-[:COUNTRY_FROM]-(u:user)" +
            "<-[:CREATE_EXPENSE]-(e:expenses)<-[:FOOD_EXPENSE]-(f:foodBeverages),(e)" +
            "<-[:HOUSE_EXPENSE]-(h:house) RETURN c, u, e, f, h")
    List<Country> findAllExpensesByCountryId(@Param("id") UUID id);

    @Query("MATCH (c:country)<-[:COUNTRY_FROM]-(u:user) " +
            "WITH c.name AS name, COUNT(DISTINCT u) AS numofusers, COLLECT(u) AS users " +
            "WITH name, numofusers, size(users) AS countrytotal, users " +
            "MATCH (a:user) " +
            "WITH count(Size(users)) AS total_users, name, numofusers " +
            "Return name, numofusers, round((numofusers/toFloat(total_users)) *100,3) AS percentage")
    List<CountryWithUsersPercentageResponse> findAllCountryWithUsersPercentage();

    //MATCH (c:country {id: "d4aab57a-8eef-4f92-9271-36843ed9e737"})-[*]-(n)
    //RETURN c, n

    //MATCH (c:country {id: "d4aab57a-8eef-4f92-9271-36843ed9e737"})<-[:COUNTRY_FROM]-(u:user{user_id: "3f085910-d134-11ed-ada1-0242ac120002"})-[*]-(n)
    //RETURN c, n

//    MATCH (c:country)-[:COUNTRY_FROM]-(u:user)
//    WITH c, COUNT(DISTINCT u) AS num_users
//    RETURN AVG(num_users)


//    MATCH (c:country)<-[:COUNTRY_FROM]-(u:user)
//    WITH c.name AS name, COUNT(DISTINCT u) AS numofusers, COLLECT(u) AS users
//    WITH name, numofusers, size(users) AS countrytotal, users
//    MATCH (a:user)
//    WITH count(Size(users)) AS total_users, name, numofusers
//    Return name, numofusers, round((numofusers/toFloat(total_users)) *100,2, 'UP') AS percentage

}
