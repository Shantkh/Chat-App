package com.app.assigmentsubmitionapp.repo.neo4j;

import com.app.assigmentsubmitionapp.modules.neo4j.UsersNode;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UsersNodeRepository extends Neo4jRepository<UsersNode, UUID> {
    @Query("MATCH (u:user) WHERE u.user_email = $email RETURN *")
    List<UsersNode> findAllByEmail(@Param("email") String email);

}
