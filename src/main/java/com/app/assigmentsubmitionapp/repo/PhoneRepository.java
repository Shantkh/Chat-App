package com.app.assigmentsubmitionapp.repo;

import com.app.assigmentsubmitionapp.user.PhoneNumbers;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PhoneRepository extends JpaRepository<PhoneNumbers, UUID> {
}
