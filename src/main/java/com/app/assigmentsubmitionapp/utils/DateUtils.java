package com.app.assigmentsubmitionapp.utils;

import org.springframework.stereotype.Component;

import javax.persistence.Convert;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

@Component
public class DateUtils {


    public static Timestamp newTimeStamp(){
        return new Timestamp(new Date().getTime());
    }

}