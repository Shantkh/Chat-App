package com.app.assigmentsubmitionapp.utils;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.modules.EmailDetails;
import com.app.assigmentsubmitionapp.modules.ForgotPasswordRequest;
import com.app.assigmentsubmitionapp.repo.JwtUserRepository;
import com.app.assigmentsubmitionapp.security.domain.LoginCredentials;
import com.app.assigmentsubmitionapp.user.JwtUser;
import com.app.assigmentsubmitionapp.user.PhoneNumbers;
import com.app.assigmentsubmitionapp.user.dto.UserRegistrationRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.app.assigmentsubmitionapp.enums.ExceptionEnums.*;

@Component
@RequiredArgsConstructor
public class UserPreconditions {
    private final JwtUserRepository userRepository;

    public void isEmailVerified(Optional<JwtUser> userInfo) {
        if (!userInfo.get().isUserEmailVerified())
            throw new ApplicationExceptions(VERIFY_EMAIL);
    }

    public void isPoneNumberVerified(Optional<JwtUser> userInfo) {
        var phoneNumber = userInfo.get().getUsersDetails().getPhoneNumbers();
        List<PhoneNumbers> phoneNumbersList = phoneNumber.stream()
                .map(e -> PhoneNumbers.builder().id(e.getId()).phoneNumber(e.getPhoneNumber())
                        .isNumberVerified(e.isNumberVerified()).build()).toList();
        for (PhoneNumbers phoneNumbers : phoneNumbersList) {
            boolean isVerified = phoneNumbers.isNumberVerified();
            if (!isVerified)
                throw new ApplicationExceptions(VERIFY_PHONE_NUMBER);
        }
    }


    public void checkUserLoginDetails(LoginCredentials loginRequest) {
        UserPreconditions userPreconditions = new UserPreconditions(userRepository);
        userPreconditions.checkIfUserExists(loginRequest);
        Optional<JwtUser> userInfo = userRepository.findJwtUserByUsername(loginRequest.getEmail());
        userPreconditions.present(userInfo);
        userPreconditions.isEmailVerified(userInfo);
//        userPreconditions.isPoneNumberVerified(userInfo);
    }

    public void checkUserRegisterDetails(UserRegistrationRequest signUpRequest) {
        JwtUser user = JwtUser.builder().username(signUpRequest.getEmail()).email(signUpRequest.getEmail()).build();
        isUserExists(user);
        isUserEmailExists(user);
    }

    private void isUserEmailExists(JwtUser user) {
        if (userRepository.existsByEmail(user.getEmail()))
            throw new ApplicationExceptions(EMAIL_ALREADY_EXSIST);

    }

    private void isUserExists(JwtUser user) {
        if (userRepository.existsByEmail(user.getEmail()))
            throw new ApplicationExceptions(String.format(USERNAME_ALREADY_EXSIST));

    }

    public void checkIfUserExists(LoginCredentials user) {
        present(userRepository.findJwtUserByEmail(user.getEmail()));
    }

    public void present(Optional<JwtUser> userInfo) {
        var result = userInfo.isPresent();
        if (!result) throw new ApplicationExceptions(USER_NOT_FOUND);
    }

    public void checkUserEmailExsist(ForgotPasswordRequest user) {
        present(userRepository.findJwtUserByUsername(user.getEmail()));
    }

    public void checkCodeAndEmail(JwtUser user, EmailDetails emailDetailsByCode) {
        if (!user.getEmail().equals(emailDetailsByCode.getRecipient()))
            throw new ApplicationExceptions(WRONG_VERIFICATION_CODE);
    }
}
