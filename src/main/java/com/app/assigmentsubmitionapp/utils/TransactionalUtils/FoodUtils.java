package com.app.assigmentsubmitionapp.utils.TransactionalUtils;

import com.app.assigmentsubmitionapp.modules.neo4j.ExpensesNode;
import com.app.assigmentsubmitionapp.modules.neo4j.FoodBeverages;
import com.app.assigmentsubmitionapp.repo.neo4j.FoodBeveragesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

import static com.app.assigmentsubmitionapp.enums.Neo4jEnums.*;
@Component
@RequiredArgsConstructor
public class FoodUtils {

    private final FoodBeveragesRepository foodBeveragesRepository;
    public FoodBeverages buildAndSaveExpense(Map<String, String> map, ExpensesNode expensesNode) {
        var id = UUID.randomUUID();
        FoodBeverages ex = FoodBeverages
                .builder()
                .id(id)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .name(map.get(EXPENSE))
                .price(new BigDecimal(map.get(PRICE)))
                .currency(map.get(CURRENCY))
                .expensesNode(expensesNode)
                .build();
        return ex;
    }

    public void saveFoodAngGrocery(FoodBeverages result) {
        foodBeveragesRepository.save(result);
    }
}
