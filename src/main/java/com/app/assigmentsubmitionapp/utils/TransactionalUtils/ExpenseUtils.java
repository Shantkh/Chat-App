package com.app.assigmentsubmitionapp.utils.TransactionalUtils;

import com.app.assigmentsubmitionapp.modules.neo4j.ExpensesNode;
import com.app.assigmentsubmitionapp.modules.neo4j.UsersNode;
import com.app.assigmentsubmitionapp.repo.neo4j.ExpensesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ExpenseUtils {

    private final ExpensesRepository expensesRepository;
    public ExpensesNode createExpenseNode(UUID userId, UsersNode user) {
        return ExpensesNode.builder()
                .id(UUID.randomUUID())
                .userId(userId)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .usersNode(user)
                .build();

    }

    public void saveExpense(ExpensesNode expensesNode) {
        expensesRepository.save(expensesNode);
    }
}
