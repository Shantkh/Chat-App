package com.app.assigmentsubmitionapp.utils.TransactionalUtils;

import com.app.assigmentsubmitionapp.modules.neo4j.Country;
import com.app.assigmentsubmitionapp.modules.neo4j.dto.UserAndCountry;
import com.app.assigmentsubmitionapp.modules.neo4j.UsersNode;
import com.app.assigmentsubmitionapp.repo.neo4j.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class CountryUtils {

    private final CountryRepository countryRepository;

    public Country createCountry(String countryName) {
        return Country.builder()
                .id(UUID.randomUUID())
                .name(countryName)
                .build();
    }
    public void saveCountry(Country country) {
        countryRepository.save(country);
    }

    public Country findByCountryName(UsersNode usersNode) {
        return countryRepository.findByName(usersNode.getCountry().getName());
    }

    public UserAndCountry findByUserAndName(UsersNode usersNode, Country country) {
        return countryRepository.findByUsersEqualsAndName(usersNode.getEmail(),
                Objects.requireNonNull(country).getName());
    }
}
