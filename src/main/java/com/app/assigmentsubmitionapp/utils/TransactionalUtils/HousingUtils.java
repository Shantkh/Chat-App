package com.app.assigmentsubmitionapp.utils.TransactionalUtils;

import com.app.assigmentsubmitionapp.modules.neo4j.ExpensesNode;
import com.app.assigmentsubmitionapp.modules.neo4j.Housing;
import com.app.assigmentsubmitionapp.repo.neo4j.HousingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

import static com.app.assigmentsubmitionapp.enums.Neo4jEnums.*;

@Component
@RequiredArgsConstructor
public class HousingUtils {

    private final HousingRepository housingRepository;
    public Housing buildAndSaveExpense(Map<String, String> map, ExpensesNode expensesNode) {
        var id = UUID.randomUUID();
        return  Housing
                .builder()
                .id(id)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .name(map.get(EXPENSE))
                .price(new BigDecimal(map.get(PRICE)))
                .currency(map.get(CURRENCY))
                .expensesNode(expensesNode)
                .build();
    }

    public void saveHousing(Housing result) {
        housingRepository.save(result);
    }
}
