package com.app.assigmentsubmitionapp.utils.TransactionalUtils;

import com.app.assigmentsubmitionapp.modules.neo4j.ExpensesNode;
import com.app.assigmentsubmitionapp.modules.neo4j.UnknownResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.app.assigmentsubmitionapp.enums.Neo4jEnums.*;

@Component
@RequiredArgsConstructor
public class UnknownUtils {
    public List<UnknownResponse> buildAndReturnAndSave(Map<String, String> map, ExpensesNode expenseAction) {
        List<UnknownResponse> notifications = new ArrayList<>();
        var id = UUID.randomUUID();
        UnknownResponse ex = UnknownResponse.builder()
                .id(id)
                .message("please help AI understand the details of.")
                .expense(map.get(EXPENSE))
                .price(new BigDecimal(map.get(PRICE)))
                .currency(map.get(CURRENCY))
                .category("Please specify category.")
                .build();
        notifications.add(ex);
        return notifications;
    }
}
