package com.app.assigmentsubmitionapp.utils.TransactionalUtils;

import com.app.assigmentsubmitionapp.modules.neo4j.Country;
import com.app.assigmentsubmitionapp.modules.neo4j.UsersNode;
import com.app.assigmentsubmitionapp.repo.neo4j.UsersNodeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class UsersUtils {

    private final UsersNodeRepository usersNodeRepository;

    public UsersNode createUser(UsersNode usersNode, Country country) {
        return UsersNode.builder()
                .id(UUID.randomUUID())
                .userId(usersNode.getUserId())
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .name(usersNode.getName())
                .email(usersNode.getEmail())
                .country(country)
                .build();
    }

    public void saveUser(UsersNode user) {
        usersNodeRepository.save(user);
    }

    public List<UsersNode> findAllByEmail(UsersNode usersNode) {
        return usersNodeRepository.findAllByEmail(usersNode.getEmail());
    }
}
