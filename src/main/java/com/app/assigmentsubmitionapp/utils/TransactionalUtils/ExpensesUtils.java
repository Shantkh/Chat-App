package com.app.assigmentsubmitionapp.utils.TransactionalUtils;

import com.app.assigmentsubmitionapp.modules.neo4j.ExpensesNode;
import com.app.assigmentsubmitionapp.modules.neo4j.UsersNode;
import com.app.assigmentsubmitionapp.repo.neo4j.ExpensesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;
@Service
@RequiredArgsConstructor
public class ExpensesUtils {

    private final ExpensesRepository expensesRepository;

    public ExpensesNode findUser(UUID userId) {
        return expensesRepository.findByUserId(userId).get(0);
    }

    public void saveExpense(ExpensesNode expensesNode) {
        expensesRepository.save(expensesNode);
    }

    public ExpensesNode createExpenseNode(UUID userId, UsersNode user) {
        return ExpensesNode.builder()
                .id(UUID.randomUUID())
                .userId(userId)
                .usersNode(user)
                .build();
    }
}
