package com.app.assigmentsubmitionapp.utils.TransactionalUtils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@RequiredArgsConstructor
public class TransactionalUtils {

    public boolean checkSymbols(String s) {
        return Arrays.stream(s.split(""))
                .allMatch(e -> e.matches("\\p{Punct}") && s.length() < 2);
    }

    public boolean checkUpperCase(String s) {
        if (!s.matches("[A-Z]+")) {
            return false;
        }
        // Check if string contains any digit or symbol
        for (char ch : s.toCharArray()) {
            if (Character.isDigit(ch) || !Character.isLetter(ch)) {
                return false;
            }
        }
        return true;
    }

    public boolean checkNumber(String s) {
        return s.matches(".*\\d+.*");
    }


}
