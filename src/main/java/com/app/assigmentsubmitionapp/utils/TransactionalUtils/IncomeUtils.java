package com.app.assigmentsubmitionapp.utils.TransactionalUtils;

import com.app.assigmentsubmitionapp.modules.neo4j.IncomesNode;
import com.app.assigmentsubmitionapp.modules.neo4j.UsersNode;
import com.app.assigmentsubmitionapp.repo.neo4j.IncomesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class IncomeUtils {
    private final IncomesRepository incomesRepository;

    public IncomesNode createIncomeNode(UUID userId, UsersNode user) {
        return IncomesNode.builder()
                .id(UUID.randomUUID())
                .userId(userId)
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .usersNode(user)
                .build();
    }

    public void saveIncoming(IncomesNode incomesNode) {
        incomesRepository.save(incomesNode);
    }
}
