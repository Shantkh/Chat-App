package com.app.assigmentsubmitionapp.utils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@Slf4j
@RequiredArgsConstructor
public class VerificationUtils {

    private final Random random = new Random();

    public String getRandomNumberString() {
        var code = random.nextInt(900000) + 100000;
        return String.valueOf(code);
    }
}