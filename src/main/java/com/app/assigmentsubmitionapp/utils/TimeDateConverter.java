package com.app.assigmentsubmitionapp.utils;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import javax.persistence.Convert;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
@Convert(disableConversion = true)
public class TimeDateConverter implements AttributeConverter<Timestamp, String> {
    @Override
    public String convertToDatabaseColumn(Timestamp attribute) {
        try {
            if (attribute == null) return DateUtils.newTimeStamp().toString();
            Timestamp timestamp = Timestamp.valueOf(attribute.toString());
            return timestamp.toString();
        } catch (Exception e) {
            throw new ApplicationExceptions("convertToDatabaseColumn " + e.getMessage());
        }
    }

    @Override
    public Timestamp convertToEntityAttribute(String dbData) {

        SimpleDateFormat dateFormat = null;
        if (dbData.contains(".")) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        }
        if (dbData.contains(",")) {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        }
        Timestamp timestamp = null;
        Date parsedDate;
        try {
            parsedDate = dateFormat.parse(dbData);
            timestamp = new Timestamp(parsedDate.getTime());
            return timestamp;
        } catch (Exception e) {
            throw new ApplicationExceptions("convertToEntityAttribute " + e.getMessage());
        }
    }
}
