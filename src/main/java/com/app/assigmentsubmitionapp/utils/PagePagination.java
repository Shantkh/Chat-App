package com.app.assigmentsubmitionapp.utils;

import com.app.assigmentsubmitionapp.user.PaginationWithSorting;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class PagePagination {

    public PageRequest pagePagination(PaginationWithSorting paginationWithSorting) {
        checkValidations(paginationWithSorting);
        return PageRequest.of(
                paginationWithSorting.getPageNumber(),
                paginationWithSorting.getContentSize(),
                Sort.Direction.fromString(paginationWithSorting.getSortDirection()),
                paginationWithSorting.getSortElement()
        );
    }

    private void checkValidations(PaginationWithSorting paginationWithSorting) {
        AppPreconditions.checkLimitationOfInts(paginationWithSorting.getPageNumber(),0,1000);
        AppPreconditions.checkLimitationOfInts(paginationWithSorting.getContentSize(),2);
        AppPreconditions.checkLimitationOfInts(paginationWithSorting.getContentSize(),9,26);
        AppPreconditions.checkLimitationOfString(paginationWithSorting.getSortDirection().toLowerCase(),"asc","desc");
    }
}