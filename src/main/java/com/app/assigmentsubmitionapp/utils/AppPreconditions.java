package com.app.assigmentsubmitionapp.utils;

import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.modules.localsettings.LocalSettings;
import com.app.assigmentsubmitionapp.modules.neo4j.Country;
import com.app.assigmentsubmitionapp.modules.neo4j.UsersNode;
import org.apache.xpath.operations.Bool;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.app.assigmentsubmitionapp.enums.MessagesConst.*;


public final class AppPreconditions {

    public static <T> List<T> checkNotNullOrEmptyList(final List<T> t) {
        List<T> checked = Objects.requireNonNull(t);
        if (checked.size() == 0) throw new ApplicationExceptions(LIST_EMPTY);
        return checked;
    }

    public static <T> T checkNotNullOrEmpty(final T t) {
        if (t == null)
            throw new ApplicationExceptions(ITS_NULL);
        return t;
    }

    public static <T> T checkLimitationOfInts(final T t, final int a, final int b) {
        int n = Converters.convertObjectToInt(t);
        if (n < a || n > b) {
            throw new ApplicationExceptions(PAGE_NUMBER_ERROR_MESSAGE);
        }
        return t;
    }

    public static <T> T checkLimitationOfInts(final T t, final int a) {
        int n = Converters.convertObjectToInt(t);
        if (n < a) {
            throw new ApplicationExceptions(SIZE_NEGATIVE_ERROR_MESSAGE);
        }
        return t;
    }

    public static <T> T checkLimitationOfString(final T t, final String... a) {
        boolean isEquals = false;
        for (String s : a) {
            if (t.equals(s)) {
                isEquals = true;
                break;
            }
        }
        if (!isEquals) {
            throw new ApplicationExceptions(DIRECTION_OR_ELEMENT_ERROR);
        }
        return t;
    }

    public static <T> T checkNotNullOrEmptySet(final Set<T> t) {
        Set<T> checked = Objects.requireNonNull(t);
        if (checked.size() == 0) throw new ApplicationExceptions(LIST_EMPTY);
        return (T) checked;
    }

    public static <T> T checkNotNullOrEmptyOptional(Optional<T> t) {
        Optional<T> checked = Objects.requireNonNull(t);
        if (!checked.isPresent() || checked.isEmpty()) throw new ApplicationExceptions(EMPTY_DATA);
        return (T) checked;
    }

    public static <T> Boolean checkEmptyList(final List<T> t) {
        List<T> checked = Objects.requireNonNull(t);
        return (checked.size() == 0);
    }

    public static <T> Boolean checkEmptyList(final Optional<T> t) {
        Optional<T> checked = Objects.requireNonNull(t);
        return (checked.isEmpty());
    }

    public static <T> Boolean checkNull(final T t) {
        return t == null;
    }

    public static boolean checkObjectsNull(Country country, List<UsersNode> usersList) {
        return (checkNull(country)) && (checkEmptyList(usersList));

    }
}
