package com.app.assigmentsubmitionapp.utils;

import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class Converters {

    public static <T> Integer convertObjectToInt(T t){
        return Integer.valueOf(String.valueOf(t));
    }
}
