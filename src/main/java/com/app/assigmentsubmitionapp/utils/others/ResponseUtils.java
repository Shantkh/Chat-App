package com.app.assigmentsubmitionapp.utils.others;

import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Optional;
@Component
@RequiredArgsConstructor
public class ResponseUtils<R,T> {

    public Optional<ResponseBuilder<? super R>> responseWithOk(T t){
        return Optional.ofNullable(ResponseBuilder.builder()
                .data(t)
                .date(DateUtils.newTimeStamp())
                .httpStatus(HttpStatus.OK)
                .httpStatusCode(HttpStatus.OK.value())
                .isSuccess(false)
                .build());
    }
}
