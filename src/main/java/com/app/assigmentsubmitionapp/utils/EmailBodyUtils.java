package com.app.assigmentsubmitionapp.utils;

import com.app.assigmentsubmitionapp.user.JwtUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
@Slf4j
public class EmailBodyUtils {


    public static String newUserRegistrationEmailVerificationEmailBody(String c, String e, String scheme, String host, int port) {
        var email = Base64.getEncoder().encodeToString(e.getBytes());
        var code = Base64.getEncoder().encodeToString(c.getBytes());
        var action_url = scheme + "://" + host + ":" + port + "/activate-account/" + email + "/" + code;
        log.info("action_url {}", action_url);
        return "your action url for confirmation is " + action_url;
    }

    public static String forgotPasswordEmailVerificationEmailBody(String c, String e, String scheme, String host, int port) {
        var email = Base64.getEncoder().encodeToString(e.getBytes());
        var code = Base64.getEncoder().encodeToString(c.getBytes());
        var action_url = scheme + "://" + host + ":" + port + "/change-password-code/" + email + "/" + code;
        log.info("action_url {}", action_url);
        return "your action url for confirmation is " + action_url;
    }

    public static String welcomeToOurApp(JwtUser user){
        return "welcome to the site";
    }

    public static String invitationToJoinTheApp(){
        return "";
    }
    
    public static String commentReceived(){
       return  "";
    }

    public static String invoice(){
        return "";
    }
    public static String receipt(){
        return "";
    }

    public static String PasswordChanged(String email) {
        return "Password changed";
    }
}
