package com.app.assigmentsubmitionapp.utils;

import com.app.assigmentsubmitionapp.modules.EmailDetails;
import com.app.assigmentsubmitionapp.user.JwtUser;
import lombok.RequiredArgsConstructor;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.UUID;

import static com.app.assigmentsubmitionapp.enums.UserSendEmailActivity.CONFIRM_PASSWORD_CHANGED;

@RequiredArgsConstructor
public class EmailUtils {

    public static EmailDetails createNewRegisterEmailValidationWithCode(HttpServletRequest request, String email, String action) {
        VerificationUtils verificationUtils = new VerificationUtils();
        var code = Objects.requireNonNull(verificationUtils).getRandomNumberString();
        var host = request.getServerName();
        var port = request.getServerPort();
        var scheme = request.getScheme();

        return EmailDetails.builder()
                .id(UUID.randomUUID())
                .msgBody(EmailBodyUtils.newUserRegistrationEmailVerificationEmailBody(code, email, scheme, host, port))
                .subject(action)
                .recipient(email)
                .createdDate(DateUtils.newTimeStamp())
                .code(code)
                .build();
    }

    public static EmailDetails createdForgotPasswordCode(HttpServletRequest request,String email, String action) {
        VerificationUtils verificationUtils = new VerificationUtils();
        var code = Objects.requireNonNull(verificationUtils).getRandomNumberString();
        var host = request.getServerName();
        var port = request.getServerPort();
        var scheme = request.getScheme();

        return EmailDetails.builder()
                .id(UUID.randomUUID())
                .msgBody(EmailBodyUtils.forgotPasswordEmailVerificationEmailBody(code, email, scheme, host, port))
                .subject(action)
                .recipient(email)
                .createdDate(DateUtils.newTimeStamp())
                .code(code)
                .build();
    }

    public static EmailDetails createdEmailConfirmDetails(JwtUser user, EmailDetails emailDetailsByCode, String action) {
        return EmailDetails.builder()
                .id(emailDetailsByCode.getId())
                .msgBody(EmailBodyUtils.welcomeToOurApp(user))
                .subject(action)
                .recipient(user.getEmail())
                .createdDate(DateUtils.newTimeStamp())
                .build();
    }

    public static EmailDetails createdPasswordChanged(String email, String confirmPasswordChanged) {
        return  EmailDetails.builder()
                .id(UUID.randomUUID())
                .msgBody(EmailBodyUtils.PasswordChanged(email))
                .subject(confirmPasswordChanged)
                .recipient(email)
                .createdDate(DateUtils.newTimeStamp())
                .build();

    }
}
