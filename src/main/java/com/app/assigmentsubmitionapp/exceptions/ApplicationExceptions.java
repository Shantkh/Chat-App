package com.app.assigmentsubmitionapp.exceptions;


public class ApplicationExceptions extends RuntimeException {

    public ApplicationExceptions(String message) {
        super(message);
    }

    public ApplicationExceptions() {
    }
}