package com.app.assigmentsubmitionapp.api;

import com.app.assigmentsubmitionapp.enums.Urls;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.nernlp.module.UserTransactionsStringRequest;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;

@SecurityRequirement(name = "ner-api")
@OpenAPIDefinition(info = @Info(title = "NER Controller API",
        version = "1.0",
        description = "AI to understand the language of the string captured from the photos, and understand the content depending on the language."))
public interface NERApi {

    @RequestMapping(value = Urls.NER_DETECTOR,
            method = {RequestMethod.POST})
    ResponseEntity<ResponseBuilder<?>> NERWithLanguages(@RequestBody final UserTransactionsStringRequest userTransactions) throws IOException;
}
