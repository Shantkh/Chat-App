package com.app.assigmentsubmitionapp.api.expenses;

import com.app.assigmentsubmitionapp.enums.Urls;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryUpdateRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryUpdateRequest;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.UUID;

@RequestMapping
@SecurityRequirement(name = "admin-api")
@OpenAPIDefinition(info = @Info(title = "Admin SUB SUB category Web API",
        version = "1.0",
        description = "Crud for Sub Sub expenses Categories."))
public interface ExpSubSubCategoryApi {

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.GET_ALL_SUB_SUB_EXPENSES_SUB_CATEGORY_ID,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> allExpSubCategories(@PathVariable(name = "sub_cat_id")final UUID subSubExpCategoryId);

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.GET_SUB_SUB_EXPENSES_CATEGORY_BY_ID,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> subExpCategoriesByExpCategoryId(@PathVariable(name = "sub_sub_cat_id") final UUID subSubExpCategoryId);

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.UPDATE_SUB_SUB_EXPENSES_CATEGORY,
            method = {RequestMethod.PUT})
    ResponseEntity<ResponseBuilder<?>> updateExpSubSubCategory(@RequestBody final ExpSubSubCategoryUpdateRequest subCategoryUpdateRequest);


    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.DELETE_SUB_SUB_EXPENSES_CATEGORY,
            method = {RequestMethod.DELETE})
    ResponseEntity<ResponseBuilder<?>> deleteExpSubSubCategory(@PathVariable("id") final UUID id);


    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.ADD_SUB_SUB_EXPENSES_CATEGORY,
            method = {RequestMethod.POST})
    ResponseEntity<ResponseBuilder<?>> addExpSubCategory(@RequestBody final ExpSubSubCategoryRequest categoryRequest);
}
