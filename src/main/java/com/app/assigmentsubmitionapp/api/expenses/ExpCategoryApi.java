package com.app.assigmentsubmitionapp.api.expenses;

import com.app.assigmentsubmitionapp.enums.Urls;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryRequest;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.UUID;

@RequestMapping
@SecurityRequirement(name = "admin-api")
@OpenAPIDefinition(info = @Info(title = "Admin category Web API",
        version = "1.0",
        description = "Crud for Expenses Categories."))
public interface ExpCategoryApi {

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.GET_ALL_EXPENSES_CATEGORIES,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> allExpCategories();

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.UPDATE_EXPENSES_CATEGORY,
            method = {RequestMethod.PUT})
    ResponseEntity<ResponseBuilder<?>> updateExpCategory(@RequestBody final ExpCategoryRequest categoryRequest);

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.DELETE_EXPENSES_CATEGORY,
            method = {RequestMethod.DELETE})
    ResponseEntity<ResponseBuilder<?>> deleteExpCategory(@PathVariable("id") final UUID id);

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.ADD_EXPENSES_CATEGORY,
            method = {RequestMethod.POST})
    ResponseEntity<ResponseBuilder<?>> addExpCategory(@RequestBody final ExpCategoryRequest categoryRequest);
}
