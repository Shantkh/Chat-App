package com.app.assigmentsubmitionapp.api.incomes;

import com.app.assigmentsubmitionapp.enums.Urls;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncCategoryRequest;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.UUID;

@RequestMapping
@SecurityRequirement(name = "admin-api")
@OpenAPIDefinition(info = @Info(title = "Admin category Web API",
        version = "1.0",
        description = "Crud for Income Categories."))
public interface IncCategoryApi {

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.ADD_INCOME_CATEGORY,
            method = {RequestMethod.POST})
    ResponseEntity<ResponseBuilder<?>> addIncCategory(@RequestBody final IncCategoryRequest incCategoryRequest);

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.DELETE_INCOME_CATEGORY,
            method = {RequestMethod.DELETE})
    ResponseEntity<ResponseBuilder<?>> deleteIncCategory(@PathVariable("id") final UUID id);


    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.GET_ALL_INCOME_CATEGORIES,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> allIncCategories();


    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.UPDATE_INCOME_CATEGORY,
            method = {RequestMethod.PUT})
    ResponseEntity<ResponseBuilder<?>> updateIncCategory(@RequestBody final IncCategoryRequest categoryRequest);

}
