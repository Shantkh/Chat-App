package com.app.assigmentsubmitionapp.api.incomes;

import com.app.assigmentsubmitionapp.enums.Urls;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryUpdateRequest;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.UUID;

@RequestMapping
@SecurityRequirement(name = "admin-api")
@OpenAPIDefinition(info = @Info(title = "Admin SUB SUB category Web API",
        version = "1.0",
        description = "Crud for Sub Sub income Categories."))
public interface IncSubSubCategoryApi {

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.GET_ALL_SUB_SUB_INCOME_SUB_CATEGORY_ID,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> allIncSubCategories(@PathVariable(name = "sub_cat_id")final UUID subSubExpCategoryId);

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.GET_SUB_SUB_INCOME_CATEGORY_BY_ID,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> subIncCategoriesByExpCategoryId(@PathVariable(name = "sub_sub_cat_id") final UUID subSubExpCategoryId);

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.UPDATE_SUB_SUB_INCOME_CATEGORY,
            method = {RequestMethod.PUT})
    ResponseEntity<ResponseBuilder<?>> updateIncSubSubCategory(@RequestBody final IncSubSubCategoryUpdateRequest subCategoryUpdateRequest);


    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.DELETE_SUB_SUB_INCOME_CATEGORY,
            method = {RequestMethod.DELETE})
    ResponseEntity<ResponseBuilder<?>> deleteIncSubSubCategory(@PathVariable("id") final UUID id);


    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = Urls.ADD_SUB_SUB_INCOME_CATEGORY,
            method = {RequestMethod.POST})
    ResponseEntity<ResponseBuilder<?>> addIncSubCategory(@RequestBody final IncSubSubCategoryRequest categoryRequest);
}
