package com.app.assigmentsubmitionapp.api;

import com.app.assigmentsubmitionapp.enums.Urls;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.localsettings.LocalSettings;
import com.app.assigmentsubmitionapp.user.dto.*;
import com.app.assigmentsubmitionapp.user.PaginationWithSorting;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RequestMapping
@SecurityRequirement(name = "user-api")
@OpenAPIDefinition(info = @Info(title = "User Update Web API",
        version = "1.0",
        description = "User Update details, profile, settings and others."))
public interface UserApi {

//    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @RequestMapping(value = Urls.CHANGE_PASSWORD_USERS,
            method = {RequestMethod.POST})
    ResponseEntity<ResponseBuilder<?>> changePassword(@RequestBody final UserChangePassword userChangePassword) throws IllegalAccessException;

//    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
    @RequestMapping(value = Urls.CHANGE_ROLE_USERS,
            method = {RequestMethod.POST})
    ResponseEntity<ResponseBuilder<?>> changeRole(@RequestBody final UserChangeRole userChangeRole) throws IllegalAccessException;

//    @PreAuthorize("hasAnyRole('ROLE_ADMIN','DB-ROLE_USER')")
    @RequestMapping(value = Urls.CLOSE_ACCOUNT_USERS,
            method = {RequestMethod.POST})
    ResponseEntity<ResponseBuilder<?>> closeAccount(@RequestBody final UserCloseAccount userCloseAccount) throws IllegalAccessException;

//    @PreAuthorize("hasAnyRole('ROLE_ADMIN','DB-ROLE_USER')")
    @RequestMapping(value = Urls.UPDATE_LOCAL_SETTINGS,
            method = {RequestMethod.PUT})
    ResponseEntity<ResponseBuilder<?>> localSettingUpdate(@RequestBody final LocalSettings localSettings) throws IllegalAccessException;

    //    @PreAuthorize("hasAnyRole('ROLE_ADMIN','DB-ROLE_USER')")
    @RequestMapping(value = Urls.USER_LOCAL_SETTINGS,
            method = {RequestMethod.POST})
    ResponseEntity<ResponseBuilder<?>> userLocalSetting(@RequestBody final UserLocalSettings userLocalSettings) throws IllegalAccessException;
}
