package com.app.assigmentsubmitionapp.api;

import com.app.assigmentsubmitionapp.enums.Urls;
import com.app.assigmentsubmitionapp.modules.ForgotPasswordRequest;
import com.app.assigmentsubmitionapp.modules.NewPasswordRequest;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.user.dto.UserRegistrationRequest;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;

@SecurityRequirement(name = "web-api")
@OpenAPIDefinition(info = @Info(title = "User Credential Web API",
        version = "1.0",
        description = "User registration, Login, Forgot password, email Verifications."))
public interface WebApi {
    @RequestMapping(value = Urls.REGISTER,
            method = {RequestMethod.POST},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseBuilder<?>> save(@Valid @RequestBody final UserRegistrationRequest userRegistrationRequest,
                                            HttpServletRequest request) throws IllegalAccessException, IOException;

    @RequestMapping(value = Urls.ACTIVATE_ACCOUNT,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> accountVerification(@PathVariable String email,
                                                           @PathVariable String code);

    @RequestMapping(value = Urls.FORGOT_PASSWORD,
            method = {RequestMethod.POST},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseBuilder<?>> forgotPassword(@Valid @RequestBody final ForgotPasswordRequest forgotPasswordRequest,
                                                      HttpServletRequest request);

    @RequestMapping(value = Urls.FORGOT_PASSWORD_CODE_VERIFY,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> forgotPasswordVerificationEmailCode(@PathVariable String email,
                                                                           @PathVariable String code);

    @RequestMapping(value = Urls.UPDATE_PASSWORD,
            method = {RequestMethod.POST},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseBuilder<?>> updatePassword(@Valid @RequestBody final NewPasswordRequest newPasswordRequest);
}
