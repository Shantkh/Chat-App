package com.app.assigmentsubmitionapp.api;

import com.app.assigmentsubmitionapp.enums.Urls;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.user.PaginationWithSorting;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@RequestMapping
@SecurityRequirement(name = "admin-api")
public interface AdminApi {

//    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ADMIN')")
    @RequestMapping(value = Urls.GET_ALL_USERS_ADMIN,
            method = {RequestMethod.POST},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseBuilder<?>> getUsersList(@Valid @RequestBody final PaginationWithSorting paginationWithSorting) throws IllegalAccessException;

}
