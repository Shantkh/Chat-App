package com.app.assigmentsubmitionapp.api.transactional;

import com.app.assigmentsubmitionapp.enums.Urls;
import com.app.assigmentsubmitionapp.modules.ResponseBuilder;
import com.app.assigmentsubmitionapp.modules.neo4j.UsersNode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

public interface TransactionsApi {

    @RequestMapping(value = Urls.TRANSACTION_CREATE_USER,
            method = {RequestMethod.POST},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseBuilder<?>> createNodeUser(@Valid @RequestBody final UsersNode usersNode) throws IllegalAccessException;

    @RequestMapping(value = Urls.TRANSACTION_EXPENSES_CATEGORY,
            method = {RequestMethod.POST},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<ResponseBuilder<?>> createExpense(@Valid @RequestBody final List<List<String>> expenseActions,
                                                     @Valid @PathVariable("expense_id") UUID userid);


    //admin
    @RequestMapping(value = Urls.TRANSACTION_GET_COUNTRIES_WITH_USER_COUNT,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> getCountriesWithUsersCount();

    @RequestMapping(value = Urls.TRANSACTION_GET_COUNTRIES,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> getCountries();

    @RequestMapping(value = Urls.TRANSACTION_GET_COUNTRIES_USERS_PERCENTAGE,
            method = {RequestMethod.GET})
    ResponseEntity<ResponseBuilder<?>> getCountriesWithUsersPercentage();

}
