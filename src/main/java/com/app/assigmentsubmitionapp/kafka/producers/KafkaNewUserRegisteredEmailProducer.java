package com.app.assigmentsubmitionapp.kafka.producers;

import com.app.assigmentsubmitionapp.enums.KafkaConstants;
import com.app.assigmentsubmitionapp.modules.EmailDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class KafkaNewUserRegisteredEmailProducer {
    @Autowired
    private KafkaTemplate<String, EmailDetails> KafkaTemplate;
    public void sendMessage(EmailDetails data){

        Message<EmailDetails> message = MessageBuilder
                .withPayload(data)
                .setHeader(KafkaHeaders.TOPIC, KafkaConstants.GROUP_NEW_USER_TOPIC)
                .build();

        KafkaTemplate.send(message);
    }

}