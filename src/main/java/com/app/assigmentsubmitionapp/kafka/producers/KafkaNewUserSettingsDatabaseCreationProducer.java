package com.app.assigmentsubmitionapp.kafka.producers;

import com.app.assigmentsubmitionapp.enums.KafkaConstants;

import com.app.assigmentsubmitionapp.modules.EmailDetails;
import com.app.assigmentsubmitionapp.user.JwtUser;
import com.app.assigmentsubmitionapp.user.dto.SettingUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class KafkaNewUserSettingsDatabaseCreationProducer {
    @Autowired
    private KafkaTemplate<String, SettingUser> KafkaTemplate;

    public void sendMessage(SettingUser data){

        Message<SettingUser> message = MessageBuilder
                .withPayload(data)
                .setHeader(KafkaHeaders.TOPIC, KafkaConstants.SETTING_TOPIC)
                .build();

        KafkaTemplate.send(message);
    }
}
