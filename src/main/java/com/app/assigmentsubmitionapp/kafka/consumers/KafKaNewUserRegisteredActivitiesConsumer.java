package com.app.assigmentsubmitionapp.kafka.consumers;

import com.app.assigmentsubmitionapp.enums.KafkaConstants;
import com.app.assigmentsubmitionapp.exceptions.ApplicationExceptions;
import com.app.assigmentsubmitionapp.modules.EmailDetails;
import com.app.assigmentsubmitionapp.repo.EmailRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
@Log4j2
@RequiredArgsConstructor
public class KafKaNewUserRegisteredActivitiesConsumer {


    private final JavaMailSender javaMailSender;

    private final EmailRepository emailRepository;
    @Value("${spring.mail.username}")
    private String sender;

    @KafkaListener(topics = KafkaConstants.GROUP_NEW_USER_TOPIC,
            groupId = KafkaConstants.GROUP_NEW_USER_ID)
    public void consume(EmailDetails data) {
        try {

            MimeMessage mailMessage = javaMailSender.createMimeMessage();
            mailMessage.setSubject(data.getSubject(), "UTF-8");

            MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true, "UTF-8");
            helper.setFrom("bizcityapp@gmail.com");
            helper.setTo(data.getRecipient());
            helper.setText(data.getMsgBody(), true);

            javaMailSender.send(mailMessage);
            emailRepository.save(data);

            log.info("Mail Sent Successfully...");
        } catch (Exception e) {
            log.error("email exception {}", e.getMessage());
            throw new ApplicationExceptions(e.getMessage());
        }

        log.info(String.format("Message received -> %s %s %s", data.getRecipient(), data.getId(), data.getCode()));
    }

}