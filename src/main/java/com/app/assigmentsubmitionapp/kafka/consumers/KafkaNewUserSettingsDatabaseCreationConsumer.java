package com.app.assigmentsubmitionapp.kafka.consumers;

import com.app.assigmentsubmitionapp.enums.KafkaConstants;
import com.app.assigmentsubmitionapp.enums.currency.Currencies;
import com.app.assigmentsubmitionapp.enums.localsettings.Lengths;
import com.app.assigmentsubmitionapp.enums.localsettings.Mass;
import com.app.assigmentsubmitionapp.enums.localsettings.Temperature;
import com.app.assigmentsubmitionapp.enums.localsettings.Volume;
import com.app.assigmentsubmitionapp.modules.localsettings.LocalSettings;
import com.app.assigmentsubmitionapp.repo.localsettings.LocalSettingsRepository;
import com.app.assigmentsubmitionapp.user.dto.SettingUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;

@Service
@Log4j2
@RequiredArgsConstructor
public class KafkaNewUserSettingsDatabaseCreationConsumer {

    private final LocalSettingsRepository localSettingsRepository;
    @KafkaListener(topics = KafkaConstants.SETTING_TOPIC,
            groupId = KafkaConstants.SETTING_ID)
    public void consume(SettingUser jwtUser) {
        LocalSettings localSettings  =LocalSettings.builder()
                .id(UUID.randomUUID())
                .userEmail(jwtUser.getEmail())
                .mass(Mass.KILOGRAM)
                .temperature(Temperature.FAHRENHEIT)
                .lengths(Lengths.METER)
                .volume(Volume.LITER)
                .currency(Currencies.USD)
                .country(Currencies.USD.getCountry())
                .symbol(Currencies.USD.getSymbol())
                .build();
        localSettingsRepository.save(localSettings);
    }
}
