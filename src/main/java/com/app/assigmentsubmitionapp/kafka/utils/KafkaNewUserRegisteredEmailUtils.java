package com.app.assigmentsubmitionapp.kafka.utils;

import com.app.assigmentsubmitionapp.kafka.producers.KafkaNewUserRegisteredEmailProducer;
import com.app.assigmentsubmitionapp.modules.EmailDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
@Log4j2
public class KafkaNewUserRegisteredEmailUtils {
    private final KafkaNewUserRegisteredEmailProducer kafkaProducer;
    public void userRegisteredAction(EmailDetails u) {
        EmailDetails emailDetails = EmailDetails.builder()
                .id(u.getId())
                .recipient(u.getRecipient())
                .subject(u.getSubject())
                .msgBody(u.getMsgBody())
                .code(u.getCode())
                .build();

        kafkaProducer.sendMessage(emailDetails);
    }

}
