package com.app.assigmentsubmitionapp.kafka.utils;

import com.app.assigmentsubmitionapp.kafka.producers.KafkaNewUserSettingsDatabaseCreationProducer;
import com.app.assigmentsubmitionapp.user.dto.SettingUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
@Log4j2
public class KafkaNewUserSettingsDatabaseCreationUtils {

    private final KafkaNewUserSettingsDatabaseCreationProducer kafkaProducer;

    public void userDatabaseCreationAction(SettingUser u) {
        SettingUser jwtUser = SettingUser.builder()
                .email(u.getEmail())
                .build();
        kafkaProducer.sendMessage(jwtUser);
    }


}
