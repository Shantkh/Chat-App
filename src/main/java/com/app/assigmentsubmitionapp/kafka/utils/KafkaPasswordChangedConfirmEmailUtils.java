package com.app.assigmentsubmitionapp.kafka.utils;

import com.app.assigmentsubmitionapp.kafka.producers.KafkaPasswordChangedEmailProducer;
import com.app.assigmentsubmitionapp.modules.EmailDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
@Log4j2
public class KafkaPasswordChangedConfirmEmailUtils {
    private final KafkaPasswordChangedEmailProducer kafkaProducer;

    public void userPasswordConfirmActivity(EmailDetails u) {
        EmailDetails emailDetails = EmailDetails.builder()
                .id(u.getId())
                .recipient(u.getRecipient())
                .subject(u.getSubject())
                .msgBody(u.getMsgBody())
                .build();

        kafkaProducer.sendMessage(emailDetails);
    }

}