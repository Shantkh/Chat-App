package com.app.assigmentsubmitionapp.mapper.transactional;

import com.app.assigmentsubmitionapp.modules.neo4j.Country;
import com.app.assigmentsubmitionapp.modules.neo4j.dto.CountryIdNameResponse;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class TransactionalMapper {
    public Optional<List<CountryIdNameResponse>> convertFromListCountryObjectToListCountryIdNameResponse(List<Country> s) {
        return Optional.of(s.stream().map(e -> CountryIdNameResponse.builder()
                .id(e.getId())
                .name(e.getName())
                .build())
                .toList());
    }
}
