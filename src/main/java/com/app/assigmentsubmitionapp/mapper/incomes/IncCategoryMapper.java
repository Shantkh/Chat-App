package com.app.assigmentsubmitionapp.mapper.incomes;

import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryResponse;
import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncCategoryResponse;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class IncCategoryMapper {
    public IncCategory fromIncCategoryRequestToIncCategoryObject(IncCategoryRequest incCategoryRequest) {

        return IncCategory.builder()
                .incCategoryName(incCategoryRequest.getIncCategoryName())
                .incCategoryDescription(incCategoryRequest.getIncCategoryDescription())
                .build();
    }

    public IncCategoryResponse fromIncCategoryObjectToCategoryResponse(IncCategory s) {
        return IncCategoryResponse.builder()
                .id(s.getId())
                .categoryName(s.getIncCategoryName())
                .categoryDescription(s.getIncCategoryDescription())
                .createdOn(DateUtils.newTimeStamp())
                .updatedOn(DateUtils.newTimeStamp())
                .build();
    }

    public Optional<List<IncCategoryResponse>> fromCategoryObjectListToListCategoryResponse(List<IncCategory> s) {
        return Optional.of(s.stream()
                .map(e -> IncCategoryResponse.builder()
                        .id(e.getId())
                        .updatedOn(e.getUpdatedOn())
                        .createdOn(e.getCreatedOn())
                        .categoryName(e.getIncCategoryName())
                        .categoryDescription(e.getIncCategoryDescription())
                        .build())
                .collect(Collectors.toList()));
    }
}
