package com.app.assigmentsubmitionapp.mapper.incomes;

import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.incomes.IncCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubCategoryResponse;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class IncSubCategoryMapper {
    public Optional<List<IncSubCategoryResponse>> fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(List<IncSubCategory> subCategories) {
        return Optional.of(subCategories.stream()
                .map(e -> IncSubCategoryResponse.builder()
                        .id(e.getId())
                        .updatedOn(e.getUpdatedOn())
                        .createdOn(e.getCreatedOn())
                        .subCategoryName(e.getIncSubCategoryName())
                        .subCategoryDescription(e.getIncSubCategoryDescription())
                        .build())
                .collect(Collectors.toList()));
    }

    public Optional<IncSubCategoryResponse> fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional<IncSubCategory> subCategories) {
        return subCategories.map(e -> IncSubCategoryResponse.builder()
                .id(e.getId())
                .createdOn(e.getCreatedOn())
                .updatedOn(e.getUpdatedOn())
                .subCategoryName(e.getIncSubCategoryName())
                .subCategoryDescription(e.getIncSubCategoryName())
                .build());
    }

    public IncSubCategory fromSubCategoryRequestToSubCategoryObject(IncSubCategoryRequest categoryRequest) {
        return IncSubCategory.builder()
                .incCategory(IncCategory.builder().id(categoryRequest.getCategoryId())
                        .build())
                .createdOn(DateUtils.newTimeStamp())
                .updatedOn(DateUtils.newTimeStamp())
                .incSubCategoryName(categoryRequest.getSubCategoryName())
                .incSubCategoryDescription(categoryRequest.getSubCategoryDescription())
                .build();
    }

    public IncSubCategory fromSubCategoryRequestToSubCategoryObjectById(IncSubCategoryRequest categoryRequest) {
        return IncSubCategory.builder()
                .id(categoryRequest.getCategoryId())
                .incCategory(IncCategory.builder().id(categoryRequest.getCategoryId())
                        .build())
                .incSubCategoryName(categoryRequest.getSubCategoryName())
                .incSubCategoryDescription(categoryRequest.getSubCategoryDescription())
                .build();
    }

    public IncSubCategoryResponse fromSubCategoryObjectToSubCategoryResponse(IncSubCategory s) {
        return IncSubCategoryResponse.builder()
                .id(s.getId())
                .createdOn(s.getCreatedOn())
                .updatedOn(s.getUpdatedOn())
                .subCategoryName(s.getIncSubCategoryName())
                .subCategoryDescription(s.getIncSubCategoryDescription())
                .build();
    }
}
