package com.app.assigmentsubmitionapp.mapper.incomes;

import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryResponse;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.IncSubSubCategory;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.incomes.dto.IncSubSubCategoryResponse;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class IncSubSubCategoryMapper {
    public Optional<List<IncSubSubCategoryResponse>> fromListSubCategoryObjectToOptionalSubSubCategoryResponse(List<IncSubSubCategory> subCategories) {
        return Optional.of(subCategories.stream()
                .map(e -> IncSubSubCategoryResponse.builder()
                        .id(e.getId())
                        .subSubCategoryName(e.getIncSubSubCategoryName())
                        .subSubCategoryDescription(e.getIncSubSubCategoryDescription())
                        .createdOn(e.getCreatedOn())
                        .updatedOn(e.getUpdatedOn())
                        .build())
                .collect(Collectors.toList()));
    }

    public Optional<IncSubSubCategoryResponse> fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(Optional<IncSubSubCategory> subCategories) {
        return subCategories
                .map(e -> IncSubSubCategoryResponse.builder()
                        .id(e.getId())
                        .subSubCategoryDescription(e.getIncSubSubCategoryDescription())
                        .subSubCategoryName(e.getIncSubSubCategoryName())
                        .createdOn(e.getCreatedOn())
                        .updatedOn(e.getUpdatedOn())
                        .build());
    }

    public IncSubSubCategoryResponse fromSubSubCategoryObjectToSubSubCategoryResponse(IncSubSubCategory s) {
        return IncSubSubCategoryResponse.builder()
                .id(s.getId())
                .createdOn(s.getCreatedOn())
                .updatedOn(s.getUpdatedOn())
                .subSubCategoryName(s.getIncSubSubCategoryName())
                .subSubCategoryDescription(s.getIncSubSubCategoryDescription())
                .build();
    }

    public IncSubSubCategory fromSubSubCategoryRequestToSubSubCategoryObject(IncSubSubCategoryRequest s) {
        return IncSubSubCategory.builder()
                .incSubCategory(IncSubCategory.builder().id(s.getSubCategoryId())
                        .build())
                .createdOn(DateUtils.newTimeStamp())
                .updatedOn(DateUtils.newTimeStamp())
                .incSubSubCategoryName(s.getSubSubCategoryName())
                .incSubSubCategoryDescription(s.getSubSubCategoryDescription())
                .build();
    }
}
