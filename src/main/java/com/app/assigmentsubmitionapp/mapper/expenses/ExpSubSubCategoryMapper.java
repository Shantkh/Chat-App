package com.app.assigmentsubmitionapp.mapper.expenses;

import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubSubCategoryResponse;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ExpSubSubCategoryMapper {
    public Optional<List<ExpSubSubCategoryResponse>> fromListSubCategoryObjectToOptionalSubSubCategoryResponse(List<ExpSubSubCategory> subCategories) {
        return Optional.of(subCategories.stream()
                .map(e -> ExpSubSubCategoryResponse.builder()
                        .id(e.getId())
                        .subSubCategoryName(e.getExpSubSubCategoryName())
                        .subSubCategoryDescription(e.getExpSubSubCategoryDescription())
                        .createdOn(e.getCreatedOn())
                        .updatedOn(e.getUpdatedOn())
                        .build())
                .collect(Collectors.toList()));
    }

    public Optional<ExpSubSubCategoryResponse> fromOptionalObSubSubCategoryObjectToOptionalSubSubCategoryResponse(Optional<ExpSubSubCategory> subCategories) {
        return subCategories
                .map(e -> ExpSubSubCategoryResponse.builder()
                        .id(e.getId())
                        .subSubCategoryDescription(e.getExpSubSubCategoryDescription())
                        .subSubCategoryName(e.getExpSubSubCategoryName())
                        .createdOn(e.getCreatedOn())
                        .updatedOn(e.getUpdatedOn())
                        .build());
    }

    public ExpSubSubCategoryResponse fromSubSubCategoryObjectToSubSubCategoryResponse(ExpSubSubCategory s) {
        return ExpSubSubCategoryResponse.builder()
                .id(s.getId())
                .createdOn(s.getCreatedOn())
                .updatedOn(s.getUpdatedOn())
                .subSubCategoryName(s.getExpSubSubCategoryName())
                .subSubCategoryDescription(s.getExpSubSubCategoryDescription())
                .build();
    }

    public ExpSubSubCategory fromSubSubCategoryRequestToSubSubCategoryObject(ExpSubSubCategoryRequest s) {
        return ExpSubSubCategory.builder()
                .expSubCategory(ExpSubCategory.builder().id(s.getSubCategoryId())
                        .build())
                .createdOn(DateUtils.newTimeStamp())
                .updatedOn(DateUtils.newTimeStamp())
                .expSubSubCategoryName(s.getSubSubCategoryName())
                .expSubSubCategoryDescription(s.getSubSubCategoryDescription())
                .build();
    }
}
