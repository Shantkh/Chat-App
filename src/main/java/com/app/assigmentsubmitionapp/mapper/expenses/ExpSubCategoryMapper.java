package com.app.assigmentsubmitionapp.mapper.expenses;

import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.ExpSubCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpSubCategoryResponse;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ExpSubCategoryMapper {
    public Optional<List<ExpSubCategoryResponse>> fromOptionalSubCategoryObjectToOptionalSubCategoryResponse(List<ExpSubCategory> subCategories) {
        return Optional.of(subCategories.stream()
                .map(e -> ExpSubCategoryResponse.builder()
                        .id(e.getId())
                        .updatedOn(e.getUpdatedOn())
                        .createdOn(e.getCreatedOn())
                        .subCategoryName(e.getExpSubCategoryName())
                        .subCategoryDescription(e.getExpSubCategoryDescription())
                        .build())
                .collect(Collectors.toList()));
    }

    public Optional<ExpSubCategoryResponse> fromOptionalObSubCategoryObjectToOptionalSubCategoryResponse(Optional<ExpSubCategory> subCategories) {
        return subCategories.map(e -> ExpSubCategoryResponse.builder()
                .id(e.getId())
                .createdOn(e.getCreatedOn())
                .updatedOn(e.getUpdatedOn())
                .subCategoryName(e.getExpSubCategoryName())
                .subCategoryDescription(e.getExpSubCategoryDescription())
                .build());
    }

    public ExpSubCategory fromSubCategoryRequestToSubCategoryObject(ExpSubCategoryRequest categoryRequest) {
        return ExpSubCategory.builder()
                .expCategory(ExpCategory.builder().id(categoryRequest.getCategoryId())
                        .build())
                .createdOn(DateUtils.newTimeStamp())
                .updatedOn(DateUtils.newTimeStamp())
                .expSubCategoryName(categoryRequest.getSubCategoryName())
                .expSubCategoryDescription(categoryRequest.getSubCategoryDescription())
                .build();
    }

    public ExpSubCategory fromSubCategoryRequestToSubCategoryObjectById(ExpSubCategoryRequest categoryRequest) {
        return ExpSubCategory.builder()
                .id(categoryRequest.getCategoryId())
                .expCategory(ExpCategory.builder().id(categoryRequest.getCategoryId())
                        .build())
                .expSubCategoryName(categoryRequest.getSubCategoryName())
                .expSubCategoryDescription(categoryRequest.getSubCategoryDescription())
                .build();
    }

    public ExpSubCategoryResponse fromSubCategoryObjectToSubCategoryResponse(ExpSubCategory s) {
        return ExpSubCategoryResponse.builder()
                .id(s.getId())
                .createdOn(s.getCreatedOn())
                .updatedOn(s.getUpdatedOn())
                .subCategoryName(s.getExpSubCategoryName())
                .subCategoryDescription(s.getExpSubCategoryDescription())
                .build();
    }
}
