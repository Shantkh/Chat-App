package com.app.assigmentsubmitionapp.mapper.expenses;

import com.app.assigmentsubmitionapp.modules.expenses.ExpCategory;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryRequest;
import com.app.assigmentsubmitionapp.modules.expenses.dto.ExpCategoryResponse;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ExpCategoryMapper {
    
    public Optional<List<ExpCategoryResponse>> fromCategoryObjectToListCategoryResponse(List<ExpCategory> s) {
        return Optional.of(s.stream()
                .map(e -> ExpCategoryResponse.builder()
                        .id(e.getId())
                        .updatedOn(e.getUpdatedOn())
                        .createdOn(e.getCreatedOn())
                        .categoryName(e.getExpCategoryName())
                        .categoryDescription(e.getExpCategoryDescription())
                        .build())
                .collect(Collectors.toList()));
    }


    public ExpCategory fromCategoryRequestToCategoryObject(ExpCategoryRequest categoryRequest) {
        return ExpCategory.builder()
                .expCategoryName(categoryRequest.getExpCategoryName())
                .expCategoryDescription(categoryRequest.getExpCategoryDescription())
                .createdOn(DateUtils.newTimeStamp())
                .updatedOn(DateUtils.newTimeStamp())
                .build();
    }

    public ExpCategoryResponse fromCategoryToCategoryResponse(ExpCategory response) {
        return ExpCategoryResponse.builder()
                .id(response.getId())
                .createdOn(response.getCreatedOn())
                .updatedOn(response.getUpdatedOn())
                .categoryName(response.getExpCategoryName())
                .categoryDescription(response.getExpCategoryDescription())
                .build();
    }
}
