package com.app.assigmentsubmitionapp.mapper;

import com.app.assigmentsubmitionapp.modules.UserRegisteredResponse;
import com.app.assigmentsubmitionapp.user.*;
import com.app.assigmentsubmitionapp.user.dto.UserRegisterResponse;
import com.app.assigmentsubmitionapp.user.dto.UserRegistrationRequest;
import com.app.assigmentsubmitionapp.user.dto.UserResponse;
import com.app.assigmentsubmitionapp.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class JwtUserMapper {
    private final PasswordEncoder passwordEncoder;
    private final static String DEFAULT_AVATAR = "https://img.myloview.com/stickers/default-avatar-profile-icon-vector-social-media-user-image-700-205124837.jpg";


    public Optional<List<UserResponse>> convertFromJwtUserObjectToJwtUsersListResponse(List<JwtUser> list) {
        return Optional.of(list.stream()
                .map(e -> UserResponse.builder()
                        .id(e.getId())
                        .email(e.getEmail())
                        .firstName(e.getFirstname())
                        .lastName(e.getLastname())
                        .isEnabled(e.isEnabled())
                        .roles(e.getRole())
//                        .usersDetails(UsersDetails.builder()
//                                .id(e.getUsersDetails().getId())
//                                .userVerified(e.getUsersDetails().isUserVerified())
//                                .createdOn(e.getUsersDetails().getCreatedOn())
//                                .updatedOn(e.getUsersDetails().getUpdatedOn())
//                                .gender(e.getUsersDetails().getGender())
//                                .avatar(e.getUsersDetails().getAvatar())
//                                .phoneNumbers(e.getUsersDetails().getPhoneNumbers())
//                                .addresses(e.getUsersDetails().getAddresses())
//                                .build())
                        .build())
                .collect(Collectors.toList()));
    }

    public JwtUser convertFromUserRequestToUserObject(UserRegistrationRequest signUser) {
        return JwtUser.builder()
                .username(signUser.getEmail())
                .password(passwordEncoder.encode(signUser.getPassword()))
                .role(signUser.getRoles() == null ? Set.of(Role.ROLE_USER) : signUser.getRoles())
                .email(signUser.getEmail())
                .firstname(signUser.getFirstname())
                .lastname(signUser.getLastname())
                .createdOn(DateUtils.newTimeStamp())
                .updatedOn(DateUtils.newTimeStamp())
                .enabled(false)
                .userEmailVerified(false)
                .usersDetails(UsersDetails.builder()
                        .id(UUID.randomUUID())
                        .userVerified(false)
                        .createdOn(DateUtils.newTimeStamp())
                        .updatedOn(DateUtils.newTimeStamp())
                        .gender(Gender.NOT_SET)
                        .avatar(DEFAULT_AVATAR)
                        .age(signUser.getAge())
                        .gender(Gender.valueOf(signUser.getGender()))
                        .phoneNumbers(Collections.singleton(PhoneNumbers.builder()
                                .id(UUID.randomUUID())
                                .createdOn(DateUtils.newTimeStamp())
                                .updatedOn(DateUtils.newTimeStamp())
                                .phoneNumber("")
                                .isNumberVerified(false)
                                .numberTypes(NumberType.NOT_SET)
                                .build()))
                        .addresses(Collections.singleton(Address.builder()
                                .id(UUID.randomUUID())
                                .createdOn(DateUtils.newTimeStamp())
                                .updatedOn(DateUtils.newTimeStamp())
                                .address("")
                                .state("")
                                .zipCode("")
                                .city("")
                                .country(signUser.getCountry())
                                .homeId("")
                                .addressTypes(AddressType.NOT_SET)
                                .isLocationValidated(false)
                                .build()))
                        .build())
                .build();
    }

    public Optional<UserRegisteredResponse> convertFromUserObjectToUserApiResponse(JwtUser s) {
        return Optional.of(UserRegisteredResponse.builder().id(s.getId().toString()).build());
    }


    public Optional<UserRegisterResponse> convertFromUserObjectToUserResponse(JwtUser s) {
        return Optional.of(UserRegisterResponse.builder()
                .id(s.getId())
                .isEnabled(s.isEnabled())
                .isEmailVerified(s.isUserEmailVerified())
//                .roles(new HashSet<>(s.getRole()))
                .email(s.getEmail())
                .build());
    }
}
