FROM openjdk:17-alpine
COPY *.jar /app/my-app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app/my-app.jar"]
